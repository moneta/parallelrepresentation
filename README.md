# Scripts for Preprocessed and analyses of Representations of context and context-dependent values in vmPFC compete for guiding behavior. Moneta Garvert Heekeren & Schuck, Narute Communications (2023)

In general, if anything across the codes is missing or unlclear, you are most welcome to contact Nir Moneta, always happy to chat and explain whatever needed! 

## pre print
can be found here: https://www.biorxiv.org/content/10.1101/2021.03.17.435844v3

## License 
Script and data are licensed under: Creative Commons Attribution-ShareAlike 4.0 International Public License (see LICENSE file)

All custom-written code is licensed under the MIT license.
Please cite the original paper (please see the Citation section) if you use (parts of) the code and data published here.
Please note, that code and data from third-parties, e.g., software like containers (e.g., fMRIPrep, MRIQC, etc.) may have different licenses.
All in all, properly licensing mixed code and data from different sources is tough so please be mindful, respect and credit the work of others and ask if you are unsure about the appropriate referencing of sources. Thanks!

## General description:
Provided are scripts for the **task** (ran on psychtoolbox in MATLAB, see readme in folder), **preprocessing** of the data (including *fmriprep*, extracting confounds from fmriprep and *physiological parameters*, *smoothing* and *functional ROI extraction*, see README inside preprocessing folder) and **analyses** scripts which includes **behavioral** analyses (mixed effect models), **univariate anlayses** (including all *first and second level GLMs*, generating *Betas for the RSA* analyses and testing estimatability of design and trial-wise analyses using *Variance Inflation Factor (VIF)* calculations) and **multivariate analyses** (including RSA, decoding scripts and mixed effect models of the decoding results).

## Data availability
All data and scripts are provided in order to dive right into different levels in the main analyses:

For behavioral analysis, see simply **analyses\behavioral\Behavioral_analysis.RmD** and the attached **analyses\behavioral\behavioral_guide.md**. Data can be found here: **\Behavioral\BEHAVIORAL_DATA_Main_Cohort.txt** for main cohort and **\Behavioral\BEHAVIORAL_DATA_Replication_Cohort.txt** for replication cohort (outside the scanner) across all experiemental parts.

For fMRI analyses, if you are simply interested in testing another variable in the mixed effect models of classifier probabilities and/or RSA, we provide the classifier probabilities and RSA correlations used in the manuscript in **data\fMRI\original**. 
data in **\fMRI\original\fMRI_decoding_EV.txt**,**fMRI_decoding_Context.txt**,**fMRI_decoding_EVBACK.txt** is classifier decoding probabilities from the vmPFC of decodeing EV, Context and EVBACK respectively. Data in **\fMRI\original\RSA_CorrsByBlock.csv** is the RSAs of each subject. 

We additionally provide the data from our functionally defined ROI in 2D (TR x voxel) as well as 3D nifti files. These data is already preprocessed (fmriprep) and smoothed 4mm (used in analyses) or 8mm (used in univariate) as well as in MNI and native space. You can use these data to run decoding analyses and/or univariate GLMs (scripts for the univariate analyses mentioned in SI are also attached). 

**IMPORTANT**: If you choose to go down this path (generate your own decoding/RSAs) make sure you save the resulting tables in **\data\fMRI\new** and change in the analyses scripts the variable ```inputtype``` from ```ORIGINAL``` to ```NEW```. 

Lastly, we provide some additional data:
- defaced structural scans (pydeface 2.0.2) in the BIDS folder. 
- *FarbeFB.csv*: we asked participants prior to the experiment about general color preference. We thought to look at the influence of prior preferences on value signals but then realized that the color staircasing made it such that the categorical naming of colors became sometimes unclear (esp. in the red-orange-pink world). We neertheless attach the questionaire results here. 
- *MRI_Sequences.pdf*: output of the scanner with description of the sequences in case something is missing in the elaborated methods section in the manuscript and/or BIDS json files. 

### additional README files
Description of the data provided: XXXX
Description of behavioral analyses: **analyses\behavioral\behavioral_guide.md**
Description of fMRI analyses: **analyses\fmri\fMRI_guide.md** and specific for classification and RSA models: **analyses\fmri\multivariate\main_analyses\main_fmri_analyses_guide.md**. 

Note: because we did not seed the randomization in the original analyses, some of the results are not 100% identical (due to randomized updampling in the decoding analyses). Additionally, the RSA results differ very slightly since the original RSA GLMs ran on whole brain data and these now run on extracted data (i.e. some internal SPM SNR calculations change). Crucially, all effects remain not only significant but also within the same conventional significance range (e.g. p value might change from 0.0022 to 0.0025). 

### How to get the data?
All individual fMRI datasets can be found at https://gin.g-node.org/nirmoneta/SODIVA and are shared under Creative Commons Attribution-ShareAlike 4.0 International Public License (see LICENSE file in repository). You can retrive the data here: PATHTOGIN (for downloading the whole data set)
Make sure you copy the entire data to '/dataorigin' so the scripts could refer to it.
IMPORTANT: the 'dataorigin' folder is only used for in-between analyses steps. Eventually, the final output of the multivariate analyses is saved in '\data\fMRI\new' (classifiers results as well as RSA for each subject)

### data folder structure:
**BIDS**
**sourcedata** 
has all json files, defaced structural images but no whole brain BOLD.
**derivatives**  
- Data2D: data from functionaly definned ROIs (smoothed 4mm or 8mm, native space or MNI). Rows are TRs and columns are voxels
- Data3D: same as Data2D only in 3D nifti format (each file is a TR)
- Multivariate_PP_Data: data after multivariate preprocessing, to be used for the classifiers
- DecodedData: classifier decoding results
- RSAs: RSA for each subject extracted from the beta maps as well as collapsed across the whole group. 

Additional folders in derivatives: 
- ROIs: ROI for group (MNI) or for each subject (native space) in nifti format
- Confounds_univariate: confound tables for each subject to provide to SPM
- Confounds_multivariate: confound tables for each subject to provide to signal_clean of nilearn
- Univariate: first and second levels ran only within the ROI as well as VIF folder for design estimability testings. 

# Versions 
## R
### R studio:
platform       x86_64-w64-mingw32          
arch           x86_64                      
os             mingw32                     
system         x86_64, mingw32             
status                                     
major          3                           
minor          6.3                         
year           2020                        
month          02                          
day            29                          
svn rev        77875                       
language       R                           
version.string R version 3.6.3 (2020-02-29)
nickname       Holding the Windsock
### Packages for Behavioral scripts:
ggplot2 3.3.5, gridExtra 3.3.5, lme4 3.3.5, fs 3.3.5, car 3.3.5, plyr 3.3.5, plotly 3.3.5, ggsignif 3.3.5, RColorBrewer 3.3.5, ggthemes 3.3.5, gtable 3.3.5, sjPlot 3.3.5, sjmisc 3.3.5, knitr 3.3.5, kableExtra 3.3.5, MASS 3.3.5, arrayhelpers 3.3.5, dplyr 3.3.5, grid 3.3.5, ggthemes 3.3.5, corrplot 3.3.5, svglite 3.3.5
### Packages for fMRI scripts (additional to behavioral):
glmmTMB 3.3.5,ggeffects 3.3.5

## MATLAB
### Packages for preprocessing and univariate analyses 
TAPAS 3.2.0, SPM12 (12)
## Python
### Packages for preprocessing
Please see the extensive list and details of fmriprep version: 1.2.6 either in manuscript or on the documentation of fmriprep. 
SPM12 (7771), ANTs 2.2.0
### Packages for decoding scripts 
pandas 1.1.5, numpy 1.19.5,nilearn 0.6.2 (later changed to 0.7.0), sklearn 0.22.0 (later changed to 0.22.2), nipype 1.7.1

