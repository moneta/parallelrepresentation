#!/bin/bash
# ==============================================================================
# SCRIPT INFORMATION:
# ==============================================================================
# SCRIPT: RUN FMRIPREP ON TARDIS (MPIB CLUSTER)
# ==============================================================================
# DEFINE ALL PATHS:
# ==============================================================================
# path to the current shell script:
# path to the fmriprep ressources folder:
PATH_FMRIPREP=$HOME/tools/fMRIPrep
# path to the fmriprep singularity image:
PATH_CONTAINER=${PATH_FMRIPREP}/fmriprep-latest.simg
# path to the freesurfer license file on tardis:
PATH_FS_LICENSE=${PATH_FMRIPREP}/fs_600_license.txt
# path to the data directory (in bids format):
PATH_BIDS=$HOME/SODIVA/BIDS/bidssource
# path to the output directory:
PATH_OUT=$HOME/SODIVA/BIDS/derivatives
# path to the working directory:
PATH_WORK=$HOME/SODIVA/work/fmriprep
# path to the log directory:
PATH_LOG=$HOME/logs/fMRIPrep/
# path to the text file with all subject ids:
PATH_SUB_LIST="$HOME/SODIVA/Scripts/sublists/sublist_New_ToConv.txt"

cd "${PATH_BIDS}"
# ==============================================================================
# CREATE RELEVANT DIRECTORIES:
# ==============================================================================
# create output directory:
if [ ! -d ${PATH_OUT} ]; then
	mkdir -p ${PATH_OUT}
fi
# create working directory:
if [ ! -d ${PATH_WORK} ]; then
	mkdir -p ${PATH_WORK}
fi
# create directory for log files:
if [ ! -d ${PATH_LOG} ]; then
	mkdir -p ${PATH_LOG}
fi
# ==============================================================================
# DEFINE PARAMETERS:
# ==============================================================================
# maximum number of cpus per process:
N_CPUS=8
# maximum number of threads per process:
N_THREADS=8
# memory demand in *GB*
MEM_GB=40
# memory demand in *MB*
MEM_MB="$((${MEM_GB} * 1000))"
# user-defined subject list
#PARTICIPANTS=$1
# read subject ids from the list of the text file
SUB_LIST=$(cat ${PATH_SUB_LIST} | tr '\n' ' ')
# ==============================================================================
# RUN FMRIPREP:
# ==============================================================================
for SUB in ${SUB_LIST}; do
	# name of the job
	echo "#PBS -N SoDiva_fmriprep_sub-${SUB}_35H" >> job
	# set the expected maximum running time for the job:
	echo "#PBS -l walltime=35:00:00" >> job
	# determine how much RAM your operation needs:
	echo "#PBS -l mem=${MEM_GB}GB" >> job
	# email notification on abort/end, use 'n' for no notification:
	echo "#PBS -m n" >> job
	# write (output) log to log folder 
	echo "#PBS -o ${PATH_LOG}_${SUB}_output_35H" >> job
	# write (error) log to log folder
	echo "#PBS -e ${PATH_LOG}_${SUB}_errors_35H" >> job
	# request multiple cpus
	echo "#PBS -l nodes=1:ppn=${N_CPUS}" >> job
	# define the fmriprep command:
	echo "singularity run --cleanenv ${PATH_CONTAINER} \
	--fs-license-file ${PATH_FS_LICENSE} \
	${PATH_BIDS} ${PATH_OUT} \
	participant --participant_label ${SUB} \
	-w ${PATH_WORK} \
	--mem_mb ${MEM_MB} --nthreads ${N_CPUS} --omp-nthreads $N_THREADS \
	--write-graph --stop-on-first-crash \
	--output-space T1w fsnative template fsaverage \
	--notrack --verbose --resource-monitor" >> job
	# submit job to cluster queue and remove it to avoid confusion:
	qsub job
	rm -f job
done
