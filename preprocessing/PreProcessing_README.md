# Preprocessing procdedute for data for the analysis of Representations of context and context-dependent values in vmPFC compete for guiding behavior. Moneta Garvert Heekeren & Schuck, 2023

# License 
Script and data are licensed under: Creative Commons Attribution-ShareAlike 4.0 International Public License (see LICENSE file)

# description
These scripts were used to preprocess the fMRI data for the manuscript.
Note that since currently the data shared is only within the ROI to reproduce the main results in the manuscript, these scripts can not currently run.
Nevertheless the scripts are shared in case any questions arise. In case of any issues please contact Nir Moneta who will be happy to help. 

## Preprocessing steps 
(for details see manuscript)

### (1) fmriprep (fmriprep_cluster.sh)
Bash script used to run fmriprep on the MPIB cluster.

### (2) Phisyo (folder)
MATLAB scripts used to create the physiological parameters
Note that these require the first DICOM image to be able to run and as it is not shared, one can not run these scripts. 

### (3) Confounds
Script used to generate the confounds file from phisyo and fmriprep outputs for the univariate and multivariate preprocessing

### (4) smoothing (SmoothMe.m)
Scripts used to run smoothing of MNI as well as native space (4mm and 8mm)

### (5) ROI
Scripts to generate univariate ROI can bbe found in analyses/fmri/univariate/first_and_second_levels/
Here we have scripts to transform ROI to native space (ANTtransformME.sh)
And to extract and save the data from the ROI in new .nii files (Extract_ROI.py)