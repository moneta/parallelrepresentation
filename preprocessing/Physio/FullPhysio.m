% This script runs the full physiological parameters estimation 
sublistfile = '....subject2convert.csv'; % list to convert to new bids names, cant share this information

VaterFolderDicom = 'Z:\Data\SoDiva\MRI\Data_MRI_SoDiva'; % where the dicoms are to get the list and use first one of each run
MotherFolderPhysio = 'X:\SODIVA\BIDS\Data_Physio'; % original physio to change name and copy 

PhysioNewLocation =  'X:\SODIVA\BIDS\derivatives\Physio'; % for output of all the physio

%% Part 1: generate the multiple regressor for GLM based on physio
% (1) Rename and savee Physio files from scanner output to my BIDS 
%     Additionally move first dicom to the same folder 
[Sub2FixPhysio,Sub2FixDicom] = RenameNsavePhysio(sublistfile,PhysioNewLocation,VaterFolderDicom,MotherFolderPhysio);

%% Manual correction on subjects : Physio {sub 4}, DICOM {sub10 run 4, sub 29 run 1}
% moved manually the DICOMs
% sub 4 has double,toilet break after run 2:
% run 1 started: '10:56:54.325000', run 2 started: '11:14:22.702500'
% run 3 started: '11:57:14.710000', run 4 started: '12:14:46.582500'
%% Part 2: run the tapas toolbox on all subjects PER RUN
% here for sub 4 we have a correction inside
% and for sub 3, 8, 10 and 35 the json file is not full, probably because
% the dicoms are doubled, so we have correction to count how many volums. 
homedir = 'X:/';
addpath(genpath('C:\Users\moneta\Google Drive\Matlab\toolboxes\spm12'));

bidssource = fullfile(homedir,'SODIVA','BIDS','bidssource');
ProblemsInTapas = {};
for s = 1:40
    for r = 1:4
        try
        SPMtry(s,r,PhysioNewLocation,bidssource,VaterFolderDicom);
        fprintf('Ran on with sub %d run %d \n',s,r);
        catch
        ProblemsInTapas = {ProblemsInTapas,['Problem with sub ',s,' run ',r]}; 
        fprintf('Problem with sub %d run %d \n',s,r);
        continue
        end 
    end 
end 

%% now run a GLM, e.g. X:\SODIVA\Scripts\GLM_Anal\GLM_MainScripts\NewGLMS\PhysioTest\PhysioGLMandPhy.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RUN GLM ELSEWHERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Now run the physio contrasts - can also be ran on the cluster
% homedir = 'X:/';
 homedir = '/home/mpib/moneta/';
 addpath(genpath('/home/mpib/moneta/tools/spm12'))
 addpath(genpath('/home/mpib/moneta/SODIVA/Scripts/Physio/FinalRelevantScripts'))
GLMFolder = fullfile(homedir,'SODIVA','BIDS','derivatives','Univariate','PhysioTry','RelevantDim_GLM_Physiotest');

spm('defaults','fmri');
spm_jobman('initcfg');
% spm default memory
spm_get_defaults('stats.maxmem',2^32) % 34 needs 18 GB at least, 35 needs 36, just run : 2^35/1000000000 (because the num is in bytes
spm_get_defaults('stats.resmem', true)

SubNames = [1:2,4:17,19:20,22:27,29:33,35:40];
for s = SubNames
    Physiocontrasts(s,GLMFolder)
    fprintf('Ran on with sub %d \n',s,r);
end 