function [Sub2Fix,Sub2FixDicom] = RenameNsavePhysio(sublistfile,Namedfolder,VaterFolderDicom,MotherFolderPhysio)
%Namedfolder = PhysioNewLocation;
fid = fopen(sublistfile); sublist = textscan(fid, '%s'); fclose(fid);
sublistDICOM = sublist;
sublist{1}{1} = 'MR103'; % fixing old error

OriginalFolders = dir(fullfile(MotherFolderPhysio,'*MR*'));
OriginalFoldersDicom = dir(fullfile(VaterFolderDicom,'*MR*')); % missing MR142 but its not a valid subject anyway

sublist = sublist{1}; sublistDICOM = sublistDICOM{1};
Sub2Fix = {}; Sub2FixDicom = {};

for s = 1:40
    if s < 10, sub = strcat('sub-0',num2str(s)); else, sub = strcat('sub-',num2str(s)); end
    %% Moving the physio
    Index = find(contains({OriginalFolders.name},sublist{s}));
    Orig  = fullfile(OriginalFolders(Index).folder,OriginalFolders(Index).name);
    
    Files2Move = dir(Orig);
    Files2Move(1:2) = [];
    if numel(Files2Move)~=4
        fprintf('\n PhysioError in subject %d, moving on fix manually',s)
        Sub2Fix = [Sub2Fix,sublist(s)];
        % only subject 4 that has 2 recordings split by runs
         NewF   = fullfile(Namedfolder,sub);
         if ~exist(NewF,'dir')
            mkdir(NewF)
         end 
         for f = 1:8
            NewName = strcat(sub,'_Physio',Files2Move(f).name(5:end));
            copyfile(fullfile(Files2Move(f).folder,Files2Move(f).name),fullfile(NewF,NewName))
         end 
    else
        NewF   = fullfile(Namedfolder,sub);
        if ~exist(NewF,'dir')
            mkdir(NewF)
        end 
        for f = 1:4
            NewName = strcat(sub,'_Physio',Files2Move(f).name(5:end));
            copyfile(fullfile(Files2Move(f).folder,Files2Move(f).name),fullfile(NewF,NewName))
        end 
        
    end 
    %% moving the dicom
    IndexDicom = find(contains({OriginalFoldersDicom.name},sublistDICOM{s}));
    
    for r = 1:4
    OrigDicF  = dir(fullfile(OriginalFoldersDicom(IndexDicom).folder,OriginalFoldersDicom(IndexDicom).name,'*',['TASKRUN',num2str(r),'*']));
    OrigFirstDicom = dir(fullfile(OrigDicF(2).folder,OrigDicF(2).name,'*0001*.IMA'));
       if numel(OrigFirstDicom)>2 || numel(OrigFirstDicom)==0
         fprintf('\n DICOM Error in subject %d, run %d, moving on fix manually',s,r)
         Sub2FixDicom = [Sub2FixDicom,sublistDICOM(s)];
       else 
         NewF   = fullfile(Namedfolder,sub);
         if ~exist(NewF,'dir')
             mkdir(NewF)
         end 
         copyfile(fullfile(OrigFirstDicom(1).folder,OrigFirstDicom(1).name),fullfile(NewF,['Run',num2str(r),'_',sub,'_',OrigFirstDicom(1).name]))
       end 
   end 
      
end 


