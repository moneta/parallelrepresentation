    function Physiocontrasts(s,GLMFolder)
if s < 10, sub = strcat('sub-0',num2str(s));else,sub = strcat('sub-',num2str(s));end 

SPM1 = load(fullfile(GLMFolder,sub,'SPM.mat'),'SPM');
SPM = SPM1.SPM;
%physio = load(fullfile(PhysioNewLocation,sub,'physio.mat'),'physio');
%model = physio.physio.model;

% Physio contrasts
namesPhysContrasts = {'All Phys','Cardiac','Respiratory','Card X Resp Interation','Movement','fmriprep','All Phys + Move + fmriprep'};

betas = {SPM.Vbeta.descrip}';
betas = strcat(betas,'end');
Index = find(contains(betas,' R1end')); % first R in each block
% Now work from first R
% in our GLM the first 11 regressoss are fmriprep, then 6 motion then 18 tapas Physio
IndFirstTapas = Index+17;

colPhys = []; % colPhys - all 18 of tapas regressors
colCard = []; % first 6 of tapas
colResp = []; % 7 - 14 reg of tapas
colMult = []; % interaction 15-18 in tapas
colMove = []; % 12 -17 in all
colAllfmriprep = []; % first 11 in all
colAll = [];

for run = 1:4
colPhys = [colPhys,IndFirstTapas(run):IndFirstTapas(run)+17];
colCard = [colCard,IndFirstTapas(run):IndFirstTapas(run)+5];
colResp = [colResp,IndFirstTapas(run)+6:IndFirstTapas(run)+13];
colMult = [colMult,IndFirstTapas(run)+14:IndFirstTapas(run)+17];
colMove = [colMove,Index(run)+11:Index(run)+16];
colAllfmriprep = [colAllfmriprep,Index(run):Index(run)+10];
colAll = [colAll,Index(run):Index(run)+34];
end

colHRV = [];
colRVT = [];
colRois = [];
%[colPhys, colCard, colResp, colMult, colHRV, colRVT, colRois, colMove, colAll] = ...
%    tapas_physio_check_get_regressor_columns(SPM, model);
con{1} = colPhys;
con{2} = colCard;
con{3} = colResp;
con{4} = colMult;
con{5} = colMove;
con{6} = colAllfmriprep;
con{7} = colAll;

% CAN FIX THE FIRST FMRIPREP FRAMEWISE TO BE SEPERATED

% execute computation only for non-empty contrasts;
indValidContrasts = find(~cellfun(@isempty, con));

matlabbatch = [];
matlabbatch{1}.spm.stats.con.spmmat{1} = fullfile(GLMFolder,sub,'SPM.mat');

nContrasts = numel(indValidContrasts);
for c = 1:nContrasts
    iC = indValidContrasts(c);
    F{c} = zeros(max(con{iC})); % spm will autocomplete the rest with zeros
    F{c}(sub2ind(size(F{c}),con{iC}, con{iC})) = 1;
    matlabbatch{1}.spm.stats.con.consess{c}.fcon.convec{1} = F{c};
    
    matlabbatch{1}.spm.stats.con.consess{c}.fcon.name = ...
            namesPhysContrasts{indValidContrasts(c)};
end

% remove additional contrasts in matlabbatch
matlabbatch{1}.spm.stats.con.consess(nContrasts+1:end) = [];

spm_jobman('run', matlabbatch);

end 