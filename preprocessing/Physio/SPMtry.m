%-----------------------------------------------------------------------
% Job saved on 05-Sep-2018 03:20:54 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------


%addpath(genpath('C:\Users\moneta\Google Drive\Matlab\toolboxes\spm12'));
function SPMtry(s,r,PhysioLocation,bidssource,VaterFolderDicom)
%PhysioLocation = PhysioNewLocation;
if s < 10, sub = strcat('sub-0',num2str(s)); else, sub = strcat('sub-',num2str(s)); end 

FirstDicom = dir(fullfile(PhysioLocation,sub,['Run',num2str(r),'*.IMA']));

%NumOfVol = 0;
fname = fullfile(bidssource,sub,'func',[sub,'_task-sodiva_rec-prenorm_run-0',num2str(r),'_bold.json']);
val = jsondecode(fileread(fname));
if isfield(val,'dcmmeta_shape')
    NumOfVol = val.dcmmeta_shape(4);
else
   % ProblemInJson = [ProblemInJson,strcat(sub,'_run_',num2str(i))];
    switch sub
        case {'sub-03'}
            runfolder = dir(fullfile(VaterFolderDicom,'*MR105*','*',['TASKRUN',num2str(r),'*']));
        case {'sub-08'}
            runfolder  = dir(fullfile(VaterFolderDicom,'*MR110*','*',['TASKRUN',num2str(r),'*']));
        case {'sub-10'}
            runfolder = dir(fullfile(VaterFolderDicom,'*MR112*','*',['TASKRUN',num2str(r),'*']));
        case {'sub-35'}
            runfolder = dir(fullfile(VaterFolderDicom,'*MR137*','*',['TASKRUN',num2str(r),'*']));
    end 
    DICOMs = dir(fullfile(runfolder(2).folder,runfolder(2).name,'*.IMA')); % seconf one is the one we need
    arr = {DICOMs.name};
    f=cellstr(arr);
    IMnames = cellfun(@(x) x(end-11:end),f,'un',0);
    %24247778.IMA
    NumOfVol = numel(unique(IMnames)); % uniqur since sometimes its double    
end 
 
% for subject 4 
% for i = 1:4
%   fname = fullfile(bidssource,sub,'func',[sub,'_task-sodiva_rec-prenorm_run-0',num2str(i),'_bold.json']);
%   val = jsondecode(fileread(fname));
%   val.AcquisitionTime
% end 


% fname = fullfile('X:\SODIVA\BIDS\bidssource',sub,'func',[sub,'_task-sodiva_rec-prenorm_run-0',num2str(Run),'_bold.json']);
% val = jsondecode(fileread(fname));
% Run1vol = val.dcmmeta_shape(4);
% 
% fname = fullfile('X:\SODIVA\BIDS\bidssource',sub,'func',[sub,'_task-sodiva_rec-prenorm_run-0',num2str(Run),'_bold.json']);
% val = jsondecode(fileread(fname));

%aqtime = val.AcquisitionTime;
%ScansStartTime = (str2double(aqtime(1:2))*60*60*60 + str2double(aqtime(4:5))*60*60 +  str2double(aqtime(7:8))*60 ) * 1000 + str2double(aqtime(10:end));

matlabbatch = [];
%matlabbatch{1}.spm.tools.physio.save_dir ={fullfile('X:\SODIVA\BIDS\derivatives\Physio\',sub,['Run_',num2str(r)])};
matlabbatch{1}.spm.tools.physio.save_dir ={fullfile(PhysioLocation,sub)};
if ~exist(matlabbatch{1}.spm.tools.physio.save_dir{1},'dir')
    mkdir(matlabbatch{1}.spm.tools.physio.save_dir{1});
end 
matlabbatch{1}.spm.tools.physio.log_files.vendor = 'Siemens';
matlabbatch{1}.spm.tools.physio.log_files.cardiac = {fullfile(PhysioLocation,sub,[sub,'_Physio.puls'])};
matlabbatch{1}.spm.tools.physio.log_files.respiration = {fullfile(PhysioLocation,sub,[sub,'_Physio.resp'])};
if s==4 && r>=3
   matlabbatch{1}.spm.tools.physio.log_files.cardiac = {fullfile(PhysioLocation,sub,[sub,'_Physio_1.puls'])};
   matlabbatch{1}.spm.tools.physio.log_files.respiration = {fullfile(PhysioLocation,sub,[sub,'_Physio_1.resp'])};  
end 
    
matlabbatch{1}.spm.tools.physio.log_files.scan_timing = {fullfile(FirstDicom(1).folder,FirstDicom(1).name)};
matlabbatch{1}.spm.tools.physio.log_files.sampling_interval = [];
matlabbatch{1}.spm.tools.physio.log_files.relative_start_acquisition = 0;
matlabbatch{1}.spm.tools.physio.log_files.align_scan = 'last';

matlabbatch{1}.spm.tools.physio.scan_timing.sqpar.Nslices = 64;
matlabbatch{1}.spm.tools.physio.scan_timing.sqpar.NslicesPerBeat = [];
matlabbatch{1}.spm.tools.physio.scan_timing.sqpar.TR = 1.25;
matlabbatch{1}.spm.tools.physio.scan_timing.sqpar.Ndummies = 0;
matlabbatch{1}.spm.tools.physio.scan_timing.sqpar.Nscans =NumOfVol;% Run1vol;  %
matlabbatch{1}.spm.tools.physio.scan_timing.sqpar.onset_slice = 32;
matlabbatch{1}.spm.tools.physio.scan_timing.sqpar.time_slice_to_slice = 1.25/64;
matlabbatch{1}.spm.tools.physio.scan_timing.sqpar.Nprep = [];
matlabbatch{1}.spm.tools.physio.scan_timing.sync.scan_timing_log = struct([]);
matlabbatch{1}.spm.tools.physio.preproc.cardiac.modality = 'PPU';
matlabbatch{1}.spm.tools.physio.preproc.cardiac.initial_cpulse_select.auto_matched.min = 0.4;
matlabbatch{1}.spm.tools.physio.preproc.cardiac.initial_cpulse_select.auto_matched.file = 'initial_cpulse_kRpeakfile.mat';
matlabbatch{1}.spm.tools.physio.preproc.cardiac.posthoc_cpulse_select.off = struct([]);
matlabbatch{1}.spm.tools.physio.model.output_multiple_regressors = [sub,'_run_',num2str(r),'multiple_regressors.txt'];
matlabbatch{1}.spm.tools.physio.model.output_physio = [sub,'_run_',num2str(r),'physio.mat'];
matlabbatch{1}.spm.tools.physio.model.orthogonalise = 'none';
matlabbatch{1}.spm.tools.physio.model.censor_unreliable_recording_intervals = false;
matlabbatch{1}.spm.tools.physio.model.retroicor.yes.order.c = 3;
matlabbatch{1}.spm.tools.physio.model.retroicor.yes.order.r = 4; % NICO USED 3 HERE
matlabbatch{1}.spm.tools.physio.model.retroicor.yes.order.cr = 1;
matlabbatch{1}.spm.tools.physio.model.rvt.no = struct([]);
matlabbatch{1}.spm.tools.physio.model.hrv.no = struct([]);
matlabbatch{1}.spm.tools.physio.model.noise_rois.no = struct([]);
matlabbatch{1}.spm.tools.physio.verbose.level = 0;  %% 0 = suppress graphical output; 1 = main plots; 2 = debugging plots; 3 = all plots
matlabbatch{1}.spm.tools.physio.verbose.fig_output_file = '';
matlabbatch{1}.spm.tools.physio.verbose.use_tabs = false;

matlabbatch{1}.spm.tools.physio.model.movement.no = struct([]);
matlabbatch{1}.spm.tools.physio.model.other.no = struct([]);

% Including the motion and other also helps control for number of slices. 
% matlabbatch{1}.spm.tools.physio.model.movement.yes.file_realignment_parameters = {fullfile('X:\SODIVA\BIDS\Data_Physio_named',sub,['AllRuns_',sub,'_OnlyMotion_ConfoundsPrePhysio.mat'])};
% matlabbatch{1}.spm.tools.physio.model.movement.yes.order = 6;
% matlabbatch{1}.spm.tools.physio.model.movement.yes.censoring_method = 'FD';
% matlabbatch{1}.spm.tools.physio.model.movement.yes.censoring_threshold = 0.5;
% matlabbatch{1}.spm.tools.physio.model.other.yes.input_multiple_regressors = {fullfile('X:\SODIVA\BIDS\Data_Physio_named',sub,['AllRuns_',sub,'_AllbutMotion_ConfoundsPrePhysio.mat'])};



spm_jobman('run', matlabbatch);

end 
% 
% SPM1 = load(fullfile('X:\SODIVA\BIDS\derivatives\Physio\',sub,'SPM.mat'),'SPM');
% SPM1.SPM.swd = fullfile('X:\SODIVA\BIDS\derivatives\Physio\',sub);
% SPM = SPM1.SPM;
% save(fullfile('X:\SODIVA\BIDS\derivatives\Physio\',sub,'SPM.mat'),'SPM');
% 
% 
% args = tapas_physio_report_contrasts(...
%                     'fileReport', fullfile('X:\SODIVA\BIDS\derivatives\Physio\',sub,'physio.ps'), ...
%                     'fileSpm', fullfile('X:\SODIVA\BIDS\derivatives\Physio\',sub,'SPM.mat'), ...
%                     'filePhysIO', fullfile('X:\SODIVA\BIDS\derivatives\Physio\',sub,'physio.mat'), ...
%                     'fileStructural',fullfile('X:\SODIVA\BIDS\derivatives\Physio\',sub,'ants_t1_to_mni_Warped.nii'));
% 


