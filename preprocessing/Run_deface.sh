#!/usr/bin/bash
# ==============================================================================
# ==============================================================================
# DEFINE ALL PATHS:
# ==============================================================================
# path to the BIDS data:
PATH_BIDS=$HOME/SODIVA/SHARE/BIDS/bidssource
# path to the log directory:
PATH_LOG=$HOME/logs/decoding/
# path to the text file with all subject ids:
PATH_SUB_LIST="$HOME/SODIVA/Scripts/sublists/sublist_Decoding35.txt"
# ==============================================================================
# CREATE RELEVANT DIRECTORIES:
# ==============================================================================
TASK_NAME="pydeface"
# define the path to the singularity container:
PATH_CONTAINER="/data/container/${TASK_NAME}/${TASK_NAME}-37-2e0c2d.sif"

# ==============================================================================
# DEFINE PARAMETERS:
# ==============================================================================
# user-defined subject list
# read subject ids from the list of the text file
SUB_LIST=$(cat ${PATH_SUB_LIST} | tr '\n' ' ')
# ==============================================================================
# RUN THE DECODING: TR and LSS
# ==============================================================================
for SUB in ${SUB_LIST}; do
	echo ${PATH_BIDS}/${SUB}/anat/*T1w.nii.gz
		for FILE in ${PATH_BIDS}/sub-${SUB}/anat/*T1w.nii.gz; do
		# to just get filename from a given path:
		FILE_BASENAME="$(basename -- $FILE)"
		# get the parent directory:
		FILE_PARENT="$(dirname "$FILE")"	
		echo '#!/bin/bash'                             > job.slurm
		echo "#SBATCH --job-name DEFACE${SUB}"  >> job.slurm
		#echo "#SBATCH --partition quick"               >>   job.slurm
		echo "#SBATCH --time 2:0:0"                   >> job.slurm
		echo "#SBATCH --mem 2GB"                      >> job.slurm
		echo "#SBATCH --cpus-per-task 1 "              >> job.slurm
		echo "#SBATCH --output ${PATH_LOG}DEFACE${SUB}.out"   >> job.slurm
		echo "#SBATCH --mail-type NONE"                >> job.slurm
		echo "source /etc/bash_completion.d/virtualenvwrapper" >> job.slurm
		#echo "workon neurogrid" >> job.slurm	
		#echo "source /etc/bash_completion.d/virtualenvwrapper" >> job.slurm
		#echo "workon neurogrid" >> job.slurm
		#echo "pydeface ${PATH_BIDS}/${SUB}/anat/*T1w.nii.gz --force" >> job.slurm
		echo "singularity run --contain -B ${FILE_PARENT}:/input:rw ${PATH_CONTAINER} \
		pydeface /input/${FILE_BASENAME} --force" >> job.slurm
		sbatch job.slurm
		rm -f job.slurm
	done
done

# pydeface $HOMEX/SODIVA/SHARE/BIDS/bidssource/sub-01/*T1w.nii.gz
