#!/usr/bin/bash
# ==============================================================================
# ==============================================================================
# DEFINE ALL PATHS:
# ==============================================================================
# path to the current code folder:
# path to the output directory:
PATH_OUT=/data/ROI/GroupMNI/
PATH_MASKS=/data/ROI/IndividualNative/
# path to the text file with all subject ids:
PATH_SUB_LIST=/data/sublist_included.txt

#PATH_OUT=/data/home/moneta/SODIVA/BIDS/derivatives/ROIS/T1w/NearestNeighbor/spmT_0001_Pos_OneDvalue_targetPlusTwoDvalue_target_T0005_K21_clusters/
#PATH_MASKS=$HOME/SODIVA/BIDS/derivatives/ROIS/MNI/spmT_0001_Pos_OneDvalue_targetPlusTwoDvalue_target_T0005_K21_clusters
#PATH_SUB_LIST="$HOME/SODIVA/Scripts/sublists/sublist_Decoding35.txt"

# ==============================================================================
# DEFINE PARAMETERS:
# ==============================================================================
# maximum number of cpus per process:
N_CPUS=1
# memory demand in *GB*
MEM=4GB
# read subject ids from the list of the text file
SUB_LIST=$(cat ${PATH_SUB_LIST} | tr '\n' ' ')
# ==============================================================================
# RUN ANTS:
# ==============================================================================
string1="T1w_"
module load ants/2.3.3-mpib0
# GROUP
for SUB in ${SUB_LIST}; do
	echo "$SUB"
	SUBBM=$HOME/SODIVA/BIDS/derivatives/BrainMasks/InterRuns/sub-${SUB}_space-subspace_Brainmask_InterRuns.nii
	SUBtrans=$HOME/SODIVA/BIDS/derivatives/fmriprep/sub-${SUB}/anat/sub-${SUB}_from-MNI152NLin2009cAsym_to-T1w_mode-image_xfm.h5
	for filename in $PATH_MASKS/*${SUB}.nii; do
		if [[ ${filename} != *"vmPFC"* ]]; then
		continue
		fi
		f="$(basename -- $filename)"
		outf=$PATH_OUT$string1$f
		antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input $filename --interpolation NearestNeighbor --output $outf --reference-image $SUBBM --transform $SUBtrans
	done
	
	for filename in $PATH_MASKS/*clusters.nii; do
		f="$(basename -- $filename)"
		outf=${PATH_OUT}${string1}sub-${SUB}_${f}
		antsApplyTransforms --default-value 0 --dimensionality 3 --float 0 --input $filename --interpolation NearestNeighbor --output $outf --reference-image $SUBBM --transform $SUBtrans
	done
done 