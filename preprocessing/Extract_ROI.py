# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 11:00:11 2020

@author: User

This is the script used to generate the 2D and 3D data ROIs
Note that some of the paths refer to the location of the whole-brain full data on the MPI cluster. 
"""
import os
from os.path import join as opj
import sys
import glob
import numpy as np
from nilearn.image import load_img,new_img_like
from nilearn.masking import apply_mask
from pathlib import Path

# InputList = glob.glob(opj(home,'SODIVA','BIDS','derivatives','smoothed','smoothed_'+space,sub,'sm'+sm+'*.nii'))
def sortinput(InputList):
     EG = list(os.path.basename(InputList[0]))
     NumsIn = [i for i,s in enumerate(EG) if s.isdigit()]
     FirstDig = min(NumsIn) # the first number
     InSplit = [int(os.path.basename(s)[FirstDig:-4]) for s in InputList]
     Outputlist = [InputList for _,InputList in sorted(zip(InSplit,InputList))]
     return Outputlist

if 'win32' in sys.platform:
    LOCAL = True 
else: 
    LOCAL = False
 
if LOCAL:
    sub = 'sub-02'
    home = opj('X:\\')
else:
    sub = 'sub-%s' % sys.argv[1]
    home = opj(os.environ['HOME'])
    
    
ExtractData = False

print(sub)
# now actually extract the data
#for sm in ['4']:
for sm in ['4','8']:
    print(sm)
    #for space in ['MNI','subspace']: # or 'subspace'
    for space in ['subspace','MNI']: # or 'subspace'
        print(space)
        #if all([space=='subspace',sm=='4']): 
         #   continue # we already have
        #X:\SODIVA\BIDS\derivatives\smoothed4D\smoothed_subspace\sub-xx
        DataSmothed = glob.glob(opj(home,'SODIVA','BIDS','derivatives','smoothed4D','smoothed_'+space,sub,'sm'+sm+'*.nii'))
        # X:\SODIVA\BIDS\derivatives\smoothed\smoothed_MNI\sub-03
        # DataSmothed = sortinput(glob.glob(opj(home,'SODIVA','BIDS','derivatives','smoothed','smoothed_'+space,sub,'sm'+sm+'*.nii')))
        # DataSmothed = ['C:\\Users\\moneta\\Desktop\\tempRSA\\sm4_subspace_sub-02_run-01_in4D.nii']
        if len(DataSmothed)==0: continue
        if space=='MNI':
            ROIsub = opj(home,'SODIVA','SHARE','BIDS','derivatives','ROIs','MNI_func_vmPFC_group.nii')
        else:
            ROIsub =  opj(home,'SODIVA','SHARE','BIDS','derivatives','ROIs','T1w_func_vmPFC_'+sub+'.nii')
        outF = opj(home,'SODIVA','SHARE','BIDS','derivatives','Data2D',space,'smoothed'+sm)
        Path(outF).mkdir(parents=True, exist_ok=True)# make if doesnt exist
        outF3D = opj(home,'SODIVA','SHARE','BIDS','derivatives','Data3D',space,'smoothed'+sm)
        #outF3D = opj('C:\\Users\\moneta\\Desktop\\tempRSA\\')
        Path(outF3D).mkdir(parents=True, exist_ok=True)# make if doesnt exist
        # 2D 
        print('2D')
        for dsm in DataSmothed:
            print(dsm)
            voxel_by_time = apply_mask(dsm,ROIsub)
            np.savetxt(X=voxel_by_time,fname=opj(outF,'_'.join(dsm.split(os.sep)[-1].split('_')[:4])+'_func_vmPFC.csv'),delimiter=',')
            #pd.DataFrame(voxel_by_time).to_csv(opj(outF,'_'.join(dsm.split(os.sep)[-1].split('_')[:4])+'_func_vmPFC.csv', header=None, index=None))
        # 3D 
        print('3D')
        for dsm in DataSmothed:
            print(dsm)
            smimg = load_img(dsm)
            SMd = np.array(smimg.dataobj).astype('<i2')
            roi = load_img(ROIsub)
            roid = np.array(roi.dataobj).astype(int)
            for z in range(SMd.shape[3]):
                SMd[:,:,:,z] = SMd[:,:,:,z]*roid
            #SMd[SMd==0]=np.nan    
            masked_image = new_img_like(smimg,SMd)
            masked_image.to_filename(opj(outF3D,'_'.join(dsm.split(os.sep)[-1].split('_')[:4])+'_func_vmPFC.nii'))

