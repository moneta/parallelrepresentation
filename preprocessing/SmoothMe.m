%% This script is meant to run a smoothing job on the cluster for all subjects
% (1) copy the 4D file, open it to 3D.
% (2) smooth (4mm,8mm) X (subspace)
% (3) smooth (4mm,8mm) X (MNIspace)
% (4) delete the 4D and 3D files

% sub = 'sub-01'
%% Initial variables and folders 
spm('defaults','fmri');
spm_jobman('initcfg');
spm_get_defaults('stats.maxmem',2^32);
spm_get_defaults('stats.resmem', true);

sub = 'sub-%%%SUBJECT%%%';
Rtype = '%%%type%%%';
switch Rtype
    case {'1'}
        fwhm=4;
        s = 1;
    case {'2'}
        fwhm=4;
        s = 2;
    case {'3'}
        fwhm=8;
        s = 1;
    case {'4'}
        fwhm=8;
        s = 2;
end


MotherFolder = fullfile('/home/mpib/moneta/SODIVA/BIDS/derivatives');
inputF = fullfile(MotherFolder,'fmriprep',sub,'func');
outputSM = fullfile(MotherFolder,'smoothed');
outputSM4D = fullfile(MotherFolder,'smoothed4D');

if ~exist(outputSM,'dir')
     mkdir(outputSM)
end
% spaces 
space = {'smoothed_MNI','smoothed_subspace'};
prefixes = {'MNI','subspace'};
spacename = {'MNI152NLin2009cAsym','T1w'};

% F W H M 
%for fwhm = [4,8]
%    for s = 1:2
    %% (1) copy the 4D file, open it to 3D and smooth
    % MNI
        
    SmoothedSpace = fullfile(outputSM,space{s},sub);
%     if ~exist(SmoothedSpace,'dir')
%          mkdir(SmoothedSpace)
%     end
%     % unzip
%     for rc = 1:4
%         sfn = dir(fullfile(inputF,[sub,'_*_rec-prenorm_run-0',num2str(rc),'_space-',spacename{s},'*_bold.nii.gz']));
%         copyfile(fullfile(inputF,sfn.name),fullfile(SmoothedSpace,sfn.name));
%         gunzip(fullfile(SmoothedSpace,sfn.name));
%         % delete the copied zipped file
%         delete(fullfile(SmoothedSpace,sfn.name)); 
%         % convert to 3D nii
%         sfn2 = dir(fullfile(SmoothedSpace,[sub,'_*_rec-prenorm_run-0',num2str(rc),'_space-',spacename{s},'*_bold.nii']));
%         spm_file_split(fullfile(SmoothedSpace,sfn2.name),SmoothedSpace)
%         % delete the 4D nii unzipped
%         delete(fullfile(SmoothedSpace,sfn2.name)); 
%      end
% 
%      clear sfn sfn2
%         
%      % collect data to smooth   
%      fs      = [];
%      fprintf(['collecting data for smoothing ',sub,' in ',space{s},'\n']);
% 
%     for r = 1:4
%        %filt        = ['^',sub,'_task-sodiva_rec-prenorm_run-0*.*\.nii$']; % filename filter
%         filt        = ['^',sub,'_task-sodiva_rec-prenorm_run-0',num2str(r),'_space-',spacename{s},'_desc-preproc_bold_00*.*\.nii$']; % filename filter
%         run         = SmoothedSpace;                 % run directory
%         f           = spm_select('List', run, filt);                            % file selection
%         fs          = [fs; cellstr([repmat([run filesep], size(f,1), 1) f repmat(',1', size(f,1), 1)])]; % SPM style filename list 
%     end 
% 
% 
%     job                                = [];                                % job structure initialization
%     job{1}.spm.spatial.smooth.data     = fs;                                % filenames
%     job{1}.spm.spatial.smooth.fwhm     = [fwhm fwhm fwhm];                  % Gaussian smoothing kernel FWHM
%     job{1}.spm.spatial.smooth.dtype    = 0;                                 % file type
%     job{1}.spm.spatial.smooth.im       = 0;                                 % no implicit masking
%     job{1}.spm.spatial.smooth.prefix   = strcat('sm',num2str(fwhm),'_',prefixes{s},'_');                               % create new images with prefix 's' (smoothing)
%     % run job
%     spm_jobman('run', job);
    fprintf('\n \n \n finished smoothing %s mm for %s \n \n \n',num2str(fwhm),space{s})
    % move back to 4D
    SmoothedSpace4D = fullfile(outputSM4D,space{s},sub);
    if ~exist(SmoothedSpace4D,'dir')
         mkdir(SmoothedSpace4D)
    end
    % empty if exists
    % if exist(SmoothedSpace4D,'dir'), rmdir(SmoothedSpace4D,'s') ; mkdir(SmoothedSpace4D); else, mkdir(SmoothedSpace4D); end
    fprintf(['collecting data for smoothing ',sub,' in',space{1},'\n']);
    for r = 1:4
        fs      = [];
        filt        = ['sm',num2str(fwhm),'_',prefixes{s},'_',sub,'_task-sodiva_rec-prenorm_run-0',num2str(r),'_*.*\.nii$']; % filename filter
        run         = SmoothedSpace;                 % run directory
        f           = spm_select('List', run, filt);                            % file selection
        fs          = [fs; cellstr([repmat([run filesep], size(f,1), 1) f repmat(',1', size(f,1), 1)])]; % SPM style filename list 
        fname = fullfile(SmoothedSpace4D,['sm',num2str(fwhm),'_',prefixes{s},'_',sub,'_run-0',num2str(r),'_in4D.nii']);
        V4 = spm_file_merge(fs,fname);
        fprintf('\n finihsed run \n')
    end 
        
    fprintf('\n \n \n finished converting back to 4D %s mm for %s \n \n \n',num2str(fwhm),space{s})   
%    end 
%end 
