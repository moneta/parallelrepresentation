% this script generates the confound list from fmriPrep for the multiple
% regressors file (before physio)
%addpath(genpath('D:\Google Drive\Matlab\toolboxes\jsonlab-master'))
clear all
if strcmp(computer,'PCWIN64'), homedir = 'X:\'; else, homedir = '/home/mpib/moneta/'; end 
PhysioFiles = fullfile(homedir,'SODIVA','BIDS','derivatives','Physio');
Motloc = fullfile(homedir,'SODIVA','BIDS','derivatives','fmriprep');
OutConfounds = fullfile(homedir,'SODIVA','BIDS','derivatives','Confounds');
OutConfounds2 = fullfile(homedir,'SODIVA','BIDS','derivatives','ConfoundsCSV');
OutConfounds3 = fullfile(homedir,'SODIVA','BIDS','derivatives','ConfoundsCSV_multivariate');

%%%% UPDATE 30.3.2020  %%%%%
% No more cosine!        %   'cosine00', 'cosine01','cosine02', 'cosine03',...
AddReg = {'framewise_displacement',... % DONT FORGET TO CHANGE NAN TO MEAN OF PARAMETER
          'a_comp_cor_00', 'a_comp_cor_01', 'a_comp_cor_02', 'a_comp_cor_03','a_comp_cor_04', 'a_comp_cor_05',...
          'trans_x','trans_y','trans_z','rot_x','rot_y','rot_z'};
%% generate a json file that explains the different parameters from the fmriprep confounds

ConfDesc.fmriprep_website = ['https://fmriprep.readthedocs.io/en/stable/outputs.html # Confounds and disscussion: https://neurostars.org/t/fmriprep-which-regressors-for-best-global-signal-regression-comparison/1878/2'];
ConfDesc.fmriprep_framewise_displacement = ['R X : R X. estimated bulk-head motion, IMPORTANT: first value need to be changed to mean of the entire vector'];
ConfDesc.fmriprep_a_comp_cor_0X =  ['R X : R X. PCA on an ROI that is likely not signal (i.e. WM/CSF) to detect noise. "a_" means anatomical based.'];
%ConfDesc.fmriprep_cosine0X =  ['R X : R X. high pass filtering. If using the a_comp_cor, Chris G. claims this needs to be also included: https://neurostars.org/t/fmriprep-which-regressors-for-best-global-signal-regression-comparison/1878, however this disscussion shows why its problematic: https://neurostars.org/t/fmriprep-which-regressors-for-best-global-signal-regression-comparison/1878'];
ConfDesc.fmriprep_trans_X_rot_X = ['R X : R X. estimated head motion parameters'];

ConfDesc.tapas_physio_website = [''];
ConfDesc.tapas_physio_cardiac = ['R X : R X']; 
ConfDesc.tapas_physio_respiratory =['R X : R X'];
ConfDesc.tapas_physio_interaction = ['R X : R X'];

%% run through subjects       
for s = 1:40
    if s<10
        sub = strcat('sub-0',num2str(s));
    else
        sub = strcat('sub-',num2str(s));
    end   
MotR = {};
Physio = {};
MotRefFileLoc = fullfile(Motloc,sub,'func');

    for r = 1:4
        %% collect motion regressors
        MotFile =  dir(fullfile(MotRefFileLoc,[sub,'_task-sodiva_rec-prenorm_run-0' num2str(r) '_desc-confounds_regressors.tsv'])); 
        MotR{r} = readtable(fullfile(MotFile(1).folder,MotFile(1).name), 'FileType', 'text', 'Delimiter', 'tab');        
        R = zeros(height(MotR{r}),numel(AddReg));
        % first FD cause its tricky input
        MotR{r}.framewise_displacement{1} = nan; %otherwise some string there
        R(:,1) = str2double(MotR{r}.framewise_displacement);
        R(1,1)= mean(R(2:end,1)); % first row is n/a
        
        for mcol = 2:numel(AddReg) % FD is before that 
          R(:,mcol) = MotR{r}.(AddReg{mcol});
        end 
        %sanity_check
        PhFile =  dir(fullfile(PhysioFiles,sub,[sub,'_run_',num2str(r),'multiple_regressors.txt']));
        Physio{r} = readtable(fullfile(PhFile(1).folder,PhFile(1).name), 'FileType', 'text', 'Delimiter', 'tab');   
        Physio{r}.Var19 = []; % strange variable created in the readtable
        PhyArr = table2array(Physio{r});
        R = [R, PhyArr];
        
        if sum(isnan(R(:)))~= 0
              fprintf('\n \n PROBLEM \n PROBLEM \n PROBLEM PROBLEM PROBLEM NAN IN THE GLM ADDITIONAL REGRESSORS \n \n \n')
        elseif size(R,2) ~= 31
             fprintf('\n \n NOT ENOGUHT REGRESSORS \n \n \n')
        end
        
        if r == 1
            FullR = R;
        else 
            FullR=[FullR;R];
        end 
    end
    csvwrite(fullfile(OutConfounds3,[sub,'_AllConfounds.csv']),FullR);
end 

