# Scripts & Preprocessed data for the analysis of Representations of context and context-dependent values in vmPFC compete for guiding behavior. Moneta Garvert Heekeren & Schuck (in review)

# pre print
can be found here: https://www.biorxiv.org/content/10.1101/2021.03.17.435844v3

# License 
Script and data are licensed under: Creative Commons Attribution-ShareAlike 4.0 International Public License (see LICENSE file)

# Guide for behavioral analysis scripts:
**Step 0: Packages and setting helping functions**
load and install needed packages and set some helping functions that will help later
**Step 1: loading and organizing data, exclusion**
Exclusion:
- We excluded one subject for bad OFC signal drop (more than 15% less voxels in functional compared to freesurfer OFC ROI from T1) (sub-18)
- we excluded one subject due to motion (sub-21)
- we excluded 3 subjects that performed less than 75% in one context in the main task. (sub 03, 28 and 34)
**Step 2: Descriptive statistics**
2.1 staircasing -> RT variance decrease
2.2 Outcome learning -> accuracy in learning the value mapping 
2.3 OneD miniblocks ->  no feature specific RT change
2.4 Accuracy main task (and all parts) -> high
2.5 Main RT effects -> EV, Congruency, EVBACK x Congruency
**Step 3: Modelling** 
3.1 RT modeling 
3.1.1 testing log RT : is it normal?
3.1.2 hierarchical model comparison
3.1.3 sanity check: with 1D in the DF
3.2 Accuracy modeling 
3.2.1 hierarchical model comparison
**Step 4: Replication sample (Additional cohort collected outside of the scanner)**
Descriptive, RT and Accuracy models. 
**Step 5: suplamentary information**
5.1. Three way interaction & nested models 
5.2. Extended correlation matrix, Extended RT model comparison and other parameters 
5.3. Exploratory RT analysis : all possible 2 way interactions 
5.4. Exploratory Behavioral accuracy analysis : all possible 2 way interactions 