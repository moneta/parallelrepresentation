#!/bin/bash
#X:\SODIVA\Scripts\sublists
# read subject ids from the list of the text file
# Path for the standalone version of spm
PATH_RUN_SPM="$HOME/tools/spm12"

GLM_LIST=(1 2 3 4 5 6 7 8 9)
for glm in ${GLM_LIST[@]}; do
	echo ${glm}
	echo '#!/bin/bash'                    > job.slurm
	echo "#SBATCH --job-name SecondL_GLMs${glm}"  >> job.slurm
	echo "#SBATCH --partition quick"               >>   job.slurm
    echo "#SBATCH --time 2:0:0"              >> job.slurm
	echo "#SBATCH --mem 4GB"              >> job.slurm
	echo "#SBATCH --cpus-per-task 1 "          >> job.slurm
	echo "#SBATCH --output $HOME/logs/GLM/All_Second_Levels_GLMs${glm}_OutPut.out"              >> job.slurm
	echo "#SBATCH --mail-type NONE"              >> job.slurm
	sed "s#%%%GLM%%%#${glm}#" $HOME/SODIVA/Scripts/SHARE/univariate/SecondLevel_OneSampleTtest.m > $HOME/SODIVA/work/GLM/SecondLevel_OneSampleTtest${glm}.m
	echo "$HOME/tools/standalone/run_spm12.sh /opt/matlab/R2017b script $HOME/SODIVA/work/GLM/SecondLevel_OneSampleTtest${glm}.m"   >> job.slurm
	sbatch job.slurm
    rm -f job.slurm
done
