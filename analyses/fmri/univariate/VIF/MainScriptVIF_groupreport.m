%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% run on cluster:
% module load spm12
% run_spm12.sh /mcr script $HOME/SODIVA/Scripts/SHARE/univariate/MainScriptVIF_groupreport.m
% Diagnose your design matrix! Part 2.2
% Get group figures and data 
% This function averages VIFs and Correlation matricies for differnet contrasts of interest (and for all onsets) across subjects and plots and saves the results. 
% All the code was written by Nir Moneta (moneta@mpib-berlin.mpg.de)
%
% This script takes as input the location of a folder that has all the subject output folders (can be copied from MainScriptVIF_subjectwise.m)
% The output of this script is:
% - mean and sem correlation matrix (seperate figures) for all the matrices done in previous stage
% - mean and sem (barplot) for all the VIFs in the previous part, across
% subjects. plus vertical line for 1 (minimum), 2 (harsh curoff) 5 (typical
% cutoff) and 10 (more than this is no go). 
% - group tables with the data (.csv)
% 
% Please addapt only (1) (can copy from MainScriptVIF_subjectwise.m). 
%
% NOTE 1: This needs to run on the cluster in an interactive node. see .md
% for instructions. 
% NOTE 2: SEM is not corrected for number of conditions but it should be good
% enough for now. 
%
% TO DO: plots could go better, esp. the VIF and splitting but im out of
% time for this scripts for now :)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (1) PATHS (actions needed - adjust the paths)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In case of existing GLM: GLMLocation needs to be a folder that in it there is one folder per subject. 
% In case of trial-wise GLM: GLMLocation would be where the GLM is saved. 
if strcmp(computer,'PCWIN64'), GLM.homedir = 'X:\SODIVA\SHARE\'; else, GLM.homedir = '/home/mpib/moneta/SODIVA/SHARE/'; end 
DervF = fullfile(GLM.homedir,'BIDS','derivatives');
InputFiles =fullfile(DervF,'Univariate','VIFS');
MotherGLM = {'VIF',...
             'ROI_GLM',...% EV1D,EV2D for making ROI
             'Classic','Classic','Classic','Classic',...
             'Classic','Classic',...
             'Split_Onsets','Split_Onsets'};%,...
GLMTypes = {'TRIALWISE',...
            'EV_EV',...
            'EV_EV','EV_EV_Congruency_EVback','EV_EV_EVbackXCongruency','EV_EVxCongruency',...
            'nu_mval_nu_mval_cond_evback','nu_mval_nu_mval_evbackSigned',...            
            'Congruency_split','CongruencyXEV_split'};

NumPrintThreshold = 30; % plot the number inside each cell only if there are less than this number on each axis - otherwise takes a long time and hard to read. 

% should run both parts? 
Run.CORR = true;
Run.VIF = true;
Run.averageBlocks = false; % true if onsets repeat between blocks (set by their name), false for looking trialwise. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START RUNNING - no adjustments needed anymore
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RUNonGLMS='All'; % e.g. [1:3]
i_glm = 0;
for GLMType = GLMTypes
    %GLMType = GLMTypes(i_glm)
    i_glm = i_glm + 1;
    % Skip GLMs if needed
    if ~strcmp(RUNonGLMS,'All')
        if ~ismember(i_glm,RUNonGLMS)%sum(i==RUNonGLMS)==0
            fprintf('skipping')
            continue; 
        end
    end 
    Output = fullfile(InputFiles,[MotherGLM{i_glm},'_',GLMType{1}]);
    fprintf('\n \n \n STARTING %s \n\n\n',Output)
    if Run.CORR
        Names = {'Onsets','Pmods','Onsets_N_pmods','Full','Existing'};
        for i = 1:numel(Names)
            Inputs = dir(fullfile(Output,'*',['*',Names{i},'_Corrs.csv']));
            if isempty(Inputs), fprintf('No data for %s \n',Names{i}); continue; end 
            T = table;
            for j = 1:numel(Inputs), T = [T;readtable(fullfile(Inputs(j).folder,Inputs(j).name), 'Delimiter', ',')]; end 
            if isempty(T), fprintf('No data for %s \n',Names{i}); continue; end 
            fprintf('found data for %d subjects \n',numel(Inputs))

            writetable(T,fullfile(Output,[Names{i},'_CORRs.csv']))
            fprintf(' saved %s \n',[Names{i},'_CORRs.csv'])

            U = varfun(@nanmean,T,'InputVariables','CORR','GroupingVariables',{'var1','var2'});
            U.GroupCount=[];
            Estim.mean = unstack(U,'nanmean_CORR','var1');
            Estim.mean = Estim.mean{:,2:end};

            U = varfun(@(X) numel(X),T,'InputVariables','CORR','GroupingVariables',{'var1','var2'});
            U.GroupCount=[];
            Estim.N = unstack(U,'Fun_CORR','var1');
            Estim.N= Estim.N{:,2:end};

            U = varfun(@(X) std(X)/sqrt(length(X)),T,'InputVariables','CORR',...
                   'GroupingVariables',{'var1','var2'});
            U.GroupCount=[];
            Estim.sem =  unstack(U,'Fun_CORR','var1');
            Estim.sem= Estim.sem{:,3:end};

            fprintf('  Done with making correlation matrix \n');

            VARIS = unique(T.var1,'stable');
            if numel(VARIS)>NumPrintThreshold, PrintValues=false; fprintf('not printing correlation values in matrix \n');
            else, PrintValues=true; fprintf('printing correlation values in matrix \n'); end
            FN = fieldnames(Estim);
            for fn =1:numel(FN)
                h = figure('Visible','Off');
                imagesc(Estim.(FN{fn}))
                colorbar
                if ~strcmp(FN{fn},'N'),caxis([-1, 1]);end
                set(gca,'YTick',1:numel(VARIS),'YTickLabel', VARIS);% something like this
                set(gca,'XTick',1:numel(VARIS),'XTickLabel',VARIS);% something like this
                set(gca,'XTickLabelRotation',45)
                set(gca,'YTickLabelRotation',-45)
                set(gca,'FontSize',6) % Creates an axes and sets its FontSize to 18
                set(gca,'xaxisLocation','top')
                set(gcf,'Units','inches');
                screenposition = get(gcf,'Position');
                set(gcf,'PaperPosition',[0 0 screenposition(3:4)],'PaperSize',[screenposition(3:4)]);
                set(gca,'FontSize',6) % Creates an axes and sets its FontSize to 18
                if PrintValues
                    [m,n]=size(Estim.(FN{fn}));
                    hold on;
                    for r = 1:m
                      for c = 1:n
                          nu = Estim.(FN{fn})(r,c);
                          val = num2str(round(nu,2));
                          %if strcmp(computer,'PCWIN64'), FontS = 12; else, FontS = 4;end
                          text(c-0.25,r,val,'FontSize',8)
                      end
                    end
                    hold off;
                end
                print(fullfile(Output,['CorrMat_',Names{i},'_',FN{fn},'.pdf']),'-dpdf','-fillpage')
                fprintf(' saved %s \n',['CorrMat_',Names{i},'_',FN{fn},'.pdf'])

            end 
        end % Correlation matrix 
    end 


    if Run.VIF
        Inputs = dir(fullfile(Output,'*',['*_VIFs.csv']));
        T = table;
        for j = 1:numel(Inputs), T = [T;readtable(fullfile(Inputs(j).folder,Inputs(j).name), 'Delimiter', ',')]; end 
        temp = T.VIFnames;
        if Run.averageBlocks % so not trialwiese
            temp =cellfun(@(x) x(7:end), temp, 'un', 0);
        else 
            temp =cellfun(@(x) x(4:end), temp, 'un', 0); 
        end 
        temp(contains(temp,'^')) = cellfun(@(x) x(1:end-2), temp(contains(temp,'^')), 'un', 0);
        temp(contains(temp,'*')) = cellfun(@(x) x(1:end-6), temp(contains(temp,'*')), 'un', 0);
        T.VIFnames = temp;
        U = varfun(@mean,T,'InputVariables','vifs','GroupingVariables','VIFnames');
        VIFs.mean = U.mean_vifs;
        VIFs.N = U.GroupCount;
        U = varfun(@(X) std(X)/sqrt(length(X)),T,'InputVariables','vifs','GroupingVariables','VIFnames');
        VIFs.sem = U.Fun_vifs;
        U = varfun(@(X) std(X),T,'InputVariables','vifs','GroupingVariables','VIFnames');
        VIFs.sd = U.Fun_vifs;
        VIFs.VARIS = U.VIFnames;
        writetable(struct2table(VIFs'),fullfile(Output,['Group_VIFS.csv']))

        fprintf('starting group figure VIF \n');
        h = figure('Visible','Off');
        N = length(VIFs.mean);
        SPLIT = ceil(N/70);
        counter = 1;
        for i=1:SPLIT
    %         subplot(SPLIT,1,i)
            ind = counter:counter+ceil(N/SPLIT);
            if i==SPLIT, ind = counter:N; end
            counter=counter+ceil(N/SPLIT)+1;
            bar(VIFs.mean(ind),'FaceColor',[0 .5 .5]);
            ylabel('Variance inflation factor'); xlabel('Predictor number');
            if max(VIFs.mean)>1, line([0,numel(VIFs.mean(ind))],[1,1],'color','green','LineStyle','--'); end
            if max(VIFs.mean)>2, line([0,numel(VIFs.mean(ind))],[2,2],'color','blue','LineStyle','--'); end
            if max(VIFs.mean)>5, line([0,numel(VIFs.mean(ind))],[5,5],'color','red','LineStyle','--'); end 
            if max(VIFs.mean)>10, line([0,numel(VIFs.mean(ind))],[10,10],'color','red','LineStyle',':'); end 
            ylim([0 max(VIFs.mean)+1])
            hold on
            %er = errorbar(1:length(VIFs.mean),VIFs.mean,VIFs.mean-VIFs.sem,VIFs.mean+VIFs.sem);    

            er = errorbar(1:length(VIFs.mean(ind)),VIFs.mean(ind),VIFs.sem(ind),VIFs.sem(ind));    
            er.Color = [0 0 0];                            
            er.LineStyle = 'none';  
            hold off
            set(gca,'XTick',1:numel(VIFs.VARIS(ind)),'XTickLabel', VIFs.VARIS(ind));% something like this
            set(gca,'XTickLabelRotation',45)
            set(gca,'FontSize',6) % Creates an axes and sets its FontSize to 18
            set(gcf,'Units','inches');
            screenposition = get(gcf,'Position');
            set(gcf,'PaperPosition',[0 0 screenposition(3:4)],'PaperSize',[screenposition(3:4)]);
            set(gca,'FontSize',5) % Creates an axes and sets its FontSize to 18
            print(fullfile(Output,['VIFS',num2str(i),'.pdf']),'-dpdf','-fillpage')
        end 

        counter = 1;
        for i=1:SPLIT
    %         subplot(SPLIT,1,i)
            ind = counter:counter+ceil(N/SPLIT);
            if i==SPLIT, ind = counter:N; end
            counter=counter+ceil(N/SPLIT)+1;
            h = figure('Visible','Off');
            bar(VIFs.N(ind),'FaceColor',[0 .5 .5]);
            ylabel('Existance of regressors across participants'); xlabel('Predictor number');
            set(gca,'XTick',1:numel(VIFs.VARIS(ind)),'XTickLabel',VIFs.VARIS(ind));% something like this
            set(gca,'XTickLabelRotation',45)
            set(gca,'FontSize',6) % Creates an axes and sets its FontSize to 18
            set(gcf,'Units','inches');
            screenposition = get(gcf,'Position');
            set(gcf,'PaperPosition',[0 0 screenposition(3:4)],'PaperSize',[screenposition(3:4)]);
            set(gca,'FontSize',5) % Creates an axes and sets its FontSize to 18
            print(fullfile(Output,['RegressorsCount',num2str(i),'.pdf']),'-dpdf','-fillpage')
        end 
            fprintf('Done figures VIF \n');
    end 
end 
close all hidden 