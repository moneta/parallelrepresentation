GLM.sub = 'sub-%%%SUBJECT%%%'; % GLM.sub ='sub-01'

% Diagnose your design matrix! 
% This function computes VIFs and Correlation matricies for differnet contrasts of interest (and for all onsets). 
% All the code was written by Nir Moneta (moneta@mpib-berlin.mpg.de)
% The VIF computation was addapted from Sam Gershman's resources, https://github.com/sjgershm/ccnl-fmri/blob/master/ccnl_vifs.m
%
% This script takes as input the location of a folder for SPM.mat for the subjects
% i.e. a folder that inside ...folder/sub-01/SPM.mat
% The output of this script is:
% 
% - Correlation matricies figure of all regressors (Full), only onsest, only
% parametric modulators (pmods), and one for onsets & pmods and one for
% contrasts if already specified in the SPM.mat 
% if the matrix is smaller than 30x30 it also prints the value inside each cell
% Note: if you used the GLM script, there are no specfied contrasts
% but you can do it on your own realtive easy (e.g. split by training sets for the classifier)! write me for help :) 
% - For all matricies also saves a .csv file with the correlations (loaded
% later in the group script)
%
% - barplot Figure of VIF for each regressor
% 
% Please go through (1) and (2) to adjust what is needed for your analysis. 
% To do: 
% - read some info from the .json bids file? 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (1) PATHS (actions needed - adjust the paths)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In case of existing GLM: GLMLocation needs to be a folder that in it there is one folder per subject. 
% In case of trial-wise GLM: GLMLocation would be where the GLM is saved. 
if strcmp(computer,'PCWIN64'), GLM.homedir = 'X:\SODIVA\SHARE\'; else, GLM.homedir = '/home/mpib/moneta/SODIVA/SHARE/'; end 
DervF = fullfile(GLM.homedir,'BIDS','derivatives');
General_inputGLM = fullfile(DervF,'Univariate','First_lvls');
Output =fullfile(DervF,'Univariate','VIFS');
if ~exist(Output,'dir'), mkdir(Output); end

MotherGLM = {'VIF',...
             'ROI_GLM',...% EV1D,EV2D for making ROI
             'Classic','Classic','Classic','Classic',...
             'Classic','Classic',...
             'Split_Onsets','Split_Onsets'};%,...
GLMTypes = {'TRIALWISE',...
            'EV_EV',...
            'EV_EV','EV_EV_Congruency_EVback','EV_EV_EVbackXCongruency','EV_EVxCongruency',...
            'nu_mval_nu_mval_cond_evback','nu_mval_nu_mval_evbackSigned',...            
            'Congruency_split','CongruencyXEV_split'};

        
% GLM.spmdir = fullfile(GLM.homedir,'tools/spm12'); % location of spm directory
% addpath(genpath(GLM.spmdir));
spm('defaults','fmri');
spm_jobman('initcfg');

GLM.ignoreEstimatability = false; % in case you have super good reason to run this on an unestimatable GLM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (2) Define how to calculate the VIF (take a look, esp. GLM.VIFOnlySimuli)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Since VIF is asking 'how each regressor is predictive by the other?' then
% which regressors are included in the computation could be critical. 
% To include all have: false, false, false on all. 
% However, my intuition is to exclude the confounds (they often correlate with each other strongly) 
% and to exclude parametric modulators if your goal is to estimate the onsets for example in multivariate. 
GLM.VIFexcludePmods = true; % if false, includes pmodes as well. 
GLM.VIFOnlySimuli = false; %false ... if true then include only Stim in VIF (e.g. for trialwise that ignores rest). if false than includes also regressors without 'stim' in their name 
GLM.VIFOnlySimuli_startwith = 'stim'; %false ... if true then include only Stim in VIF (e.g. for trialwise that ignores rest). if false than includes also regressors without 'stim' in their name 
GLM.excludeConfounds = false; %true; % if true then exclude confounds from vif

GLM.NumPrintThreshold = 30; % plot the number inside each cell only if there are less than this number on each axis - otherwise takes a long time and hard to read. 
% CHOSENGLM = str2double('%%%GLM%%%');

RUNonGLMS='All'; % e.g. [1:3]
i_glm = 0;
for GLMType = GLMTypes
    %GLMType = GLMTypes(i)
    i_glm = i_glm + 1;
    % Skip GLMs if needed
    if ~strcmp(RUNonGLMS,'All')
        if ~ismember(i_glm,RUNonGLMS)%sum(i==RUNonGLMS)==0
            fprintf('skipping')
            continue; 
        end
    end 
    % GLMs
    fprintf(num2str(i))
    GLM.Location = fullfile(General_inputGLM,MotherGLM{i_glm},GLMType{1},GLM.sub); % sub specific locations - will be cleaned beforehand
    tempout = fullfile(Output,[MotherGLM{i_glm},'_',GLMType{1}]);
    if ~exist(tempout ,'dir'), mkdir(tempout); end
    GLM.OutLocation = fullfile(tempout,GLM.sub); % sub specific location of output
    if ~exist(GLM.OutLocation ,'dir'), mkdir(GLM.OutLocation ); end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % START THE SCRIPT - no need for any adjustments anymore 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('starting: \n %s \n',GLM.OutLocation)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    % Load the SPM
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    fprintf('Loading SPM.mat \n');
    spmfilename = fullfile(GLM.Location , 'SPM.mat');
    if ~exist(spmfilename, 'file'), error('SPM.mat does not exist in %s\n', GLM.Location ); end
    load(spmfilename); % load(fullfile(OutputGLM, 'SPM.mat'));
    fprintf('Loaded SPM \n');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % (5) Get VIF and correlation matricies for each subject (no action needed)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    fprintf('Starting VIF and correlation matrix for sub %s \n',GLM.sub);

    ColNames = SPM.xX.name;% takse all conditions from SPM
    % Check for estimatability in genreal:
    Estim = spm_SpUtil('isCon',SPM.xX.X);
    if sum(Estim)~=length(Estim)
         fprintf('\n PROBLEM PROBLEM PROBLEM for %s these regressors are not uniquly specified \n',GLM.sub)
         ColNames(~Estim)
         fprintf('\n That means some of these are a linear combination of the others and GLM results are not interpretable \n')
         if ~GLM.ignoreEstimatability, return; end
    else
        fprintf('\n regressors for %s are uniquly specified! well done! \n',GLM.sub)
    end 
    % split to confounds (R) constants, parametric modulators (pmods) and onset regressors
    Rs        = find((contains(ColNames,' R')));
    Constants = find((contains(ColNames,'constant')));
    Pmods = find((contains(ColNames,'^1')));
    Onsets =  find(~(contains(ColNames,' R') | contains(ColNames,'constant') | contains(ColNames,'^1')));

    fprintf('For %s, found across all blocks %d onsets, %d pmods, %d confound regressors and %d constatnts \n',GLM.sub,length(Onsets),length(Pmods),length(Rs),length(Constants));
    % correct the R
    RdoubleDigit = find(contains(ColNames,' R') & cell2mat(cellfun(@(x) length(x)>8, ColNames, 'un', 0))==1);
    ColNames(RdoubleDigit) = cellfun(@(x) [x(1:7),'-',x(8:end)], ColNames(RdoubleDigit), 'un', 0);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Run the correlation matrix:
    % this runs correlation matrix of the regressors: only onsets, only pmods,
    % both and everything (including confounds if needed)
    % this also adds existing contrasts if specified in the GLM
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Indis = {Onsets,Pmods,[Onsets,Pmods],[1:numel(ColNames)]};
    Names = {'Onsets','Pmods','Onsets_N_pmods','Full'}; %Existing
    Method = {'AcrossBlocks','AcrossBlocks','AcrossBlocks','AcrossBlocks'};
    % add if there are already set contrasts 
    if isfield(SPM,'xCon'), if isfield(SPM.xCon,'c'), if sum(sum([SPM.xCon(:).c]))~=0
                Indis{end+1} = [1:numel(ColNames)];
                Names{end+1} = 'Existing';
                Method{end+1} = 'Existing';
    end; end; end

    for j = 1:numel(Indis)
        switch Method{j}
            case {'AcrossBlocks'}
        VariUni = unique(cellfun(@(x) x(7:end), ColNames(Indis{j}), 'un', 0),'stable');
        BasicCons = zeros(size(VariUni,2),size(ColNames,2));         
        for i = 1:numel(VariUni), BasicCons(i,contains(ColNames,VariUni{i}))=1; end
        Estim = spm_SpUtil('ConR',SPM.xX.X,BasicCons'); %  design matrix
            case {'Existing'}
        Estim = spm_SpUtil('ConR',SPM.xX.X,[SPM.xCon(:).c]); %  design matrix
        VariUni = {SPM.xCon(:).name};
        end
        % clean Variable names
        VariUni(contains(VariUni,'^')) = cellfun(@(x) x(1:end-2), VariUni(contains(VariUni,'^')), 'un', 0);
        VariUni(contains(VariUni,'*')) = cellfun(@(x) x(1:end-6), VariUni(contains(VariUni,'*')), 'un', 0);

        if ~strcmp(computer,'PCWIN64'),h = figure('Visible','Off'); else, figure; end
        imagesc(Estim)
        colorbar
        set(gca,'YTick',1:numel(VariUni),'YTickLabel', VariUni);% something like this
        set(gca,'XTick',1:numel(VariUni),'XTickLabel',VariUni);% something like this
        set(gca,'XTickLabelRotation',45)
        set(gca,'YTickLabelRotation',-45)
        set(gca,'FontSize',6) % Creates an axes and sets its FontSize to 18
        set(gca,'xaxisLocation','top')
        set(gcf,'Units','inches');
        screenposition = get(gcf,'Position');
        set(gcf,'PaperPosition',[0 0 screenposition(3:4)],'PaperSize',[screenposition(3:4)]);
        set(gca,'FontSize',6) % Creates an axes and sets its FontSize to 18    
        [m,n]=size(Estim);
        if numel(VariUni)<GLM.NumPrintThreshold
            hold on;
            for r = 1:m
              for c = 1:n
                  nu = Estim(r,c);
                  val = num2str(round(nu,2));
                  text(c-0.25,r,val,'FontSize',8)
              end
            end
            hold off;
        end
        print(fullfile(GLM.OutLocation,[Names{j},'_CorrMat.pdf']),'-dpdf','-fillpage')
        fprintf('saved %s \n',[GLM.sub,'_',Names{j},'_CorrMat.pdf'])
        % make table and save
        N = numel(VariUni);
        CorrTable = table; 
        CorrTable.var1 = repmat(VariUni ,[1,N])';
        var2 = cellfun(@(x) repmat({x},[N,1]),VariUni , 'un', 0);
        CorrTable.var2 = vertcat(var2{:});
        CorrTable.CORR = Estim(:); % i think this should work! 
        CorrTable.sub=repmat({GLM.sub},[N^2,1]);
        writetable(CorrTable,fullfile(GLM.OutLocation,[GLM.sub,'_',Names{j},'_Corrs.csv']))
        fprintf('saved %s \n',[GLM.sub,'_',Names{j},'_Corrs.csv'])
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % %%%%%%%%%%%% Get VIF %%%%%%%%%%%%%% mostly is gracefully taken from Gershman lab (LINK)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    fprintf('getting VIFs for %s \n',GLM.sub);
    % Variance Inflation Factors for regs of interest (iC)
    warning off % some nuisance covs could be redundant; we don't care.
    Ind = true(1,length(SPM.xX.iC));
    NamesNoConstant = SPM.xX.name(~contains(SPM.xX.name,'constant')); % this is same as SPM.xX.iC
    if GLM.excludeConfounds,  Ind=Ind&~contains(NamesNoConstant,' R');end
    if GLM.VIFexcludePmods, Ind=Ind&~contains(NamesNoConstant,'^1');end
    if GLM.VIFOnlySimuli, Ind=Ind&contains(NamesNoConstant,GLM.VIFOnlySimuli_startwith);end
    %%%%%%%%%%%%%%%%%%%%%
    %vifs = getvif(SPM.xX.X(:, SPM.xX.iC(Ind)), 0);
    no_add_intercept = 0;
    X = SPM.xX.X(:, SPM.xX.iC(Ind));
    wh_is_intercept = find( ~any(diff(X)) ); % which column is the intercept?
    if ~isempty( wh_is_intercept ), X(:, wh_is_intercept) = []; end % only using remove
    [n, k] = size(X);
    % calculate
    % -------------------------------------------------------------------------
    % initialize, exclude the intercept
    vifs = [];  %ones(1, length(wh));
    intcpt = ones(n, 1);
    for i = 1:k
        if no_add_intercept, Xi = X;
        else, Xi = [X intcpt]; end
        % check that model has an intercept, or that all preds are
        % mean-centered.  if both of these conditions are false, Rsquared is uninterpretable see [http://statistiksoftware.blogspot.com/2013/01/why-we-need-intercept.html]
        % and thus VIF is uninterpretable.  Yoni, May 2015
        hasint = any(var(Xi)==0);
        totallymc = sum(mean(Xi))==0;

        if ~hasint && ~totallymc
            warning('Model has no intercept, and model is not mean-centered; VIF is not interpertable');
            vifs = NaN * zeros(1, length(wh));
            return
        end
        y = Xi(:, i); 
        Xi(:, i) = [];
        b = Xi\y;
        fits = Xi * b;
        rsquare = var(fits) / var(y);
        % sometimes rounding error makes rsquare>1
        if rsquare >= 1, rsquare = .9999999; end
        vifs(end + 1) = 1 / (1 - rsquare);
        if i==round(max(k)/4), fprintf('finished 1/4 of vifs \n');end
        if i==round(k/2), fprintf('finished 1/2 of vifs \n');end
        if i==round(3*k/4), fprintf('finished 3/4 of vifs \n');end
    end  % regressor

    names = SPM.xX.name(SPM.xX.iC(Ind));
    warning on
    fprintf('Got VIFs \n');

    fprintf('starting figures VIF for %s \n',GLM.sub);
    allvifs =  vifs;
    VIFnames = names;
    h = figure('Visible','Off');
    bar(allvifs,'FaceColor',[0 .5 .5]);
    ylabel('Variance inflation factor'); xlabel('Predictor number');
    if max(allvifs)>1, line([0,numel(allvifs)],[1,1],'color','green','LineStyle','--'); end
    if max(allvifs)>2, line([0,numel(allvifs)],[2,2],'color','blue','LineStyle','--'); end
    if max(allvifs)>5, line([0,numel(allvifs)],[5,5],'color','red','LineStyle','--'); end 
    if max(allvifs)>10, line([0,numel(allvifs)],[10,10],'color','red','LineStyle',':'); end 
    ylim([0 max(allvifs)+1])
    %set(gca,'YTick',1:numel(VIFnames),'YTickLabel', VIFnames);% something like this
    set(gca,'XTick',1:numel(VIFnames),'XTickLabel',VIFnames);% something like this
    set(gca,'XTickLabelRotation',45)
    %set(gca,'YTickLabelRotation',-45)
    set(gca,'FontSize',6) % Creates an axes and sets its FontSize to 18
    set(gca,'xaxisLocation','top')
    set(gcf,'Units','inches');
    screenposition = get(gcf,'Position');
    set(gcf,'PaperPosition',[0 0 screenposition(3:4)],'PaperSize',[screenposition(3:4)]);
    set(gca,'FontSize',6) % Creates an axes and sets its FontSize to 18
    print(fullfile(GLM.OutLocation,[GLM.sub,'_VIFS.pdf']),'-dpdf','-fillpage')
    fprintf('saved %s \n',[GLM.sub,'_VIFS.pdf'])

    outT.vifs =vifs';
    outT.VIFnames =VIFnames';
    outT.sub = repmat(GLM.sub,[length(VIFnames) 1]);
    writetable(struct2table(outT),fullfile(GLM.OutLocation,[GLM.sub,'_VIFs.csv']))
    fprintf('saved %s \n',[GLM.sub,'_VIFs.csv'])
    fprintf('DONE')
end


close all hidden
