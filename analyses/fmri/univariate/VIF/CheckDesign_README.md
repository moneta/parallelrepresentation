# Run diagnostics on your design

These scripts run correlation matrix and VIF (Variance Inflation Factor) as measurements for collinearity in a design. 
If you have an existing SPM.mat, skip step 1. 

What do you need? 
- some pilot fMRI data. <br/>(I think you could give any data since the calculations don't depend on the BOLD, but the SPM requires some data as input)<br/>
- Existing SPM.mat OR event files, BIDS style: for each regressor (e.g. stim) you need a field stim_onset and stim_duration (when you don't want stick function duration =0)

---
## Part 1 (Optional): run your own GLM (easy trial-wise GLM possible as well!)
This could be helpful for multivariate analysis, estimate collinearity and report the VIF values, especially for very fast designs. 

You can load any hypothetical design in the shape of an event file, as long as your timings are in seconds and relative to the blocks' first TI (so start at ca. 5 seconds). NOTE: that your last timing is not beyond the data you provided. 

**SCRIPT**: MainScriptVIF_runGLM.m
matlab script to run trial-wise beta (without estimating the GLM) 

Please go through (1) and (2) to adjust 18 lines in total (including paths) for this script to work for your design. 

Adding also an .sh file to run on cluster. for that, **change in .sh file** the subject list, location of the main script and your 'work' directory (for the temporary subject specific script). *no need to compile nor have your own spm standalone*

---
##  Part 2:  Compute and plot VIF and collinearity correlation matrices (single sub)
Takes an existing SPM.mat file and computes colinearity correlation matricies and Variance Inflation Factor (VIF) for the regressors and specified contrasts (if there are any).

**SCRIPT**: MainScriptVIF_subjectwise.m
matlab script to run and plot results for each subject 

Please go through (1), (2.1) and (2.2) to adjust 10  lines in total (including paths) for this script to work for your design. 

Adding also an .sh file to run on cluster to run above subjectwise in the cluster, using the cluster's spm standalone as a hack to change a matlab script and run it without needing to compile everything every time.
 *no need to compile nor have your own spm standalone*

---
##  Part 3:  Compute and plot VIF and collinearity correlation matrices (group report)

Please go through first 5 lines to adapt the path

### how to run the group report on tardis: 
- open an interactive node (```srun --pty bash```) and navigate to folder of the group report script.<br/>
- make sure to load correct matlab (```module load matlab/R2017b```)<br/>
- open matlab (```matlab```)<br/>
- run the group report (```MainScriptVIF_groupreport.m```)<br/>


# you are good to go! 
----

#### Notes on the implemented VIF computation:
- at the moment, the VIF ignores blocks and models every onset regressor in the design on it's own. i.e. it doesn't collapse across blocks like the collinearity matrices do (if specified). 
- Since the VIF is a regression, which parameters are included is important. An important decision to be made (and discussed) is if to include the parametric modulators (is their predictability same as for the onset?), and the inclusion of the confounds (which are highly correlated with themselves and might again not indicate something on the ITI\ISI used)
- The VIF plots show vertical lines for 1 (minimum), 2 (harsh curoff) 5 (typical cutoff) and 10 (more than this is no go). 


#### A bit more theory:

### What is VIF? and why is this important?
Because this is the most crucial test for the order of trials in the design (i.e. balanced design between trials) and for finding the correct ISI and ITI (timing within- and between-trials). Specifically, in fmri, each event is convolved with the HRF, meaning it’s a very long event. For example, if the feedback of a trial comes always 500ms after the stimulus, their (hrf-) events would correlate strongly. This is way we jitter the ISI and ITI, but we always ask: "what is the correct timing?" 

There is a way to test in advance how much your regressors would collinear and to optimize both the between-trial design and your within-trial timing (including clear threshold set by experts).
Here is what you need:
[Read this](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0126255) and [Watch this](https://www.youtube.com/watch?v=NJu2lb8uZSg&list=PLB2iAtgpI4YGoXjV-Ar_xuRIxvND9Q3WJ&index=5) .

From the paper: 
> "...the variance inflation factor (VIF) can be used to study collinearity directly. The VIF for a regressor Xi is defined by VIF_i 1/(1-R^2), where R^2 i is the R^2 from the model where the regressor of interest is modeled as a function of all other regressors (Xi = β1X1+...+βi−1Xi−1+βi+1Xi+1+...+βN XN+E) [see Wikipedia](wikipedia.org/wiki/Variance_inflation_factor). If the R^2 value is
high, the VIF will also be high, indicating the regressor of interest is a linear combination of the other regressors and a collinearity problem is present. **Typically a cutoff of 5 or 10 is used as a threshold for problematic collinearity**. This rule of thumb however is criticized by [4](https://link.springer.com/article/10.1007/s11135-006-9018-6) who note that the VIF should be considered in the context of group sizes. In practice, it would be useful to record efficiency as well as VIF for the candidate designs in order to maximize efficiency while guaranteeing a low level of collinearity. In addition, if the study has already been carried out, the VIF can help assess the degree of the collinearity." (Mumford, J. A., Poline, J. B., & Poldrack, R. A. (2015). Orthogonalization of regressors in fMRI models. PloS one)
Note: 1 is the minimum VIF. 

### What are the collinearity correlation matrices? 
- The script computes for each subject the colinearity of contrasts for all onsets. 
- If indicated, this script will collapse the contrasts of the same name across blocks. 
- If there are pre-specified contrasts in the SPM.mat, it will also show the correlation between those. 
- Lastly it produces group average, SEM, and number of subject in which the onset/contrast appeared in (for unbalanced designs)



