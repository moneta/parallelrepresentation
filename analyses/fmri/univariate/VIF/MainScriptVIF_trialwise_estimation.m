GLM.sub = 'sub-%%%SUBJECT%%%'; 
% Diagnose your design matrix! part 1 (optional)
% Run GLM (no contrasts, trialwise option)
% This does not estimate the GLM, only runs the basics to get the convolved design matrix
% All the code was written by Nir Moneta (moneta@mpib-berlin.mpg.de)
%
% The output of this script is an SPM.mat file
% The eventfile need to have BIDS fields: cue_onset, stim_onset,
% outcomt_onset or any other event. %
% Please go through (1), (2.1) and (2.2) to adjust what is needed for your design. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (1) PATHS (actions needed - adjust the paths)
% IMPORTANT: the GLMLocation/subject_name will be cleaned before starting SPM.  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
GLMNAME = 'TRIALWISE';
% location of spm directory
GLM.spmMem = 32; %32 is 4GB.  2^X how much memory to give SPM
% location for GLMs
if strcmp(computer,'PCWIN64'), GLM.homedir = 'X:\SODIVA\SHARE\'; else, GLM.homedir = '/home/mpib/moneta/SODIVA/SHARE/'; end 
DervF = fullfile(GLM.homedir,'BIDS','derivatives');
OutConfounds = fullfile(DervF,'Confounds_univariate'); % has fmriprep and physio together
General_outputGLM = fullfile(DervF,'Univariate','First_lvls');
if ~exist(General_outputGLM,'dir'), mkdir(General_outputGLM); end
Gvif = fullfile(General_outputGLM,'VIF');
if ~exist(Gvif,'dir'), mkdir(Gvif); end
GLMLocation = fullfile(Gvif,GLMNAME);
if ~exist(GLMLocation ,'dir'), mkdir(GLMLocation); end

GLM.EventFileLoc = fullfile(GLM.homedir,'BIDS','bidssource',GLM.sub,'func'); % where are the event files saved
GLM.inputSM = fullfile(DervF,'Data3D','MNI',['smoothed','8']);% where are the subject specific input nifti for the GLM, if needed add the GLM.sub
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (2.1) Set GLM (actions needed)
%  in the regressors structure, one entry per trial event type:
%    - name :    from event files, need to have name_onset field (e.g.
%                cue_onset
%    - split:    which columns in event files to split the onset by its unique values.
%                at the moment only takes one column. e.g. if you have a 'Context'
%                column with unique values of 'color' and 'motion' and you
%                assign 'Context' to the 'cue' name, then there will be one
%                onset for 'cue' where Context=='color' and one for
%                Context=='motion'. All you need to do is say 'Context'. 
%    - duration: if 0, fits a stick function. if 1, needs a column in event
%                file of X_duration (e.g. cue_duration)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

GLM.regressors.names = {'cue','stim','outcome'};
GLM.regressors.split = {'Context','TrialsInBlock','nosplit'};
GLM.regressors.duration = [0,1,0]; % need to have X_duration where X is the name from regressors.names
GLM.Nblock = 4;% how many blocks
GLM.trialsinblock = 108; % how many trials in each block
% location of confound files as well as the filter for SPM 
for rcol = 1:GLM.Nblock
    % For no confounds use this line: 
    GLM.OutConfounds{rcol} = {fullfile(OutConfounds,[GLM.sub,'_','Run-',num2str(rcol),'_AllConfounds.mat'])}; % needs to ne inside a cell, spm stuff...;
    % Filter to find files for the task: this is my smoothed fmriprep output but no need for smoothed if its just for VIF, just dont forget the ^ at the start :) 
    GLM.SPMfilter{rcol} = ['^sm8_MNI_',GLM.sub,'_run-0',num2str(rcol),'_func_vmPFC.nii'];
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (2.2) Overarching GLM parameters (make sure they fit you!)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
GLM.TR  = 1.25; % repetition time (TR) in seconds
GLM.fmri_t    = 16; % microtime resolution for HRF convolution - this is slice number / multiband factor = how many slices in a TR in time (thought 16 is also spm default)
GLM.fmri_t0   = GLM.fmri_t/2; % microtime onset for HRF convolution (half the above in default fmriprep setteings of slice time correction)
% See here for details on microtime: https://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=spm;52ac3e2.1412
GLM.autocorr = 'AR(1)'; % could also be 'FAST', your decision. error serial correlation work-around - AR(1) model based covariance, % component for pre-whitening
%%% All the rest are set to default and can be change below in the code
GLM.ignoreEstimatability = false; % in case you have super good reason to run this on an unestimatable GLM

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START THE SCRIPT - no need for any adjustments anymore 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
spm('defaults','fmri');
spm_jobman('initcfg');
spm_get_defaults('stats.maxmem',2^GLM.spmMem);
spm_get_defaults('stats.resmem', true);
fprintf('SPM will run under %.03f GB limitation of memory \n',2^GLM.spmMem/1000000000)

GLM.Location = fullfile(GLMLocation,GLM.sub); % sub specific locations - will be cleaned beforehand ! 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RUN GLM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
if exist(GLM.Location ,'dir'), rmdir(GLM.Location ,'s'); mkdir(GLM.Location ); 
else, mkdir(GLM.Location );end
fprintf('\n \n \n \n Starting trialwise GLM \n \n \n \n');
%%%% final control over settings (will print it)
GLM

%%% Collect scanes and event files
events = [];
for rcol = 1:GLM.Nblock
    % create SPM style file list for model specification
    run_dir = GLM.inputSM;   
    filt          =  GLM.SPMfilter{rcol};% filename filter
    %f             = spm_select('List',run_dir, filt);
    %fs            = cellstr([repmat([run_dir filesep], size(f,1), 1) f repmat(',1', size(f,1), 1)]);
    f             = spm_select('List',run_dir, filt);
    N   = nifti([run_dir filesep f]);
    dim = [N.dat.dim 1 1 1 1 1];
    n   = dim(4);
    fs={[run_dir filesep f ',', num2str(1)]};
    for s =2:n
        fs=[fs;[run_dir filesep f ',', num2str(s)]];
    end 
    InScans{rcol} = fs;     % allocate data filenames to SPM12 job structure
    fprintf('for block %d found %d scans \n',rcol,length(InScans{rcol}))
    % collect event files for each run
    eventsfile = dir(fullfile(GLM.EventFileLoc,[GLM.sub,'_task-sodiva_rec-prenorm_run-0' num2str(rcol) '_events.tsv'])); % this has 2 options that are the same 
    events{rcol} = readtable(fullfile(eventsfile(1).folder,eventsfile(1).name), 'FileType', 'text', 'Delimiter', 'tab');
    % trials relative to experiment
    GLM.TinBlck = length(events{rcol}.stim_onset);
    events{rcol}.TrialsInBlock = (((rcol-1)*GLM.TinBlck+1):(rcol*GLM.TinBlck))';% for names which trial does the block start?
end 
% job structure initialization
job                             = [];
job.spm.stats.fmri_spec.dir  = {GLM.Location}; % spm folder
% def = spm_get_defaults; to see defaults
BasicCond = [];
BasicCond.name = 0;
BasicCond.onset = 0;
BasicCond.duration = 0;
BasicCond.tmod     = 0;                       
BasicCond.orth     = 0;
BasicCond.pmod = struct('name', {}, 'param', {}, 'poly', {});
for r = 1:GLM.Nblock
    job.spm.stats.fmri_spec.sess(r).scans = InScans{r};
    c = 1;
    for cond = 1:numel(GLM.regressors.names)
        if strcmp(GLM.regressors.split{cond},'nosplit') % one onset for all
           job.spm.stats.fmri_spec.sess(r).cond(c)=BasicCond;
           job.spm.stats.fmri_spec.sess(r).cond(c).name = GLM.regressors.names{cond};
           job.spm.stats.fmri_spec.sess(r).cond(c).onset = events{r}.([GLM.regressors.names{cond},'_onset']);
           if GLM.regressors.duration(cond)~=0, job.spm.stats.fmri_spec.sess(r).cond(c).duration = (events{r}.([GLM.regressors.names{cond},'_end'])(Ind)) - (events{r}.([GLM.regressors.names{cond},'_onset'])(Ind));end           
           c=c+1;
        elseif strcmp(GLM.regressors.split{cond},'TrialsInBlock') % one onset for each trial
            TrialsInBlock = ((r-1)*GLM.trialsinblock+1):(r*GLM.trialsinblock);% 
            for t = TrialsInBlock
               Ind = events{r}.TrialsInBlock==t; % if the trial is in the block
               job.spm.stats.fmri_spec.sess(r).cond(c) = BasicCond;
               job.spm.stats.fmri_spec.sess(r).cond(c).name = strcat(GLM.regressors.names{cond},num2str(t));% add trial to name
               job.spm.stats.fmri_spec.sess(r).cond(c).onset = (events{r}.([GLM.regressors.names{cond},'_onset'])(Ind)); 
               if GLM.regressors.duration(cond)~=0, job.spm.stats.fmri_spec.sess(r).cond(c).duration = (events{r}.([GLM.regressors.names{cond},'_end'])(Ind)) - (events{r}.([GLM.regressors.names{cond},'_onset'])(Ind));end           
               c=c+1;
            end 
        else % split by another column 
            [SPnames,ia1,SPnamesNumeric] = unique(events{r}.(GLM.regressors.split{cond}));
            if ~iscell(SPnames),SPnames= mat2cell(SPnames,ones(1,numel(SPnames)));end % make names in cell if not
            if ~ischar(SPnames),SPnames=cellfun(@(x) num2str(x), SPnames, 'un', 0);end % make names char if not
            for sp2 = 1:numel(SPnames) % go by each value in column and make regressor
               Ind = SPnamesNumeric==sp2;  
               job.spm.stats.fmri_spec.sess(r).cond(c) = BasicCond;
               job.spm.stats.fmri_spec.sess(r).cond(c).name = strcat(GLM.regressors.names{cond},SPnames{sp2});
               job.spm.stats.fmri_spec.sess(r).cond(c).onset = (events{r}.([GLM.regressors.names{cond},'_onset'])(Ind)); 
               if GLM.regressors.duration(cond)~=0, job.spm.stats.fmri_spec.sess(r).cond(c).duration = (events{r}.([GLM.regressors.names{cond},'_duration'])(Ind));end           
               c=c+1;
            end 

        end
    end   % finished regressors of interest
    
if ~isempty(GLM.OutConfounds{r}),  job.spm.stats.fmri_spec.sess(r).multi_reg = GLM.OutConfounds{r}; % No need to add for VIF. its harder to estimate later anyway RegressorsOfNoInt{r}; 
else, job.spm.stats.fmri_spec.sess(r).multi_reg = {''}; end  % No need to add for VIF. its harder to estimate later anyway RegressorsOfNoInt{r}; 
job.spm.stats.fmri_spec.sess(r).multi = {''}; % not sure yet what this is 
job.spm.stats.fmri_spec.sess(r).regress = []; % not sure yet what this is 
job.spm.stats.fmri_spec.sess(r).hpf = 128;   % high-pass filter, spm default
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Overarching GLM settings (set above, rest is default)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
job.spm.stats.fmri_spec.timing.units     = 'secs'; % temporal units
job.spm.stats.fmri_spec.timing.RT        = GLM.TR; % repetition time (TR)
job.spm.stats.fmri_spec.timing.fmri_t    = GLM.fmri_t; % microtime resolution for HRF convolution
job.spm.stats.fmri_spec.timing.fmri_t0   = GLM.fmri_t0 ; % microtime onset for HRF convolution
% See here for details on microtime: https://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=spm;52ac3e2.1412
job.spm.stats.fmri_spec.bases.hrf.derivs = [0 0]; % no modelling of HRF derivatives
job.spm.stats.fmri_spec.volt             = 1; % interaction modeling for factorial designs
job.spm.stats.fmri_spec.global           = 'None'; % global normalization
job.spm.stats.fmri_spec.mthresh          = 0.8; % threshold for masking (not documented)
job.spm.stats.fmri_spec.mask             = {''}; % no mask is {''}
job.spm.stats.fmri_spec.cvi              = GLM.autocorr; % error serial correlation work-around - AR(1) model based covariance, % component for pre-whitening
%job.spm.stats.fmri_spec.cvi             = 'FAST'; % error serial correlation work-around - AR(1) model based covariance, % component for pre-whitening
job.spm.stats.fmri_spec.fact             = struct('name', {}, 'levels', {}); % no factorial design
job = spm_run_fmri_spec(job.spm.stats.fmri_spec);

fprintf('Finished trialswise GLM \n')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% Finished trialwise GLM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

