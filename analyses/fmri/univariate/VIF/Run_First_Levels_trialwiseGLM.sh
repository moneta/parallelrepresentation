#!/bin/bash
# This script takes the basic MainScriptVIF_runGLM.m, copies and changes the %%%%SUBJECT%%% to the actual loaded subject name. 
PATH_SCRIPT="$HOME/SODIVA/Scripts/SHARE/univariate/MainScriptVIF_trialwise_estimation.m"
# change the subject list - need to have only numbers, one in a row (e.g. 01 then 02 then 03)
PATH_SUB_LIST="$HOME/SODIVA/Scripts/sublists/sublist_35subs.txt"
# read subject ids from the list of the text file
SUB_LIST=$(cat ${PATH_SUB_LIST} | tr '\n' ' ')
# or maybe : declare SUB_LIST=[01 02 03]
# add random work folder for temporary scripts to be saved
Work_folder="$HOME/SODIVA/work/GLM/"
# logs for cluster
for sub in ${SUB_LIST}; do
	echo '#!/bin/bash'                                                     > job.slurm
	echo "#SBATCH --job-name MainScriptVIF_${sub}"                         >> job.slurm
    echo "#SBATCH --time 2:0:0"                                           >> job.slurm
	echo "#SBATCH --mem 6GB"                                               >> job.slurm
	echo "#SBATCH --cpus-per-task 1 "                                >> job.slurm
	echo "#SBATCH --output $HOME/logs/GLM/MainScriptVIF_${sub}_OutPut.out" >> job.slurm
	echo "#SBATCH --mail-type NONE"                                        >> job.slurm
	sed "s#%%%SUBJECT%%%#${sub}#" $PATH_SCRIPT > $Work_folder/MainScriptVIF_runGLM${sub}.m
	echo "module load spm12" >> job.slurm
    echo "run_spm12.sh /mcr script $Work_folder/MainScriptVIF_runGLM${sub}.m" >> job.slurm
	sbatch job.slurm
    rm -f job.slurm
done
