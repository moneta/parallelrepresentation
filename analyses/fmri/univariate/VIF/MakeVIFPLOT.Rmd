---
title: "R Notebook"
output: html_notebook
---

Make VIF plot
# VIF
One figure of VIF for univariate GLMs
subplot 1: GLM1-4 are same onset structure
subplot 2: GLM5
subplot 3: GLM6

Also compute the trialwise average, sd, min and max

```{r}

library(ggplot2)
library(fs)
library(gridExtra)

setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
wd = rstudioapi::getActiveDocumentContext()$path
wdt = path_split(wd)
wdt = wdt[[1]][1:(length(wdt[[1]])-3)]
motherfolder = paste(wdt,collapse='/')
FigLocs = paste(motherfolder,'output','fMRI','Univariate',sep='/')
saveDiva <- function(P,Pname,Panel=c(1,1)){
ggsave(filename = paste(Pname,".svg",sep=''),
#ggsave(filename = paste(Pname,".png",sep=''),
       plot = P, path = FigLocs, scale = 1,
       dpi = "retina", width = 5.5*Panel[2], height = 4.5*Panel[1],bg = "transparent")}


#X:\SODIVA\BIDS\derivatives\Univariate\Univariate-GLMs\MainUnivariateAnalysis\VIFs\mval_mval
VIFs1 = data.table::fread('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/mval_mval/Group_VIFS.csv', header = TRUE)
VIFs1$type = 'GLM1-4'
VIFs2 = data.table::fread('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/Condition_Split_mval_evback/Group_VIFS.csv', header = TRUE)
VIFs2$type = 'GLM5'
VIFs3 = data.table::fread('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/Flexi_mvalXcond/Group_VIFS.csv', header = TRUE)
VIFs3$type = 'GLM6'
VIFS=rbind(VIFs1,VIFs2,VIFs3)
VIFS$barcol=.2
VIFS$barcol[startsWith(VIFS$VARIS,'OneD') | startsWith(VIFS$VARIS,'TwoD') | VIFS$VARIS=='Congruent'  | VIFS$VARIS=='Incongruent']=.5
VIFS$order=3
VIFS$order[startsWith(VIFS$VARIS,'Cue')]=1
VIFS$order[startsWith(VIFS$VARIS,'OneD') | startsWith(VIFS$VARIS,'TwoD') | VIFS$VARIS=='Congruent'  | VIFS$VARIS=='Incongruent']=2
VIFS$order[startsWith(VIFS$VARIS,'Outcome')]=4
VIFS$sem = VIFS$sd/sqrt(35)

#X:\SODIVA\BIDS\derivatives\Univariate\Univariate-GLMs\MainUnivariateAnalysis\VIFs\Condition_Split_mval_evback\sub-40\sub-40_VIFs.csv
# individual data 
subs = dir('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/mval_mval/','*')
subs=subs[startsWith(subs,'sub-')]
VIFs1sub = data.frame()
VIFs2sub = data.frame()
VIFs3sub = data.frame()
for (id in subs){
  f = paste('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/mval_mval/',id,'/',id,'_VIFs.csv',sep='')
  VIFs1sub=rbind(VIFs1sub,as.data.frame(data.table::fread(f,header = TRUE)))
  f = paste('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/Condition_Split_mval_evback/',id,'/',id,'_VIFs.csv',sep='')
  VIFs2sub = rbind(VIFs2sub,as.data.frame(data.table::fread(f,header = TRUE)))
  f = paste('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/Flexi_mvalXcond/',id,'/',id,'_VIFs.csv',sep='')
  VIFs3sub = rbind(VIFs3sub,as.data.frame(data.table::fread(f,header = TRUE)))
}
VIFs1sub$type = 'GLM1-4'
VIFs2sub$type = 'GLM5'
VIFs3sub$type = 'GLM6'

VIFSsub=rbind(VIFs1sub,VIFs2sub,VIFs3sub)
vnames = c()
for (v in VIFSsub$VIFnames){vnames=c(vnames,substr(v,7,nchar(v)-6))}
VIFSsub$VARIS=vnames
VIFSsub$barcol=.2
VIFSsub$barcol[startsWith(VIFSsub$VARIS,'OneD') | startsWith(VIFSsub$VARIS,'TwoD') | VIFSsub$VARIS=='Congruent'  | VIFSsub$VARIS=='Incongruent']=.5
VIFSsub$order=3
VIFSsub$order[startsWith(VIFSsub$VARIS,'Cue')]=1
VIFSsub$order[startsWith(VIFSsub$VARIS,'OneD') | startsWith(VIFSsub$VARIS,'TwoD') | VIFSsub$VARIS=='Congruent'  | VIFSsub$VARIS=='Incongruent']=2
VIFSsub$order[startsWith(VIFSsub$VARIS,'Outcome')]=4


#scale_color_manual(values=c("gray80", "gray40", "gray0"))+
# vifPset = function(){coord_cartesian(ylim=c(1,6))+
# theme_Publication(Tsize = 16)+
# xlab('regressors')+ylab(bquote(mu[VIF]))+
# scale_x_discrete(guide = guide_axis(angle = 45))+theme(axis.text.x = element_text(size = 14))+
#   scale_color_manual(values=c("black", "red"))}

# for all plots
theme_Publication <- function(LegendPosition='none',Tsize = 24, base_family="helvetica") { # Tsize = 20
      (theme_bw(base_size = Tsize, base_family=base_family)
       + theme(plot.title = element_text(face = "bold",size = rel(1.2)),
               text = element_text(),
               panel.border = element_rect(colour = NA),
               axis.title = element_text(face = "bold",size = rel(1)),
               axis.title.y = element_text(angle=90,vjust =2),
               axis.title.x = element_text(vjust = -0.2),
               axis.text = element_text(), 
               axis.line = element_line(colour="black"),
               axis.ticks = element_line(),
               panel.grid.major = element_blank(),
               panel.grid.minor = element_blank(),
               legend.key = element_rect(colour = NA),
               legend.position = LegendPosition,
               legend.title = element_text(face="italic"),
               plot.caption = element_text(size = rel(0.6)),
               panel.background = element_rect(fill = "transparent"),#, colour = "transparent"),
               plot.background = element_rect(fill = "transparent", colour = NA),#"transparent"),
               plot.margin=unit(c(2,2,2,2),"mm")))}

VIFSsub$mean = VIFSsub$vifs

VIFplot1 = ggplot(VIFS[VIFS$type=='GLM1-4',],aes(x=reorder(VARIS,order),y=mean,color=as.factor(barcol)))+
geom_bar(stat = "summary", position = "dodge", fun = "mean")+
geom_jitter(data=VIFSsub[VIFSsub$type=='GLM1-4',],width = 0.1)+
geom_errorbar(width=.5, aes(ymin=mean-sem, ymax=mean+sem),size = 0.5, position = position_dodge(width = 0.9))+
geom_hline(yintercept = 5,color='red',linetype='dashed',size=1)+
coord_cartesian(ylim=c(1,6))+
theme_Publication(Tsize = 16)+
xlab('regressors')+ylab(bquote(mu[VIF]))+
scale_x_discrete(guide = guide_axis(angle = 45))+theme(axis.text.x = element_text(size = 14))+
  scale_color_manual(values=c("black", "red"))


VIFplot2 = ggplot(VIFS[VIFS$type=='GLM5',],aes(x=VARIS,y=mean,color=as.factor(barcol)))+
geom_bar(stat = "summary", position = "dodge", fun = "mean")+
geom_jitter(data=VIFSsub[VIFSsub$type=='GLM5',],width = 0.1)+
geom_errorbar(width=.5, aes(ymin=mean-sem, ymax=mean+sem),size = 0.5, position = position_dodge(width = 0.9))+
geom_hline(yintercept = 5,color='red',linetype='dashed',size=1)+
coord_cartesian(ylim=c(1,6))+
theme_Publication(Tsize = 16)+
xlab('regressors')+ylab(bquote(mu[VIF]))+
scale_x_discrete(guide = guide_axis(angle = 45))+theme(axis.text.x = element_text(size = 14))+
  scale_color_manual(values=c("black", "red"))

VIFplot3 = ggplot(VIFS[VIFS$type=='GLM6',],aes(x=VARIS,y=mean,color=as.factor(barcol)))+
geom_bar(stat = "summary", position = "dodge", fun = "mean")+
geom_jitter(data=VIFSsub[VIFSsub$type=='GLM6',],width = 0.1)+
geom_errorbar(width=.5, aes(ymin=mean-sem, ymax=mean+sem),size = 0.5, position = position_dodge(width = 0.9))+
geom_hline(yintercept = 5,color='red',linetype='dashed',size=1)+
coord_cartesian(ylim=c(1,6))+
theme_Publication(Tsize = 16)+
xlab('regressors')+ylab(bquote(mu[VIF]))+
scale_x_discrete(guide = guide_axis(angle = 45))+theme(axis.text.x = element_text(size = 14))+
  scale_color_manual(values=c("black", "red"))

grid.arrange(VIFplot1,VIFplot2,VIFplot3,layout_matrix=rbind(c(1,1,2,2,3,3,3)))

P = arrangeGrob(VIFplot1,VIFplot2,VIFplot3,layout_matrix=rbind(c(1,1,2,2,3,3,3)))
saveDiva(P,'FigS9_VIFS_GLMS',Panel=c(1,3))

# Mark the stimulus VIF somehow
# rearrange the order of the VIF
# add on y axis the numbers
# not sure the SEM is correct

#trialwise
VIFsLSA = data.table::fread('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/SingleTrial/Group_VIFS.csv', header = TRUE)

mean(VIFsLSA$mean)
sd(VIFsLSA$mean)
min(VIFsLSA$mean)
max(VIFsLSA$mean)

#trialwise, cue by context outcome by EV
VIFsLSA = data.table::fread('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/SingleTrial_CueBYCtxt_OutBYvalue/Group_VIFS.csv', header = TRUE)
VIFsLSA=VIFsLSA[grep("Stim", VIFsLSA$VARIS),]
mean(VIFsLSA$mean)
sd(VIFsLSA$mean)
min(VIFsLSA$mean)
max(VIFsLSA$mean)

#trialwise, cue by context, outcome single trial

VIFsLSA = data.table::fread('X:/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/VIFs/SingleTrial_CueBYCtxt_OutFullSplit/Group_VIFS.csv', header = TRUE)
VIFsLSA=VIFsLSA[grep("Stim", VIFsLSA$VARIS),]

mean(VIFsLSA$mean)
sd(VIFsLSA$mean)
min(VIFsLSA$mean)
max(VIFsLSA$mean)

```
