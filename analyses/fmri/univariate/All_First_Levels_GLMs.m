
if ~strcmp(computer,'PCWIN64')
    spm('defaults','fmri');
    spm_jobman('initcfg');
    spm_get_defaults('stats.maxmem',2^34);% 34 with 20GB
    spm_get_defaults('stats.resmem', true);
end

if strcmp(computer,'PCWIN64')
    TRYOUT = true; % make true for the cases of test run to look at the GLMs in a csv
    sub = 'sub-01';
else
    TRYOUT = false;
    sub = 'sub-%%%SUBJECT%%%';
end

%TRYOUT = true;
%%  Representations of context and context-dependent values in vmPFC compete for guiding behavior
%   Nir Moneta, Mona M. Garvert, Hauke R. Heekeren, and Nicolas W. Schuck
%  (C) Nir Moneta, moneta@mpib-berlin.mpg.de
%% DESCRIPTION (can't go to top due to cluster issues)
% This script runs first level GLMs
% all GLMs follow: Cue + OneD + pmods + TwoD + pmods + Outcome 
% + 35 regressors of no interest(include 17 from fmriprep and 18 from physiological parameters)
% all GLMs have wrong and noanswer trials modeled seperately.
% --------------- ROI GLM ------------------
% runs on smoothed 4mm (like multivariate analysis)
% only have pmods for EV (1D and 2D)
% --------------- Classic ------------------
% 'EV_EV' - pmods for EV on both 1D and 2D
% 'EV_EV_Congruency_EVback' - 2D pmods have also congruency and EVBACK
% 'EV_EV_EVbackXCongruency' - 2D pmods have also congruency x EVBACK interaction term 
% 'EV_EVbackXCongruency' - 2D pmods have also congruency x EV interaction term 
% 'nu_mval_nu_mval_cond_evback','nu_mval_nu_mval_evbackSigned' - same as above only have neusiance regressors as well.  
% --------------- Split_Onsets -------------
% 'Congruency_split': split 2D onset between Congruency, pmods for EV and EVBACK 
% 'CongruencyXEV_split' split all onsets 1D and 2D by Congruency and EV. 
MotherGLM = {'ROI_GLM',...% EV1D,EV2D for making ROI
             'Classic','Classic','Classic','Classic',...
             'Classic','Classic',...
             'Split_Onsets','Split_Onsets'};%,...
GLMTypes = {'EV_EV',...
            'EV_EV','EV_EV_Congruency_EVback','EV_EV_EVbackXCongruency','EV_EVxCongruency',...
            'nu_mval_nu_mval_cond_evback','nu_mval_nu_mval_evbackSigned',...            
            'Congruency_split','CongruencyXEV_split'};
         
%% folder structure:
if strcmp(computer,'PCWIN64'), homedir = 'X:\SODIVA\SHARE\'; else, homedir = '/home/mpib/moneta/SODIVA/SHARE/'; end 
DervF = fullfile(homedir,'BIDS','derivatives');
EventFileLoc = fullfile(homedir,'BIDS','bidssource',sub,'func');
OutConfounds = fullfile(DervF,'Confounds_univariate'); % has fmriprep and physio together
General_outputGLM = fullfile(DervF,'Univariate','First_lvls');
if ~exist(General_outputGLM,'dir'), mkdir(General_outputGLM); end
% in case needing to run only some GLMs, give here, e.g. [1,2,5]. otherwise 'All'
RUNonGLMS = 'All';%[4];
% RUNonGLMS = [7:9];
Onlycontrasts = false;
%RunReportResults = true;

%% Start setting GLMs, first informative structure
for i = 1:numel(GLMTypes)
    %% General GLM definitions
    GLM{i}.Input.scanType = 'MNI'; % or 'T1w'
    GLM{i}.Input.smoothed = '8'; % or 4
    if strcmp(MotherGLM{i},'ROI_GLM'), GLM{i}.Input.smoothed = '4'; end 
    GLM{i}.write_residuals = false; 
    % no need because the res image is exactly that
    if strcmp(GLM{i}.Input.scanType,'MNI')
		GLM{i}.Input.space = 'MNI152NLin2009cAsym';%'T1w';% or 'MNI152NLin2009cAsym'
	else
		GLM{i}.Input.space = 'T1w';%'T1w';% or 'MNI152NLin2009cAsym'
    end
    %GLM{i}.Input.mask = 'brain_mask_sub'; % or 'none'
    GLM{i}.Input.imp_threshold = 0.0; % implicit threshol, if giving a mask (i.e. explicit) then no need to give also implicit
    %if strcmp(MotherGLM{i},'RSA_GLM')
    if strcmp(GLM{i}.Input.scanType,'MNI') 
        GLM{i}.Input.mask = fullfile(DervF,'ROIs','MNI_func_vmPFC_group.nii');
    else
        GLM{i}.Input.mask = fullfile(DervF,'ROIs',['T1w_func_vmPFC_',sub,'.nii']);
    end
    %end
    %% Initializing basic condition
    BasicCond = struct('name','na',...
                       'accfield','all',...               % 'accuracy' / 'wrong' / 'noanswer' / 'all'
                       'dimensions','Both',...            % OneD / TwoD / Both
                       'addFields',{{'none'}},...         % in cell additional fields to section for the onset index. OR 'none'
                       'ValueField','none',...            % For spliting onset by values only, otherwise 'none'
                       'ValueFieldBack','none',...
                       'duration',{{'StimDuration'}},...  %  'RT' / 'StimDuration' / '0'
                       'onset_field',{{'Stim'}},...       % which onset to use? 'Stim' / 'Cue' / 'Outcome'
                       'Orth',0);                         % 1- yes, 0 -no
    BasicCond.Pmod{1} = 'none';%{'NAME','FIELD From Events', 'poly number'} OR 'none'
    
    %% conditions of interest (regressors and pmods) and contrasts
    switch MotherGLM{i}
        case {'ROI_GLM','Classic'}
            %------------------------------------------------------------------------------------------
            %-------------------------  C L A S S I C A L      G L M s   ------------------------------
            %------------------------------------------------------------------------------------------
            % OneD Stimuli 
            GLM{i}.Conditions{1} = BasicCond;
            GLM{i}.Conditions{1}.name = 'OneD'; 
            GLM{i}.Conditions{1}.accfield = 'accuracy';
            GLM{i}.Conditions{1}.dimensions = 'OneD';
            % TwoD Stimuli 
            GLM{i}.Conditions{2} = BasicCond;
            GLM{i}.Conditions{2}.name = 'TwoD'; 
            GLM{i}.Conditions{2}.accfield = 'accuracy';
            GLM{i}.Conditions{2}.dimensions = 'TwoD';
            Cind = 3; % next condition will be 3
            % Set pmods: 
            % cell array of 2 cell arrays:
            % { {pmods for OneD} , {pmods for TwoD}}
            switch GLMTypes{i}
                case {'EV_EV'},                       pmods = {{'value_target'},{'value_target'}};
                case {'EV_EV_Congruency_EVback'},     pmods  = {{'value_target'},{'value_target','Condition','EVBACK'}};
                case {'EV_EV_EVbackXCongruency'},     pmods = {{'value_target'},{'value_target','EVBACKSigned'}};
                case {'EV_EVxCongruency'},            pmods = {{'value_target'},{'MVALxCOND'}};
                case {'nu_mval_nu_mval_cond_evback'}, pmods = {{'RightSideCorrect','switch_vector','Color','value_target'},{'RightSideCorrect','switch_vector','Color','value_target','EVBACKSigned'}}; 
                case {'nu_mval_nu_mval_evbackSigned'},pmods = {{'RightSideCorrect','switch_vector','Color','value_target'},{'RightSideCorrect','switch_vector','Color','value_target','EVBACKSigned'}};                                                    
            end 
            % set pmods into GLM structure
            % Structure: GLM{i}.Conditions{1}.Pmod{1} = {['OneD',pmodName],pmodName,1};%{'NAME','FIELD From Events', 'poly number'} OR 'none'  
            names4pmod = {'OneD_','TwoD_'};
            for d = 1:2 % dim 1 ,dim 2
                for p = 1:numel(pmods{d})
                        GLM{i}.Conditions{d}.Pmod{p} = {[names4pmod{d},pmods{d}{p}],pmods{d}{p},1};%{'NAME','FIELD From Events', 'poly number'} OR 'none'  
                end 
            end 
            % CONTRASTS OF INTEREST
            % Complicated code but clear as output at the end
            % Contrasts that start with '-' are set as negative
            % Contrasts that end with '05' are set as 0.5 and not 1 (default)
            PmodStrucure = [numel(pmods{1}),numel(pmods{2})];
            %if strcmp(pmods{1},{'na'}), PmodStrucure(1) = 0; else, PmodStrucure(1) = numel(pmods{1}); end
            %if strcmp(pmods{2},{'na'}), PmodStrucure(2) = 0; else, PmodStrucure(2) = numel(pmods{2}); end
            GLM{i}.contrasts.regressors   = {}; % regressors order 
            GLM{i}.contrasts.regressors = [GLM{i}.contrasts.regressors,{'OneD_onset'}];
            for p = 1:PmodStrucure(1)
                GLM{i}.contrasts.regressors = [GLM{i}.contrasts.regressors,{['OneD_',pmods{1}{p}]}];
            end 
            GLM{i}.contrasts.regressors = [GLM{i}.contrasts.regressors,{'TwoD_onset'}];
            for p = 1:PmodStrucure(2)
                GLM{i}.contrasts.regressors = [GLM{i}.contrasts.regressors,{['TwoD_',pmods{2}{p}]}];
            end 
            % for all of them we want first contrast of each pmod
            % for EV_EV we also take the onset contrast
            GLM{i}.contrasts.names        = {}; % name of contrasts
            GLM{i}.contrasts.contrastsdet = {}; % which regressors to take to each contrast
            for p = GLM{i}.contrasts.regressors
                if ~strcmp(GLMTypes{i},'EV_EV')
                    if strcmp(p{1},'OneD_onset') || strcmp(p{1},'TwoD_onset')
                        continue
                    end 
                end 
                GLM{i}.contrasts.names = [GLM{i}.contrasts.names,p{1}];
                GLM{i}.contrasts.contrastsdet = [GLM{i}.contrasts.contrastsdet,{p}];
            end 
            % now add specific onset contrasts 
            if strcmp(GLMTypes{i},'EV_EV') % add onset diff and EV together
                GLM{i}.contrasts.names = [GLM{i}.contrasts.names,'OneD_onset_Plus_TwoD_onset','OneD_onset_Minus_TwoD_onset'];
                GLM{i}.contrasts.contrastsdet = [GLM{i}.contrasts.contrastsdet,{{'OneD_onset','TwoD_onset'}},{{'OneD_onset','-TwoD_onset'}}];
                
                GLM{i}.contrasts.names = [GLM{i}.contrasts.names,['OneD',pmods{1}{1},'PlusTwoD',pmods{2}{1}],['OneD',pmods{1}{1},'MinusTwoD',pmods{2}{1}]];
                GLM{i}.contrasts.contrastsdet = [GLM{i}.contrasts.contrastsdet,{{['OneD_',pmods{1}{1}],['TwoD_',pmods{2}{1}]}},{{['OneD_',pmods{1}{1}],['-TwoD_',pmods{2}{1}]}}];
            end 
            % add nu joint between 1D and 2D 
            % nu_mval_nu_mval_cond_evback nu_mval_nu_mval_evbackSigned
            if strcmp(GLMTypes{i},'nu_mval_nu_mval_cond_evback') || strcmp(GLMTypes{i},'nu_mval_nu_mval_cond_evback')
                for p = {'RightSideCorrect','switch_vector','Color'}% first 3
                    GLM{i}.contrasts.names = [GLM{i}.contrasts.names,['OneD_',p{1},'_Plus_TwoD_',p{1}]];
                    GLM{i}.contrasts.contrastsdet = [GLM{i}.contrasts.contrastsdet,{{['OneD_',p{1}],['TwoD_',p{1}]}}];
                end 
            end 
        case {'Split_Onsets'}
            switch GLMTypes{i}
                case {'Congruency_split'}    
                    % OneD 
                    GLM{i}.Conditions{1} = BasicCond;
                    GLM{i}.Conditions{1}.name = 'OneD'; 
                    GLM{i}.Conditions{1}.accfield = 'accuracy';
                    GLM{i}.Conditions{1}.dimensions = 'OneD';
                    % TwoD Congruent
                    GLM{i}.Conditions{2} = BasicCond;
                    GLM{i}.Conditions{2}.name = 'Congruent'; 
                    GLM{i}.Conditions{2}.accfield = 'accuracy';
                    GLM{i}.Conditions{2}.dimensions = 'TwoD';
                    GLM{i}.Conditions{2}.addFields = {'Congruent'};
                    % TwoD Incongruent
                    GLM{i}.Conditions{3} = BasicCond;
                    GLM{i}.Conditions{3}.name = 'Incongruent'; 
                    GLM{i}.Conditions{3}.accfield = 'accuracy';
                    GLM{i}.Conditions{3}.dimensions = 'TwoD';
                    GLM{i}.Conditions{3}.addFields = {'Incongruent'};
                    Cind = 4;
                    % pmods
                    pmods = {{'value_target'},{'value_target','EVBACK'}};
                    names4pmod = {'OneD','Congruent','Incongruent'};
                    for d = 1:3 
                        GLM{i}.Conditions{d}.Pmod{1} = {[names4pmod{d},pmods{2}{1}],pmods{2}{1},1};%{'NAME','FIELD From Events', 'poly number'} OR 'none'  
                        if d>1 %not 1D
                        GLM{i}.Conditions{d}.Pmod{2} = {[names4pmod{d},pmods{2}{2}],pmods{2}{2},1};%{'NAME','FIELD From Events', 'poly number'} OR 'none'  
                        end
                    end
                    GLM{i}.contrasts.names        = {'OneDOnset',['OneD',pmods{1}{1}],... %individuals
                                                    'CongOnset',['Cong',pmods{2}{1}],['Cong',pmods{2}{2}],...
                                                    'InconOnset',['Incon',pmods{2}{1}],['Incon',pmods{2}{2}],...                                                       
                                                    ['OneD',pmods{1}{1},'Plus','Cong',pmods{2}{1},'Plus','Incon',pmods{2}{1}],... %EV
                                                    ['Cong',pmods{2}{1},'Plus','Incon',pmods{2}{1}],...
                                                    ['Cong',pmods{2}{1},'Minus','Incon',pmods{2}{1}],...
                                                    ['Cong',pmods{2}{1},'Minus','OneD',pmods{1}{1}],...]
                                                    ['OneD',pmods{1}{1},'Minus','Incon',pmods{2}{1}],...
                                                    ['Cong',pmods{2}{2},'Plus','Incon',pmods{2}{2}],...% EVBACK
                                                    ['Cong',pmods{2}{2},'Minus','Incon',pmods{2}{2}],...
                                                    ['CongOnsetMinInconOnset'],... % onsets
                                                    ['CongOnsetMinOneDOnset'],...
                                                    ['OneDOnsetMinCongOnset'],...
                                                    ['OneDOnsetMinInconOnset']};


                    GLM{i}.contrasts.regressors   =  {'OneDOnset',['OneD',pmods{1}{1}],...
                                                      'CongOnset',['Cong',pmods{2}{1}],['Cong',pmods{2}{2}],...
                                                      'InconOnset',['Incon',pmods{2}{1}],['Incon',pmods{2}{2}]};

                    GLM{i}.contrasts.contrastsdet = {{'OneDOnset'},{['OneD',pmods{1}{1}]},...
                                                     {'CongOnset'},{['Cong',pmods{2}{1}]},{['Cong',pmods{2}{2}]},...
                                                     {'InconOnset'},{['Incon',pmods{2}{1}]},{['Incon',pmods{2}{2}]},...
                                                     {['OneD',pmods{1}{1}],['Cong',pmods{2}{1}],['Incon',pmods{2}{1}]},...
                                                     {['Cong',pmods{2}{1}],['Incon',pmods{2}{1}]},...
                                                     {['Cong',pmods{2}{1}],['-Incon',pmods{2}{1}]},...
                                                     {['Cong',pmods{2}{1}],['-OneD',pmods{2}{1}]},...
                                                     {['OneD',pmods{2}{1}],['-Incon',pmods{2}{1}]},...
                                                     {['Cong',pmods{2}{2}],['Incon',pmods{2}{2}]},...
                                                     {['Cong',pmods{2}{2}],['-Incon',pmods{2}{2}]},...
                                                     {'CongOnset','-InconOnset'},...
                                                     {'CongOnset','-OneDOnset'},...
                                                     {'-CongOnset','OneDOnset'},...
                                                     {'OneDOnset','-InconOnset'}};  
                case {'CongruencyXEV_split'}
                %------------------------------------------------------------------------------------------
                %------------------------------------------------------------------------------------------
                %-------------------------  BASE FOR FLRXIBLE FACTORIAL GLM    ----------------------------
                %------------------------------------------------------------------------------------------
                %------------------------------------------------------------------------------------------
                Values = [30,50,70];
                for c = 1:3
                    % OneD 
                    GLM{i}.Conditions{c} = BasicCond;
                    GLM{i}.Conditions{c}.name = strcat('OneD',num2str(Values(c))); 
                    GLM{i}.Conditions{c}.accfield = 'accuracy';
                    GLM{i}.Conditions{c}.dimensions = 'OneD';
                    GLM{i}.Conditions{c}.ValueField = Values(c);
                    % TwoD Congruent
                    GLM{i}.Conditions{c+3} = BasicCond;
                    GLM{i}.Conditions{c+3}.name = strcat('TwoDCong',num2str(Values(c))); 
                    GLM{i}.Conditions{c+3}.accfield = 'accuracy';
                    GLM{i}.Conditions{c+3}.dimensions = 'TwoD';
                    GLM{i}.Conditions{c+3}.addFields = {'Congruent'};
                    GLM{i}.Conditions{c+3}.ValueField = Values(c);
                    % TwoD Incongruent
                    GLM{i}.Conditions{c+6} = BasicCond;
                    GLM{i}.Conditions{c+6}.name = strcat('TwoDIncong',num2str(Values(c))); 
                    GLM{i}.Conditions{c+6}.accfield = 'accuracy';
                    GLM{i}.Conditions{c+6}.dimensions = 'TwoD';
                    GLM{i}.Conditions{c+6}.addFields = {'Incongruent'};
                    GLM{i}.Conditions{c+6}.ValueField = Values(c);
                end
                Cind = 10;

                GLM{i}.contrasts.names        = {'OneD30','OneD50','OneD70',...
                                                 'Cong30','Cong50','Cong70',...
                                                 'Incong30','Incong50','Incong70',...
                                                 'MainEffectValue',...
                                                 'MainEffectCond',...
                                                 'Interaction',...
                                                 'ConInconVal70',...
                                                 'OneDminCong',...
                                                 'CongminOneD',...
                                                 'OneDminIncong',...
                                                 'IncongminOneD'};

                GLM{i}.contrasts.regressors   = {'OneD30','OneD50','OneD70',...
                                                 'Cong30','Cong50','Cong70',...
                                                 'Incong30','Incong50','Incong70'};

                GLM{i}.contrasts.contrastsdet = {{'OneD30'},{'OneD50'},{'OneD70'},...
                                                 {'Cong30'},{'Cong50'},{'Cong70'},...
                                                 {'Incong30'},{'Incong50'},{'Incong70'},...
                                                 {'-OneD30','-Cong30','-Incong30','OneD70','Cong70','Incong70'},...
                                                 {'Cong30','-Incong30','Cong50','-Incong50','Cong70','-Incong70'},...
                                                 {'-Cong30','Incong30','Cong70','-Incong70'},...
                                                 {'Cong70','-Incong70'},...
                                                 {'OneD30','OneD50','OneD70','-Cong30','-Cong50','-Cong70'},...
                                                 {'-OneD30','-OneD50','-OneD70','Cong30','Cong50','Cong70'},...
                                                 {'OneD30','OneD50','OneD70','-Incong30','-Incong50','-Incong70'},...
                                                 {'-OneD30','-OneD50','-OneD70','Incong30','Incong50','Incong70'}};
            end     	
    end   
    %% conditions of no interest (regressors and pmods)
    % Wrong trials
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'WrongStim'; 
    GLM{i}.Conditions{Cind}.accfield = 'wrong';
    Cind =Cind+1;
    % Noanswe trials
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'NoAnswer'; 
    GLM{i}.Conditions{Cind}.accfield = 'noanswer';
    Cind =Cind+1;

    % Cue
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'CueColor'; 
    GLM{i}.Conditions{Cind}.addFields = {'Color'};
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Cue'};
    Cind =Cind+1;

    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'CueMotion'; 
    GLM{i}.Conditions{Cind}.addFields = {'Motion'};
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Cue'};
    Cind =Cind+1;
    
    % Outocme
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'Outcome_correct'; 
    GLM{i}.Conditions{Cind}.accfield = 'accuracy';
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Outcome'};
    Cind =Cind+1;
    
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'Outcome_wrong'; 
    GLM{i}.Conditions{Cind}.accfield = 'wrong';
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Outcome'};
    Cind =Cind+1;
    
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'Outcome_noanswer'; 
    GLM{i}.Conditions{Cind}.accfield = 'noanswer';
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Outcome'};
end 

%% Run the GLMs
i = 0;
for GLMType = GLMTypes
    %GLMType = GLMTypes(i)
    i = i + 1;
    % Skip GLMs if needed
    if ~strcmp(RUNonGLMS,'All'), if ~ismember(i,RUNonGLMS), continue; end; end 
    
    fprintf('\n \n \n \n Starting GLM %s \n \n \n \n',GLMType{1})
    %% Input for GLM
    % since all GLMs have the same input, we can look for it once. 
    % inputSM = fullfile(DervF,'smoothed',['smoothed_',GLM{i}.Input.scanType],sub);
    inputSM = fullfile(DervF,'Data3D',GLM{i}.Input.scanType,['smoothed',GLM{i}.Input.smoothed]);
    % job structure initialization
    events = [];
    for rcol = 1:4
        % create SPM style file list for model specification
        run_dir = inputSM;   
        if ~TRYOUT && ~Onlycontrasts
            filt          = ['^sm',GLM{i}.Input.smoothed,'_',GLM{i}.Input.scanType,'_',sub,'_run-0',num2str(rcol),'_func_vmPFC.nii']; % filename filter sm4_MNI_sub-01_run-01_func_vmPFC
            f             = spm_select('List',run_dir, filt);
            N   = nifti([run_dir filesep f]);
            dim = [N.dat.dim 1 1 1 1 1];
            n   = dim(4);
            fs={[run_dir filesep f ',', num2str(1)]};
            for s =2:n
                fs=[fs;[run_dir filesep f ',', num2str(s)]];
            end 
            InScans{rcol} = fs;     % allocate data filenames to SPM12 job structure
        else, InScans{rcol} = {''};
        end
        % collect event files for each run 
        eventsfile = dir(fullfile(EventFileLoc,[sub,'_task-sodiva_rec-prenorm_run-0' num2str(rcol) '_events.tsv'])); % this has 2 options that are the same 
        events{rcol} = readtable(fullfile(eventsfile(1).folder,eventsfile(1).name), 'FileType', 'text', 'Delimiter', 'tab');
        events{rcol}.MVALmEVBACK = events{rcol}.value_target - events{rcol}.EVBACK;
        events{rcol}.absMVALmEVBACK = abs(events{rcol}.MVALmEVBACK);
        events{rcol}.MVALpEVBACKSigned = events{rcol}.value_target + events{rcol}.EVBACKSigned;
        events{rcol}.MVALxCOND = events{rcol}.value_target.*events{rcol}.Condition;
        % Regressors of no interest 
        RegressorsOfNoInt{rcol} = {fullfile(OutConfounds,[sub,'_','Run-',num2str(rcol),'_AllConfounds.mat'])}; % needs to ne inside a cell, spm stuff...
    end 
    %% make / clear folders for the subject
    % General Output
    if ~exist(fullfile(General_outputGLM,MotherGLM{i}),'dir'), mkdir(fullfile(General_outputGLM,MotherGLM{i})); end
    
    if ~exist(fullfile(General_outputGLM,MotherGLM{i},GLMType{1}),'dir'), mkdir(fullfile(General_outputGLM,MotherGLM{i},GLMType{1})); end
    %Sub specific output - delete if existing
    outputGLM = fullfile(General_outputGLM,MotherGLM{i},GLMType{1},sub);
    if ~exist(outputGLM,'dir') ; mkdir(outputGLM); end
    % rmdir if not tryout
    if ~TRYOUT && ~Onlycontrasts, if exist(outputGLM,'dir'), rmdir(outputGLM,'s') ; mkdir(outputGLM); else, mkdir(outputGLM); end ; end
    %% Running jobs 
    % job structure initialization
    job                             = [];
    job{1}.spm.stats.fmri_spec.dir  = {outputGLM}; % spm folder
    RegNames = {};
    for r = 1:4
        job{1}.spm.stats.fmri_spec.sess(r).scans = InScans{r};
        ActualC = 1; % keeping track of actual SPM condition in case GLM conditions are empty (e.g. no noanswer trial in a block)
        for c = 1:numel(GLM{i}.Conditions)
            % setting the Ind which is row indicator for trials in the event file
            % Accuracy
            if strcmp(GLM{i}.Conditions{c}.accfield,'all'), Ind = events{r}.stim_onset ~= 0; % all trials
            else,                                           Ind = events{r}.(GLM{i}.Conditions{c}.accfield)==1; end
            % OneD/TwoD/Both
            if ~strcmp(GLM{i}.Conditions{c}.dimensions,'Both'), Ind = Ind & events{r}.(GLM{i}.Conditions{c}.dimensions)==1;end 
            % are there moer fields to add to onset index?
            if ~strcmp(GLM{i}.Conditions{c}.addFields{1},'none') 
               for f = 1:numel(GLM{i}.Conditions{c}.addFields)
                    Ind = Ind & events{r}.(GLM{i}.Conditions{c}.addFields{f})==1;
               end 
            end 
            % in case of different onset per value:
            if ~strcmp(GLM{i}.Conditions{c}.ValueField,'none'), Ind = Ind & (events{r}.value_target==GLM{i}.Conditions{c}.ValueField);end 
            if ~strcmp(GLM{i}.Conditions{c}.ValueFieldBack,'none'), Ind = Ind & (events{r}.EVBACK==GLM{i}.Conditions{c}.ValueFieldBack);end 
            %fprintf('\n Condition %d',c)    
            %events{r}(Ind,:)
            SplitTrials = true; % changes soon
            TrialsInBlock = ((r-1)*108+1):(r*108);% for names which trial does the block start?
            Tcount = 1;                    
            if sum(Ind)~=0 %If there is at least one event, start mapping the GLM to spm structure, e.g. no noanswer trials in a block
                if strcmp(GLM{i}.Conditions{c}.name,'AllStimsGoSplit')
                    job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).name = strcat('Stim',num2str(TrialsInBlock(Tcount-1)));
                else
                    job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).name = GLM{i}.Conditions{c}.name;
                end
                RegNames=[RegNames,['B',num2str(r),'_',job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).name]];
                % condition onsets
                switch GLM{i}.Conditions{c}.onset_field{1}
                    case {'Stim'},    job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).onset = (events{r}.stim_onset(Ind)); 
                    case {'Cue'},     job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).onset = (events{r}.cue_onset(Ind)); 
                    case {'Outcome'}, job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).onset = (events{r}.outcome_onset(Ind)); 
                end 

                switch GLM{i}.Conditions{c}.duration{1}
                    case {'RT'},           job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).duration = (events{r}.response_time(Ind)); 
                    case {'StimDuration'}, job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).duration = (events{r}.StimDuration(Ind));
                    case {'0'},            job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).duration = 0;
                end 

                job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).tmod     = 0;                       % no temporal modulation 
                job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).orth     = GLM{i}.Conditions{c}.Orth;  % orthogonalization of potential parametric regressors (no)

                for p = 1:numel(GLM{i}.Conditions{c}.Pmod)
                    if strcmp(GLM{i}.Conditions{c}.Pmod{p},'none')
                        job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).pmod = struct('name', {}, 'param', {}, 'poly', {}); % no parametric modulation     
                    else
                        PmodValues = (events{r}.(GLM{i}.Conditions{c}.Pmod{p}{2})(Ind));
                        % DEMEANING including zeros of OneD but only the Ind values
                        PmodValues = PmodValues - mean(PmodValues);
                        job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).pmod(p) = struct('name', GLM{i}.Conditions{c}.Pmod{p}{1}, 'param', {PmodValues}, 'poly',{GLM{i}.Conditions{c}.Pmod{p}{3}}); %  parametric modulation
                        RegNames=[RegNames,['B',num2str(r),'_',GLM{i}.Conditions{c}.Pmod{p}{1}]];
                    end
                end % finish pmod
                ActualC = ActualC +1 ; % so in case we dont have a single case, we just jump to the next one and ActualC is not updated to a new value. 
            end % finish actual conditions making
        end % finish predefined conditions
    % run specific regressors:
    job{1}.spm.stats.fmri_spec.sess(r).multi_reg = RegressorsOfNoInt{r}; %{fullfile(outputGLM,['Run',num2str(r),'_MotionReg.mat'])};
    end % finished runs
    % save cons
    % writetable(cell2table(RegNames'),fullfile(outputGLM,'regressornames.csv'))
    %% get the brain mask for the run 
    MaskLoc = GLM{i}.Input.mask;
    %% basic overarching GLM definitions:
    job{1}.spm.stats.fmri_spec.timing.units     = 'secs'; % temporal units
    job{1}.spm.stats.fmri_spec.timing.RT        = 1.25; % repetition time (TR)
    job{1}.spm.stats.fmri_spec.timing.fmri_t    = 16; % microtime resolution for HRF convolution
    job{1}.spm.stats.fmri_spec.timing.fmri_t0   = 8; % microtime onset for HRF convolution
    % See here for details on microtime: https://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=spm;52ac3e2.1412
    job{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0]; % no modelling of HRF derivatives
    job{1}.spm.stats.fmri_spec.volt             = 1; % interaction modeling for factorial designs
    job{1}.spm.stats.fmri_spec.global           = 'None'; % global normalization
    job{1}.spm.stats.fmri_spec.mthresh          = GLM{i}.Input.imp_threshold; % threshold for masking (not documented)
    job{1}.spm.stats.fmri_spec.mask             = {MaskLoc}; % no mask is {''}
    job{1}.spm.stats.fmri_spec.cvi              = 'AR(1)'; % error serial correlation work-around - AR(1) model based covariance, % component for pre-whitening
    %job{1}.spm.stats.fmri_spec.cvi             = 'FAST'; % error serial correlation work-around - AR(1) model based covariance, % component for pre-whitening
    job{1}.spm.stats.fmri_spec.fact             = struct('name', {}, 'levels', {}); % no factorial design

    if ~TRYOUT && ~Onlycontrasts
    save(fullfile(outputGLM,'job_glm_formulation.mat'), 'job');    
    fprintf('Formulating GLM\n')
    spm_jobman('run', job);
    end
    %% (C) GLM Estimation
    % -------------------------------------------------------------------------
    % % initialize job structure
    job                                         = [];
    job{1}.spm.stats.fmri_est.spmmat            = {fullfile(outputGLM,'SPM.mat')};% allocate to job structure
    job{1}.spm.stats.fmri_est.method.Classical  = 1;% set estimation method to classical restricted maximum likelihood
    if GLM{i}.write_residuals
        job{1}.spm.stats.fmri_est.write_residuals = 1; % for RSA we need residuals
    end 
    if ~TRYOUT && ~Onlycontrasts
    fprintf('Estimating  GLM\n') % estimate the GLM
    spm_jobman('run', job);
    end
    %% Evaluation 
    % From SPM:
    % Contrasts  can  be  either  replicated (thus testing average effects over sessions) or created per session.
    % In both cases, zero padding up to the length of each session and the block effects is done automatically.    
    if ~strcmp(GLM{i}.contrasts.names{1},'NoContrasts')
        % Converting contrast strings to spm-defined contrasts.
        for reg2cnt = 1:numel(GLM{i}.contrasts.contrastsdet)
            CONTRASTplus = false(1,numel(GLM{i}.contrasts.regressors));
            CONTRASTminus = false(1,numel(GLM{i}.contrasts.regressors));
            CONTRASThalfPlus =  false(1,numel(GLM{i}.contrasts.regressors));
            CONTRASThalfMinus =  false(1,numel(GLM{i}.contrasts.regressors));
            for betas = 1:numel(GLM{i}.contrasts.contrastsdet{reg2cnt})
                CONTRASTplus = CONTRASTplus | strcmp(GLM{i}.contrasts.regressors,GLM{i}.contrasts.contrastsdet{reg2cnt}{betas});
                CONTRASTminus = CONTRASTminus | strcmp(strcat('-',GLM{i}.contrasts.regressors),GLM{i}.contrasts.contrastsdet{reg2cnt}{betas});
                CONTRASThalfPlus = CONTRASThalfPlus | strcmp(strcat(GLM{i}.contrasts.regressors,'05'),GLM{i}.contrasts.contrastsdet{reg2cnt}{betas});
                CONTRASThalfMinus = CONTRASThalfMinus | strcmp(strcat('-',GLM{i}.contrasts.regressors,'05'),GLM{i}.contrasts.contrastsdet{reg2cnt}{betas});
            end 
            HalfMultiPlus= zeros(1,numel(GLM{i}.contrasts.regressors));
            HalfMultiMinus= zeros(1,numel(GLM{i}.contrasts.regressors));
            HalfMultiPlus(CONTRASThalfPlus) = 0.5;
            HalfMultiMinus(CONTRASThalfMinus) = -0.5;
            GLM{i}.contrasts.vectors{reg2cnt} = double(CONTRASTplus) + double(CONTRASTminus).*(-1) + HalfMultiPlus + HalfMultiMinus;  
            %GLM{i}.contrasts.vectors{reg2cnt} = GLM{i}.contrasts.vectors{reg2cnt}.*HalfMultip;
        end 
        % initialize job structure 
        job                                                    = []; 
        % allocate to job structure
        job{1}.spm.stats{1}.con.spmmat                         = {fullfile(outputGLM,'SPM.mat')};
        % Allocate t-contrast structure
        for cont = 1:numel(GLM{i}.contrasts.names)
            job{1}.spm.stats{1}.con.consess{cont}.tcon.name    = GLM{i}.contrasts.names{cont}; 
            job{1}.spm.stats{1}.con.consess{cont}.tcon.weights = GLM{i}.contrasts.vectors{cont};
            job{1}.spm.stats{1}.con.consess{cont}.tcon.sessrep = 'repl';    
        end 
        % Don't delete existing contrasts
        job{1}.spm.stats{1}.con.delete = 1;   
        % evaluate the GLM
        if ~TRYOUT % from here no correction for only ocntrasts
        fprintf('Evaluating GLM contrasts \n')
        spm_jobman('run', job);    
        end
        
        % Generate results in case of EV_EV GLM (smoothed 8 or 4)
        if (strcmp(GLMTypes{i},'EV_EV') &&  strcmp(MotherGLM{i},'Classic')) || (strcmp(GLMTypes{i},'EV_EV') &&  strcmp(MotherGLM{i},'ROI_GLM'))
            Names = {'OneDTwoDConj'};
            Thresh = 0.001;
            Cons = {[2 4]};
            Conj = {1}; 
            % where to copy the results
            OutFolder_Res =  fullfile(General_outputGLM,'ValueContrasts',['FromFirstLevel_EV_EV_T_00',num2str(Thresh*1000)],MotherGLM{i});
            if ~exist(OutFolder_Res,'dir'), mkdir(OutFolder_Res); end
            % default settings
            jobD = [];
            jobD{1}.spm.stats.results.spmmat = {fullfile(outputGLM, 'SPM.mat')};
            jobD{1}.spm.stats.results.conspec.threshdesc = 'none';
            jobD{1}.spm.stats.results.conspec.thresh = Thresh;
            jobD{1}.spm.stats.results.conspec.mask.none = 1;
            jobD{1}.spm.stats.results.units = 1;
            for c = 1:numel(Names)
                job = jobD;
                K = 0 ;
                NAME = [Names{c},'_',sub,'_T_00',num2str(Thresh*1000),'_K',num2str(K)] ;
                job{1}.spm.stats.results.conspec.titlestr = NAME; 
                job{1}.spm.stats.results.conspec.contrasts = Cons{c};   
                job{1}.spm.stats.results.conspec.conjunction = Conj{c};
                jobD{1}.spm.stats.results.conspec.extent = K;
                % if conjunction so square root of the p value
%                 if Conj{c} ==1,job{1}.spm.stats.results.conspec.thresh = Thresh^0.5;end
                fprintf('GLM contrast results \n')
                if ~TRYOUT
                    [output_list, ~] = spm_jobman('run', job);
                    % this was done to extract the K for the FDRc level at 0.005
                    K = output_list{1, 1}.TabDatvar.ftr{5,2}(4);
                end 
                if K>10^100, fprintf('\n \n K is infint \n \n \n '); % if no K, continue, might change threshold later
                else 
                    job{1}.spm.stats.results.conspec.extent = K;
                    % run again with K 
                    NAME = [Names{c},'_',sub,'_T_00',num2str(Thresh*1000),'_K',num2str(K)] ;
                    job{1}.spm.stats.results.conspec.titlestr = NAME;
                    job{1}.spm.stats.results.export{1}.tspm.basename = [NAME,'_Image'];
                    job{1}.spm.stats.results.export{2}.nary.basename = [NAME,'_clusters'];
                    job{1}.spm.stats.results.export{3}.binary.basename = [NAME,'_binary'];
                    if ~TRYOUT
                        spm_jobman('run', job);
                        if ~Conj{c} ==1
                        copyfile(fullfile(outputGLM,['spmT_000',num2str(Cons{c}),'_',NAME,'_binary.nii']),fullfile(OutFolder_Res,['binary_',NAME,'.nii']));
                        copyfile(fullfile(outputGLM,['spmT_000',num2str(Cons{c}),'_',NAME,'_clusters.nii']),fullfile(OutFolder_Res,['clusters_',NAME,'.nii'])); 
                        else % thats how SPM saves it 
                        copyfile(fullfile(outputGLM,['spmT_000',num2str(2),'_',NAME,'_binary.nii']),fullfile(OutFolder_Res,['binary_',NAME,'.nii']));
                        copyfile(fullfile(outputGLM,['spmT_000',num2str(2),'_',NAME,'_clusters.nii']),fullfile(OutFolder_Res,['clusters_',NAME,'.nii'])); 
                        end 
                    end
                end
            end 
        end 
    end 
    if ~TRYOUT
    SPMs = load(fullfile(outputGLM,'SPM.mat'));
    writetable(cell2table({SPMs.SPM.Vbeta.descrip}'),fullfile(outputGLM,'regressornames.csv'));
    end 
end 

fprintf('\n \n Finished GLM on sub %s \n \n', sub)
close all

counter = 1;
if TRYOUT
    for i = 1:numel(GLM)
       if ~strcmp(RUNonGLMS,'All'), if ~ismember(i,RUNonGLMS), continue; end; end 
       % name:
       fprintf('%s\n',GLMTypes{i})
       % conditions: 
       OutT = struct2table([GLM{i}.Conditions{:}]);
       for c = 1:numel(GLM{i}.Conditions)
           PMODS = [];
           if strcmp(GLM{i}.Conditions{c}.Pmod{1},'none')
                PMODS = ['nothing'];
           else
               for p = 1:numel(GLM{i}.Conditions{c}.Pmod)
                   PMODS = [PMODS,'--',GLM{i}.Conditions{c}.Pmod{p}{2}];
               end 
           end 
           OutT.Pmod{c} = PMODS;
       end 
       writetable(cell2table({GLMTypes{i}}),fullfile(General_outputGLM,'AllGLMs.xlsx'),'Sheet',1,'Range',['A',num2str(counter)]);
       writetable(OutT,fullfile(General_outputGLM,'AllGLMs.xlsx'),'Sheet',1,'Range',['A',num2str(counter+2)]);
       % Contrasts:
       if ~strcmp(GLM{i}.contrasts.names{1},'NoContrasts')
           T1 = table;
           T1.NAME =  GLM{i}.contrasts.names';
           VarN = {};
           for r = 1:numel(GLM{i}.contrasts(:).regressors), VarN = [VarN,[GLM{i}.contrasts.regressors{r}]];end
           T2 = array2table(cell2mat(GLM{i}.contrasts(:).vectors'),'VariableNames',VarN);
           OutT2 = [T1,T2];
           writetable(OutT2,fullfile(General_outputGLM,'AllGLMs.xlsx'),'Sheet',1,'Range',['K',num2str(counter+2)]);
       end 
      counter = counter + max(height(OutT),height(OutT2)) + 5;
    end 
end 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
%     est = spm_SpUtil('IsCon',SPM.xX.xKXs);
%     est = spm_SpUtil('IsCon',SPM.xX.X)
