#!/bin/bash

PATH_SUB_LIST="$HOME/SODIVA/Scripts/sublists/sublist_35subs.txt"
#X:\SODIVA\Scripts\sublists
# read subject ids from the list of the text file
SUB_LIST=$(cat ${PATH_SUB_LIST} | tr '\n' ' ')
# Path for the standalone version of spm
PATH_RUN_SPM="$HOME/tools/spm12"

#SUB_LIST="01"

for sub in ${SUB_LIST}; do
	echo ${sub}
	echo '#!/bin/bash'                    > job.slurm
	echo "#SBATCH --job-name firdtlvls${sub}"  >> job.slurm
	#echo "#SBATCH --partition quick"               >>   job.slurm
    echo "#SBATCH --time 4:0:0"              >> job.slurm
	echo "#SBATCH --mem 24GB"              >> job.slurm
	echo "#SBATCH --cpus-per-task 1 "          >> job.slurm
	echo "#SBATCH --output $HOME/logs/GLM/All_First_Levels_GLMs${sub}_OutPut.out"              >> job.slurm
	echo "#SBATCH --mail-type NONE"              >> job.slurm  
	sed "s#%%%SUBJECT%%%#${sub}#" $HOME/SODIVA/Scripts/SHARE/univariate/All_First_Levels_GLMs.m > $HOME/SODIVA/work/GLM/SAll_First_Levels_GLMs${sub}.m
	echo "$HOME/tools/standalone/run_spm12.sh /opt/matlab/R2017b script $HOME/SODIVA/work/GLM/SAll_First_Levels_GLMs${sub}.m"   >> job.slurm
	sbatch job.slurm
    rm -f job.slurm
done
