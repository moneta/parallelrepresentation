
if ~strcmp(computer,'PCWIN64')
    spm('defaults','fmri');
    spm_jobman('initcfg');
    spm_get_defaults('stats.maxmem',2^34);% 34 with 20GB
    spm_get_defaults('stats.resmem', true);
end

if strcmp(computer,'PCWIN64')
    TRYOUT = false; % make true for the cases of test run to look at the GLMs in a csv
    sub = 'sub-01';
else
    TRYOUT = false;
    sub = 'sub-%%%SUBJECT%%%';
end

%TRYOUT = true;
%%  Representations of context and context-dependent values in vmPFC compete for guiding behavior
%   Nir Moneta, Mona M. Garvert, Hauke R. Heekeren, and Nicolas W. Schuck
%  (C) Nir Moneta, moneta@mpib-berlin.mpg.de
%% DESCRIPTION (can't go to top due to cluster issues)
% This script runs first level GLMs
% all GLMs follow: Cue + OneD + pmods + TwoD + pmods + Outcome 
% + 35 regressors of no interest(include 17 from fmriprep and 18 from physiological parameters)
% all GLMs have wrong and noanswer trials modeled seperately.
% --------------- RSA_GLM ------------------
% GLMs for RSA, smoothed 4 and native space
% 'EV1D_EVxEVBACK2D' - onset for each level of EV 1D and EV x EVBACK for 2D
% ------------------------------------------
MotherGLM = {'RSA_GLM'};%,...
GLMTypes = {'EV1D_EVxEVBACK2D'};
         
%% folder structure:
if strcmp(computer,'PCWIN64'), homedir = 'X:\SODIVA\SHARE\'; else, homedir = '/home/mpib/moneta/SODIVA/SHARE/'; end 
DervF = fullfile(homedir,'BIDS','derivatives');
EventFileLoc = fullfile(homedir,'BIDS','bidssource',sub,'func');
OutConfounds = fullfile(DervF,'Confounds_univariate'); % has fmriprep and physio together
General_outputGLM = fullfile(DervF,'Univariate','First_lvls');
if ~exist(General_outputGLM,'dir'), mkdir(General_outputGLM); end
% in case needing to run only some GLMs, give here, e.g. [1,2,5]. otherwise 'All'
RUNonGLMS = 'All';%[4];
% RUNonGLMS = [7:9];
Onlycontrasts = false;
%RunReportResults = true;

%% Start setting GLMs, first informative structure
for i = 1:numel(GLMTypes)
    %% General GLM definitions
    GLM{i}.Input.scanType = 'subspace'; % or 'MNI'
    GLM{i}.Input.smoothed = '4'; % or 8
    GLM{i}.write_residuals = false; % 
    % no need because the res image is exactly that
    if strcmp(GLM{i}.Input.scanType,'MNI')
		GLM{i}.Input.space = 'MNI152NLin2009cAsym';%'T1w';% or 'MNI152NLin2009cAsym'
	else
		GLM{i}.Input.space = 'T1w';%'T1w';% or 'MNI152NLin2009cAsym'
    end
    %GLM{i}.Input.mask = 'brain_mask_sub'; % or 'none'
    GLM{i}.Input.imp_threshold = 0.0; % implicit threshol, if giving a mask (i.e. explicit) then no need to give also implicit
    if strcmp(GLM{i}.Input.scanType,'MNI') 
        GLM{i}.Input.mask = fullfile(DervF,'ROIs','MNI_func_vmPFC_group.nii');
    else
        GLM{i}.Input.mask = fullfile(DervF,'ROIs',['T1w_func_vmPFC_',sub,'.nii']);
    end
    %% Initializing basic condition
    BasicCond = struct('name','na',...
                       'accfield','all',...               % 'accuracy' / 'wrong' / 'noanswer' / 'all'
                       'dimensions','Both',...            % OneD / TwoD / Both
                       'addFields',{{'none'}},...         % in cell additional fields to section for the onset index. OR 'none'
                       'ValueField','none',...            % For spliting onset by values only, otherwise 'none'
                       'ValueFieldBack','none',...
                       'duration',{{'StimDuration'}},...  %  'RT' / 'StimDuration' / '0'
                       'onset_field',{{'Stim'}},...       % which onset to use? 'Stim' / 'Cue' / 'Outcome'
                       'Orth',0);                         % 1- yes, 0 -no
    BasicCond.Pmod{1} = 'none';%{'NAME','FIELD From Events', 'poly number'} OR 'none'
    
    %% conditions of interest (regressors and pmods) and contrasts
    switch MotherGLM{i}
        case {'RSA_GLM'}
            %------------------------------------------------------------------------------------------
            %--------------------------------------------  RSA ---------------------------------------
            %------------------------------------------------------------------------------------------
            Cind = 1;
            Values = [30,50,70];
            % OneD
            for c = 1:3
                GLM{i}.Conditions{Cind} = BasicCond;
                GLM{i}.Conditions{Cind}.name = strcat('OneD',num2str(Values(c))); 
                GLM{i}.Conditions{Cind}.accfield = 'accuracy';
                GLM{i}.Conditions{Cind}.dimensions = 'OneD';
                GLM{i}.Conditions{Cind}.ValueField = Values(c);
                GLM{i}.Conditions{Cind}.ValueFieldBack = 'none';
                Cind = Cind+1;
            end	
            % TwoD
            for c = 1:3
                % TwoD EV
                for cb = 1:3
                    % TwoD EVBACK
                    GLM{i}.Conditions{Cind} = BasicCond;
                    GLM{i}.Conditions{Cind}.name = strcat('TwoD_EV',num2str(Values(c)),'_EVBACK',num2str(Values(cb))); 
                    GLM{i}.Conditions{Cind}.accfield = 'accuracy';
                    GLM{i}.Conditions{Cind}.dimensions = 'TwoD';
                    GLM{i}.Conditions{Cind}.ValueField = Values(c);
                    GLM{i}.Conditions{Cind}.ValueFieldBack = Values(cb);
                    Cind = Cind+1;
                end % end evback
            end	% end EV 
            % regressors and contrasts 
            GLM{i}.contrasts.names        = {'OneD30','OneD50','OneD70',...
                                             'TwoD30','TwoD50','TwoD70',... %across EVBACK
                                             'TwoD_EVBACK30','TwoD_EVBACK50','TwoD_EVBACK70',... % across EV 
                                             'TwoD_EV30_EVBACK30','TwoD_EV30_EVBACK50','TwoD_EV30_EVBACK70',...% individuals 
                                             'TwoD_EV50_EVBACK30','TwoD_EV50_EVBACK50','TwoD_EV50_EVBACK70',...% individuals 
                                             'TwoD_EV70_EVBACK30','TwoD_EV70_EVBACK50','TwoD_EV70_EVBACK70'};% individuals 
            GLM{i}.contrasts.regressors   = {'OneD30','OneD50','OneD70',...
                                             'TwoD_EV30_EVBACK30','TwoD_EV30_EVBACK50','TwoD_EV30_EVBACK70',...
                                             'TwoD_EV50_EVBACK30','TwoD_EV50_EVBACK50','TwoD_EV50_EVBACK70',...
                                             'TwoD_EV70_EVBACK30','TwoD_EV70_EVBACK50','TwoD_EV70_EVBACK70'};
            GLM{i}.contrasts.contrastsdet = {{'OneD30'},{'OneD50'},{'OneD70'},...
                                            {'TwoD_EV30_EVBACK30','TwoD_EV30_EVBACK50','TwoD_EV30_EVBACK70'},...% across EVBACK
                                            {'TwoD_EV50_EVBACK30','TwoD_EV50_EVBACK50','TwoD_EV50_EVBACK70'},...% across EVBACK
                                            {'TwoD_EV70_EVBACK30','TwoD_EV70_EVBACK50','TwoD_EV70_EVBACK70'}... % across EVBACK
                                            {'TwoD_EV30_EVBACK30','TwoD_EV50_EVBACK30','TwoD_EV70_EVBACK30'},... % across EV
                                            {'TwoD_EV30_EVBACK50','TwoD_EV50_EVBACK50','TwoD_EV70_EVBACK50'},... % across EV
                                            {'TwoD_EV30_EVBACK70','TwoD_EV50_EVBACK70','TwoD_EV70_EVBACK70'}... % across EV
                                            {'TwoD_EV30_EVBACK30'},{'TwoD_EV30_EVBACK50'},{'TwoD_EV30_EVBACK70'},...% individuals 
                                            {'TwoD_EV50_EVBACK30'},{'TwoD_EV50_EVBACK50'},{'TwoD_EV50_EVBACK70'},...% individuals 
                                            {'TwoD_EV70_EVBACK30'},{'TwoD_EV70_EVBACK50'},{'TwoD_EV70_EVBACK70'}};   % individuals 	
    end   
    %% conditions of no interest (regressors and pmods)
    % Wrong trials
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'WrongStim'; 
    GLM{i}.Conditions{Cind}.accfield = 'wrong';
    Cind =Cind+1;
    % Noanswe trials
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'NoAnswer'; 
    GLM{i}.Conditions{Cind}.accfield = 'noanswer';
    Cind =Cind+1;
    
    % Cue
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'CueColor'; 
    GLM{i}.Conditions{Cind}.addFields = {'Color'};
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Cue'};
    Cind =Cind+1;

    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'CueMotion'; 
    GLM{i}.Conditions{Cind}.addFields = {'Motion'};
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Cue'};
    Cind =Cind+1;
    
    % Outocme
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'Outcome_correct'; 
    GLM{i}.Conditions{Cind}.accfield = 'accuracy';
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Outcome'};
    Cind =Cind+1;
    
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'Outcome_wrong'; 
    GLM{i}.Conditions{Cind}.accfield = 'wrong';
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Outcome'};
    Cind =Cind+1;
    
    GLM{i}.Conditions{Cind} = BasicCond;
    GLM{i}.Conditions{Cind}.name = 'Outcome_noanswer'; 
    GLM{i}.Conditions{Cind}.accfield = 'noanswer';
    GLM{i}.Conditions{Cind}.duration = {'0'};
    GLM{i}.Conditions{Cind}.onset_field = {'Outcome'};
end 

%% Run the GLMs
i = 0;
for GLMType = GLMTypes
    i = i + 1;
    % Skip GLMs if needed
    if ~strcmp(RUNonGLMS,'All'), if ~ismember(i,RUNonGLMS), continue; end; end 
    
    fprintf('\n \n \n \n Starting GLM %s \n \n \n \n',GLMType{1})
    %% Input for GLM
    % since all GLMs have the same input, we can look for it once. 
    inputSM = fullfile(DervF,'Data3D',GLM{i}.Input.scanType,['smoothed',GLM{i}.Input.smoothed]);
    % job structure initialization
    events = [];
    for rcol = 1:4
        % create SPM style file list for model specification
        run_dir = inputSM;   
        if ~TRYOUT && ~Onlycontrasts
            filt          = ['^sm',GLM{i}.Input.smoothed,'_',GLM{i}.Input.scanType,'_',sub,'_run-0',num2str(rcol),'_func_vmPFC.nii']; % filename filter sm4_MNI_sub-01_run-01_func_vmPFC
            f             = spm_select('List',run_dir, filt);
            N   = nifti([run_dir filesep f]);
            dim = [N.dat.dim 1 1 1 1 1];
            n   = dim(4);
            fs={[run_dir filesep f ',', num2str(1)]};
            for s =2:n
                fs=[fs;[run_dir filesep f ',', num2str(s)]];
            end 
            InScans{rcol} = fs;     % allocate data filenames to SPM12 job structure
        else, InScans{rcol} = {''};
        end
        % collect event files for each run
        eventsfile = dir(fullfile(EventFileLoc,[sub,'_task-sodiva_rec-prenorm_run-0' num2str(rcol) '_events.tsv'])); % this has 2 options that are the same 
        events{rcol} = readtable(fullfile(eventsfile(1).folder,eventsfile(1).name), 'FileType', 'text', 'Delimiter', 'tab');
        events{rcol}.MVALmEVBACK = events{rcol}.value_target - events{rcol}.EVBACK;
        events{rcol}.absMVALmEVBACK = abs(events{rcol}.MVALmEVBACK);
        events{rcol}.MVALpEVBACKSigned = events{rcol}.value_target + events{rcol}.EVBACKSigned;
        events{rcol}.MVALxCOND = events{rcol}.value_target.*events{rcol}.Condition;
        % Regressors of no interest 
        RegressorsOfNoInt{rcol} = {fullfile(OutConfounds,[sub,'_','Run-',num2str(rcol),'_AllConfounds.mat'])}; % needs to ne inside a cell, spm stuff...
    end 
    %% make / clear folders for the subject
    % General Output
    if ~exist(fullfile(General_outputGLM,MotherGLM{i}),'dir'), mkdir(fullfile(General_outputGLM,MotherGLM{i})); end
    
    if ~exist(fullfile(General_outputGLM,MotherGLM{i},GLMType{1}),'dir'), mkdir(fullfile(General_outputGLM,MotherGLM{i},GLMType{1})); end
    %Sub specific output - delete if existing
    outputGLM = fullfile(General_outputGLM,MotherGLM{i},GLMType{1},sub);
    if ~exist(outputGLM,'dir') ; mkdir(outputGLM); end
    % rmdir if not tryout
    if ~TRYOUT && ~Onlycontrasts, if exist(outputGLM,'dir'), rmdir(outputGLM,'s') ; mkdir(outputGLM); else, mkdir(outputGLM); end ; end
    %% Running jobs 
    % job structure initialization
    job                             = [];
    job{1}.spm.stats.fmri_spec.dir  = {outputGLM}; % spm folder
    RegNames = {};
    for r = 1:4
        job{1}.spm.stats.fmri_spec.sess(r).scans = InScans{r};
        ActualC = 1; % keeping track of actual SPM condition in case GLM conditions are empty (e.g. no noanswer trial in a block)
        for c = 1:numel(GLM{i}.Conditions)
            % setting the Ind which is row indicator for trials in the event file
            % Accuracy
            if strcmp(GLM{i}.Conditions{c}.accfield,'all'), Ind = events{r}.stim_onset ~= 0; % all trials
            else,                                           Ind = events{r}.(GLM{i}.Conditions{c}.accfield)==1; end
            % OneD/TwoD/Both
            if ~strcmp(GLM{i}.Conditions{c}.dimensions,'Both'), Ind = Ind & events{r}.(GLM{i}.Conditions{c}.dimensions)==1;end 
            % are there moer fields to add to onset index?
            if ~strcmp(GLM{i}.Conditions{c}.addFields{1},'none') 
               for f = 1:numel(GLM{i}.Conditions{c}.addFields)
                    Ind = Ind & events{r}.(GLM{i}.Conditions{c}.addFields{f})==1;
               end 
            end 
            % in case of different onset per value:
            if ~strcmp(GLM{i}.Conditions{c}.ValueField,'none'), Ind = Ind & (events{r}.value_target==GLM{i}.Conditions{c}.ValueField);end 
            if ~strcmp(GLM{i}.Conditions{c}.ValueFieldBack,'none'), Ind = Ind & (events{r}.EVBACK==GLM{i}.Conditions{c}.ValueFieldBack);end 
            %fprintf('\n Condition %d',c)    
            %events{r}(Ind,:)
            SplitTrials = true; % changes soon
            TrialsInBlock = ((r-1)*108+1):(r*108);% for names which trial does the block start?
            Tcount = 1;                    
            if sum(Ind)~=0 %If there is at least one event, start mapping the GLM to spm structure, e.g. no noanswer trials in a block
                if strcmp(GLM{i}.Conditions{c}.name,'AllStimsGoSplit')
                    job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).name = strcat('Stim',num2str(TrialsInBlock(Tcount-1)));
                else
                    job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).name = GLM{i}.Conditions{c}.name;
                end
                RegNames=[RegNames,['B',num2str(r),'_',job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).name]];
                % condition onsets
                switch GLM{i}.Conditions{c}.onset_field{1}
                    case {'Stim'},    job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).onset = (events{r}.stim_onset(Ind)); 
                    case {'Cue'},     job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).onset = (events{r}.cue_onset(Ind)); 
                    case {'Outcome'}, job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).onset = (events{r}.outcome_onset(Ind)); 
                end 

                switch GLM{i}.Conditions{c}.duration{1}
                    case {'RT'},           job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).duration = (events{r}.response_time(Ind)); 
                    case {'StimDuration'}, job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).duration = (events{r}.StimDuration(Ind));
                    case {'0'},            job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).duration = 0;
                end 

                job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).tmod     = 0;                       % no temporal modulation 
                job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).orth     = GLM{i}.Conditions{c}.Orth;  % orthogonalization of potential parametric regressors (no)

                for p = 1:numel(GLM{i}.Conditions{c}.Pmod)
                    if strcmp(GLM{i}.Conditions{c}.Pmod{p},'none')
                        job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).pmod = struct('name', {}, 'param', {}, 'poly', {}); % no parametric modulation     
                    else
                        PmodValues = (events{r}.(GLM{i}.Conditions{c}.Pmod{p}{2})(Ind));
                        % DEMEANING including zeros of OneD but only the Ind values
                        PmodValues = PmodValues - mean(PmodValues);
                        job{1}.spm.stats.fmri_spec.sess(r).cond(ActualC).pmod(p) = struct('name', GLM{i}.Conditions{c}.Pmod{p}{1}, 'param', {PmodValues}, 'poly',{GLM{i}.Conditions{c}.Pmod{p}{3}}); %  parametric modulation
                        RegNames=[RegNames,['B',num2str(r),'_',GLM{i}.Conditions{c}.Pmod{p}{1}]];
                    end
                end % finish pmod
                ActualC = ActualC +1 ; % so in case we dont have a single case, we just jump to the next one and ActualC is not updated to a new value. 
            end % finish actual conditions making
        end % finish predefined conditions
    % run specific regressors:
    job{1}.spm.stats.fmri_spec.sess(r).multi_reg = RegressorsOfNoInt{r}; %{fullfile(outputGLM,['Run',num2str(r),'_MotionReg.mat'])};
    end % finished runs
    % save cons
    % writetable(cell2table(RegNames'),fullfile(outputGLM,'regressornames.csv'))
    %% get the brain mask for the run 
    MaskLoc = GLM{i}.Input.mask;
    %% basic overarching GLM definitions:
    job{1}.spm.stats.fmri_spec.timing.units     = 'secs'; % temporal units
    job{1}.spm.stats.fmri_spec.timing.RT        = 1.25; % repetition time (TR)
    job{1}.spm.stats.fmri_spec.timing.fmri_t    = 16; % microtime resolution for HRF convolution
    job{1}.spm.stats.fmri_spec.timing.fmri_t0   = 8; % microtime onset for HRF convolution
    % See here for details on microtime: https://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=spm;52ac3e2.1412
    job{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0]; % no modelling of HRF derivatives
    job{1}.spm.stats.fmri_spec.volt             = 1; % interaction modeling for factorial designs
    job{1}.spm.stats.fmri_spec.global           = 'None'; % global normalization
    job{1}.spm.stats.fmri_spec.mthresh          = GLM{i}.Input.imp_threshold; % threshold for masking (not documented)
    job{1}.spm.stats.fmri_spec.mask             = {MaskLoc}; % no mask is {''}
    job{1}.spm.stats.fmri_spec.cvi              = 'AR(1)'; % error serial correlation work-around - AR(1) model based covariance, % component for pre-whitening
    %job{1}.spm.stats.fmri_spec.cvi             = 'FAST'; % error serial correlation work-around - AR(1) model based covariance, % component for pre-whitening
    job{1}.spm.stats.fmri_spec.fact             = struct('name', {}, 'levels', {}); % no factorial design

    if ~TRYOUT && ~Onlycontrasts
    save(fullfile(outputGLM,'job_glm_formulation.mat'), 'job');    
    fprintf('Formulating GLM\n')
    spm_jobman('run', job);
    end
    %% (C) GLM Estimation
    % -------------------------------------------------------------------------
    % % initialize job structure
    job                                         = [];
    job{1}.spm.stats.fmri_est.spmmat            = {fullfile(outputGLM,'SPM.mat')};% allocate to job structure
    job{1}.spm.stats.fmri_est.method.Classical  = 1;% set estimation method to classical restricted maximum likelihood
    if GLM{i}.write_residuals
        job{1}.spm.stats.fmri_est.write_residuals = 1; % for RSA we need residuals
    end 
    if ~TRYOUT && ~Onlycontrasts
    fprintf('Estimating  GLM\n') % estimate the GLM
    spm_jobman('run', job);
    end
    %% Evaluation 
    % From SPM:
    % Contrasts  can  be  either  replicated (thus testing average effects over sessions) or created per session.
    % In both cases, zero padding up to the length of each session and the block effects is done automatically.    
    if ~strcmp(GLM{i}.contrasts.names{1},'NoContrasts')
        % Converting contrast strings to spm-defined contrasts.
        for reg2cnt = 1:numel(GLM{i}.contrasts.contrastsdet)
            CONTRASTplus = false(1,numel(GLM{i}.contrasts.regressors));
            CONTRASTminus = false(1,numel(GLM{i}.contrasts.regressors));
            CONTRASThalfPlus =  false(1,numel(GLM{i}.contrasts.regressors));
            CONTRASThalfMinus =  false(1,numel(GLM{i}.contrasts.regressors));
            for betas = 1:numel(GLM{i}.contrasts.contrastsdet{reg2cnt})
                CONTRASTplus = CONTRASTplus | strcmp(GLM{i}.contrasts.regressors,GLM{i}.contrasts.contrastsdet{reg2cnt}{betas});
                CONTRASTminus = CONTRASTminus | strcmp(strcat('-',GLM{i}.contrasts.regressors),GLM{i}.contrasts.contrastsdet{reg2cnt}{betas});
                CONTRASThalfPlus = CONTRASThalfPlus | strcmp(strcat(GLM{i}.contrasts.regressors,'05'),GLM{i}.contrasts.contrastsdet{reg2cnt}{betas});
                CONTRASThalfMinus = CONTRASThalfMinus | strcmp(strcat('-',GLM{i}.contrasts.regressors,'05'),GLM{i}.contrasts.contrastsdet{reg2cnt}{betas});
            end 
            HalfMultiPlus= zeros(1,numel(GLM{i}.contrasts.regressors));
            HalfMultiMinus= zeros(1,numel(GLM{i}.contrasts.regressors));
            HalfMultiPlus(CONTRASThalfPlus) = 0.5;
            HalfMultiMinus(CONTRASThalfMinus) = -0.5;
            GLM{i}.contrasts.vectors{reg2cnt} = double(CONTRASTplus) + double(CONTRASTminus).*(-1) + HalfMultiPlus + HalfMultiMinus;  
            %GLM{i}.contrasts.vectors{reg2cnt} = GLM{i}.contrasts.vectors{reg2cnt}.*HalfMultip;
        end 
        % initialize job structure 
        job                                                    = []; 
        % allocate to job structure
        job{1}.spm.stats{1}.con.spmmat                         = {fullfile(outputGLM,'SPM.mat')};
        % Allocate t-contrast structure
        for cont = 1:numel(GLM{i}.contrasts.names)
            job{1}.spm.stats{1}.con.consess{cont}.tcon.name    = GLM{i}.contrasts.names{cont}; 
            job{1}.spm.stats{1}.con.consess{cont}.tcon.weights = GLM{i}.contrasts.vectors{cont};
            job{1}.spm.stats{1}.con.consess{cont}.tcon.sessrep = 'repl';    
        end 
        % Don't delete existing contrasts
        job{1}.spm.stats{1}.con.delete = 1;   
        % evaluate the GLM
        if ~TRYOUT % from here no correction for only ocntrasts
        fprintf('Evaluating GLM contrasts \n')
        spm_jobman('run', job);    
        end
    end 
    if ~TRYOUT
    SPMs = load(fullfile(outputGLM,'SPM.mat'));
    writetable(cell2table({SPMs.SPM.Vbeta.descrip}'),fullfile(outputGLM,'regressornames.csv'));
    end 
end 

%     SPMs = load(fullfile( '/home/mpib/moneta/SODIVA/BIDS/derivatives/Univariate/Univariate-GLMs/MainUnivariateAnalysis/First_lvls/LS-A/trialwisebeta/sub-01','SPM.mat'));

fprintf('\n \n Finished GLM on sub %s \n \n', sub)
close all

counter = 1;
if TRYOUT
    for i = 1:numel(GLM)
       if ~strcmp(RUNonGLMS,'All'), if ~ismember(i,RUNonGLMS), continue; end; end 
       % name:
       fprintf('%s\n',GLMTypes{i})
       % conditions: 
       OutT = struct2table([GLM{i}.Conditions{:}]);
       for c = 1:numel(GLM{i}.Conditions)
           PMODS = [];
           if strcmp(GLM{i}.Conditions{c}.Pmod{1},'none')
                PMODS = ['nothing'];
           else
               for p = 1:numel(GLM{i}.Conditions{c}.Pmod)
                   PMODS = [PMODS,'--',GLM{i}.Conditions{c}.Pmod{p}{2}];
               end 
           end 
           OutT.Pmod{c} = PMODS;
       end 
       writetable(cell2table({GLMTypes{i}}),fullfile(General_outputGLM,'AllGLMs.xlsx'),'Sheet',1,'Range',['A',num2str(counter)]);
       writetable(OutT,fullfile(General_outputGLM,'AllGLMs.xlsx'),'Sheet',1,'Range',['A',num2str(counter+2)]);
       % Contrasts:
       if ~strcmp(GLM{i}.contrasts.names{1},'NoContrasts')
           T1 = table;
           T1.NAME =  GLM{i}.contrasts.names';
           VarN = {};
           for r = 1:numel(GLM{i}.contrasts(:).regressors), VarN = [VarN,[GLM{i}.contrasts.regressors{r}]];end
           T2 = array2table(cell2mat(GLM{i}.contrasts(:).vectors'),'VariableNames',VarN);
           OutT2 = [T1,T2];
           writetable(OutT2,fullfile(General_outputGLM,'AllGLMs.xlsx'),'Sheet',1,'Range',['K',num2str(counter+2)]);
       end 
      counter = counter + max(height(OutT),height(OutT2)) + 5;
    end 
end 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
%     est = spm_SpUtil('IsCon',SPM.xX.xKXs);
%     est = spm_SpUtil('IsCon',SPM.xX.X)
