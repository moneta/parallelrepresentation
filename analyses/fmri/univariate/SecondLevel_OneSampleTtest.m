% SecondLevel_runOnCluster
% This function implements a second-level analysis
% Its meant to open on a cluster node and run
% Running four types of second level GLMs:
% This script runs different GLMs
% For each one it takes the basic contrast and run for each one CON>0
clear all
% Initialization of SPM12 and spm_jobman
% -------------------------------------------------------------------------
%addpath(genpath('G:\My Drive\Matlab\toolboxes\spm12'))
spm('defaults','fmri');
spm_jobman('initcfg');

% show spm default memory
spm_get_defaults('stats.maxmem',2^32)
spm_get_defaults('stats.resmem', true)

CHOSENGLM = str2double('%%%GLM%%%');
% Data location specification 
% -------------------------------------------------------------------------
% data source directory
if strcmp(computer,'PCWIN64'), homedir = 'X:\SODIVA\SHARE\'; else, homedir = '/home/mpib/moneta/SODIVA/SHARE/'; end 
DervF = fullfile(homedir,'BIDS','derivatives');
General_inputGLM = fullfile(DervF,'Univariate','First_lvls');
General_outputGLM =fullfile(DervF,'Univariate','Second_lvls');
if ~exist(General_outputGLM,'dir'), mkdir(General_outputGLM); end

%% simple t test
SimpleTtest_outputGLM = fullfile(General_outputGLM,'Simple_t_test');
if ~exist(SimpleTtest_outputGLM,'dir'), mkdir(SimpleTtest_outputGLM); end


MotherGLM = {'ROI_GLM',...% EV1D,EV2D for making ROI
             'Classic','Classic','Classic','Classic',...
             'Classic','Classic',...
             'Split_Onsets','Split_Onsets'};%,...
GLMTypes = {'EV_EV',...
            'EV_EV','EV_EV_Congruency_EVback','EV_EV_EVbackXCongruency','EV_EVxCongruency',...
            'nu_mval_nu_mval_cond_evback','nu_mval_nu_mval_evbackSigned',...            
            'Congruency_split','CongruencyXEV_split'};

%ShouldROI = false; 
ShouldROI = true; 

InputGLMTypes = {};  
OutputGLMTypes = {};
ConNames = {};
%for i = 1:numel(GLMTypes)
i = CHOSENGLM;
InputGLMTypes{i} = fullfile(General_inputGLM,MotherGLM{i},GLMTypes{i});
% load the SPM contrast names 
SPMmat = load(fullfile(InputGLMTypes{i},'sub-01','SPM.mat'));
ConNames{i} = {SPMmat.SPM.xCon.name};
for c = 1:numel(ConNames{i})
    ConNames{i}{c}=ConNames{i}{c}(1:(length(ConNames{i}{c})-15));
end 
if ~ShouldROI
    OutputGLMTypes{i} = fullfile(SimpleTtest_outputGLM,MotherGLM{i},GLMTypes{i}); 
else
    OutputGLMTypes{i} = fullfile(SimpleTtest_outputGLM,[MotherGLM{i},'_roi'],GLMTypes{i}); 
end
%% Set directories and pmods
if ~exist(OutputGLMTypes{i},'dir'), mkdir(OutputGLMTypes{i}); end
fprintf('Starting GLM %s',GLMTypes{i}); 
SubF = dir(fullfile(InputGLMTypes{i},'sub-*'));
fprintf('\n number of subjects %d \n',numel(SubF))   
if numel(SubF)~=35
    quit(1, "force"); 
end 
%% One sample t test for each subject group - cycle over contrasts
% -------------------------------------------------------------------------
connames = ConNames{i};
for con = 1:numel(connames)
    %% GLM formulaation
    fNames = [];
    for s = 1:numel(SubF) % hack to get number of subjects
        sub_dir = fullfile(SubF(s).folder,SubF(s).name);
        if con < 10, filt =[['con_000' num2str(con)] '*\.nii$'];else, filt= [['con_00'  num2str(con)] '*\.nii$'];end
        sub_glm_dir = sub_dir;
        f           = spm_select('List', sub_glm_dir, filt);
        fs          = cellstr([repmat([sub_glm_dir filesep], 1, 1) f repmat(',1', 1, 1)]);
        fNames      = [fNames; fs];
    end 
    % create a subdirectory for each second level analysis
    con_dir = fullfile(OutputGLMTypes{i},['CON_0' num2str(con) '_' connames{con}]);       
    if exist(con_dir, 'dir'), rmdir(con_dir,'s'); mkdir(con_dir); else,mkdir(con_dir);end   
    % Formulate one sample t-test GLM
    % ---------------------------------------------------------------------
    job                                                       = [];
    job{1}.spm.stats.factorial_design.dir                     = {con_dir};
    job{1}.spm.stats.factorial_design.des.t1.scans            = cellstr(fNames);
    job{1}.spm.stats.factorial_design.cov                     = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    job{1}.spm.stats.factorial_design.multi_cov               = struct('files', {}, 'iCFI', {}, 'iCC', {});
    job{1}.spm.stats.factorial_design.masking.tm.tm_none      = 1;
    job{1}.spm.stats.factorial_design.masking.im              = 1;
    if  ShouldROI==true
        job{1}.spm.stats.factorial_design.masking.em = fullfile(DervF,'ROIs','MNI_func_vmPFC_group.nii');
        %job{1}.spm.stats.factorial_design.masking.em = {fullfile(homedir,'SODIVA','BIDS','derivatives','ROIS','MNI','spmT_0001_Pos_OneDvalue_targetPlusTwoDvalue_target_T0005_K21_clusters','GroupROI_Big_vmPFC_spmT_0001_Pos_OneDvalue_targetPlusTwoDvalue_target_T0005_K21_clusters.nii')};
    else 
        job{1}.spm.stats.factorial_design.masking.em  = {''};
    end 
    job{1}.spm.stats.factorial_design.globalc.g_omit          = 1;
    job{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no  = 1;
    job{1}.spm.stats.factorial_design.globalm.glonorm         = 1;

    % run the job
    fprintf('GLM Formulation \n');
    spm_jobman('run', job)

    %% GLM Estimation 
    % ---------------------------------------------------------------------
    job                             = [];
    job{1}.stats{1}.fmri_est.spmmat = {fullfile(con_dir, 'SPM.mat')}; 
    % matlabbatch{1}.spm.stats.fmri_est.write_residuals = 0;
    % matlabbatch{1}.spm.stats.fmri_est.method.Classical = 1;
    % Run the job
    fprintf('GLM Estimation \n');
    spm_jobman('run', job);
    %% GLM Evaluation (for +1 and -1 contrasts)
    % ---------------------------------------------------------------------
    conDirection = [1,-1];
    conDirectionName = {'Pos','Neg'};

    job = [];
    % contrast name
    tconnames = {strcat('Pos_',connames{con}),strcat('Neg_',connames{con})};
    % contrast vector
    tconvecs  = {1,-1};
    % Allocate SPM.mat file
    job{1}.stats{1}.con.spmmat = {fullfile(con_dir, 'SPM.mat')}; 
    % cycle over contrast specifications
    for c = 1:numel(tconnames)
        % Allocate t-contrast structure
        job{1}.stats{1}.con.consess{c}.tcon.name    = tconnames{c};       
        job{1}.stats{1}.con.consess{c}.tcon.convec  = tconvecs{c};         
        job{1}.stats{1}.con.consess{c}.tcon.sessrep = 'none';             
        % Delete existing contrasts
        job{1}.stats{1}.con.delete = 1;   
    end 
    % Run the job
    fprintf('GLM Evaluation \n')
    spm_jobman('run', job);
    % evaluate contrast
    % ---------------------------------------------------------------------
    for c = 1:numel(tconnames)
        % First dummy to get K
        if strcmp(connames{con},'OneDvalue_targetPlusTwoDvalue_target') &&  strcmp(MotherGLM{i},'Smoothed_4_ROI') % special case for pmods
            Thresh = 0.0005;
            Tstr = '0005';
        else
            Thresh = 0.001;
            Tstr = '001';
        end
        K = 0;
        job                                             = [];                   % initialize job structure
        job{1}.spm.stats.results.spmmat                 = {fullfile(con_dir,'SPM.mat')}; % SPM filename
        job{1}.spm.stats.results.conspec.titlestr       = [tconnames{c},'_T',Tstr,'_K',num2str(K)];%'OneDpTwoD_MVAL_T0005_K21';  % contrast report title
        job{1}.spm.stats.results.conspec.contrasts      = c; % Context index
        job{1}.spm.stats.results.conspec.threshdesc     = 'none';               % no FWE control for threshold
        job{1}.spm.stats.results.conspec.thresh         = Thresh;                % p <0.001
        job{1}.spm.stats.results.conspec.extent         = K;                    % no extent threshold (number of voxels)
        job{1}.spm.stats.results.conspec.conjunction    = 1;                    % if simple contrast: not used. if more than one contrast then 1 is conjunction as null (AND statement) and 0 is global null (OR statement). 
        job{1}.spm.stats.results.conspec.mask.none      = 1;                    % ? some SPM masking
        job{1}.spm.stats.results.units                  = 1;                    % what?
        [output_list, ~] = spm_jobman('run', job);
        % this was done to extract the K for the FDRc level at 0.001
        K = output_list{1, 1}.TabDatvar.ftr{5,2}(4);
        if K==inf
            job{1}.spm.stats.results.conspec.thresh = 0.005; 
            fprintf('\n K is infint trying at 0.005 \n'); 
            Tstr = '005';
            [output_list, ~] = spm_jobman('run', job);
            K = output_list{1, 1}.TabDatvar.ftr{5,2}(4);
        end
        if K~=inf %K is not inft.
            job{1}.spm.stats.results.conspec.extent = K;
            job{1}.spm.stats.results.conspec.titlestr       = [tconnames{c},'_T',Tstr,'_K',num2str(K)];%'OneDpTwoD_MVAL_T0005_K21';  % contrast report title
            job{1}.spm.stats.results.export{1}.tspm.basename = [tconnames{c},'_T',Tstr,'_K',num2str(K),'_Image'];
            job{1}.spm.stats.results.export{2}.nary.basename = [tconnames{c},'_T',Tstr,'_K',num2str(K),'_clusters'];
            job{1}.spm.stats.results.export{3}.binary.basename =[tconnames{c},'_T',Tstr,'_K',num2str(K),'_binary'];
            % evaluate the GLM with the corerct K value
            fprintf('Evaluating GLM contrast result \n %s \n',tconnames{c})
            spm_jobman('run', job);
        else 
            fprintf('\n K was still inf, cant evaluate \n');
        end
    end 

end

fprintf('\n \n FINISHED GLM Simple T test %s \n \n ',OutputGLMTypes{i});

%end

fprintf('\n \n FINISHED GLMs \n \n ');




