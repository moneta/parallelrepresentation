# Scripts & Preprocessed data for the analysis of Representations of context and context-dependent values in vmPFC compete for guiding behavior. Moneta Garvert Heekeren & Schuck , Narute Communications (2023)

# License 
Script and data are licensed under: Creative Commons Attribution-ShareAlike 4.0 International Public License (see LICENSE file)

# Guide for fMRI analysis scripts:
## Multivariate
### decoding pipline:
#### (1) multivariate preprocessing
First we need to run *Multivariate_Preprocessing.py* which takes the native space ROI data and:
(a) detrends, highpass filter 128 and regress confounds out
(b) takes values that are more then 8 sd from the mean and pushes them towards the mean
(c) zscore each voxel across time

#### (2) decoding
Then, the main script *SODIVAME.py* uses all the rest of the scripts in the folder to run the decoding of EV, Context and EVback. 
Inside *basic_functions.py* is the basic definition of the classification in a dictionary and inside *ClassificationFunctions.py* is the class to run the classifier and additional needed functions.

#### (3) classifier probabilities analyses (main analysis)
Here we take the classifiers' assigned probabilities to the correct classes (EV, EVBACK and Context classifiers) and run mixed effect beta regressions on them. We also use the behavioral data to link decodability to behavior.
For full details see **analyses\fmri\multivariate\main_analyses\main_fmri_analyses_guide.md**.

#### RSA_analysis
#### (1) multivariate preprocessing and RSA  generation
*GetBetas_RSA.py* is used to extracts the beta images from the univariate RSA-GLMs, compute the Euclidean distances and divide by the residuals of the GLM (noise cleaning)

#### (2) RSA main analyses 
Here we take the extracted RSAs for the participants and run mixed effect gamma regression models on the results. 
For full details see **analyses\fmri\multivariate\main_analyses\main_fmri_analyses_guide.md**.

# Univariate
## GLMs
Tried my best to comment throughout the univariate scripts. Since this is not a main part of the paper there is no detailed README but if you have any question you are wellcome to contact Nir Moneta. 

## VIF calculations (CheckYourDesign)
Scripts to test for Variance Inflation Factor (VIF) used in this manuscript. See relevant README (CheckDesign_README.md).
These require however the SPM files of GLMs. There are newer versions written in python (not used in this manuscript) to calculate VIF and colinearity also BEFORE having actual data. If you are interested in such tests please contact Nir Moneta. 