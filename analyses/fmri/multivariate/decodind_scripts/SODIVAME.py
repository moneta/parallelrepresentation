# -*- coding: utf-8 -*-
"""
Created on Sun May 24 10:14:24 2020

@author: User
"""

'''
##############################################################################
################        Set initial variables                #################
##############################################################################
'''
import os
from os.path import join as opj
import sys
import pandas as pd
import numpy as np

""" add script to path to load functions """
#if 'win32' in sys.platform: sys.path.append(opj('X:\\','SODIVA','Scripts','Multivariate','Pyscripts','simple2'))
#else:                       sys.path.append(opj(os.environ['HOME'],'SODIVA','Scripts','Multivariate','Pyscripts','simple2'))
#if 'win32' in sys.platform: sys.path.append(opj('G:\\','My Drive','Git_SODIVA_scripts','Analyses','fmri','multivariate','decodind_scripts'))
if 'win32' in sys.platform: sys.path.append(opj('X:\\','SODIVA','Scripts','SHARE','decodind_scripts'))
else:                       sys.path.append(opj(os.environ['HOME'],'SODIVA','Scripts','SHARE','decodind_scripts'))

import basic_functions
import ClassificationFuncitons 

if 'win32' in sys.platform: 
    sub = 'sub-01' #sub = 'sub-14' #sub = 'sub-01' 
    dtype = 'TR'
else:                       
    sub = 'sub-%s' % sys.argv[1]  
    dtype = 'TR'

if 'win32' in sys.platform: path_root = opj('X:\\','SODIVA','SHARE','BIDS')
else:                       path_root = opj(os.environ['HOME'],'SODIVA','SHARE','BIDS')    

def saveoutputs(OutTables,PredTables,SWDFs,Inputs,SubDict):
    basic_functions.MakeIfNotThere(opj(SubDict['paths']['output']))
    for i in np.unique(Inputs):
        # get indecies that have same input
        ins = [a for a, j in enumerate(Inputs) if j == i]
        OutF = opj(SubDict['paths']['output'],i)
        basic_functions.MakeIfNotThere(OutF)
        pd.concat([outT for i,outT in enumerate(OutTables) if i in ins], axis=0, ignore_index=True,sort=False).to_csv(opj(OutF,i+'_'+ID.SubDict['subject']+'_EvaluatedResults_LogReg.csv'),sep=',',index=False)
        pd.concat([predT for i,predT in enumerate(PredTables) if i in ins], axis=0, ignore_index=True,sort=False).to_csv(opj(OutF,i+'_'+ID.SubDict['subject']+'_PredictionResults_LogReg.csv'),sep=',',index=False)
        pd.concat([swdfT for i,swdfT in enumerate(SWDFs) if i in ins], axis=0, ignore_index=True,sort=False).to_csv(opj(OutF,i+'_'+ID.SubDict['subject']+'_SWDFs_LogReg.csv'),sep=',',index=False)
        print('Saved Output at: \n%s\n Called: \n%s\n' % (OutF,i+'_'+SubDict['subject']+'***.csv'))

def MakeItHappen(ID):
    '''
    run decoding
    '''
    print('Starting decoding')
    OutTables,SWDFs,sub_events,PredTables,Inputs = ClassificationFuncitons.RunDecoding(ID)
    if OutTables!=None: # if ID only has preprocessing it will be None
        saveoutputs(OutTables,PredTables,SWDFs,Inputs,ID.SubDict)   




# =============================================================================
# 
# 
# RunDecoding = True
# #### preprocessing
# ID = basic_functions.Inputmaker(sub,path_root)
# print('starting DECODING')
# #### Decoding EV
# ID.updateDict('name','LogReg_1DEV')
# ID.updateDict('Train=Test',True)
# ID.updateDict('Run_dec',RunDecoding)
# ID.updateDict('label',['value_target'])
# ID.updateDict('SampleWeights',['value_target','Context','SIDE'])
# ID.updateDict('UpasmpleBy',['value_target','Context','SIDE'])
# ID.finalizeInputDict()
# =============================================================================






RunDecoding = True
#### preprocessing
ID = basic_functions.Inputmaker(sub,path_root)
print('starting DECODING')
#### Decoding EV
ID.updateDict('name','LogReg_1DEV')
#ID.updateDict('dtype','TR')
ID.updateDict('Run_dec',RunDecoding)
ID.updateDict('label',['value_target'])
ID.updateDict('SampleWeights',['value_target','Context','SIDE'])
ID.updateDict('UpasmpleBy',['value_target','Context','SIDE'])
ID.finalizeInputDict()

#### Decoding Context
ID.updateDict('name','LogReg_1DContext')
#ID.updateDict('dtype','TR')
ID.updateDict('Run_dec',RunDecoding)
ID.updateDict('label',['Context'])
ID.updateDict('SampleWeights',['value_target','Context','SIDE'])
ID.updateDict('UpasmpleBy',['value_target','Context','SIDE'])
ID.finalizeInputDict()

#### Decoding EVBACK 3 classifiers
ID.updateDict('name','LogReg_2DEVBACK')
#ID.updateDict('dtype','TR')
ID.updateDict('Run_dec',RunDecoding)
ID.updateDict('label',['EVBACK_Two_rest','EVBACK_Three_rest','EVBACK_Four_rest'])
ID.updateDict('UpasmpleBy',['value_target','EVBACK','Context']) # SIDE doesnt work
ID.updateDict('Trainingsplit',['TwoD'])
ID.updateDict('SampleWeights',['label']) # SIDE cant work, not enough
ID.updateDict('multi_class','multinomial')
ID.finalizeInputDict()
'''run all! '''
ID.printme()
MakeItHappen(ID) 