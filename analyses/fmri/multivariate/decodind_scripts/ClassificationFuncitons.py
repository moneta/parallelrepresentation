# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 16:17:24 2020

@author: User

This library contains two parts: 
    (A) Classifier: class to be used for classification
    (B) DoEverything: function to run the pipline of the clasisification

Detailed description:
(A) Classifier
    Class with different methods.
    Input: 
        
    Methods:
        -__init__
            initializes basic output structures
            important: 
                outtable: a table of copy of all events, basic structure for each classification iteration
                FinalOutput, EvaluateResults are used to log each iteration of outtable (i.e. each classification analysis)
                
        -init_loadevents
            loads events. runs once. 
            
        -init_outtable
            makes a copy of events at the begining of each classification iteration
            
        -split_traintest:
            splits the events to trainign and testing:
                Either based on oneD only (TrainOneD = True) or both oneD and TwoD (TrainOneD = False)
                    This generates 4 folds: 
                        e.g. train on blocks 2-4 and test on 1....
                Either splits by context (Train on one and test on other) or not (SplitCtxt = True/False)
                    Crucially, splitting by context generates 8 folds:
                        e.g. train on color blocks 2-4 test on motion block 1,
                             train on motion blocks 2-4 test on color block 1....
                             
        -MakeSampleWeights
            Compute a weight to each sample of the classifier:
                takes the unique levels of the (string) combination of columns of the event file (input: Factors, usually: [OneD,Context])
                and assigns weights to make sure each level is represented in the same way. 
                This makes sure that when having 4 'color-mval-2' and 5 'color-mval-2' trials, 
                each 'unit' is equally represented (so no context bias) and each value (which would be the class) is build from 2 'units' - so also equally represented. 
                
        -PrintSampleWeights
            Control to print out the sample weights to make sure they make sense. 
        
        -ZscoreMe
            Zscores the data (input) based on the fields corresponding to columns in event-table (input 'Factors')
            This also logs the data type (TR/Beta) and ROI name since its the first usage and all following steps will use the same information until this method is called again. 
        
        -SimulateData
            Simulates data based on the event file. mainly to control the SVRme and LogRegMe methods work well. 
            
        -SVRme
            runs SVR analysis across the training and testing folds. 
            Output:
                General score: either the build in SVR-Score or a simple correlation (for now nico said stay with correlation)
                trial-wise: the predicted value from the regression
            
        -LogRegMe
            runs Logistic regression analysis across the training and testing folds. 
            Output:
                General score: how often the classifier predicted with highest probability the correct label
                trial-wise: predicted probability for each one of the labels.
                
        -LogOutput
            Log an iteration of the classification analysis
        -SaveOutput
            Save the output at the end of all iterations
        -Evaluate
            Plot and save evaluation of all analyses across iterations
            
(B) DoEverything
    Main function to run analysis. 
    
    General workfolow:
        (1) Load the data 
            Data is already in 2D tables samples X features as Nilearn likes it :) 
        (2) Initialize the classifier class and loop through analysis options
            LOOP through trainig split options:
                split train/test
                make sample weights
                
                LOOP through the different datas (i.e. different ROIs)
                    LOOP through the Z scoring options
                        Zscore the data
                        (Simulate if indicated)
                        Run logistic regression
                        Log outtable in Finalresults list and intitialize a new outtable
                        Loop through Cparameters of SVR
                            Run SVR
                            Log outtable in Finalresults list and intitialize a new outtable
                After finishing all possible iteration in one data, save it
            After finishing all ROIs evaluate the results to see which analysis structure works best across ROIs
"""
import os
from os.path import join as opj
import glob
import pandas as pd
import numpy as np
import sklearn.utils.class_weight as sktCW
from matplotlib import pyplot as plt
from sklearn.linear_model import LogisticRegression
from nilearn.image import load_img
from nilearn.masking import apply_mask
from sklearn.metrics import balanced_accuracy_score
import itertools
from basic_functions import pnl
import sys
import random

class Classifier:
    def __init__(self,SubDict,InputDict_in):
        self.SubDict = SubDict
        self.InputDict_in=InputDict_in
        self.InDict = InputDict_in['decoding']#
        self.foldspecs = self.InDict['foldspecs']
        self.classpec  = self.InDict['classifiers']
        self.FinalOutput, self.EvaluateResults= [], []
        self.verbose = self.classpec['verbose']
        self._init_loadevents()
        pnl('\n Loaded Events \n')
        self._init_outtable()
        #self.labels = self.foldspecs['label']
        self.labels = self.foldspecs['label']
        self._split_traintest()
        pnl('\n Train test is split \n')
        self._upsampleMe()
                
    def _init_loadevents(self):
        sub_events_li = []
        [sub_events_li.append(pd.read_csv(eve,sep='\t')) for eve in self.SubDict['paths']['sub_events_loc']]
        self.sub_events = pd.concat(sub_events_li, axis=0, ignore_index=True)
        self.sub_events['TrialIndex'] = range(432)
        #self.sub_events['value_target_normalized'] = (self.sub_events['value_target'] - np.mean(self.sub_events['value_target']))/np.std(self.sub_events['value_target'])
        #self.sub_events['value_target_scaled'] = (self.sub_events['value_target'] - np.min(self.sub_events['value_target'])) / (np.max(self.sub_events['value_target']) - np.min(self.sub_events['value_target']))
        # Set Target vs Rest labels: 1 if target -1 if not
        # MVAL vs Rest
        for l,v in zip(['Two_rest','Three_rest','Four_rest'],[30,50,70]): 
            self.sub_events[l] = np.ones(shape=len(self.sub_events['TrialIndex']))*-1   
            self.sub_events.loc[(self.sub_events['value_target']==v),l] *=-1
        #EVBACK vs Rest 
        for l,v in zip(['EVBACK_Two_rest','EVBACK_Three_rest','EVBACK_Four_rest'],[30,50,70]): 
            self.sub_events[l] = np.ones(shape=len(self.sub_events['TrialIndex']))*-1     
            self.sub_events.loc[(self.sub_events['EVBACK']==v),l] *=-1
        # adding binary switch label
        #self.sub_events['switch_123_rest'] = np.ones(shape=len(self.sub_events['TrialIndex']))*-1     
        #self.sub_events.loc[(self.sub_events['switch_vector']<=3),'switch_123_rest'] *=-1     
        
        # SIDE
        self.sub_events=self.sub_events.assign(**{'SIDE':'Left'})
        self.sub_events.loc[self.sub_events['RightSideCorrect']==1,'SIDE']='Right'
        # did it switch? 
        self.sub_events=self.sub_events.assign(**{'SWITCH':'Yes'})
        self.sub_events.loc[self.sub_events['switch_vector']>1,'SWITCH']='NO'
        # DimMatch value target and evback? 
        self.sub_events=self.sub_events.assign(**{'DimMatch':False})
        self.sub_events.loc[self.sub_events['value_target']==self.sub_events['EVBACK'],'DimMatch']=True
        # EVBACK VD == 20
        self.sub_events=self.sub_events.assign(**{'VD20':False})
        self.sub_events.loc[self.sub_events['BackDiff']==20,'VD20']=True # no negatives there
        self.sub_events=self.sub_events.drop(["cue_onset","cue_end","stim_onset","stim_end","outcome_onset","outcome_end","noanswer",
                 "CondSz","PECond","PEBackTgt","PEEVback","BackDiff","BackSum","BackSumTDiff","BackDiffSigned","BackSumSigned","BackSumTDiffSigned","StimDuration",
                 "Color","Motion",'LOGresponse_time'], axis=1)       
    def _init_outtable(self):
        self.out_table = self.sub_events.copy()

    def _split_traintest(self):
        self.Trains,self.Tests = [], []
        self.TrainedOn = '-'.join(self.foldspecs['Trainingsplit'])
        self.TrainTest = self.TrainedOn+'_'+self.foldspecs['TestScore']
        for t in range(4): 
            TestInd  = (self.sub_events['Block']==t+1)
            if self.foldspecs['TrainOnAccurate']:
                TrainInd = (self.sub_events['Block']!=t+1) & (self.sub_events['accuracy']==1)
            if 'TwoD' in self.foldspecs['Trainingsplit']:
                TrainInd = TrainInd & (self.sub_events['OneD']==0)
            if 'OneD' in self.foldspecs['Trainingsplit']:
                TrainInd = TrainInd & (self.sub_events['OneD']==1)
            self.Tests.append(self.sub_events.loc[TestInd].copy())
            self.Trains.append(self.sub_events.loc[TrainInd].copy())
        self.FoldNum = len(self.Trains)
        if self.InDict['other']['Train=Test']: self.Tests = self.Trains
    
    def _upsampleMe(self):
        # here we upample each training example 
        # for each block we need to have equal examples of each value / context
        self.UpasmpleBy= self.foldspecs['UpasmpleBy'] #['value_target','Context']
        self.UpSampleFactors = '-'.join(self.foldspecs['UpasmpleBy'])#(Factors)
        # how many units are there to upsample?
        self._ups_GetUpSampleUnits()
        self.FAILEDUPSAMPLE = False
        if self.foldspecs['UpasmpleBy']==None: 
            pnl('No upsampling')
            return()
        pnl('Starting Upsample')
        # first attempt  per block then per training set.
        worked = [self._ups_loop(r,BLOCKWISE=True) for r in range(len(self.Trains))]
        if all(worked): 
            pnl('\n only upsampled per block \n')
        else:
            pnl('\n need to upsample per Train set as well \n')
            worked2 = [self._ups_loop(r,BLOCKWISE=False) for r in range(len(self.Trains))]
            if all(worked2): pnl('\n upsampled per block then per train \n')
            else:   
                [pnl('------------------------!!!!!!!!!!!!!------------------------')  for i in range(5)]
                pnl('\n\n\n FAILED TO UPSAMPLE AND SKIPPING TO NEXT ANALYSIS \n\n\n')
                [pnl('------------------------!!!!!!!!!!!!!------------------------')  for i in range(5)]
                self.FAILEDUPSAMPLE = True
                print('FAILEDUPSAMPLE  = True')
                #sys.exit("Can not upsample also not for training")
        for r in range(len(self.Trains)):
            pnl('Upgesampled training %d has size %s' % (r,str(self.Trains[r].shape)))
   
    def _ups_GetUpSampleUnits(self):
        EGs = self.sub_events.applymap(str).copy()
        if 'TwoD' in self.foldspecs['Trainingsplit']: EGs = EGs.loc[self.sub_events['OneD']==0]
        if 'OneD' in self.foldspecs['Trainingsplit']: EGs = EGs.loc[self.sub_events['OneD']==1]
        C2B = EGs[self.UpasmpleBy].agg('-'.join, axis=1).copy()
        C2B = C2B.reset_index(drop=True)
        Uniclass,Nums = np.unique(C2B,return_counts=True)
        self.UpSampleUnits=len(Uniclass)
        if any(['Context' in self.foldspecs['Trainingsplit'],'ContextWithin' in self.foldspecs['Trainingsplit'],'ContextTrain' in self.foldspecs['Trainingsplit'], 'TrainColor' in self.foldspecs['Trainingsplit'],'TrainMotion' in self.foldspecs['Trainingsplit']]):#
            self.UpSampleUnits = self.UpSampleUnits/2
        pnl('Upsampling total %d units \n %s' % (self.UpSampleUnits,str(Uniclass)))
                
    def _ups_loop(self,r,BLOCKWISE=True): # TRAINS=DIVA.Trains, UpasmpleBy = DIVA.UpasmpleBy, r = 0
            #need to reset since in the second time there are duplicates.
            #but we anyway use the TrialIndex and not normal indecies 
            OutInd = [None]*len(np.unique(self.Trains[r]['Block']))
            self.Trains[r]=self.Trains[r].reset_index(drop=True)
            for i,block in enumerate(np.unique(self.Trains[r]['Block'])):
                pnl('starting train %d block %d' % (r,block))
                AreWeDone = False
                while not AreWeDone:
                    pnl('going once')
                    # if BLOCKWISE IND is only one block, otherwise its all. 
                    # this is repetetive so when its not blockwise it still takes 4 blocks but whatever
                    if BLOCKWISE: IND = self.Trains[r]['Block']==block
                    else: IND = self.Trains[r]['Block']>-1
                    IND = IND.reset_index(drop=True)
                    EGs = self.Trains[r].loc[IND].applymap(str).copy()
                    C2B = EGs[self.UpasmpleBy].agg('-'.join, axis=1).copy()
                    C2B = C2B.reset_index(drop=True)
                    Uniclass,Nums = np.unique(C2B,return_counts=True)
                    MaxNum = max(Nums)
                    if all([len(np.unique(Nums))==1,Nums[0]==MaxNum]): 
                        AreWeDone = True
                        pnl('done')
                        continue
                    all_lists=[]
                    [all_lists.append(np.unique(self.Trains[r][ups])) for ups in self.UpasmpleBy]
                    SubPlist = list(itertools.product(*all_lists))
                    # PROBLEM!!! if there isn't one level it wont be upsampled at all. 
                    # E.g. if there is no EVBACKSigned = -30.... 
                    for sp in SubPlist:  # SubPlist columns are organized as self.UpasmpleBy
                        # get number of examples for sp
                        if len(sp)!=len(self.UpasmpleBy): print('ERROR 1 In upsample: look in code')
                        SpecEGs = self.Trains[r].loc[IND].copy()
                        for s,uni in zip(sp,self.UpasmpleBy):
                            SpecEGs = SpecEGs.loc[SpecEGs[uni]==s]
                        if SpecEGs.shape[0]>=MaxNum: continue
                        NumDiff = MaxNum - SpecEGs.shape[0]
                        NumDiff = min(SpecEGs.shape[0],NumDiff) # becasue its no replace, only adding up to doubling the set each iteration
                        T2add = np.random.choice(np.array(SpecEGs.shape[0]), size=NumDiff, replace=False)
                        self.Trains[r] = pd.concat([self.Trains[r],SpecEGs.iloc[T2add].copy()])
                if self.foldspecs['UpsBalanceLabels']: self._ups_OnevsRest(r,IND)
                # Sanity check - return if number of unique classes is same as supposed to be
                Sanity = self.Trains[r].loc[IND].applymap(str).copy()
                C2B = Sanity[self.UpasmpleBy].agg('-'.join, axis=1).copy()
                C2B = C2B.reset_index(drop=True)
                Uniclass,Nums = np.unique(C2B,return_counts=True)
                # print when blockwise every time when not only once 
                if any([BLOCKWISE, block==max(np.unique(self.Trains[r]['Block']))]):
                    pnl('Training %d block %d has %d labels named labels %s have %s' % (r,block,len(Uniclass),str(Uniclass),str(Nums)))   
                pnl('meant to have %d sample units and we have %d' % (self.UpSampleUnits,len(Uniclass)))
                OutInd[i] = self.UpSampleUnits==len(Uniclass)
            return(all(OutInd))
                
    def _ups_OnevsRest(self,r,IND):
            # one vs rest
            # assumes there is an integer that multiplying the label 1 in will result in same amount as label -1
            Lyes =  sum((self.Trains[r][self.label]==1) & IND)
            Lno  =  sum((self.Trains[r][self.label]==-1)& IND)
            if Lyes != Lno: # if not balanced
                if any([np.mod(Lno,Lyes)==0,np.mod(Lyes,Lno)==0]):
                    pnl('there is an integer that multiplying one label will result in same amount as other label')
                    MultiInt = max(Lno,Lyes)/min(Lno,Lyes)
                    if min(Lno,Lyes)==Lyes: Lab2Multi = 1
                    else: Lab2Multi = -1
                    i,donemulti = 1,False
                    while not donemulti:
                        ADDTR = self.Trains[r].loc[(self.Trains[r][self.label]==Lab2Multi) & IND]
                        self.Trains[r] = pd.concat([self.Trains[r],ADDTR.copy()])
                        i+=1
                        if i==MultiInt: donemulti=True
                else: pnl('ERROR 2 In upsample: look in code')      
                
    def _makeSampleWeights(self):
        if 'label' in self.foldspecs['SampleWeights']:
            self.foldspecs['SampleWeights'].remove('label')
            self.foldspecs['SampleWeights'].append(self.label)
        self.WeightFactors = '-'.join(self.foldspecs['SampleWeights'])#(Factors)
        self.sample_weights = []
        self.sumWeights = []
        if self.foldspecs['SampleWeights']==None: 
            self.WeightFactors = 'None'
            pnl('No sample weights')
        else: pnl('computing sample weights')
        for r in range(self.FoldNum):
            if self.WeightFactors=='None':
                self.sample_weights.append(None)
            else:
                events = self.Trains[r].applymap(str)
                C2B = events[self.foldspecs['SampleWeights']].agg('-'.join, axis=1).copy()
                C2B = C2B.reset_index(drop=True)
                Uniclass = np.unique(C2B)
                ClassWeight = sktCW.compute_class_weight('balanced',classes=Uniclass,y=C2B)
                SW = np.ones(shape=[len(C2B),1])
                SumOfWeights = []
                for i,c in enumerate(Uniclass):
                    SW[C2B==c] = ClassWeight[i]
                    SumOfWeights.append(sum(SW[C2B==c]))    
                SWflat = [item for sublist in SW for item in sublist]
                self.sample_weights.append(np.array(SWflat))
                self.sumWeights.append(SumOfWeights)
        if self.label in self.foldspecs['SampleWeights']:
            self.foldspecs['SampleWeights'].remove(self.label)
            self.foldspecs['SampleWeights'].append('label')
            
            
    def _printSampleWeights(self):
        for r in range(self.FoldNum):
            pnl('is number of elements == sum of weights?')
            pnl('%.2f==%.2f' % (sum(self.sample_weights[r]),len(self.sample_weights[r])))
            for i,sws in enumerate(self.sumWeights[r]):
                pnl('For fold %d, class %d, Sum of weights is: %.2f' % (r,i,sws))
            if 'value_target' in self.WeightFactors:    
                print('-------------------------------------------')
                print('For training set ',r+1)
                print('Number of elements is ',len(self.sample_weights[r]),'and the sum of weights ',sum(self.sample_weights[r]))
                for val in [30,50,70]:
                    print('For value ',val, 'sum of weights is ',sum(self.sample_weights[r][(self.Trains[r]['value_target']==val)]))
            if 'Context' in self.WeightFactors:
                print('-------------------------------------------')
                print('For training set ',r+1)
                for val in [30,50,70]:
                    for ctxt in ['Color','Motion']:
                        sampN = sum((self.Trains[r]['value_target']==val) & (self.Trains[r]['Context']==ctxt))
                        sampW = self.sample_weights[r][(self.Trains[r]['value_target']==val) & (self.Trains[r]['Context']==ctxt)]
                        sampUni = np.unique(sampW)
                        print('For Val',val,'For context',ctxt, 'there are ',sampN,'examples with ',sampUni,'as weight')

    def _registerclassifier(self):
        self.alllabs = np.unique(self.Trains[0][self.label])
        self.labelnum = len(self.alllabs)# self.labelnum = len(np.unique(self.sub_events[self.label])) 
        self.labeltype = type(self.alllabs[0]).__name__[:3] # 3 for cases its int64 for example
        if self.labeltype=='flo': self.labeltype='int' # easier for the cose later and for us its the same 
        pnl('\n Label %s of %d classes of type %s \n  which are: %s' % (self.label,self.labelnum,self.labeltype,str(self.alllabs)))
        # making sure only one RUN is set to True and then setting classifier parameters. 
        if self.classpec['LogReg']['Run1']:
            self.classname ,self.Cparam = 'LogReg', self.classpec['LogReg']['LogRegCparam']
            self.solver,self.panelty,self.multi_class= self.classpec['LogReg']['solver'],self.classpec['LogReg']['panelty'],self.classpec['LogReg']['multi_class']
        # set classifier
        if self.classname=='LogReg':
            self.classifier =  LogisticRegression(C = self.Cparam, penalty = self.panelty, multi_class = self.multi_class, solver = self.solver, max_iter = 4000,verbose=self.verbose) 
            pnl('Classifier is set: \n %s %s %s' %(self.classname,'C='+str(self.Cparam),'_'.join([self.solver,self.panelty,self.multi_class])))
        if self.labeltype=='int':    self.pred = np.zeros(shape=[self.out_table.shape[0],1])#self.labelnum
        elif  self.labeltype=='str': self.pred = np.zeros(shape=[self.out_table.shape[0],1],dtype=str)#self.labelnum
        self.score  = np.zeros([self.FoldNum,1]) # this is alwyas 1
        # for logReg get also probabilities for each class
        if self.classname=='LogReg': self.p2ps = np.zeros(shape=[self.out_table.shape[0],self.labelnum])#
        
    def PrintFoldsEGAndSW(self):
        li = []
        for r in range(self.FoldNum):
            train_sw   = self.sample_weights[r]
            for lab in self.alllabs:
                train_stim = self.Trains[r][self.label] == lab
                test_stim  = (self.Tests[r][self.label] == lab) & (self.Tests[r]['accuracy']==1) #& (self.Tests[r]['OneD']==0)
                pnl('for fold ' + str(r) + ' for label '+str(lab)+ ' there are ' + str(sum(train_stim)) + ' train EGs and ' + str(sum(test_stim)) + ' Test EGs' )
                pnl('The training SW is ' + str(np.unique(train_sw[train_stim])))
                # making DF of these columns: fold x4, label title, specific label x2/3/6, MVAL(All, 30,50,70) number of EG in training, number in testing, sample weights 
                train_stim_vals = [(self.Trains[r][self.label] == lab) & (self.Trains[r]['value_target'] == val) for val in [30,50,70]]
                test_stim_vals = [(self.Tests[r][self.label] == lab) & (self.Tests[r]['accuracy']==1) & (self.Tests[r]['value_target'] == val) for val in [30,50,70]]
                li.append([self.TrainedOn,r,self.label,lab,sum(train_stim),sum(test_stim),sum(train_stim_vals[0]),sum(train_stim_vals[1]),sum(train_stim_vals[2]),sum(test_stim_vals[0]),sum(test_stim_vals[1]),sum(test_stim_vals[2]),str(np.unique(train_sw[train_stim]))])
        DF = pd.DataFrame(li.copy(),columns = ['Trainsplit','Fold','label','speclabal','alltrains','alltests','Trains30','Trains50','Trains70','Tests30','Tests50','Tests70','TrainSW'])
        return(DF)

    def RegisterData(self,Data_PreZ,DataType,ROIname,Zfields = ['Block']):
            self.ROI = ROIname
            self.Dtype = DataType
            self.Zscoring = '-'.join(self.InDict['other']['Zscoring'])
            events = self.sub_events.applymap(str)
            C2B = events[self.InDict['other']['Zscoring']].agg('-'.join, axis=1).copy()
            Uniclass = np.unique(C2B)
            # Eliminate nan or Zeros 
            Ns = np.isnan(np.sum(Data_PreZ, axis=0))
            Zeros = np.sum(Data_PreZ, axis=0)==0
            Col2drop = Ns | Zeros
            Data_PreZ =  Data_PreZ.compress(np.logical_not(Col2drop), axis=1)
            pnl('eliminated %d voxels of zeros and %d voxels of nan' % (sum(Zeros),sum(Ns)))
            pnl('data shape ' + str(Data_PreZ.shape))
            if self.InDict['other']['Zagain']: 
                pnl('Z scoring again')
                for z in Uniclass:
                    ind = C2B==z
                    Data2Z = Data_PreZ[ind,:]
                    Xmean = np.nanmean(np.array(Data2Z), axis = 0,keepdims=True)
                    Xstd =  np.nanstd(Data2Z, axis = 0,keepdims=True) 
                    Data2Z = (Data2Z - Xmean)/Xstd
                    Data_PreZ[ind,:] = Data2Z
            self.Data2Classify = Data_PreZ
    
    def classifyMe(self):        
        self._makeSampleWeights()
        #if self.InDict['outputs_dec']['PrintSW']:
        self._printSampleWeights()
        self._registerclassifier()
        pnl('Start classify')
        self.LabelMeanTest,self.LabelMeanTrain  = np.zeros([self.FoldNum,self.labelnum]),np.zeros([self.FoldNum,self.labelnum])
           
        for r in range(self.FoldNum): 
            # take relevant train/test
            train_data = self.Data2Classify[self.Trains[r]['TrialIndex'],:]
            test_data  = self.Data2Classify[self.Tests[r]['TrialIndex'],:]
            if self.labeltype=='int':
                train_stim = self.Trains[r][self.label].values.astype(int)
                test_stim  = self.Tests[r][self.label].values.astype(int)
            elif self.labeltype=='str':
                train_stim = self.Trains[r][self.label].values.astype(str) 
                test_stim  = self.Tests[r][self.label].values.astype(str)
           
            # fit: sample_weights in case of None wont affect anything
            self.classifier.fit(train_data, train_stim,sample_weight=self.sample_weights[r])
            #self.classifier.fit(train_data, train_stim)
            pnl('training %d has size %s' % (r,str(len(train_stim))))
            # get predicted label 
            self.pred[self.Tests[r]['TrialIndex'],0] =  self.classifier.predict(test_data)
            if self.classname=='LogReg': self.p2ps[self.Tests[r]['TrialIndex'],:] = self.classifier.predict_proba(test_data)
            # get test score
            if   self.foldspecs['TestScore']=='OneD_correct':  TestInd = (self.Tests[r]['accuracy']==1) & (self.Tests[r]['OneD']==1)
            elif self.foldspecs['TestScore']=='Correct':       TestInd = (self.Tests[r]['accuracy']==1)      
            elif self.foldspecs['TestScore']=='All':           TestInd = (self.Tests[r]['accuracy']<5) # just hack to get all
            elif self.foldspecs['TestScore']=='TwoD_correct':  TestInd = (self.Tests[r]['accuracy']==1) & (self.Tests[r]['OneD']==0) # just hack to get all
            
            self.score[r,0] = balanced_accuracy_score(test_stim[TestInd],self.classifier.predict(test_data[TestInd]))      
            for i,val in enumerate(self.alllabs):
                self.LabelMeanTest[r,i]  = np.mean(np.mean(test_data[TestInd & (self.Tests[r][self.label]==val),:],axis= 1))
                self.LabelMeanTrain[r,i] = np.mean(np.mean(train_data[(self.Trains[r][self.label]==val),:],axis= 1))
    
    def LogOutput(self):
        # get mean value per value target
        self.LabelMeanFullData= np.zeros(shape=[1,3])
        for i,val in enumerate([30,50,70]):
            self.LabelMeanFullData[0,i] = np.mean(np.mean(self.Data2Classify[self.sub_events['value_target']==val,:],axis= 1))
        # Logs all thetrial-wise parameters in a the output table
        # first the one value columns:
        self.out_table = self.out_table.assign(**{'sub': self.SubDict['subject'], 'ROI': self.ROI, 'Dtype': self.Dtype,
                     'TrainedOn':self.TrainedOn ,'TrainTest':self.TrainTest ,'WeightFactors': self.WeightFactors,
                     'UpSampleFactors': self.UpSampleFactors,'Zscoring':  self.Zscoring,'Classifier':self.classname ,
                     'Cparam':self.Cparam ,'label':self.label ,
                     'input':self.InDict['input'],
                     'delay':5,
                     'FullDataMean30':self.LabelMeanFullData[0,0] ,'FullDataMean50': self.LabelMeanFullData[0,1],'FullDataMean70': self.LabelMeanFullData[0,2],
                     'description': self.InDict['Output']})
        self.out_table = self.out_table.assign(**{'label': self.label, 'alllabs': '-'.join(map(str, self.alllabs)),'labelnum':self.labelnum,'labeltype':self.labeltype})       
        if self.classname == 'LogReg':
            self.out_table = self.out_table.assign(**{'solver': '_'.join([self.solver,self.panelty,self.multi_class])})
            self.out_table['PredictedLabel'] = self.pred
            for i,lab in enumerate(self.classifier.classes_):
                # In case of one vs rest 
                if '_rest' in self.label:
                    if lab==-1: Nlab = self.label[-4:] # rest
                    elif lab==1: Nlab = self.label[:-5] # label name
                else: Nlab = lab
                self.out_table['P2P'+str(Nlab)] = self.p2ps[:,i]
        self.OrganizeTable() # fixes parameters we might need for analysis
        self.FinalOutput.append(self.out_table.copy())       
        # Logs summary parameters in table to evaluate the results
        self.List2DF = []
        for f in range(self.FoldNum):
             self.List2DF.append([self.InDict['input'],self.SubDict['subject'],f+1,self.ROI,self.Dtype,self.TrainedOn,self.TrainTest,self.WeightFactors,self.UpSampleFactors,self.Zscoring,self.classname,self.Cparam,self.score[f][0]])
        DF = pd.DataFrame(self.List2DF.copy(),columns = ['Input','Sub','Fold','ROI','Dtype','TrainedOn','TrainTest','WeightFactors','UpSampleFactors','Zscoring','classname','Cparam','Score'])
        DF = DF.assign(**{'FullDataMean30': self.LabelMeanFullData[0,0],'FullDataMean50' : self.LabelMeanFullData[0,1], 'FullDataMean70':self.LabelMeanFullData[0,2]})
        DF = DF.assign(**{'label': self.label, 'alllabs': '-'.join(map(str, self.alllabs)),'labelnum':self.labelnum,'labeltype':self.labeltype})      
        DF = DF.assign(**{'delay': 5})       
        if self.classname == 'LogReg':     DF=DF.assign(**{'solver': '_'.join([self.solver,self.panelty,self.multi_class])})
        for i,val in enumerate(self.alllabs):
            DF=DF.assign(**{'Testmean-'+str(val):self.LabelMeanTest[:,i],'Trainmean-'+str(val): self.LabelMeanTrain[:,i]})
        self.EvaluateResults.append(DF.copy())
        self._init_outtable()
        pnl('Logged Output table')
    
    def OrganizeTable(self):
        self.out_table.assign(**{'CorrectPred' : False, 'P2Pcorrect': 0,'DimNum':1,'P2PEVBACK':0})
        self.out_table.loc[self.out_table['PredictedLabel']==self.out_table['value_target'],'CorrectPred'] = True
        Vals = [30,50,70]
        for v in Vals:
            if 'P2P'+str(v) in self.out_table.columns:
                self.out_table.loc[(self.out_table['value_target']==v),'P2Pcorrect'] = self.out_table.loc[(self.out_table['value_target']==v),'P2P'+str(v)]
        self.out_table['MVALminEVBACK']        = self.out_table['value_target'] - self.out_table['EVBACK']
        self.out_table['absMVALminEVBACK'] = abs(self.out_table['MVALminEVBACK'])
        self.out_table['MVALplusEVBACK']       = self.out_table['value_target'] + self.out_table['EVBACK']
        self.out_table['MVALminEVBACKSigned']  = self.out_table['value_target'] - self.out_table['EVBACKSigned']
        self.out_table['MVALplusEVBACKSigned'] = self.out_table['value_target'] + self.out_table['EVBACKSigned']
        
        OldVals,NewVals = [0,1,2,3,4],[0,10,30,50,70]
        for f in ['value_back_target','value_back_nontarget','value_nontarget']:
           self.out_table[f] = [NewVals[OldVals.index(x)] for x in self.out_table[f]]
        
        self.out_table.loc[self.out_table['OneD']==0, 'DimNum']=2 
        for val in [30,50,70]:
            if 'P2P'+str(v) in self.out_table.columns:
                self.out_table.loc[self.out_table['EVBACK']==val,'P2PEVBACK'] = self.out_table.loc[self.out_table['EVBACK']==val,'P2P'+str(val)]
        
    def Evaluate(self):
        self.FinalResults = pd.concat(self.EvaluateResults, axis=0, ignore_index=True,sort=False)
                   
           
            
'''
##############################################################################
################        (C) Do Everything                    #################
# This is the main function the goes through the Classifier abalysis options
##############################################################################
'''    
#@profile
def RunDecoding(ID):  
    SubDict = ID.SubDict
    OutTables = [] 
    PredTables = []
    SWDFs = []     
    Names =[]
    for InputDict in ID.AllInputDicts: #InputDict = ID.AllInputDicts[1]
        print('loading')
        Names.append(InputDict['name'])
        DataType = 'TR'
        ROIname = 'func_vmPFC'
        DataLoc = opj(SubDict['paths']['input'],SubDict['subject']+'func_vmPFC_data_for_decoding.csv')
        # DataLoc = 'X:\\SODIVA\\SHARE\\BIDS\\derivatives\\Multivaraite_PP_Data\\sub-01func_vmPFC_data_for_decoding.tsv.gz'
        LoadedData = np.loadtxt(fname=DataLoc,delimiter=',')
        print('Loaded')    
        print('registering classifier')    
        DIVA = Classifier(SubDict,InputDict)
        print('registering data')    
        DIVA.RegisterData(LoadedData,DataType,ROIname)
        for lab in DIVA.labels: # lab = DIVA.labels[0]
            print('decoding '+lab)    
            DIVA.label = lab
            DIVA.classifyMe()
            SWDF = DIVA.PrintFoldsEGAndSW()
            SWDFs.append(SWDF.copy())
            if DIVA.FAILEDUPSAMPLE: 
                continue
            print('logging '+lab)    
            DIVA.LogOutput() 
        print('Evaluating')    
        DIVA.Evaluate()
        #DIVA.FinalResults['description'] = InputDict['decoding']['Output']
        OutTables.append(DIVA.FinalResults)
        DF = pd.concat(DIVA.FinalOutput, axis=0, ignore_index=True,sort=False)
        DF['description'] = InputDict['decoding']['Output']
        PredTables.append(DF) 
        
    return(OutTables,SWDFs,DIVA.sub_events,PredTables,Names)    
# =============================================================================
#         if InputDict['decoding']['Run_dec']:
#             # should i reload the data? only if its not loaded yet
#             if InputDict['decoding']['input']!=CurrentLoaded:
#                 LoadedData,DataType,ROIname = loadPreProDIVA(SubDict,InputDict)
#                 CurrentLoaded = InputDict['decoding']['input']
#             print('loaded data')
#             pnl('starting classification \n '+ InputDict['decoding']['Output'])  
#                         
#             DIVA = Classifier(SubDict,InputDict)
# =============================================================================
             
# =============================================================================
#         # this is here since only logging when its actual running. 
#         Inputs.append(CurrentLoaded)
#         for r,data in enumerate(LoadedData):
#             pnl(ROIname[r])
#             DIVA.RegisterData(data,DataType[r],ROIname[r])
#             if InputDict['decoding']['other']['Simulate']: DIVA.SimulateData(data)
#             for lab in DIVA.labels: # lab = DIVA.labels[0]
#                 DIVA.label = lab
#                 DIVA.classifyMe()
#                 SWDF = DIVA.PrintFoldsEGAndSW()
#                 SWDFs.append(SWDF.copy())
#                 if DIVA.FAILEDUPSAMPLE: 
#                     continue
#                 DIVA.LogOutput() 
#                 if InputDict['decoding']['outputs_dec']['PlotData']:
#                     DIVA.PlotData()
#                     DIVA.PlotRandomVoxels()
#         DIVA.Evaluate()
#         #DIVA.FinalResults['description'] = InputDict['decoding']['Output']
#         OutTables.append(DIVA.FinalResults)
#         DF = pd.concat(DIVA.FinalOutput, axis=0, ignore_index=True,sort=False)
#         DF['description'] = InputDict['decoding']['Output']
#         PredTables.append(DF) 
#     if CurrentLoaded==None: 
#         pnl('Had no data to decode')
#         return(None,None,None,None,None)
#     return(OutTables,SWDFs,DIVA.sub_events,PredTables,Inputs)    
#     
#     
# =============================================================================
    
'''
END
'''   
