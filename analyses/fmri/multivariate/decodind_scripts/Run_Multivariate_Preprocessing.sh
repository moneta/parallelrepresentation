#!/usr/bin/bash
# ==============================================================================
# ==============================================================================
# DEFINE ALL PATHS:
# ==============================================================================
# path to the current code folder:
PATH_CODE=$HOME/SODIVA/Scripts/SHARE/
# cd into the directory of the current script:
cd ${PATH_CODE}
# path to the current script:
PATH_SCRIPT=${PATH_CODE}/Multivariate_Preprocessing.py
# path to the log directory:
PATH_LOG=$HOME/logs/decoding/
# path to the text file with all subject ids:
PATH_SUB_LIST="$HOME/SODIVA/Scripts/sublists/sublist_Decoding35.txt"
# ==============================================================================
# CREATE RELEVANT DIRECTORIES:
# ==============================================================================
# create directory for log files:
if [ ! -d ${PATH_LOG} ]; then
	mkdir -p ${PATH_LOG}
fi
# ==============================================================================
# DEFINE PARAMETERS:
# ==============================================================================
# user-defined subject list
# read subject ids from the list of the text file
SUB_LIST=$(cat ${PATH_SUB_LIST} | tr '\n' ' ')
# ==============================================================================
# RUN THE DECODING: TR and LSS
# ==============================================================================

for SUB in ${SUB_LIST}; do
	echo ${SUB}
	echo '#!/bin/bash'                             > job.slurm
	echo "#SBATCH --job-name MPP${SUB}"  >> job.slurm
	echo "#SBATCH --partition quick"               >>   job.slurm
	echo "#SBATCH --time 2:0:0"                   >> job.slurm
	echo "#SBATCH --mem 10GB"                      >> job.slurm
	echo "#SBATCH --cpus-per-task 1 "              >> job.slurm
	echo "#SBATCH --output ${PATH_LOG}MPP${SUB}out"   >> job.slurm
	echo "#SBATCH --mail-type NONE"                >> job.slurm
	echo "source /etc/bash_completion.d/virtualenvwrapper" >> job.slurm
	echo "workon neurogrid" >> job.slurm
	echo "python3 ${PATH_SCRIPT} ${SUB}" >> job.slurm
	sbatch job.slurm
	rm -f job.slurm
done


