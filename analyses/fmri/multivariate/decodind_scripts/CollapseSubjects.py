# -*- coding: utf-8 -*-
"""
Created on Wed May 25 14:39:50 2022

@author: moneta
"""



''' collapse subjects '''

import os
from os.path import join as opj
import sys
import pandas as pd
import glob
import numpy as np
from pathlib import Path

if 'win32' in sys.platform: path_root = opj('X:\\','SODIVA','SHARE','BIDS')
else:                       path_root = opj(os.environ['HOME'],'SODIVA','SHARE','BIDS')    

OutFolder =opj(path_root,'derivatives','DecodedData','group')
Path(OutFolder).mkdir(parents=True, exist_ok=True)# make if doesnt exist

# which types are there?
types = np.unique([i.split(os.sep)[-1] for i in glob.glob(opj(path_root,'derivatives','DecodedData','sub*','*'))])
for t in types:
    print('starting '+t)
    files = np.sort(glob.glob(opj(path_root,'derivatives','DecodedData','sub*',t,'*PredictionResults_LogReg.csv')))
    print(files)
    DFs = pd.concat([pd.read_csv(eve,sep=',') for eve in files], axis=0, ignore_index=True,sort=False).to_csv(opj(OutFolder,'Group_'+t+'_'+'PredictionResults_LogReg.csv'),sep=',',index=False)
    print('finished '+t)
    
#A = [pd.read_csv(eve,sep=',') for eve in files]
#B = pd.concat(A, axis=0, ignore_index=True,sort=False)