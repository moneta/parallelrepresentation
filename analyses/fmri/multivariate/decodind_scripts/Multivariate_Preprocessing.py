# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 11:00:11 2020

@author: User

This script loads for each subject the TR / Betas and subject specific ROIs
Then it masks the data per ROI (transforming to 2D sample X features structure) and saves

This also detrends the data btut doesnt Zscore it

Output: saves one  table per subject per ROI, once for Betas and once for TRs

Workflow:
    Get paths and logger from 'basic function'
    Load events for the subject
    Load the data and the ROIs
    Mask the data (betas or Raw data)
    - if raw data, also detrend
    Save data per subject per ROI in 2D: sample x feature structure
    
"""
import os
from os.path import join as opj
import sys
import glob
import pandas as pd
import numpy as np
from nilearn.image import load_img
from nilearn.masking import apply_mask
from nilearn.signal import clean
from pathlib import Path


if 'win32' in sys.platform:
    LOCAL = True 
else: 
    LOCAL = False
 
if LOCAL:
    sub = 'sub-01'
    home = opj('X:\\')
else:
    sub = 'sub-%s' % sys.argv[1]
    home = opj(os.environ['HOME'])


def ManualCleanExtremeblockwise(Data,sessions,Threshold = 8,recursive = True):
    # Data is TR xVoxels 
    tempData = Data.copy()
    for b in np.unique(sessions):
        DONE = False
        while not DONE:
            # for each block i take the data, modify and save it back. 
            Data2Modify = Data[sessions==b,:]
            STD = np.std(Data2Modify,axis=0)
            MEANS = np.mean(Data2Modify,axis=0)
            # what are the values for beind 8SD from the mean in each direction for each voxel (columns)? 
            PosExt,NegExt = MEANS +Threshold*STD, MEANS -Threshold*STD
            # which data points to change? i checked manually and this works perfect and fastest
            Data2= (Data2Modify> PosExt) | (Data2Modify< NegExt) 
            Vs = np.sum(Data2,axis=0).astype(bool)            
            # are there no more dots to change? this needs to be here because it needs to be after the new SD is calculated and relative to that. 
            DONE = np.sum(Data2)==0
            print('There are %d extreme data points in block %d before correction' % (np.sum(Data2),b+1))
            for i in np.arange(Data2Modify.shape[1])[Vs]: # only go through columns that have datapoints to change
                PosInd = (Data2Modify[:,i] > PosExt[i]) 
                NegInd = (Data2Modify[:,i] < NegExt[i]) 
                for j,(P,N) in enumerate(zip(PosInd,NegInd)):
                    if P: Data2Modify[j,i] = MEANS[i] + 0.5*(abs(MEANS[i] - Data2Modify[j,i]))
                    if N: Data2Modify[j,i] = MEANS[i] - 0.5*(abs(MEANS[i] - Data2Modify[j,i]))
            Data2= (Data2Modify> PosExt) | (Data2Modify< NegExt) 
            print('After correction: %d extremes in block %d' % (np.sum(Data2),b+1))
            Data[sessions==b,:] = Data2Modify
            if not recursive: DONE=True  # in case we dont want recursive but only one time
    print('Total changed %d data points' % (np.sum(tempData != Data)))
    return(Data)  


sm = '4' # for multivariate only this smooth level
space = 'subspace' # or 'MNI'

NEWBIDS = opj(home,'SODIVA','SHARE','BIDS')
BIDSsource = opj(NEWBIDS,'bidssource',sub)
print('loading\n' + '\n'.join(glob.glob(opj(BIDSsource,'func','*.tsv'))))
sub_events = pd.concat([pd.read_csv(eve,sep='\t') for eve in np.sort(glob.glob(opj(BIDSsource,'func','*.tsv')))], axis=0, ignore_index=True)
sub_events['TrialIndex'] = range(432)
confounds = opj(NEWBIDS,'derivatives','ConfoundsCSV_multivariate',sub+'_AllConfounds.csv')
OutFolder = opj(NEWBIDS,'derivatives','Multivaraite_PP_Data')
Path(OutFolder).mkdir(parents=True, exist_ok=True)# make if doesnt exist

if space=='subspace':
    ROIsub =  opj(home,'SODIVA','SHARE','BIDS','derivatives','ROIs','T1w_func_vmPFC_'+sub+'.nii')
else: # MNI
    ROIsub = opj(home,'SODIVA','SHARE','BIDS','derivatives','ROIs','MNI_func_vmPFC_group.nii')

inData = glob.glob(opj(home,'SODIVA','SHARE','BIDS','derivatives','Data2D',space,'smoothed'+sm,'*.csv'))
inData = [d for d in inData if sub in d.split(os.sep)[-1].split('_')]
DataTr=[]
num_tr=[]
for b in range(4):
     bdata = [d for d in inData if 'run-0'+str(b+1) in d.split(os.sep)[-1].split('_')]
     voxel_by_time = np.loadtxt(bdata[0],delimiter=',')
     DataTr.append(voxel_by_time.copy())
     num_tr.append(voxel_by_time.shape[0])
DataTr = np.concatenate(DataTr,axis=0)
run_indices = np.repeat(range(4),num_tr)
print('Starting detrend, highpass 128 and confounds')
DataTr =  clean(signals = DataTr, sessions = run_indices, t_r = 1.25, 
                confounds = confounds,
                detrend = True,high_pass = 1/128, 
                standardize = False)
# now we moderate the extremes
print('Takign extremes out at %d sd from mean' % (8))
DataTr = ManualCleanExtremeblockwise(DataTr.copy(),sessions = run_indices,Threshold = 8,recursive = False)
# now we do the z scoring and it does it per block because of run_indices (i checked)
print('Starting Zscoring')
DataTr =  clean(signals = DataTr, sessions = run_indices, t_r = 1.25,
                detrend = False, 
                standardize = True)          
     
AddTR = np.cumsum(num_tr)              
rlevant_TR = round((sub_events['stim_onset']+ 5)/1.25) # Delay is in seconds ! 
rlevant_TR = rlevant_TR.values.astype(int)
# now we have the 432 TRs to take but they are all relevant within the block
# need to add to blocks 2-3-4 the TR of the previous blocks *ADDtr
# across blocks that need an addition to TR number
for eveb in np.array([1,2,3]): 
    ind = np.arange(eveb*108,108+eveb*108,1)
    rlevant_TR[ind] = rlevant_TR[ind]+AddTR[eveb-1]    
DataTr = DataTr[rlevant_TR,]
# save
OutPath = opj(OutFolder,sub+'func_vmPFC_data_for_decoding.csv')
np.savetxt(X=DataTr,fname=OutPath,delimiter=',')