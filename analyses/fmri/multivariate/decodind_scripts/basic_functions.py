# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 12:22:02 2020

@author: User

===============================================================================
(1) Helping functions: 
    - MakeIfNotThere(Path):
    Makes folders if they dont exist
    
    - MakeSubdict(sub,path_root) 
    based on input subject name and root path (depending on OS) and makes all the paths needed. 
    
    - SetLogger(SubDict)
    If on cluster, set the logger to log the outcome (otherwise it usually justs prints it)
    
    - copyData
    copy from cluster to local in order to plot easily
    
    -  
(2) Plotting functions:
    
    2.1 Plotting results:
        - PlotResults_SingleSubject: for single subject's output table (inc. mean in ROI)
        - EvaluationPlot_elaborated: for single subject OR Group loading the data and plotting the results. 
        - Evaluation_Ttests:         evaluate the results and plot the T test of the classification accuracy 
    2.2 Plotting the data from the ROI:
        - PlotROIsSub:               Plot t values (from 1st level GLM) from the ROI
        - PlotROIsSub_PrePostZ:      Plot the data before and after the Z scoring (to find inflated voxels)
===============================================================================

"""
import os
from os.path import join as opj
import sys
import glob
import time
import logging
import numpy as np
import pandas as pd

''' #########  HELPING FUNCTIONS #########'''

# =============================================================================
# ID = Inputmaker('sub','path',add2name = 'test')
# ID.updateDict('tmaps','TTTTTTTT')
# ID.updateDict('Run_dec',False)
# ID.finalizeInputDict()
# ID.printme()
# 
# =============================================================================
def pnl(string):
#    return
    if 'win32' in sys.platform: SLog = False
    else: SLog = True
    print(string)       
    if SLog: logging.info(string)
    
class Inputmaker:
    def __init__(self,sub,path_root,add2name = None): # X:\SODIVA\SHARE\BIDS
        self.sub = sub
        self.root = path_root
        self.add2name=add2name
        self._SubDictmaker()
        self._inputmaker()
       # self._SetLogger()
        self.AllInputDicts =[]
        
    def MakeIfNotThere(self,Path):
        if not os.path.exists(Path):
            os.makedirs(Path)
    def printme(self):
        import json
        print(json.dumps(self.AllInputDicts, indent = 3))
        
    def _SubDictmaker(self):
        derivfold = opj(self.root,'derivatives')
        self.SubDict={
       'subject' : self.sub,
       'paths': {'root'  : self.root,
                 'sub_events_loc': np.sort(glob.glob(opj(self.root,'bidssource',self.sub,'func','*.tsv'))),
                 #'sub_events_loc': sorted(glob.glob(opj(derivfold,'EventsFiles_Corrected','Corrected_'+self.sub+'*.tsv'))),
                 'input': opj(derivfold,'Multivaraite_PP_Data'),
                 'output': opj(derivfold,'DecodedData',self.sub)}}
                               
    def _inputmaker(self):
        self.InputDict = {
         'name': 'NAN',
         'decoding':      {'input':'sm4_func_vmPFC',
                           'Run_dec': True,
                           'foldspecs': {'Trainingsplit': ['OneD'],#['TwoD'],['OneD','Context'], ['All'],['OneD'],['ContextWithin']  # for later,
                                         'HoldUPSSW': False, # if true, does NOT automatically do upsampel and sample weights 
                                         'TrainOnAccurate': True, # ALWAYS ignores noanswe anyway. those are hard to use. 
                                         'SampleWeights': ['value_target','Context'],#,'Context'],# ['value_target','Context'], # for later
                                         'UpasmpleBy' : ['value_target','Context'],# or None for no upsammpling - Factors used to upsample each block in each training set. 
                                         'UpsBalanceLabels': False, # in case of one vs rest - need to set to TRUE to additianlly balnce by labels 
                                         'TestScore' : 'Correct',  #'TwoD_correct','OneD_correct', 'Correct' 'All' Looking at classification accuracy in OneD trials only at the moment
                                         'label': 'value_target'}, # Two_rest, Three_rest,Four_rest   
                           'classifiers': {'LogReg':{'Run1': True,
                                                     'solver': 'lbfgs',#'lbgfs_L2_ovr', 'SAGA_L1_multi','SAGA_L1_ovr','liblinear_L1_ovr','liblinear_L2_ovr'
                                                     'panelty':'l2', # or l1 when possible
                                                     'multi_class':'multinomial',# or ovr when possible,
                                                     'LogRegCparam': 1.0},
                                           'verbose' : 0}, # 1 for elaborated output
                            'other': {'Zagain' : False, #if True use data already zscored and no need to zscore after
                                      'Zscoring' : ['Block'], # in case of another z scoring is required ,['Block','Context']for later
                                      'Train=Test': False}}} # for double dipping sanity check
        print('\n  -------  InputDict initialized  -------\n')

    def finalizeInputDict(self):#),resetinputdict=False):
        # update the decoding
        sid = self.InputDict['decoding']
        self.decodingADDS = []
        if self.add2name!=None: self.decodingADDS.append(self.add2name)
        if sid['classifiers']['LogReg']['Run1']: 
            self.decodingADDS.append('LogReg')
            self.decodingADDS.append(sid['classifiers']['LogReg']['solver']+'-'+sid['classifiers']['LogReg']['panelty']+'-'+sid['classifiers']['LogReg']['multi_class'])
        self.decodingADDS.append('Train-' + ''.join(sid['foldspecs']['Trainingsplit']))
        if sid['foldspecs']['SampleWeights']!=None:            
            self.decodingADDS.append('SW-' + ''.join(sid['foldspecs']['SampleWeights']))
        else: self.decodingADDS.append('SW-no')
        if sid['foldspecs']['UpasmpleBy']!=None:            
            self.decodingADDS.append('UPs-' + ''.join(sid['foldspecs']['SampleWeights']))
        else: self.decodingADDS.append('Ups-no')
        self.decodingADDS.append('TestScore-' + ''.join(sid['foldspecs']['TestScore']))
        self.decodingADDS.append('label-' + ''.join(sid['foldspecs']['label']))
        if sid['other']['Zagain']:            
            self.decodingADDS.append('Z2-' + ''.join(sid['other']['Zscoring']))
        if sid['other']['Train=Test']: self.decodingADDS.append('Tr-Te')
        self.InputDict['decoding']['Output'] = '_'.join(self.decodingADDS)
        print('\nDecoding according to: \n' + self.InputDict['decoding']['Output'] )
        self.AllInputDicts.append(self.InputDict.copy())
        print('\n  -------  InputDict saved -------\n')        
        self._inputmaker()
         
    def updateDict(self,key,newval):
        if self._checkuniquefields():
            found=False
            for Dict in [self.SubDict,self.InputDict]:
                found = any([found,self._updateDict(Dict,key,newval)])
            if not found: print('Couldnt find the key')
        else: print('Didnt update field: Keys are not unique')
        
    def _updateDict(self,Dict,key,newval):
            if key in Dict:
                print('\nChanging Key: \n' + key)
                print('Before: ' + str(Dict[key]))
                Dict.update({key: newval})
                print('After: ' + str(Dict[key]))
                return True
            for k, d in Dict.items():
                if isinstance(d,dict):
                    item = self._updateDict(d, key,newval)
                    if item: return True
                    
    def _checkuniquefields(self):
        allkeys = []
        for Dict in [self.SubDict,self.InputDict]:
            A = list(self._checkuni(Dict))
            import inspect
            while any(inspect.isgenerator(item) for item in A): # at least one generator
                for i,it in enumerate(A):
                    if inspect.isgenerator(it): A[i]= list(A[i])
                B = []
                temp = [B.append(a) if not isinstance(a, list) else [B.append(a2) for a2 in a] for a in A]
                A = B
            if len(A) == len(set(A)): allkeys.append(True)
            else: 
                allkeys.append(False)
                print(A)
        if all(allkeys): 
            print('All keys are unique')
            return(True)
        else: print('Keys not unique')

    def _checkuni(self,Dict):
        for key in Dict:
            yield key
        for k, d in Dict.items():
            if isinstance(d,dict):
                y = self._checkuni(d)
                yield y      
        
def MakeIfNotThere(Path):
    if not os.path.exists(Path):
        os.makedirs(Path)        
    
def copyData(SourceDataLoc,DataLoc):
    MakeIfNotThere(DataLoc)
    from shutil import copyfile
    i = 0
    for item in os.listdir(SourceDataLoc):
            s = os.path.join(SourceDataLoc, item)
            d = os.path.join(DataLoc, item)
            copyfile(s, d)
            i+=1
    print('Copied ' +str(i)+ ' files')
    
def OrgTable(DataLoc,Subs='All'):
    OT = [] 
    li = []
    if Subs=='All': [li.append(pd.read_csv(eve,sep=',')) for eve in glob.glob(DataLoc)]
    else: [li.append(pd.read_csv(eve,sep=',')) for eve in glob.glob(DataLoc) if any([s in eve for s in Subs])]
    OT = pd.concat(li, axis=0, ignore_index=True)
    print('loaded %d' % len(li))
 
    np.unique(OT['ROI'])
    OT['CorrectPred'] = False
    OT.loc[OT['PredictedLabel']==OT['value_target'],'CorrectPred'] = True
    OT['P2Pcorrect'] = 0
    Vals = [30,50,70]
    for v in Vals:
        #OT.loc[(OT['CorrectPred']==True) & (OT['value_target']==v),'P2Pcorrect'] = OT.loc[(OT['CorrectPred']==True) & (OT['value_target']==v),'P2P'+str(v)]
        OT.loc[(OT['value_target']==v),'P2Pcorrect'] = OT.loc[(OT['value_target']==v),'P2P'+str(v)]
        
    OT['MVALminEVBACK']        = OT['value_target'] - OT['EVBACK']
    OT['absMVALminEVBACK'] = abs(OT['MVALminEVBACK'])
    OT['MVALplusEVBACK']       = OT['value_target'] + OT['EVBACK']
    OT['MVALminEVBACKSigned']  = OT['value_target'] - OT['EVBACKSigned']
    OT['MVALplusEVBACKSigned'] = OT['value_target'] + OT['EVBACKSigned']
    
    OldVals = [0,1,2,3,4]
    NewVals = [0,10,30,50,70]
    
    OT['value_back_target'] = [NewVals[OldVals.index(x)] for x in OT['value_back_target']]
    OT['value_back_nontarget'] = [NewVals[OldVals.index(x)] for x in OT['value_back_nontarget']]
    OT['value_nontarget'] = [NewVals[OldVals.index(x)] for x in OT['value_nontarget']]
    
    OT['DimMatch'] = False
    OT.loc[OT['value_target']==OT['EVBACK'],'DimMatch']=True
    
    OT['DimNum'] = 1
    OT.loc[OT['OneD']==0, 'DimNum']=2 
    
    OT['P2PEVBACK'] = 0
    for val in [30,50,70]:
        OT.loc[OT['EVBACK']==val,'P2PEVBACK'] = OT.loc[OT['EVBACK']==val,'P2P'+str(val)]
    #np.log(0.33)
    
    OT.columns
    OT = OT.loc[:, ['sub', 'ROI',
                    'accuracy', 'wrong', 'noanswer', 
                    'response_time',
                    'Block', 'Trial', 'Context', 'switch_vector', 'RightSideCorrect',
                    'value_target', 'value_nontarget', 'value_back_target', 'value_back_nontarget',
                    'DimNum',
                    'Condition', 
                    'EVBACK', 'EVBACKSigned', 'DimMatch',
                    'MVALminEVBACK', 'absMVALminEVBACK','MVALplusEVBACK', 'MVALminEVBACKSigned', 'MVALplusEVBACKSigned',
                    'P2P30', 'P2P50', 'P2P70',
                    'PredictedLabel','CorrectPred', 'P2Pcorrect','P2PEVBACK',
                    'TrainedOn', 'TrainTest', 'WeightFactors', 'UpSampleFactors',
                    'Zscoring', 'Classifier', 'Cparam', 'UseSampleW',
                    'LabelsMethod', 'solver']]
    return(OT)

      
'''
########
THE END!
######## 
'''   