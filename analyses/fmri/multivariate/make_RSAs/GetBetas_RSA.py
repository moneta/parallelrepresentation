# -*- coding: utf-8 -*-
"""
Created on Wed May 25 16:39:14 2022

@author: moneta
"""

import glob 
import os
from os.path import join as opj
import sys
import pandas as pd
import numpy as np
from nilearn.image import load_img
from nilearn.masking import apply_mask
from sklearn.metrics.pairwise import pairwise_distances
from pathlib import Path


def sortinput(InputList):
     EG = list(os.path.basename(InputList[0]))
     NumsIn = [i for i,s in enumerate(EG) if s.isdigit()]
     FirstDig = min(NumsIn) # the first number
     InSplit = [int(os.path.basename(s)[FirstDig:-4]) for s in InputList]
     Outputlist = [InputList for _,InputList in sorted(zip(InSplit,InputList))]
     return Outputlist

if 'win32' in sys.platform: 
    sub = 'sub-14' #sub = 'sub-14' #sub = 'sub-01' 
else:                       
    sub = 'sub-%s' % sys.argv[1]  

if 'win32' in sys.platform: path_root = opj('X:\\','SODIVA','SHARE')
else:                       path_root = opj(os.environ['HOME'],'SODIVA','SHARE')    

NEWBIDS = opj(path_root,'BIDS')
OutF = opj(NEWBIDS,'derivatives','RSAs','subs')
Path(OutF).mkdir(parents=True, exist_ok=True)# make if doesnt exist
GLMtype = 'EV1D_EVxEVBACK2D'
GLMfolder = opj(NEWBIDS,'derivatives','Univariate','First_lvls','RSA_GLM',GLMtype,sub)
Betas = sortinput(glob.glob(opj(GLMfolder,'beta*')))
connames = pd.read_csv(opj(GLMfolder,'regressornames.csv'))
connames=connames['Var1'].values.tolist()
Blocks = [a.split(' ')[-2][3] for a in connames]
connames = [a.split(' ')[-1].split('*')[0]+'_B'+b for a,b in zip(connames,Blocks)]
relbetas = list()
concont = list()
for beta,con in zip(Betas,connames):
    if con[0]=='R': continue
    if con[0:3]=='con': continue
    if con[0:3]=='Cue': continue
    if con[0:3]=='Out': continue
    if con[0:3]=='Wro': continue
    if con[0:3]=='NoA': continue
    if con[0:4]=='OneD': continue
    relbetas.append(beta)
    concont.append(con)
    
# ROI        
ROIsub =  opj(NEWBIDS,'derivatives','ROIs','T1w_func_vmPFC_'+sub+'.nii')
data_task = load_img(relbetas)
# if the image is now Beta x Voxels (3060 x 998) then: 
voxel_by_time = apply_mask(data_task,ROIsub)
# subtract the mean pattern, i.e. the mean across conditions for each voxel, from each response pattern 
# for each column reduce the mean
voxel_by_time = voxel_by_time-np.mean(voxel_by_time,axis=0)

# Covariance 
resis = np.sort(glob.glob(opj(GLMfolder,'ResMS*.nii')))
data_res = load_img(resis)
VARS = np.squeeze(apply_mask(data_res, ROIsub))
# if the image is now TR x Voxels (3060 x 998) then: 
if sum(VARS==0)>0: VARS[VARS==0]=0.00001 # making sure cant divide by 0
# save to after look at the range of values that its not too small
np.savetxt(opj(OutF,'COVS_'+GLMtype+'_'+sub+'.csv'), VARS, delimiter=',')
# DF for betas
A = [a.split('_') for a in concont]
DFlist = []
for a in A:
    if len(a)<=3:#'OneD':
       DFlist.append([a[0][:4]]+[a[0][4:]]+[np.nan]+a[1:])
    elif a[0]=='TwoD':
        a[1]=a[1].replace('EV','')
        a[2]=a[2].replace('EVBACK','')
        DFlist.append(a)
DF = pd.DataFrame(DFlist, columns = ['Dim', 'EV', 'EVBACK','Block'])
DF.loc[:,'OG']=concont
''' 
We correlate across all betas (across blocks)
Then for each we do Pearson Cosine and Euclidean (only using Euclidean later)
We also divide by residuals
'''

''' Across All '''
def getdist(indf,iscov=False):
    if iscov:
        addname='Cov'
    else: 
        addname=''
    corrM = indf.corr(method='pearson')
    cossM =pd.DataFrame(pairwise_distances(indf.values.transpose(),metric='cosine'),columns = corrM.columns,index=corrM.index)
    eucM =pd.DataFrame(pairwise_distances(indf.values.transpose(),metric='euclidean'),columns = corrM.columns,index=corrM.index)
    #malM =pd.DataFrame(pairwise_distances(indf.values.transpose(),metric='mahalanobis'),columns = corrM.columns,index=corrM.index)
    outDF = corrM.reset_index().melt(id_vars=['index'])
    outDF.columns = ['BETA1','BETA2','Corr'+addname]
    cossM2 = cossM.reset_index().melt(id_vars=['index'])
    eucM2 = eucM.reset_index().melt(id_vars=['index'])
    #malM2 = malM.reset_index().melt(id_vars=['index'])
    outDF.loc[:,'Coss'+addname] = cossM2['value']
    outDF.loc[:,'Euc'+addname] = eucM2['value']
    #FinalDF.loc[:,'Mal'+addname] = malM2['value']
    return outDF
    

FDF1 = getdist(pd.DataFrame(voxel_by_time.transpose(),columns=concont),iscov=False)
FDF2 = getdist(pd.DataFrame((voxel_by_time/VARS).transpose(),columns=concont),iscov=True)
FDF1.loc[:,['CorrCov','CossCov','EucCov']] = FDF2.loc[:,['CorrCov','CossCov','EucCov']]
FinalDF=FDF1.copy()

# set up rest of parametes 
for v in ['1','2']:
    FinalDF.loc[:,'EV'+v] = ''
    FinalDF.loc[:,'EVBACK'+v]= ''
    FinalDF.loc[:,'Block'+v]= ''
    FinalDF.loc[:,'DIM'+v]= ''
    for t in np.unique(FinalDF['BETA'+v]):
        FinalDF.loc[FinalDF['BETA'+v]==t,['EV'+v,'EVBACK'+v,'Block'+v,'DIM'+v]] = DF.loc[DF['OG']==t,['EV','EVBACK','Block','Dim']].values

FinalDF.loc[:,'sub']=sub
FinalDF.to_csv(opj(OutF,GLMtype+'_'+sub+'_CorrsByBlock.csv'),index=False)

