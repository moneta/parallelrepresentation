# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 14:02:49 2023

@author: moneta
"""

import glob 
import os
from os.path import join as opj
import sys
import pandas as pd
from pathlib import Path

if 'win32' in sys.platform: 
    sub = 'sub-14' #sub = 'sub-14' #sub = 'sub-01' 
else:                       
    sub = 'sub-%s' % sys.argv[1]  

if 'win32' in sys.platform: path_root = opj('X:\\','SODIVA','SHARE')
else:                       path_root = opj(os.environ['HOME'],'SODIVA','SHARE')    

NEWBIDS = opj(path_root,'BIDS')
InF = opj(NEWBIDS,'derivatives','RSAs','subs')
AllFiles = glob.glob(opj(InF,'*.csv'))
OutFolder = opj(NEWBIDS,'derivatives','RSAs','group')
Path(OutFolder).mkdir(parents=True, exist_ok=True)# make if doesnt exist
CBB = [a for a in AllFiles if 'CorrsByBlock.csv' in a.split('_')]
if len(CBB)!=35:
    print('ERROR CBB')
else:
    pd.concat([pd.read_csv(eve,sep=',') for eve in CBB], axis=0, ignore_index=True,sort=False).to_csv(opj(OutFolder,'RSA_CorrsByBlock.csv'),sep=',',index=False)
