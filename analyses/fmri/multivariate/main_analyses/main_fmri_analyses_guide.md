# Scripts & Preprocessed data for the analysis of Representations of context and context-dependent values in vmPFC compete for guiding behavior. Moneta Garvert Heekeren & Schuck , Narute Communications (2023)

# License 
Script and data are licensed under: Creative Commons Attribution-ShareAlike 4.0 International Public License (see LICENSE file)

# Guide for fMRI analysis scripts:
## decodability_analysis

**Step 0: Packages and setting helping functions**
load and install needed packages and set some helping functions that will help later
**Step 1: loading and organizing data, exclusion**
Note: here you can choose to load data you produced via earlier scripts or simply the RSAs we computed. 
Default is using what we used. 

Exclusion:
- We excluded one subject for bad OFC signal drop (more than 15% less voxels in functional compared to freesurfer OFC ROI from T1) (sub-18)
- we excluded one subject due to motion (sub-21)
- we excluded 3 subjects that performed less than 75% in one context in the main task. (sub 03, 28 and 34)

A few data frames are produced: 
- WDF: all 2D trials, stacked by probability assigned to each class (P2P30,50,70)
- Data: all behaviorally accurate trials, with P2Pcorrect as the probability assigned to the correct class
- Data1D: same as Data only 1D trials
- Data2D: same as Data only 2D trials
- DataAll/DataAll2D: same only has also wrong trials (excluding noanswer trials)
- DF_3class: same as Data only excluding when EV == EVBACK (for the Pev Pevback Pother analyses)

- anything with _avg indicates same as previously only averaged by the three values on screen (value_target, value_back_target and value_back_nontarget)

- DFevback1Rest: Decoding results of the EVBACK: trained on 2D tested on 2D
- DFvbdm2DRT: data (for correlations of effects)
**Step 2: Decoding accuracy and Design correlations**
2.1 design correlation: non of the parameter of interest correlates even when taking behaviorally accurate trials
2.2 Decoding accuracy of Value classifier
2.3 Decoding accuracy of Context classifier
2.4 plot the data
**Step 3: Probability to decode correct EV (Pev, Pevback, Pother)**
3.1 models: 
3.1.1 no nuisance regressors effect - we can average across them
3.1.2 hierarchical model comparison
3.1.3 Models for extended model comparison (SI)
3.1.4 Sanity check: same results when EV != EVback
3.1.5 Plots
3.1.5.1 plot model comparison
3.1.5.2 plot main effect EVBACK
3.1.5.3 plot main effect Pcontext
3.1.6 validate main comparison on raw data and plot 
3.1.7 validate main effects (EVBACK, Pctxt) when nested inside EV
3.1.8 Perceptual similarity EVBACK effect vs EVBACK feature on screen
3.1.9 No effect of EV==EVBACK (not higher nor lower decodability)
3.2 Where is the probability going? Pev, Pevback, Pother (from same value classifier)
3.2.1 basic design correlations (given EV!=EVBACK)
3.2.2 Pevback and Pother as predictors for Pev
3.2.2.1 sanity check: also works nested inside levels of EV and EVBACK
3.2.3 Correlation of (Pev,Pevback) VS (Pev,Pother)
3.3 Decoding directly EVBACK (trained on 2D and tested on 2D)
3.3.1 organize the DF, balanced accuracy test & correlation of Pevback and EVBACK
3.3.2 correlations in design as sanity
3.3.3 Models nested inside EVBACK: effects of Pctxt and Pctxt x Pevback on Pev
3.3.4 plots: Effects on Pev: Pcontext effect, Pcontext X Pevback2D
**Step 4: Link of MRI effects to Behavior**
4.1 effect of accuracy on Pevback (from value classifier)
4.2 Behavioral accuracy predicted by neural representation
4.2.1 Incongruent trials
4.2.2 Congruent trials
4.3 Correlations of behavioral effect and fMRI decodability effects
**Step 5: Value similarity analysis (Pushed to SI as RSA does the same)**
5.1 models of all three probabilities for each trial
5.1.1 no nuisance regressors effect - we can average across them
5.2 hierarchical model comparison
5.3 Perceptual similarity confound
5.4 Plot effects
5.4.1 value similarity across dimensions
5.4.2 value similarity model comparison
5.4.3 EVBACK effect on value similarity + nested models
5.4.4 value similarity model comparison - on unaveraged data (sanity check)
5.4.5 Median Pev is highest
5.4.6 Perceptual similarity EVBACK effect vs EVBACK feature on screen

## RSA analysis
step 0 : functions and load data
Note: here you can choose to load data you produced via earlier scripts or simply the RSAs we computed. 
Default is using what we used. 
Step 2: Plot the RSA data and predictions
Step 3: Models
 3.1 Data for the models
 3.2 simple T test 
 3.3 run the mixed effect models 
     Null model
     Diagonals: EVdiag and EVBACKdiag and their model comparison
     Only EVdiag = 0 then the effect of EVBACK (replicating classifier results)
     Value differences VDev and VDevback
     plots of effects 
     Extra correlation test on freq, EVBACKdiag and VDeback
save the stats
