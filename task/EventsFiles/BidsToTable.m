%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Tabeling all BE subjects %%%%%%%
% 
% 
% It generates one big table for all 

pilot = true;

if ~pilot
    DataPath = 'Z:\Data\SoDiva\MRI\Data_EventFiles';
    addpath('D:\Users\moneta\Google Drive\Git-Sodiva\SoDivaAnalysis\MRI_RT_Analysis');
else
    DataPath = 'Z:\Data\SoDiva\BehavioralOnly\BE910Data';
    addpath('D:\Google Drive\Git-Sodiva\SoDivaAnalysis\MRI_RT_Analysis');

end 


addpath(genpath(DataPath));
% find pilots
ParNames = dir(DataPath);
%Pilots(1:2) = [];
if ~pilot, ParNames(1:3) = []; else, ParNames(1:2) = []; end

for i = 1:numel(ParNames)
% Load all data to one table
% Staircasing
fileswithin.STR = dir(fullfile(DataPath,ParNames(i).name,'OutPut','Staircasing_Data_*.mat'));
% Value learning 
fileswithin.ValL = dir(fullfile(DataPath,ParNames(i).name,'OutPut','ValMapData_*_Everything.mat'));
% OneDMini
fileswithin.OneDmini = dir(fullfile(DataPath,ParNames(i).name,'OutPut','OneDMini*.mat'));
% VBDM
fileswithin.VBDM = dir(fullfile(DataPath,ParNames(i).name,'OutPut','VBDM_Data_*.mat'));

FN = fieldnames(fileswithin);

% FOR SUBJECTS 101 and 102 we need to take field TimeFixPostCueStart from
% results to ResOutput


        for f = 1:numel(FN)       % go through exp parts
            Time.(FN{f})(i) = 0;
            for b = 1:numel(fileswithin.(FN{f})) % go through blocks within the part 
                Temp = load(fullfile(DataPath,ParNames(i).name,'OutPut',fileswithin.(FN{f})(b).name));
                DatNam = fieldnames(Temp);
                if strcmp((FN{f}),'STR')
                Temp.(DatNam{1}).ResOutput = rmfield(Temp.(DatNam{1}).ResOutput,'fixControlKey');
                end       
                    switch FN{f}
                        case {'STR', 'OneDmini'}
                             if b == 1    
                                 OutTable{i}.(FN{f}) = struct2table(Temp.(DatNam{1}).ResOutput);
                             else
                                 OutTable{i}.(FN{f}) =  vertcat(OutTable{i}.(FN{f}), struct2table(Temp.(DatNam{1}).ResOutput));
                             end 
                        case 'ValL'
                            for V = 1:numel(Temp.ValMap.ResOutput)
                                if V == 1
                                OutTable{i}.(FN{f}) = struct2table(Temp.ValMap.ResOutput{1}); 
                                else
                                OutTable{i}.(FN{f}) =  vertcat(OutTable{i}.(FN{f}), struct2table(Temp.(DatNam{1}).ResOutput{V})); % go to ResOutput to the correct block    
                                end 
                            end 
                        case 'VBDM'
                                if b == 1    
                                 OutTable{i}.(FN{f}) = struct2table(Temp.(DatNam{1}).ResOutput);
                                 else
                                     OutTable{i}.(FN{f}) =  vertcat(OutTable{i}.(FN{f}), struct2table(Temp.(DatNam{1}).ResOutput));
                                end 
                            %end 
                    end % end switch
                    
                    switch FN{f}
                        case {'STR', 'OneDmini', 'VBDM'}
                           Time.(FN{f})(i) =  Time.(FN{f})(i) + (Temp.(DatNam{1}).ResOutput.TimeTrial(end) - Temp.(DatNam{1}).ResOutput.TimeTrial(1))/60;
                        case 'ValL'
                            ValLtime = 0;
                            for V2 = 1:numel(Temp.ValMap.ResOutput)
                                ValLtime = ValLtime +  (Temp.ValMap.ResOutput{V2}.TimeTrial(end) - Temp.(DatNam{1}).ResOutput{V2}.TimeTrial(1))/60;
                            end 
                           Time.(FN{f})(i) =  ValLtime;
                    end 
                    
                    
                    
                    
            end % end go throgh blocks (files)
            
            PartiName = [];
            [PartiName{1:size(OutTable{i}.(FN{f}),1)}] = deal(ParNames(i).name);
            OutTable{i}.(FN{f}).Participant = PartiName';
            OutTable{i}.(FN{f}) = OutTable{i}.(FN{f})(:,[end, 1:end-1]);

            ExpPart = [];
            [ExpPart{1:size(OutTable{i}.(FN{f}),1)}] = deal(FN{f});
            OutTable{i}.(FN{f}).ExpPart = ExpPart';
            OutTable{i}.(FN{f}) = OutTable{i}.(FN{f})(:,[end, 1:end-1]);
          
        end % end exp parts

        
end 
fntime = fieldnames(Time);
for fnt = 1:numel(fntime)
    Time.(fntime{fnt}) = Time.(fntime{fnt})';
end 

TimeTable = struct2table(Time);

for i = 1:numel(ParNames)

        if i == 1
            for f2 = 1:numel(FN)
                FullTable.(FN{f2}) = OutTable{i}.(FN{f2});
            end 
        else 
            for f2 = 1:numel(FN)
                FullTable.(FN{f2}) = vertcat(FullTable.(FN{f2}),OutTable{i}.(FN{f2}));
            end
        end
    
end 
%cd('C:\Users\moneta\Google Drive\Git-Sodiva\SoDivaAnalysis\MRI_RT_Analysis')
cd('D:\Google Drive\Git-Sodiva\SoDivaAnalysis\MRI_RT_Analysis')
if ~pilot
save('FullTableMRI.mat','FullTable')
else
save('FullTableBEHpilot.mat','FullTable')
end 


%% new make R table 

%% load Pilots 
    Tstr = FullTable.STR;
    Tval = FullTable.ValL;
    Toned = FullTable.OneDmini;
    Tvbdm = FullTable.VBDM;

    % Set pilot name 
    PilName= [];
    [PilName{1:size(Tstr,1)}] = deal('MR100');
    Tstr.Pilot  = PilName';
    PilName= [];
    [PilName{1:size(Tval,1)}] = deal('MR100');
    Tval.Pilot  = PilName';
    PilName= [];
    [PilName{1:size(Toned,1)}] = deal('MR100');
    Toned.Pilot  = PilName';
    PilName= [];
    [PilName{1:size(Tvbdm,1)}] = deal('MR100');
    Tvbdm.Pilot  = PilName';

%% make one big table of all pilots per exp part

FT.STR = Tstr;
FT.ValL = Tval;
FT.OneD = Toned;
FT.VBDM = Tvbdm;

ExpPart = fieldnames(FT);
for ft = 1:numel(ExpPart)
    if ft ~= 3
    ExpP= [];
    [ExpP{1:size(FT.(ExpPart{ft}),1)}] = deal(ExpPart{ft});
    FT.(ExpPart{ft}).ExpP = ExpP';
    else % just for OneD 
     %  ParNum1D =  height(FT.(ExpPart{ft}))/120;
     ExpP = [];
     ExpP = repmat([repmat('OneD1',[60,1]);repmat('OneD2',[60,1])],[height(FT.(ExpPart{ft}))/120,1]);
     FT.(ExpPart{ft}).ExpP = cellstr(ExpP);
    end 
end

% reverse get the STR back values
% per participant get value map for 1-2-3-4 each context
ParNames = unique(FT.STR.Participant);
% get val map per paticipant
ValMap = zeros(4,2,numel(ParNames));
FT.STR.BackRightVal = zeros(size(FT.STR.designValueTarget));
FT.STR.BackLeftVal = zeros(size(FT.STR.designValueTarget));
for i = 1:numel(ParNames)
    for m = 1:4
        ValMap(m,1,i) = FT.STR.designValueTarget(find(strcmp(FT.STR.Participant,ParNames{i}) & FT.STR.TargetStimMotionDirection==(m-1)*45 & strcmp(FT.STR.Context,'Motion'),1));
        ValMap(m,2,i) = FT.STR.designValueTarget(find(strcmp(FT.STR.Participant,ParNames{i}) & strcmp(FT.STR.TargetStimColorType,strcat('c',num2str(m))) & strcmp(FT.STR.Context,'Color'),1));
    end 
    isSTR = strcmp(FT.STR.ExpPart,'STR');
    isPar = strcmp(FT.STR.Participant,ParNames{i});
    isColor = strcmp(FT.STR.Context,'Color');
    is2D = ~(strcmp(FT.STR.TargetStimColorType,'c5') | FT.STR.TargetStimMotionDirection==1000);
    
    for v = 1:4
        Motion =  (find(ValMap(:,1,i)==v)-1)*45;
        Color  =  strcat('c',num2str(find(ValMap(:,2,i)==v)));
        % in motion, when color is on right/left
    FT.STR.BackRightVal(isSTR & isPar & ~isColor & is2D & strcmp(FT.STR.RightStimColorType,Color)) = v;
    FT.STR.BackRightVal(isSTR & isPar & isColor & is2D & FT.STR.RightStimMotionDirection==Motion) = v;
    FT.STR.BackLeftVal(isSTR & isPar & ~isColor & is2D & strcmp(FT.STR.LeftStimColorType,Color)) = v;
    FT.STR.BackLeftVal(isSTR & isPar & isColor & is2D & FT.STR.LeftStimMotionDirection==Motion) = v;
    end 
    % when context color, 2D, STR, participant  
end 

%% Make one big table
VN = {'Pilot', 'ExpP', 'ID', 'Block', 'Trial', 'response', 'RT',...
    'Context', 'OneD', 'TwoD','Cond',...
    'Val_Tgt', 'Val_L', 'Val_R' , 'BVal_Tgt','BVal_L','BVal_R'...
    'TgtCol', 'TgtMot', 'TgtColSpe', 'TgtMotCoh'};

Rtable = table;
VN_Easy = {'Pilot', 'ExpP', 'Participant', 'Block', 'Trial', 'response', 'firstRT',  'Context',...    
    'designValueTarget', 'designValueLeft', 'designValueRight',...
    'TargetStimColorType', 'TargetStimMotionDirection'};

VN_New = {'Pilot', 'ExpP', 'ID', 'Block', 'Trial', 'response', 'RT', 'Context',...    
    'Val_Tgt', 'Val_L', 'Val_R',...
    'TgtCol', 'TgtMot'};

% for ValLearningContext
ValLContext = zeros(length(FT.ValL.designContextLeft),1);
ValLContext(FT.ValL.designContextLeft~=FT.ValL.designContextRight)= 3;
ValLContext(FT.ValL.designForcedChoice==1)= 4;
ValLContext(FT.ValL.designContextLeft==FT.ValL.designContextRight & FT.ValL.designForcedChoice~=1) = FT.ValL.designContextLeft((FT.ValL.designContextLeft==FT.ValL.designContextRight& FT.ValL.designForcedChoice~=1));
FT.ValL.Context = FT.ValL.ExpPart;
[FT.ValL.Context(ValLContext==1)] = deal({'Motion'});
[FT.ValL.Context(ValLContext==2)] = deal({'Color'});
[FT.ValL.Context(ValLContext==3)] = deal({'Mixed'});
[FT.ValL.Context(ValLContext==4)] = deal({'Forced'});
%sum(strcmp(FT.ValL.Context,'ValL'))
%sum(strcmp(FT.ValL.Context,'Motion'))

for r = 1:numel(VN_New)
Rtable = addVar2Table(Rtable,FT,VN_Easy{r},VN_New{r},ExpPart);
end 

%Rtable.Properties.VariableNames
% now the ocmplicated ones: 
Rtable.OneD = zeros(height(Rtable),1);
Rtable.OneD(strcmp(Rtable.TgtCol,'c5')|Rtable.TgtMot == 1000) = 1;
Rtable.TwoD = zeros(height(Rtable),1);
Rtable.TwoD(Rtable.OneD==0) = 1;

% back values 
% in ValL  and in OneD miniit stays 0

Rtable.BackRightVal = zeros(height(Rtable),1);
Rtable.BackRightVal(strcmp(Rtable.ExpP,'VBDM'))= FT.VBDM.designValueBackRight;
Rtable.BackRightVal(strcmp(Rtable.ExpP,'STR'))= FT.STR.BackRightVal;


Rtable.BackLeftVal = zeros(height(Rtable),1);
Rtable.BackLeftVal(strcmp(Rtable.ExpP,'VBDM'))= FT.VBDM.designValueBackLeft;
Rtable.BackLeftVal(strcmp(Rtable.ExpP,'STR'))= FT.STR.BackLeftVal;

TgtOnR = Rtable.Val_Tgt==Rtable.Val_R;
Rtable.BackTgtVal =  zeros(height(Rtable),1);
Rtable.BackTgtVal(TgtOnR) = Rtable.BackRightVal(TgtOnR);
Rtable.BackTgtVal(~TgtOnR) = Rtable.BackLeftVal(~TgtOnR);

Rtable.BacknonTgtVal =  zeros(height(Rtable),1);
Rtable.BacknonTgtVal(TgtOnR) = Rtable.BackLeftVal(TgtOnR);
Rtable.BacknonTgtVal(~TgtOnR) = Rtable.BackRightVal(~TgtOnR);


Rtable.ExpValBack = max(Rtable.BackLeftVal,Rtable.BackRightVal);


Rtable.Cond = Rtable.ExpP;
CondVec = zeros(height(Rtable),1);
CondVec((Rtable.Val_R < Rtable.Val_L & Rtable.BackRightVal < Rtable.BackLeftVal) | (Rtable.Val_R > Rtable.Val_L & Rtable.BackRightVal > Rtable.BackLeftVal))= 1; % cong
CondVec((Rtable.Val_R < Rtable.Val_L & Rtable.BackRightVal > Rtable.BackLeftVal) | (Rtable.Val_R > Rtable.Val_L & Rtable.BackRightVal < Rtable.BackLeftVal))= 2; % Incong
CondVec((Rtable.BackRightVal == Rtable.BackLeftVal)& Rtable.BackLeftVal~=0 )= 3; % Passive
CondVec((Rtable.OneD==1)) = 4; % OneD
[Rtable.Cond(CondVec==0)] = deal({'NotAvail'});
[Rtable.Cond(CondVec==1)] = deal({'Congruent'});
[Rtable.Cond(CondVec==2)] = deal({'Incongruent'});
[Rtable.Cond(CondVec==3)] = deal({'Passive'});
[Rtable.Cond(CondVec==4)] = deal({'OneD'});

Rtable.CondSz = zeros(size(Rtable.BackTgtVal));
CondSz = abs(Rtable.BackRightVal - Rtable.BackLeftVal);
Rtable.CondSz(strcmp(Rtable.Cond,'Incongruent')) = CondSz(strcmp(Rtable.Cond,'Incongruent'))*-1;
Rtable.CondSz(strcmp(Rtable.Cond,'Congruent')) = CondSz(strcmp(Rtable.Cond,'Congruent'));


%     'TgtColSpe', 'TgtMotCoh' - only in STR part
Rtable.TgtColSpe = zeros(height(Rtable),1);
Rtable.TgtColSpe(strcmp(Rtable.ExpP,'STR'))= FT.STR.TargetStimColorSpeed;

Rtable.TgtMotCoh = zeros(height(Rtable),1);
Rtable.TgtMotCoh(strcmp(Rtable.ExpP,'STR'))= FT.STR.TargetStimMotionCoherence;

% switch vector

SwV =  zeros(height(Rtable),1);
count = 1;
% Par = Rtable.ParticipantName{1};
% Block = Rtable.Block(1);
SwV(1) = 1;
for i = 2:length(SwV)
    % if same block same participant same context - count goes up. else it
    % is 0. 
    if (strcmp(Rtable.ID{i},Rtable.ID{i-1}))&&(Rtable.Block(i)==Rtable.Block(i-1))&& strcmp(Rtable.Context(i),Rtable.Context(i-1))&&Rtable.Trial(i)~=1
        count = count + 1;
    else 
        count = 1;
    end 
    
    SwV(i) = count; 
    
end 

Rtable.SwitchVec = SwV;



%%%% Z scoring 

%zscore(x(~isnan(x)))

SubN = unique(Rtable.ID);
Rtable.RTz = zeros(height(Rtable),1);
for s = 1:numel(SubN)
    IdxZ{s} = (strcmp(Rtable.ID,SubN{s}) & strcmp(Rtable.ExpP,'VBDM') & ~isnan(Rtable.RT) & Rtable.RT>0 );
    RTs{s} = Rtable.RT(IdxZ{s});
    Rtable.RTz(IdxZ{s}) = zscore(RTs{s});
end 


% change in BE100 the participant VB106
%[Rtable.ID(strcmp(Rtable.ID,'VB106')& strcmp(Rtable.Pilot,'BE100'))] = deal({'BE106'});

%cd('C:\Users\moneta\Google Drive\SoDivaAnalysis\FinalAnalysis')
%cd('C:\Users\moneta\Google Drive\Git-Sodiva\SoDivaAnalysis\MRI_RT_Analysis')
cd('D:\Google Drive\Git-Sodiva\SoDivaAnalysis\MRI_RT_Analysis')
if ~pilot
save('FinalExperiment_MRI.mat','Rtable')
writetable(Rtable,'FinalExperiment_MRI.txt','Delimiter',' ');
writetable(TimeTable,'TimeForEachPart_MRI.txt','Delimiter',' ');
else
save('FinalExperiment_BEH.mat','Rtable')
writetable(Rtable,'FinalExperiment_BEH.txt','Delimiter',' ');
writetable(TimeTable,'TimeForEachPart_BEH.txt','Delimiter',' ');
end 




% FOR SUBJECTS 101 and 102 we need to take field TimeFixPostCueStart from
% results to ResOutput - maybe BE100? BE200?
