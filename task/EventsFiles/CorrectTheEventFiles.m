% correct the event files 
% DELETE THE OLD ONES FIRST! ! ! 
EventFileLoc = fullfile('X:\SODIVA\BIDS\derivatives\EventsFiles');

files = dir(fullfile(EventFileLoc,'\*\*_events.tsv'));

NewEventFiles = fullfile('X:\SODIVA\BIDS\derivatives\EventsFiles_Corrected');

TestingIm = zeros(numel(files),108);
WrongOnce = [];
for f = 1:length(files)
    events = readtable(fullfile(files(f).folder,files(f).name), 'FileType', 'text', 'Delimiter', 'tab');
    % Changing negative reation time to be no response (meaning they
    % answered before the stim was on screen)
    NegRT = events.response_time<=0;
    events.noanswer(NegRT,:) = 1;
    events.accuracy(NegRT,:) = 0;
    events.wrong(NegRT,:)    = 0; 
    % Set the RT in case of non-reponse to be zero (becasue its a pmod now)
    events.response_time(events.noanswer==1)      =  0;
    
    % make additional columns for the events:
    events.TwoD = double(events.OneD ==0);       
    events.Condition = zeros(height(events),1);
    events.Condition(events.Congruent==1) = 1;
    events.Condition(events.Incongruent==1) = -1;
        
    
    % Make OneD and TwoD only accurate trials
%     events.OneD(events.accuracy==0) = 0;
%     events.TwoD(events.accuracy==0) = 0;
%     
    
%     % For this specific GLM: 
%     events.PmodCongruent = zeros(height(events),1);
%     events.PmodCongruent(events.Congruent == 1) = events.CondSz(events.Congruent == 1);
%     events.PmodIncongruent = zeros(height(events),1);
%     events.PmodIncongruent(events.Incongruent == 1) = abs(events.CondSz(events.Incongruent == 1));

    % Context if needed
    events.Color = zeros(height(events),1);
    events.Color(strcmp(events.Context,'Color')) = 1;
    events.Motion = zeros(height(events),1);
    events.Motion(strcmp(events.Context,'Motion')) = 1;

    events.EVBACK = max(events.value_back_target,events.value_back_nontarget);
    events.EVBACKSigned = events.EVBACK.*events.Condition;
    % PEs : 
    events.PECond = zeros(height(events),1);
    events.PECond(events.Congruent==1) = 1;
    events.PECond(events.Incongruent==1) = -1;

    events.PEBackTgt = zeros(height(events),1);
    events.PEBackTgt(events.value_target < events.value_back_target) = -1;
    events.PEBackTgt(events.value_target > events.value_back_target) = 1;

    events.PEEVback = zeros(height(events),1);
    events.PEEVback(events.value_target < events.EVBACK) = -1;
    events.PEEVback(events.value_target > events.EVBACK) = 1;
    events.PEEVback(events.value_target == events.EVBACK) = 0;
    
    % old ones:
    events.BackDiff = zeros(height(events),1);
    events.BackDiff = abs(events.value_back_target - events.value_back_nontarget);
    
    events.BackSum  = zeros(height(events),1);
    events.BackSum  = events.value_back_target + events.value_back_nontarget;
    
    events.BackSumTDiff = zeros(height(events),1);
    events.BackSumTDiff = events.BackDiff.*events.BackSum;

    events.BackDiffSigned     = events.BackDiff.*events.Condition;
    events.BackSumSigned      = events.BackSum.*events.Condition;
    events.BackSumTDiffSigned = events.BackSumTDiff.*events.Condition;

    events.StimDuration = events.stim_end - events.stim_onset;
    
    % Log the RT
    events.LOGresponse_time = log(events.response_time);
    % Zscore the log Rt
    
    % make into actual original values:
    events.value_target = events.value_target.*20 - 10;
    TwoDInd = events.TwoD==1;
    events.EVBACK(TwoDInd) = events.EVBACK(TwoDInd).*20 - 10;
    events.EVBACKSigned = events.EVBACK.*events.Condition;
    %events{rcol}.EVBACKSigned(TwoDInd) = events{rcol}.EVBACKSigned(TwoDInd).*20 - 10;
    events.BackDiff(TwoDInd) = events.BackDiff(TwoDInd).*20;
    events.BackSum(TwoDInd)   = events.BackSum(TwoDInd).*20 - 20;
    
    
    EventNewFileCSV = fullfile(NewEventFiles,strcat('Corrected_',files(f).name(1:end-4),'.csv'));
    EventNewFileTSV = fullfile(NewEventFiles,strcat('Corrected_',files(f).name(1:end-4),'.tsv'));
    
     writetable(events, EventNewFileCSV ,'Delimiter', '\t');
     copyfile(EventNewFileCSV, EventNewFileTSV);
     delete(EventNewFileCSV);

     TestingIm(f,:) = (events.OneD+events.TwoD+events.wrong+events.noanswer)';
    % which subjects have only one time wrong in one of the blocks? 
    if sum(events.wrong)==1
        WrongOnce = [WrongOnce,f];
    end 
end 

files(WrongOnce).name


imagesc(TestingIm)
find(TestingIm~=1)

% for behavioral pilot: Z:\Data\SoDiva\BehavioralOnly\BE910Data

SubFloc = fullfile('Z:\Data\SoDiva\BehavioralOnly\BE910Data');

files = dir(fullfile(SubFloc,'\*\General_Info_Sub*.mat'));

for f = 1:length(files)
    SubInf{f} = load(fullfile(files(f).folder,files(f).name));
end 

ages = [] ;
gender = {};
for f = 1:length(files)
ages = [ages ;SubInf{f}.General_Info.subjectInfo.age];
gender = [gender;SubInf{f}.General_Info.subjectInfo.gender];
end 
mean(ages)
std(ages)
sum(strcmp(gender,'f'))

%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% A{1} = readtable(fullfile(NewEventFiles,strcat('Corrected_',files(17).name(1:end-4),'.tsv')), 'FileType', 'text', 'Delimiter', 'tab');
% A{2} = readtable(fullfile(NewEventFiles,strcat('Corrected_',files(18).name(1:end-4),'.tsv')), 'FileType', 'text', 'Delimiter', 'tab');
% A{3} = readtable(fullfile(NewEventFiles,strcat('Corrected_',files(19).name(1:end-4),'.tsv')), 'FileType', 'text', 'Delimiter', 'tab');
% A{4} = readtable(fullfile(NewEventFiles,strcat('Corrected_',files(20).name(1:end-4),'.tsv')), 'FileType', 'text', 'Delimiter', 'tab');
% 
% B = [A{1};A{2};A{3};A{4}];
% B.accuracy + B.wrong + B.noanswer;
% B.OneD + B.TwoD+ B.wrong +B.noanswer;
% 
% Prob = readtable(fullfile(NewEventFiles,strcat('Corrected_',files(17).name(1:end-4),'.tsv')), 'FileType', 'text', 'Delimiter', 'tab');
% 
% TestIm = [Prob.OneD,Prob.TwoD,Prob.wrong,Prob.noanswer];
% imagesc(TestIm)
% 
% sub = 'sub-05';
%  for rcol = 1:4
%         eventsfile = dir(fullfile(NewEventFiles,['Corrected_',sub,'_task-sodiva_rec-prenorm_run-0' num2str(rcol) '_events.tsv'])); % this has 2 options that are the same 
%         events{rcol} = readtable(fullfile(eventsfile(1).folder,eventsfile(1).name), 'FileType', 'text', 'Delimiter', 'tab');
%  end 
% 
%  Conditions.name             = {'OneD', 'TwoD', 'Wrong','NoResponse'};
%  Conditions.onset_eventfield = {'OneD', 'TwoD', 'wrong','noanswer'};
%      
%     % DEFINE THE GLM:
%     job = [];
% ScndPmod = {'Condition'};
% i = 1;
% for r = 1:4
%     c = 1;
%     % OneD
%     job{r}{c}.name = 'OneD';
%     cidx = events{r}.OneD ==1;  
%     job{r}{c}.onset = (events{r}.stim_onset(cidx)); % condition onsets
%     job{r}{c}.duration = (events{r}.StimDuration(cidx)); 
%     job{r}{c}.tmod     = 0;            % no temporal modulation 
%     job{r}{c}.orth     = 0;            % orthogonalization of potential parametric regressors (no)
%     job{r}{c}.pmod(1)  = struct('name', {'RT'}, 'param', {(events{r}.response_time(cidx))}, 'poly', {1}); %  parametric modulation       
%     job{r}{c}.pmod(2)  = struct('name', {strcat('MainVal-param',num2str(c))}, 'param', {(events{r}.value_target(cidx))}, 'poly', {1}); %  parametric modulation       
%     c = c + 1;
% 
%     % TwoD
%     job{r}{c}.name = 'TwoD';
%     cidx = events{r}.TwoD ==1;  
%     job{r}{c}.onset = (events{r}.stim_onset(cidx)); % condition onsets
%     job{r}{c}.duration = (events{r}.StimDuration(cidx)); 
%     job{r}{c}.tmod     = 0;            % no temporal modulation 
%     job{r}{c}.orth     = 0;            % orthogonalization of potential parametric regressors (no)
%     job{r}{c}.pmod(1)  = struct('name', {'RT'}, 'param', {(events{r}.response_time(cidx))}, 'poly', {1}); %  parametric modulation       
%     job{r}{c}.pmod(2)  = struct('name', {strcat('MainVal-param',num2str(c))}, 'param', {(events{r}.value_target(cidx))}, 'poly', {1}); %  parametric modulation       
%     job{r}{c}.pmod(3)  = struct('name', {ScndPmod{i}}, 'param', {(events{r}.(ScndPmod{i})(cidx))}, 'poly', {1}); %  parametric modulation       
%     c = c + 1;
% 
%     cidx = events{r}.wrong==1;                               % index matrix of condition name string occurrences
%     if sum(cidx)~=0 % at least one single case
%         job{r}{c}.name = 'Wrong';    % condition label
%         job{r}{c}.onset = (events{r}.stim_onset(cidx)); % condition onsets
%         job{r}{c}.duration = (events{r}.StimDuration(cidx));  % condition duration (0 = "event-related" analysis)  
%         job{r}{c}.tmod     = 0;            % no temporal modulation 
%         job{r}{c}.orth     = 0;            % orthogonalization of potential parametric regressors (yes)
%         job{r}{c}.pmod(1)  = struct('name', {'RT'}, 'param', {(events{r}.response_time(cidx))}, 'poly', {1}); %  parametric modulation       
%         c = c + 1;
%     end
% 
%     cidx = events{r}.noanswer==1;                               % index matrix of condition name string occurrences
%     if sum(cidx)~=0 % at least one single case
%         job{r}{c}.name = 'noanswer';    % condition label
%         job{r}{c}.onset = (events{r}.stim_onset(cidx)); % condition onsets
%         job{r}{c}.duration = (events{r}.StimDuration(cidx));  % condition duration (0 = "event-related" analysis)  
%         job{r}{c}.tmod     = 0;            % no temporal modulation 
%         job{r}{c}.orth     = 0;            % orthogonalization of potential parametric regressors (yes)
%         job{r}{c}.pmod = struct('name', {}, 'param', {}, 'poly', {}); % no parametric modulation     
%         c = c + 1;
%     end 
% 
%     % Adding stick funciton on Cue and on Outcome )no Pmod(
%     job{r}{c}.name = 'Cue';    % condition label
%     %cidx = events{r}.(Conditions.onset_eventfield{c})==1;                               % index matrix of condition name string occurrences
%     job{r}{c}.onset = (events{r}.cue_onset); % condition onsets
%     % additional (potentially) condition-specific parameters
%     job{r}{c}.duration = 0;            % condition duration (0 = "event-related" analysis)  
%     job{r}{c}.tmod     = 0;            % no temporal modulation 
%     job{r}{c}.orth     = 0;            % orthogonalization of potential parametric regressors (yes)
%     job{r}{c}.pmod = struct('name', {}, 'param', {}, 'poly', {}); % no parametric modulation  
%     c = c + 1;
% 
%     job{r}{c}.name = 'Outcome';    % condition label
%     %cidx = events{r}.(Conditions.onset_eventfield{c})==1;                               % index matrix of condition name string occurrences
%     job{r}{c}.onset = (events{r}.outcome_onset); % condition onsets
%     % additional (potentially) condition-specific parameters
%     job{r}{c}.duration = 0;            % condition duration (0 = "event-related" analysis)  
%     job{r}{c}.tmod     = 0;            % no temporal modulation 
%     job{r}{c}.orth     = 0;            % orthogonalization of potential parametric regressors (no)
%     job{r}{c}.pmod = struct('name', {}, 'param', {}, 'poly', {}); % no parametric modulation  
% 
%     % run specific regressors - motion only for now
%   %  job{r}{c}.spm.stats.fmri_spec.sess(r).multi_reg = {fullfile(outputGLM,['Run',num2str(r),'_MotionReg.mat'])};
% end 
% 
% % r = 1;
% % TestIm = false(108,4*numel(job{r}));
% % for r = 1:4
% %     TestIm{r} = false(108,numel(job{r}));
% %     for c = 1:numel(job{r})
% %         idx = events{r}.stim_onset;
% %         TestIm(:,cou) = ismember(idx,job{r}{c}.onset);
% %         cou  = cou +1;
% %     end 
% % end 
% r = 3;
% TestIm = false(108,numel(job{r}));
% for c = 1:numel(job{r})
%     idx = events{r}.stim_onset;
%     TestIm(:,c) = ismember(idx,job{r}{c}.onset);
% end 
% 
% imagesc(TestIm)
% imagesc(SPM.xX.xKXs.X)
% 
% Estim = spm_SpUtil('isCon',SPM.xX.xKXs.X);