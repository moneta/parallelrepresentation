function Generate_Event_filesm
clc
clear all

Sub2Conv = importdata('X:\SODIVA\Scripts\sublists\sublist_New_ToConv.txt');

%% Data location specifications
% -------------------------------------------------------------------------
% data source root directory
src_event_dir = 'Z:\Data\SoDiva\MRI\Data_EventFiles';
% data target root directory
% tgt_event_dir = 'Z:\Data\SoDiva\MRI\BIDS\EventFiles';
%tgt_event_dir = 'Z:\Data\SoDiva\MRI\BIDS\bidssource';
tgt_event_dir = 'X:\SODIVA\BIDS\derivatives\EventsFiles';

% after BIDS ran, to change the files
SaveNewlocation = true;
% subject specific directory
sub_dir = dir(src_event_dir);
% skipping the first MR101 and MR102, they are different sequences
sub_dir(1:5) = [];

%% run subjects
for sub = 1:numel(sub_dir)
     
    if sum(double(sub==Sub2Conv))~=0 % if the subject is in the list to convert
    
fprintf('Starting subject %s \n', num2str(sub))

    if sub <10
        srdr = strcat('sub-0', num2str(sub));
    else
        srdr = strcat('sub-', num2str(sub));
    end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if SaveNewlocation
        % Here we need to change to ../func 
    %sub_event_dir = fullfile(tgt_event_dir,srdr,'func');
    sub_event_dir = fullfile(tgt_event_dir,srdr);
    if ~exist(sub_event_dir,'dir')
     mkdir(sub_event_dir)
    end
   
else 
%     sub_event_dir = fullfile(tgt_event_dir,'EventFiles',srdr);
%     if ~exist(sub_event_dir,'dir')
%         mkdir(sub_event_dir)
%     end 
end 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% run runs
    for r = 1:4
        if strcmp(sub_dir(sub).name,'MR134')
            run_event = load(fullfile(sub_dir(sub).folder,sub_dir(sub).name,'OutPut',['VBDM_Data_Mr134_Block_',num2str(r),'.mat']));
        else
            run_event = load(fullfile(sub_dir(sub).folder,sub_dir(sub).name,'OutPut',['VBDM_Data_',sub_dir(sub).name,'_Block_',num2str(r),'.mat']));
        end 
    

        A = run_event.Main_VBDM.SodivaMain.Triggers(1);

        EvT = struct2table(run_event.Main_VBDM.ResOutput);

        T = table;

        T.cue_onset       = EvT.TimeCueStart    - A;
        T.cue_end         = EvT.TimeFixPostCueStart -A;
        T.stim_onset      = EvT.TimeStimStarts  -A;
        T.stim_end        = EvT.TimePostStimFix  -A;
        T.outcome_onset   = EvT.TimeOutcome  -A;
        T.outcome_end     = EvT.TimePostOutcomeFix    -A;
        T.accuracy        = double(EvT.response==1);
        T.wrong           = double(EvT.response==2);
        T.noanswer        = double(EvT.response==3);
        T.response_time   = EvT.firstRT;
        T.value_target    = EvT.designValueTarget;
        T.value_chosen    = EvT.designValueChoosen;
        T.OneD            = double(EvT.designValueBackLeft==0);
        T.Block           = repmat([r],height(T),1);
        T.Trial           = [1:height(T)]';
        T.Context         = EvT.Context;
        T.RightSideCorrect= double(strcmp(EvT.SideCorrect,'Right'));
        
        TgtOnR = EvT.designValueTarget==EvT.designValueRight;
        T.value_nontarget = zeros(height(T),1);
        T.value_nontarget(TgtOnR) = EvT.designValueLeft(TgtOnR); % when tgt on right, take the main value on left
        T.value_nontarget(~TgtOnR) = EvT.designValueRight(~TgtOnR);

        T.value_back_target = zeros(height(T),1);
        T.value_back_target(TgtOnR) = EvT.designValueBackRight(TgtOnR); % when tgt on right, take the main value on left
        T.value_back_target(~TgtOnR) = EvT.designValueBackLeft(~TgtOnR);

        T.value_back_nontarget = zeros(height(T),1);
        T.value_back_nontarget(TgtOnR) = EvT.designValueBackLeft(TgtOnR); % when tgt on right, take the main value on left
        T.value_back_nontarget(~TgtOnR) = EvT.designValueBackRight(~TgtOnR);

        %EvT.Cond = EvT.SideCorrect;
        CondVec = zeros(height(T),1);
        CondVec((EvT.designValueRight <  EvT.designValueLeft & EvT.designValueBackRight < EvT.designValueBackLeft) | (EvT.designValueRight >  EvT.designValueLeft & EvT.designValueBackRight > EvT.designValueBackLeft))= 1; % cong
        CondVec((EvT.designValueRight <  EvT.designValueLeft & EvT.designValueBackRight > EvT.designValueBackLeft) | (EvT.designValueRight >  EvT.designValueLeft & EvT.designValueBackRight < EvT.designValueBackLeft))= 2; % Incong
%         [Rtable.Cond(CondVec==1)] = deal({'Congruent'});
%         [Rtable.Cond(CondVec==2)] = deal({'Incongruent'});
        T.Congruent       = double(CondVec == 1);
        T.Incongruent     = double(CondVec == 2);
        T.CondSz          = abs(EvT.designValueBackRight - EvT.designValueBackLeft);
        T.CondSz(logical(T.Incongruent)) = T.CondSz(logical(T.Incongruent))*-1;
        
        
        
        SwV = zeros(height(T),1);
        count = 1;
        SwV(1) = 1;
    for i = 2:length(SwV)
        if strcmp(EvT.Context(i),EvT.Context(i-1))
            count = count + 1;
        else 
            count = 1;
        end 
        SwV(i) = count;    
    end 
        T.switch_vector = SwV;
        name2save = srdr;
%         if KeepOriginalnames
%             name2save = sub_dir(sub).name;
%             if strcmp(sub_dir(sub).name,'MR103')
%                 name2save = 'MRI03';
%             end 
%         else 
%             name2save = srdr;
%         end 
   
%     csv_fileloc = fullfile(sub_event_dir, [name2save '_task-sodiva_rec-nonorm_run-0' num2str(r) '_events.csv']);
%     tsv_fileloc = fullfile(sub_event_dir, [name2save '_task-sodiva_rec-nonorm_run-0' num2str(r) '_events.tsv']);
%     % write it out as .csv file
%     % ------ ------ ------ ------
%     writetable(T, csv_fileloc ,'Delimiter', '\t');
%     % copy to tsv, delete csv - this methos was recommended by Yasmin. 
%     copyfile(csv_fileloc, tsv_fileloc);
%     delete(csv_fileloc);
    
    csv_fileloc = fullfile(sub_event_dir, [name2save '_task-sodiva_rec-prenorm_run-0' num2str(r) '_events.csv']);
    tsv_fileloc = fullfile(sub_event_dir, [name2save '_task-sodiva_rec-prenorm_run-0' num2str(r) '_events.tsv']);
    % write it out as .csv file
    % ------ ------ ------ ------
    writetable(T, csv_fileloc ,'Delimiter', '\t');
    % copy to tsv, delete csv - this methos was recommended by Yasmin. 
    copyfile(csv_fileloc, tsv_fileloc);
    delete(csv_fileloc);
    end % ending run
    
    end % end if statement on the subjects to convert    
    
end % ending sub 


end % end function