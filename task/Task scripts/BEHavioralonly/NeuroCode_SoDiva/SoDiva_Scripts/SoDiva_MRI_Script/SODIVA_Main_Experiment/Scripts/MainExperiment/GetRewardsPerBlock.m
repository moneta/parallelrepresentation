function [Reward, Kredite] = GetRewardsPerBlock(Main_VBDM,Rscheme,Phase)
% Get sum of points per block
% Phase is to determine if we are in 'MRI' or in 'BEH' (behavioral)
switch Rscheme
    case 'MRI'
        PPE = 600; % points per euro
%     case 'BEH'
%         PPE = 850; 
%     case 'Pilot'
%         PPE = 450;  
end 

    Kredite = 0;
    %ResTable = struct2table(Main_VBDM.ResOutput);
    ResTableAcc = Main_VBDM.ResOutput.response;
  %  Correct = ResTable{:,7}==1;
    Correct = ResTableAcc==1;
    fprintf('\n in this block participant was %.3f correct \n',sum(Correct)/length(Correct));
    
    
    switch Phase
        case 'first'
    for t = 1:size(Main_VBDM.ResOutput.firstKey,1)
        switch Main_VBDM.ResOutput.firstKey{t} % first response key. 
            case {'RightArrow'} % right
                Rew = 10 + (Main_VBDM.ResOutput.designValueRight(t)-1)*20;
                Kredite = Kredite + Rew; 
            case {'LeftArrow'} % left
                Rew = 10 + (Main_VBDM.ResOutput.designValueLeft(t)-1)*20;
                Kredite = Kredite + Rew;
        end    
    end
        case 'second'
    for t = 1:size(Main_VBDM.ResOutput.firstKey,1)
        switch Main_VBDM.ResOutput.firstKey{t} % first response key. 
            case {'b'} % right
                Rew = 10 + (Main_VBDM.ResOutput.designValueRight(t)-1)*20;
                Kredite = Kredite + Rew; 
            case {'g'} % left
                Rew = 10 + (Main_VBDM.ResOutput.designValueLeft(t)-1)*20;
                Kredite = Kredite + Rew;
        end    
    end       
            
    end 
    
    clear Main_VBDM ResTable

% Sum up and display on screen/output

% for bb=1:6
% fprintf('\n In %d. Block , %d Kredite \n',bb,Kredite(bb));
% end 
fprintf('\n Participant made %d points \n', Kredite);

Reward = (Kredite/PPE);
fprintf(' And he should get for it: %.2f Euro \n',Reward);

end 
