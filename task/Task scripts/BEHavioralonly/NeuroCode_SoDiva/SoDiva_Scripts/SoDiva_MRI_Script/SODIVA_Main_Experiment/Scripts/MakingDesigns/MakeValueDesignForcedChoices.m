function Design = MakeValueDesignForcedChoices

% Output:

%Design : 80 trials of forced choices presented in both sides
%Design.PartMix : 70 trials of mixed choices


%% Make Basic Value mapping choices

% New Design Type
% 1- left vlaue
% 2 - right value 
% 3 - Tgt (bzw. the highest of the two)
% 4 - context left
% 5 - context right
% 6 - Forced Choice? 1 means yes. 

% make the forced choices (later mix sides)
DesignFC = zeros(8,6);

for i = 1:4
    DesignFC(i,:)   = [i, 0, i, 1, 1, 1]; 
    DesignFC(i+4,:) = [i, 0, i, 2, 2, 1];
end 

DesignFCSideBal = zeros(16,6);

DesignFCSideBal(1:8,:) = DesignFC;
DesignFCSideBal(9:end,1) = DesignFC(:,2);
DesignFCSideBal(9:end,2) = DesignFC(:,1);
DesignFCSideBal(9:end,3) = DesignFC(:,3);
DesignFCSideBal(9:end,4) = DesignFC(:,5);
DesignFCSideBal(9:end,5) = DesignFC(:,4);
DesignFCSideBal(9:end,6) = DesignFC(:,6);



%% Make design for each Part:

    % 10 times forced choices (80 trials)
    % not more than 3 times choosing same side
    SideControl = false;
    while ~SideControl
    Design = zeros(80,6);

        for fc = 1:8:40
            Design(fc:fc+7,:) = DesignFCSideBal(easyrandi(find(DesignFCSideBal(:,4)==2)),:);  % first color (best to forget a bit that, might be easier anyway
        end 
        
        for fc = 41:8:80
            Design(fc:fc+7,:) = DesignFCSideBal(easyrandi(find(DesignFCSideBal(:,4)==1)),:);  
        end 

        SideControl = CheckMeSomeSeq(Design(:,1),3) & CheckMeSomeSeq(Design(:,2),3);
    end 
    
    % mixed of 16 FC plus 48 mixed, meaning basic of  64 trials
    % add more of 'Choose 1' trials (say plus 8 of those) so we have 72
    % trials X 3 seconds a trial = 3.5 minutes per block. 
    


end 