function [display, SodivaValMap, MainDesign, results, ResOutput, Master, Lstim, Rstim, Passed, CorrectCount, FeatCount] = ValueMap_Final_Experiment(display, Master, General_Info, MainDesign, block, CorrectCount, FeatCount)


%% For now zeroing the counters
% could think later to maybe keep the last half of it, to make for extra
% good participants much shorter
CorrectCount = zeros(10,2,3); % mod(t,12)+1 % trial count, context, target
FeatCount = zeros(2,3);

%% load training design 


% % Move used  design to used folder, if we are for real :)
% if display.ForReal
%     movefile(DesignPath,General_Info.pathUSEDdesigns)
% end 


%% Part 100: instructions:
% general intro 
SodivaValMap.TimeStimInstruction_Start = datetime('now');


    %% Initialize results and stim

TrialNum = size(MainDesign,1); % number of trials

% preallocate LStim and RStim to save as output
Lstim{TrialNum} = {};
Rstim{TrialNum} = {};


% preallocatte results: 

results.Block                       = zeros(1,TrialNum);
results.Part                        = zeros(1,TrialNum);
results.Context{TrialNum}           = [];                % motion or color
results.Trial                       = (1:TrialNum); % trial number
results.response                    = zeros(1,TrialNum); % were they right?
results.firstRT                     = zeros(1,TrialNum); % just the first reaction time
results.firstKey{TrialNum}          = [];                % just the first key
results.SideCorrect{TrialNum}       = [];                % which side was correct


results.designValueLeft             = MainDesign(:,1)';
results.designValueRight            = MainDesign(:,2)';
results.designValueTarget           = MainDesign(:,3)';
results.designContextLeft           = MainDesign(:,4)';
results.designContextRight          = MainDesign(:,5)'; % motion = 1. color = 2
results.designForcedChoice          = MainDesign(:,6)'; % yes = 1, no = 0


results.TimeTrial                   = zeros(1,TrialNum); % Time of cue appearing;
results.TimeCueStart                = zeros(1,TrialNum); % Time of cue appearing;
results.TimeStimStarts              = zeros(1,TrialNum); % time stim appeared;
results.TimeFramesPerStim           = zeros(1,TrialNum); % how many frames the stim went through in the time limit;
results.TimeFixation1               = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeOutcome                 = zeros(1,TrialNum); % time the outcome appeared
results.TimeFixation2               = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeQuestion                = zeros(1,TrialNum);
results.TimeFixation3               = zeros(1,TrialNum);
results.TimeEndQuestion             = zeros(1,TrialNum);


results.TargetStimMotionDirection     = zeros(1,TrialNum);
results.TargetStimColorType{TrialNum} = [];

results.LeftStimMotionDirection     = zeros(1,TrialNum);
results.LeftStimColorType{TrialNum} = [];

results.RightStimMotionDirection    = zeros(1,TrialNum);
results.RightStimColorType{TrialNum}= {};

 Lstim{TrialNum} = [];
 Rstim{TrialNum} = [];

%% create first stimuli
Le  = MainDesign(1,1); % left
Ri  = MainDesign(1,2); % right
Tgt = MainDesign(1,3); % correctness
Le_Ctxt = MainDesign(1,4);
Ri_Ctxt = MainDesign(1,5);

if Le~=0
    switch Le_Ctxt
        case 1 % motion
             Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);          
        case 2 % color
             Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);       
    end 
else 
    Lstim{1} = [];   
end 

if Ri~=0
    switch Ri_Ctxt
        
        case 1 % motion
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);           
        case 2 % color
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
    end
else 
 Rstim{1} = [];
end



%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% Go through the trials   %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MegaTime = GetSecs; % display.Timing of begining of the whole block

t = 0;
Passed = false;



 fprintf('\n Counter of 9 out of 10 trials is set to Zero');

while t<TrialNum && ~Passed %t = 79 is the last start of the block, meaning becomes 80 and starts
   t = t+1;
   
fprintf('\n Starting Trial %.0f', t);
    %% set trial settings (block, stim, Fixi, Context)
    
    results.Block(t) = block;

    %% Split the blocks (Instructions, Adjustment, Anchoring, ..........
    results.Part(t) = 1;

    TrialStartTime = GetSecs;
    results.TimeTrial(t) = TrialStartTime - MegaTime;
    Le  = MainDesign(t,1); % left
    Ri  = MainDesign(t,2); % right
    Tgt = MainDesign(t,3); % correctness

        if Tgt==Le
            results.SideCorrect{t} = 'Left'; 
        elseif Tgt==Ri
            results.SideCorrect{t} = 'Right'; 
        end 
        
    Le_Ctxt = MainDesign(1,4);
    Ri_Ctxt = MainDesign(1,5);


    RT = [];
    nKeys = 0;
    f = 1;  
         
    %% Draw fixation
     drawMyFixOrCue(display,Tgt,Master,'Fixation');
     FixStartTime = Screen('Flip',display.windowPtr); % get starting time
     results.TimeCueStart(t) = FixStartTime - MegaTime;
    
    %% START THE RESPONSE QUEUE - Only meant for PC for now
    KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
    KbQueueStart(display.deviceID); % starts queue
        
    while GetSecs - FixStartTime < display.Timing.ValLearning.Firstfix % wait for fix time
    end 
        
    %% Draw the Stim
    StimStartTime = GetSecs;
    results.TimeStimStarts(t) = StimStartTime - MegaTime;
    
    % Always have a noraml fixation
    % in some cases we will have some trials with also black dot on it
    
     if MainDesign(t,6)==1 % forced choice
         OneStim = true;
         if MainDesign(t,1) ==0    
             dispRight = true;
         elseif MainDesign(t,2) ==0
             dispRight = false;
         end 
      
     else % mixed trial
         OneStim = false;
     end 
     
     
    
    
    while GetSecs-StimStartTime < display.Timing.StimTime && f<=display.nFramesTrial% +display.Timing.RespFixTime; % 1.2 stimulus plus 0.8 fix
            
        drawMyFixOrCue(display,Tgt,Master,'Fixation');
        
        if OneStim
            if dispRight 
               DrawMeSomeDots(display,f,Rstim{t});                         % dont flip 
            else 
               DrawMeSomeDots(display,f,Lstim{t});                         % dont flip 
            end 
            
        else 
            DrawMeSomeDots(display,f,Rstim{t},Lstim{t});                         % dont flip 
        end 
        Screen('Flip',display.windowPtr);
        f = f +1;
    end
    
    
    results.TimeFramesPerStim(t) = f-1;


    %% draw fixation
    drawMyFixOrCue(display,Tgt,Master,'Fixation');
    FixStartTime = Screen('Flip',display.windowPtr);
    results.TimeFixation1(t) = FixStartTime - MegaTime;
        
    %% save stimuli details
    if MainDesign(t,1)~=0
        results.LeftStimMotionDirection(t)     = Lstim{t}.motionDir;
        results.LeftStimColorType{t}           = Lstim{t}.colorType;
    else 
        results.LeftStimMotionDirection(t)     = nan;
        results.LeftStimColorType{t}           = nan;
    end 
    
    if MainDesign(t,2)~=0
        results.RightStimMotionDirection(t)    = Rstim{t}.motionDir;
        results.RightStimColorType{t}          = Rstim{t}.colorType;
    else
        results.RightStimMotionDirection(t)    = nan;
        results.RightStimColorType{t}          = nan;    
    end
    % save target, repetative but easy after maybe?
    
    

    if Tgt==Le
             results.TargetStimMotionDirection(t)     = Lstim{t}.motionDir;
             results.TargetStimColorType{t}           = Lstim{t}.colorType;
             TgtContext = MainDesign(t,4);
            
       
    elseif Tgt==Ri
            results.TargetStimMotionDirection(t)     = Rstim{t}.motionDir;
            results.TargetStimColorType{t}           = Rstim{t}.colorType;
            TgtContext = MainDesign(t,5);
           
       
    end 

    %% Wait until response time is over
    % only after we can start adjusting and drawing
    % This will introduce a short delay. need to check how much
    FirstPress_NotYet = true;
    % SHAVING 120 ms of response, only in the perceptual part. No need for
    % it anyway and need time to compute results. 
    
%      while (GetSecs - FixStartTime < display.Timing.RespFixTime-0.100)  && FirstPress_NotYet  % wait for response time minus 100, maximum delay before was 106ms.  
    %% Check and Get response Takes 0.04 seconds 
    [KeyIsDown, firstKeyPressTimes] = KbQueueCheck(display.deviceID);
        if  KeyIsDown %a key was pressed : record the key and time pressed        
            firstKeyPressTimes(firstKeyPressTimes==0) = NaN; % little trick to get rid of 0s
            tResponse = min(firstKeyPressTimes); % gets the RT of the first key-press and its id
            firstKeyPressCode = find(firstKeyPressTimes==tResponse);
            RT = tResponse-StimStartTime;
            key_s = KbName(firstKeyPressCode);

             if iscell(key_s) % controlling that there wernt 2 keys pressed at the same exact moment. vary rare but gets stuck in analysis otherwise
             key = key_s{1};
           %  key = strcat(key,'_TwoKeys');
             else 
             key = key_s;
             end 

              switch Tgt
                case Le % Left
                    if KbName(key)==display.keyTargets(1) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(2) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end
                case Ri % Right
                    if KbName(key)==display.keyTargets(2) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(1) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end 
               end
            % here we are at first time of KeyIsDown, so:
            FirstPress_NotYet = false; % there was a key press, no need to check again here.
        end    
%     end 
    
       % now out of the loop, meaning 100ms before outcome should appear  
     if FirstPress_NotYet % meaning not yet a response
        results.response(t) = 3; % no response
        RT = NaN;
        key = 'No Response';
     end 
     
   KbQueueStop(display.deviceID); % stops queue
   KbQueueFlush(display.deviceID,1); % clear the response queue 
     
     
   % Save results
     results.firstRT(t)  = RT;
     results.firstKey{t} = key;
     
%     fprintf('\n Participant pressed %s',key);
    
    results.Trial(t)    = t;
    %% wait rest of time
    while GetSecs - FixStartTime < display.Timing.RespFixTime % wait for "response" time
    end 
     %% draw feedback based on correctness 400ms.
        
    Big.Left = false;
    Big.Right = false;

     if results.response(t)==1 %correct
         if Tgt == Le
             Big.Left = true;
         elseif Tgt == Ri
             Big.Right = true;
         end 
         
     % Left one 
     drawMyFeedback_Values_showing2(display,Le,'L',Big.Left);
     % Right one 
     drawMyFeedback_Values_showing2(display,Ri,'R',Big.Right);
         
     elseif results.response(t)==2 %wrong choice
         if Tgt == Le
              Big.Right = true;
         elseif Tgt == Ri
              Big.Left = true; % they were wrong and the correct was right, so left should be bigger. 
         end  
     
     % Left one 
     drawMyFeedback_Values_showing2(display,Le,'L',Big.Left);
     % Right one 
     drawMyFeedback_Values_showing2(display,Ri,'R',Big.Right);
     
     else 
     % Left one 
     drawMyFeedback_Values_showing2(display,5,'L',Big.Left); % sending 5 meaning no answer or wrong key
     % Right one 
     drawMyFeedback_Values_showing2(display,5,'R',Big.Right); % sending 5 meaning no answer or wrong key
     
     end 


     %drawMyFeedback_Values(display,Reslt)
%      % Left one 
%      drawMyFeedback_Values_showing2(display,Le,'L',Big.Left)
%      % Right one 
%      drawMyFeedback_Values_showing2(display,Ri,'R',Big.Right)
     
     OutcomeTime =  Screen('Flip',display.windowPtr) ;
     results.TimeOutcome(t) = OutcomeTime - MegaTime;
        
    % wait for feedback time
    while GetSecs-OutcomeTime < display.Timing.FeedbkTime 
    end


    %% draw last fixation
    drawMyFixOrCue(display,Tgt,Master,'Fixation');
    LastFixStartTime = Screen('Flip',display.windowPtr);
    results.TimeFixation2(t) = LastFixStartTime - MegaTime;
    
  
    
     %% create Stimuli for next trial - should take 0.14 seconds
  
    
     if t<TrialNum % not on the last trial   
         
        Le     = MainDesign(t+1,1); % left
        Ri     = MainDesign(t+1,2); % right
%         Tgt    = MainDesign(t+1,3); % correctness
        Le_Ctxt = MainDesign(t+1,4);
        Ri_Ctxt = MainDesign(t+1,5);
         
         
        if Le~=0
            switch Le_Ctxt
                case 1 % motion
                     Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);          
                case 2 % color
                     Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);       
            end 
        else 
            Lstim{t+1} = [];   
        end 

        if Ri~=0
            switch Ri_Ctxt

                case 1 % motion
                      Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);           
                case 2 % color
                      Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
            end
        else 
         Rstim{t+1} = [];
        end
    
            
     end 

     
    % wait for last fixation time minus 50 ms
    while GetSecs-LastFixStartTime < display.Timing.ValLearning.Lastfix - 0.050
    end
    
    
                    
    %% TRaining computing 
    % save the last 12 trials. 
    
    if  MainDesign(t,6) ~= 1 % meanning not forced choice
        
        FeatCount(TgtContext,Tgt-1) = FeatCount(TgtContext,Tgt-1) + 1;
        Cou = FeatCount(TgtContext,Tgt-1);
        
        if results.response(t) == 1
            CorrectCount(Cou,TgtContext,Tgt-1) = 1; % because Tgt is 2/3/4
        else 
            CorrectCount(Cou,TgtContext,Tgt-1) = 0; % because Tgt is 2/3/4
        end 
    fprintf('\n Participant did %f out of 60 trials correct',sum(sum(sum(CorrectCount))));
    end 
   
    
   
    % Check if passed trainig:
    if sum(CorrectCount(:,1,1))>=8 && sum(CorrectCount(:,1,2))>=8 && sum(CorrectCount(:,1,3))>=8 && sum(CorrectCount(:,2,1))>=8 && sum(CorrectCount(:,2,2))>=8 && sum(CorrectCount(:,2,3))>=8 && block~=1 && t>=40
        Passed = true;
    end 
    

    
    % wait rest of time
    while GetSecs-LastFixStartTime <= display.Timing.ValLearning.Lastfix; end

    
end % finished trial loop

%% Make ResOutput

    ResOutput.Block = results.Block'; 
    ResOutput.Part  = results.Part';
    ResOutput.Trial = results.Trial';
    
    ResOutput.designValueLeft = results.designValueLeft';
    ResOutput.designValueRight = results.designValueRight';
    ResOutput.designValueTarget = results.designValueTarget';  
    ResOutput.designContextLeft = results.designContextLeft';
    ResOutput.designContextRight = results.designContextRight';
    ResOutput.designForcedChoice = results.designForcedChoice';
    
    ResOutput.TargetStimMotionDirection = results.TargetStimMotionDirection';
    ResOutput.TargetStimColorType = results.TargetStimColorType';

    ResOutput.response = results.response';
    ResOutput.firstRT = results.firstRT';
    ResOutput.firstKey = results.firstKey';
    
    ResOutput.Anchor = (zeros(length(results.Trial),1));
    
    ResOutput.SideCorrect = results.SideCorrect';

    ResOutput.LeftStimMotionDirection = results.LeftStimMotionDirection';
    ResOutput.LeftStimColorType = results.LeftStimColorType';
    
    ResOutput.RightStimMotionDirection = results.RightStimMotionDirection';
    ResOutput.RightStimColorType = results.RightStimColorType';
    
    ResOutput.TimeTrial  = results.TimeTrial';
    ResOutput.TimeCueStart = results.TimeCueStart';
    ResOutput.TimeStimStarts = results.TimeStimStarts';
    ResOutput.TimeFramesPerStim = results.TimeFramesPerStim';
    ResOutput.TimeFixation1 = results.TimeFixation1';
    ResOutput.TimeOutcome = results.TimeOutcome';
    ResOutput.TimeFixation2 = results.TimeFixation2';
    ResOutput.TimeQuestion = results.TimeQuestion';
    ResOutput.TimeFixation3 = results.TimeFixation3';
    ResOutput.TimeEndQuestion = results.TimeEndQuestion';
    



end 