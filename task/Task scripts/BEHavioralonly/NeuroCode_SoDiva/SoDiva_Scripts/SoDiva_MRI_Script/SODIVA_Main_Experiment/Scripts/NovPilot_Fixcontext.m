function NovPilot_Fixcontext(General_Info, display)
%MAster = NovPilot_Fixcontext(General_Info, display, Design)
% This function should run at the beginning of the Main Experiment
% It loads the two OneDmini blocks and checks for RT for each context
% If there is a differnece (t-test/threshold) it addapts ALL motion
% coherence to match the RT of colors
% Steps of the function:
% (1) Load OneDMini blocks and Master file of participant
% (2) Extract the RT per feature
% (3) Check for differences  
% (4) Correct Coherence
% (5) Save new Master
%

% DrawFormattedText(display.windowPtr,'Ein Moment, es geht gleich weiter!','center','center',display.text.color);
% Screen('Flip',display.windowPtr); % flip to the screen

% (1) Load OneDMini blocks and Master file of participant
MasterFolder = fullfile(General_Info.SubData,['Master_Sub_',General_Info.subjectInfo.id,'.mat']); 
load(MasterFolder,'FinalMaster');
Master = FinalMaster;
clear FinalMaster

OneDMiniFiles = fullfile(General_Info.SubOutPut,['OneDMini_Data_',General_Info.subjectInfo.id, '_Phase_second.mat']);
load(OneDMiniFiles,'OneDMini')

%ValMapFolder = fullfile(General_Info.SubOutPut,['ValMapData_Sub_',General_Info.subjectInfo.id, '_Everything.mat']);
%load(ValMapFolder,'ValMap')

%DataTable = struct2table(OneDMini.ResOutput);
DataTable.firstRT = OneDMini.ResOutput.firstRT;
DataTable.response = OneDMini.ResOutput.response;
DataTable.Context = OneDMini.ResOutput.Context;
DataTable.designValueTarget = OneDMini.ResOutput.designValueTarget;

%for b = 2:numel(ValMap.ResOutput)
%    DataTable = [DataTable ; struct2table(ValMap.ResOutput{b})];
%end 
% delete forced choice
%DataTable(DataTable.designForcedChoice == 1,:) = [];
% deleting different contexts on both sides
%DataTable(DataTable.designContextLeft ~= DataTable.designContextRight ,:) = [];

% deleting RT smaller than 0 (could happen but very rare)
%DataTable(DataTable.firstRT < 0 ,:) = [];
DataTable.firstRT(DataTable.firstRT < 0 ,:) = [];
DataTable.response(DataTable.firstRT < 0 ,:) = [];
DataTable.Context (DataTable.firstRT < 0 ,:) = [];
DataTable.designValueTarget(DataTable.firstRT < 0 ,:) = [];


% first average across target values and then across context. 
% the reason is that in the Vallearning it might not be balance - so there
% could be more choose-4 correct thant choose 2. so if we average across
% the color/motion it could have value effect in it
MotionVal = zeros(1,3);
ColorVal = zeros(1,3);

for v = 2:4
    MotionVal(v-1) = nanmean(DataTable.firstRT(DataTable.response==1 & strcmp(DataTable.Context,'Motion') & DataTable.designValueTarget == v)); % 1 is motion
    ColorVal(v-1) = nanmean(DataTable.firstRT(DataTable.response==1 & strcmp(DataTable.Context,'Color')  & DataTable.designValueTarget == v)); % 2 is color
end

% could be tricky only in the case that they didnt have a single correct
% response to one of the features. but with 60 trials it is
% unlikely
MotionRT = mean(MotionVal); 
ColorRT = mean(ColorVal);

%NumOfFrames = (abs(ColorRT-MotionRT)/Speed)*(1/Coherence);
% in this time, how many frames are done? 
NumOfFrames = round(abs(ColorRT-MotionRT)/display.refreshRate);
if NumOfFrames>0
    if ColorRT > MotionRT % Color takes longer, need to make color faster 
        % shaving the first frames (i.e. different starting point)
        for c = 1:4
            Dist = NumOfFrames*Master.Colors{c}.speed*Master.Colors{c}.coherence;
            Master.Colors{c}.ColorStartPoint = Master.Colors{c}.ColorStartPoint + Dist;
        end 
    elseif ColorRT < MotionRT % Color is faster, need to make color slower 
        % If the difference is huge - this will result in starting way off to the other color. but very unlikely
        for c = 1:4
            Dist = NumOfFrames*Master.Colors{c}.speed*Master.Colors{c}.coherence;
            Master.Colors{c}.ColorStartPoint = Master.Colors{c}.ColorStartPoint - Dist;
        end
    end 

end % end change if need to change at least 1 frame



% Save new Master
MasterFolder = fullfile(General_Info.SubData,['Master_Final_Sub_',General_Info.subjectInfo.id,'.mat']); 
FinalMaster = Master;
save(MasterFolder,'FinalMaster');

% 
% %%%% GENERATE THE STIMULI %%%%
%          fprintf('\n Starting to generate Stimuli for Phase 3 OneD \n ');
% 
%           TrialNum = size(Design,1); % number of trials
%             for t = 1:TrialNum 
%                 Le  = Design(t,1); % left
%                 Ri  = Design(t,2); % right
%                 % will it be 1D trial? YES
%                     switch Design(t,5)
%                             case 1 %'Motion' % motion
%                               Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);
%                               Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);
%                             case 2 %'Color' % Color
%                               Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
%                               Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
%                     end 
%             end 
%             
%                StimFolder = fullfile(General_Info.SubStimuli,['Stimuli_OneDthird_',General_Info.subjectInfo.id,'.mat']);
%                save(StimFolder,'Stimuli');
%                fprintf('\n Saved Stimuli for OneDmini Phase 3 \n')
%                clear Stimuli
% 
% fprintf('\n Finished Create Stim for Phase 3  \n \n \n ')



% 
% 
% % (3) Check for differences 
% Alpha = 1;
%  % If Color > Motion, Motion is faster, need to make all coherence lower to make it harder to detect so motion will be slower
%  % If Color < Motion, Motion is slower, need to make coherence higher to make it easier to detect. ACHTUNG: ceiling effect of motion
% 
% % (4) Correct Coherence
% for f = 1:4 % all features    
%     % Motion
%    SpeOld = Master.Colors{f}.speed;   
%    % pushing the MotionRT towards the ColorRT - so MotionRT is like the
%    % last3trials and ColorRT is like Anchor.
%    Master.Colors{f}.speed = SpeOld + Alpha*((ColorRT-MotionRT)/MotionRT)*SpeOld;
%     % make sure coherence is not bigger than 1 and not smaller than 0.001
% %     if Master.Motions{f}.coherence > 1
% %         Master.Motions{f}.coherence =1;
% %     elseif Master.Motions{f}.coherence<0.001
% %         Master.Motions{f}.coherence =0.001;
% %     end 
% end 
end 