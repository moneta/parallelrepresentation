function Design = MakeValueDesign

% Output:

%Design.PartFC : 80 trials of forced choices presented in both sides
%Design.PartMix : 70 trials of mixed choices


%% Make Basic Value mapping choices

% New Design Type
% 1- left vlaue
% 2 - right value 
% 3 - Tgt (bzw. the highest of the two)
% 4 - context left
% 5 - context right
% 6 - Forced Choice? 1 means yes. 

% make the forced choices (later mix sides)
DesignFC = zeros(8,6);

for i = 1:4
    DesignFC(i,:)   = [i, 0, i, 1, 1, 1]; 
    DesignFC(i+4,:) = [i, 0, i, 2, 2, 1];
end 

DesignFCSideBal = zeros(16,6);

DesignFCSideBal(1:8,:) = DesignFC;
DesignFCSideBal(9:end,1) = DesignFC(:,2);
DesignFCSideBal(9:end,2) = DesignFC(:,1);
DesignFCSideBal(9:end,3) = DesignFC(:,3);
DesignFCSideBal(9:end,4) = DesignFC(:,5);
DesignFCSideBal(9:end,5) = DesignFC(:,4);
DesignFCSideBal(9:end,6) = DesignFC(:,6);


% make the mixedD choices (later mix sides)

DesignMix  = zeros(24,6);

count = 0;
for i = 1:length(DesignMix)/6
    
    % make main value comp. 
    DesignMix(count+1:count+6,1:2) = nchoosek(1:4,2);
    
    % make Tgt
    for j = 1:6
        DesignMix(count+j,3) = max(DesignMix(count+j,1),DesignMix(count+j,2));  
    end 
    
    % make cntxt for both sides
    switch count
        case 0
            DesignMix(count+1:count+6,4:5) = ones(6,2).*[1 ,1 ];
        case 6
            DesignMix(count+1:count+6,4:5) = ones(6,2).*[2 ,2 ];
        case 12
            DesignMix(count+1:count+6,4:5) = ones(6,2).*[1 ,2 ];
        case 18
            DesignMix(count+1:count+6,4:5) = ones(6,2).*[2 ,1 ];
    end 
    count = count + 6;   
end 

DesignMixSideBal = zeros(48,6);
DesignMixSideBal(1:24,:) = DesignMix;
DesignMixSideBal(25:end,1) = DesignMix(:,2);
DesignMixSideBal(25:end,2) = DesignMix(:,1);
DesignMixSideBal(25:end,3) = DesignMix(:,3);
DesignMixSideBal(25:end,4) = DesignMix(:,5);
DesignMixSideBal(25:end,5) = DesignMix(:,4);
DesignMixSideBal(:,6)      = 0; % not forced choice

%% Make design for each Part:

    % 10 times forced choices (80 trials)
    % not more than 3 times choosing same side
    SideControl = false;
    while ~SideControl
    Design.PartFC = zeros(80,6);

        for fc = 1:16:80
            Design.PartFC(fc:fc+15,:) = DesignFCSideBal(easyrandi(1:16),:);  
        end 
        SideControl = CheckMeSomeSeq(Design.PartFC(:,1),3) & CheckMeSomeSeq(Design.PartFC(:,2),3);
    end 
    
    % mixed of 16 FC plus 48 mixed, meaning basic of  64 trials
    % add more of 'Choose 1' trials (say plus 8 of those) so we have 72
    % trials X 3 seconds a trial = 3.5 minutes per block. 
    
    DesignMixSideBal = [DesignMixSideBal ; DesignFCSideBal];
    DesignMixSideBal = [DesignMixSideBal ; ones(2,6).* [1,0,1,1,1,1]]; % adding choose 1 Motion choose left
    DesignMixSideBal = [DesignMixSideBal ; ones(2,6).* [0,1,1,1,1,1]]; % adding choose 1 Motion choose right
    DesignMixSideBal = [DesignMixSideBal ; ones(2,6).* [0,1,1,2,2,1]]; % adding choose 1 Color choose left
    DesignMixSideBal = [DesignMixSideBal ; ones(2,6).* [0,1,1,2,2,1]]; % adding choose 1 Color choose right 
    
    SideAndValControl = false;
    while ~SideAndValControl
        Design.PartMix = DesignMixSideBal(easyrandi(1:length(DesignMixSideBal)),:);
        SideAndValControl = CheckMeSomeSeq(Design.PartMix(:,1),3) & CheckMeSomeSeq(Design.PartMix(:,2),3);
    end 


end 