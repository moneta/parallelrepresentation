function [display, SodivaTrain, MainDesign, results, ResOutput, Master, Anchor, Lstim, Rstim, Passed] = Staircasing_Training(display, Master, ThumbsUp, Arrow, General_Info,ClockSign)

Anchor = 0;

%% load training design 

% Load design - no need to move. 
DesignList = dir(General_Info.pathTraindesigns);

DesignNum = randi([3 length(DesignList)]);
DesignPath = fullfile(General_Info.pathTraindesigns,DesignList(DesignNum).name);
load(DesignPath,'MainDesign');
fprintf('\n training design \n %s \n', DesignPath);
copyfile(DesignPath, General_Info.SubData)

% % Move used  design to used folder, if we are for real :)
% if display.ForReal
%     movefile(DesignPath,General_Info.pathUSEDdesigns)
% end 


%% Part 100: instructions:
% general intro 
SodivaTrain.TimeStimInstruction_Start = datetime('now');


    %% Initialize results and stim

TrialNum = size(MainDesign,1); % number of trials

% preallocate LStim and RStim to save as output
Lstim{TrialNum} = {};
Rstim{TrialNum} = {};


% preallocatte results: 

results.Block                       = zeros(1,TrialNum);
results.Part                        = zeros(1,TrialNum);
results.Context{TrialNum}           = [];                % motion or color
results.Trial                       = (1:TrialNum); % trial number
results.response                    = zeros(1,TrialNum); % were they right?
results.firstRT                     = zeros(1,TrialNum); % just the first reaction time
results.firstKey{TrialNum}          = [];                % just the first key
results.SideCorrect{TrialNum}       = [];                % which side was correct

results.fixControl                  = zeros(1,TrialNum);
results.fixControlRT                = zeros(1,TrialNum);
results.fixControlKey{TrialNum}     = [];   

results.designValueLeft        = MainDesign(:,1)';
results.designValueRight       = MainDesign(:,2)';
results.designValueTarget      = MainDesign(:,3)';
results.designFixControol = MainDesign(:,4)';
results.designContext     = MainDesign(:,5)'; % motion = 1. color = 2
results.design1D          = MainDesign(:,6)'; % yes = 1, no = 0
results.designValueBackLeft = MainDesign(:,7)'; % added 
results.designValueBackRight = MainDesign(:,8)'; % added 

% added
results.designValueBackTarget = zeros(1,TrialNum); % Like the Block vector, do same to this. 
results.designValueChoosen = zeros(1,TrialNum);
results.designValueUnchosen = zeros(1,TrialNum);
results.designValueBackChoosen = zeros(1,TrialNum);
results.designValueBackUnchosen = zeros(1,TrialNum);

results.TimeTrial                   = zeros(1,TrialNum); % Time of cue appearing;
results.TimeCueStart                = zeros(1,TrialNum); % Time of cue appearing;
results.TimeStimStarts              = zeros(1,TrialNum); % time stim appeared;
results.TimeFramesPerStim           = zeros(1,TrialNum); % how many frames the stim went through in the time limit;
results.TimeFixation1               = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeOutcome                 = zeros(1,TrialNum); % time the outcome appeared
results.TimeFixation2               = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeQuestion                = zeros(1,TrialNum);
results.TimeFixation3               = zeros(1,TrialNum);
results.TimeEndQuestion             = zeros(1,TrialNum);


results.TargetStimMotionDirection     = zeros(1,TrialNum);
results.TargetStimMotionCoherence     = zeros(1,TrialNum);
results.TargetStimColorType{TrialNum} = [];
results.TargetStimColorSpeed          = zeros(1,TrialNum);
results.TargetStimColorCoherence      = zeros(1,TrialNum);



results.LeftStimMotionDirection     = zeros(1,TrialNum);
results.LeftStimMotionCoherence     = zeros(1,TrialNum);
results.LeftStimColorType{TrialNum} = [];
results.LeftStimColorSpeed          = zeros(1,TrialNum);
results.LeftStimColorCoherence      = zeros(1,TrialNum);


results.RightStimMotionDirection    = zeros(1,TrialNum);
results.RightStimMotionCoherence    = zeros(1,TrialNum);
results.RightStimColorType{TrialNum}= {};
results.RightStimColorSpeed         = zeros(1,TrialNum);
results.RightStimColorCoherence      = zeros(1,TrialNum);


SodivaTrain.NumbOfAdjustments           = zeros(4,2);       % number of adjustment for each direction
SodivaTrain.StagesOfAdjustments         = zeros(4,12);% the actual values of each adjustment done

 Lstim{TrialNum} = [];
 Rstim{TrialNum} = [];

%% create first stimuli
Le  = MainDesign(1,1); % left
Ri  = MainDesign(1,2); % right
Tgt = MainDesign(1,3); % correctness
Le_bkg = MainDesign(1,7);
Ri_bkg = MainDesign(1,8);


% will it be 1D trial?
OneD = (MainDesign(1,6)==1);

    switch MainDesign(1,5)
            case 1
                context = 'Motion';
            case 2
                context = 'Color';
    end

    switch context
            case 'Motion' % motion
                if OneD
              Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);
                else 

              %design.Bkgcount(Tgt,1) = design.Bkgcount(Tgt,1)+1;                
              %Bckg = design.background(design.Bkgcount(Tgt,1),Tgt);

              Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion', Le, Le_bkg);
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion', Ri, Ri_bkg);
                end 

            case 'Color' % Color
                if OneD
              Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
                else

              %design.Bkgcount(Tgt,2) = design.Bkgcount(Tgt,2)+1;
              %Bckg = design.background(design.Bkgcount(Tgt,2),Tgt);

              Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color', Le, Le_bkg);
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color', Ri, Ri_bkg);
                end
     end 

%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% Go through the trials   %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MegaTime = GetSecs; % display.Timing of begining of the whole block

t = 0;
Passed = false;
% IdentifiedFixi = false;
%  fprintf('\n Counter for Identify FixiControl set to false');
CorrectCount = zeros(12,1); % mod(t,12)+1
 fprintf('\n Counter of 8 out of 12 trials is set to Zero');

while t<TrialNum && ~Passed %t = 79 is the last start of the block, meaning becomes 80 and starts
   t = t+1;
   
fprintf('Stating Trial %.0f \n', t);
    %% set trial settings (block, stim, Fixi, Context)
    block = 0;
    results.Block(t) = block;   
    %% Split the blocks (Instructions, Adjustment, Anchoring, ..........
  if t==1
            Part = 0; 
            SodivaTrain.StartTime = datetime('now');
  end
            results.Part(t) = Part;
    TrialStartTime = GetSecs;
    results.TimeTrial(t) = TrialStartTime - MegaTime;
    Le  = MainDesign(t,1); % left
    Ri  = MainDesign(t,2); % right
    Tgt = MainDesign(t,3); % correctness

        if Tgt==Le
            results.SideCorrect{t} = 'Left'; 
        elseif Tgt==Ri
            results.SideCorrect{t} = 'Right'; 
        end 
        
%     if MainDesign(t,4) == 0 % normal trial
%         WasThereFix = false;
%         ConFix = false;
%     elseif MainDesign(t,4) == 1 % FixControl trial
%         WasThereFix = true;
%         ConFix = true;
%         fw = randi([display.FixControlLength, display.nFramesTrial-display.FixControlLength],1); % setting starting frame to have the extra circle
%     end
    
    switch MainDesign(t,5)
        case 1
            context = 'Motion';
        case 2
            context = 'Color';
    end
    
    
    results.Context{t} = context;
    RT = [];
    nKeys = 0;
    f = 1;  
         
    %% Draw cue
    drawMyFixOrCue(display,Tgt,Master,context, Arrow) % not flipping 
    CueStartTime = Screen('Flip',display.windowPtr); % get starting time
    results.TimeCueStart(t) = CueStartTime - MegaTime;
    
    %% START THE RESPONSE QUEUE - Only meant for PC for now
    KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
    KbQueueStart(display.deviceID); % starts queue
        
    while GetSecs - CueStartTime < display.Timing.CueTime % wait for Cue time
    end 
        
    %% Draw the Stim
    StimStartTime = GetSecs;
    results.TimeStimStarts(t) = StimStartTime - MegaTime;
    
    % Always have a noraml fixation
    % in some cases we will have some trials with also black dot on it
    
    while GetSecs-StimStartTime < display.Timing.StimTime && f<=display.nFramesTrial% +display.Timing.RespFixTime; % 1.2 stimulus plus 0.8 fix
            DrawMeSomeDots(display,f,Rstim{t},Lstim{t});                         % dont flip 
            
%             if ConFix
%                 if f >(fw-display.FixControlLength) && f < (fw + display.FixControlLength)
%                     Screen('FillOval', display.windowPtr, display.bkColor,[-display.fixation.sz/2+display.center(1),-display.fixation.sz/2+display.center(2),display.fixation.sz/2+display.center(1),display.fixation.sz/2+display.center(2)]);
%                 end
%             end 
            Screen('Flip',display.windowPtr);
            f = f +1;
    end
    results.TimeFramesPerStim(t) = f-1;
    
    %% draw fixation
    drawMyFixOrCue(display,Tgt,Master,'Fixation');
    FixStartTime = Screen('Flip',display.windowPtr);
    results.TimeFixation1(t) = FixStartTime - MegaTime;
        
    %% save stimuli details
    results.LeftStimMotionDirection(t)     = Lstim{t}.motionDir;
    results.LeftStimMotionCoherence(t)     = Lstim{t}.motionCohe;
    results.LeftStimColorType{t}           = Lstim{t}.colorType;
    results.LeftStimColorSpeed(t)          = Lstim{t}.colorSpeed;
    results.LeftStimColorCoherence(t)      = Lstim{t}.colorCoherence;

    results.RightStimMotionDirection(t)    = Rstim{t}.motionDir;
    results.RightStimMotionCoherence(t)    = Rstim{t}.motionCohe;
    results.RightStimColorType{t}          = Rstim{t}.colorType;
    results.RightStimColorSpeed(t)         = Rstim{t}.colorSpeed;
    results.RightStimColorCoherence(t)     = Rstim{t}.colorCoherence;
    
    % save target, repetative but easy after maybe?
        if Tgt==Le
             results.TargetStimMotionDirection(t)     = Lstim{t}.motionDir;
             results.TargetStimMotionCoherence(t)     = Lstim{t}.motionCohe;
             results.TargetStimColorType{t}           = Lstim{t}.colorType;
             results.TargetStimColorSpeed(t)          = Lstim{t}.colorSpeed;
             results.TargetStimColorCoherence(t)      = Lstim{t}.colorCoherence;
        elseif Tgt==Ri
            results.TargetStimMotionDirection(t)     = Rstim{t}.motionDir;
            results.TargetStimMotionCoherence(t)     = Rstim{t}.motionCohe;
            results.TargetStimColorType{t}           = Rstim{t}.colorType;
            results.TargetStimColorSpeed(t)          = Rstim{t}.colorSpeed;
            results.TargetStimColorCoherence(t)      = Rstim{t}.colorCoherence;
        end 

    %% Wait until response time is over
    % only after we can start adjusting and drawing
    % This will introduce a short delay. need to check how much
    
    % SHAVING 120 ms of response, only in the perceptual part. No need for
    % it anyway and need time to compute results. 
    FirstPress_NotYet = true;
%     while (GetSecs - FixStartTime < display.Timing.RespFixTime-0.100) && FirstPress_NotYet% wait for response time minus 100, maximum delay before was 106ms.  
    %% Check and Get response Takes 0.04 seconds 
    [KeyIsDown, firstKeyPressTimes] = KbQueueCheck(display.deviceID);
        if  KeyIsDown %a key was pressed : record the key and time pressed        
            firstKeyPressTimes(firstKeyPressTimes==0) = NaN; % little trick to get rid of 0s
            tResponse = min(firstKeyPressTimes); % gets the RT of the first key-press and its id
            firstKeyPressCode = find(firstKeyPressTimes==tResponse);
            RT = tResponse-StimStartTime;
            key_s = KbName(firstKeyPressCode);

             if iscell(key_s) % controlling that there wernt 2 keys pressed at the same exact moment. vary rare but gets stuck in analysis otherwise
             key = key_s{1};
           %  key = strcat(key,'_TwoKeys');
             else 
             key = key_s;
             end 

              switch Tgt
                case Le % Left
                    if KbName(key)==display.keyTargets(1) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(2) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end
                case Ri % Right
                    if KbName(key)==display.keyTargets(2) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(1) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end 
              end
            % here we are at first time of KeyIsDown, so:
            FirstPress_NotYet = false; % there was a key press, no need to check again here.
            % Clear the response queue for FIXI
            KbQueueStop(display.deviceID); % stops queue
            KbQueueFlush(display.deviceID,1); % clear the response queue 
            % KbQueueRelease(display.deviceID); % Not sure, might save space
            % Start response queue to see answer if there was a fix in this trial
            
            
%             KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
%             KbQueueStart(display.deviceID); % starts queue
        end    
    
%     end 
     % now out of the loop, meaning 100ms before outcome should appear  
     if FirstPress_NotYet % meaning not yet a response
        results.response(t) = 3; % no response
        RT = NaN;
        key = 'No Response';
        KbQueueStop(display.deviceID); % stops queue
        KbQueueFlush(display.deviceID,1); % clear the response queue 
            
     end 
     
   % Save results
     results.firstRT(t)  = RT;
     results.firstKey{t} = key;
    
      % Check for timing under 1 second, if not so set to 5 - clock feedback
     if results.response(t) == 1 % if correct 
         if RT > display.STRtimeLimit % if longer than 1 second 
             results.response(t) = 5;
         end 
     end 
     
     
    results.Trial(t)    = t;
    %% wait rest of time
    while GetSecs - FixStartTime < display.Timing.RespFixTime % wait for response time
    end 
     %% draw feedback based on correctness 400ms.  
    drawMyFeedback(display, results.response(t),ThumbsUp,ClockSign); % not flipping, could be used to save and adjust
    OutcomeTime =  Screen('Flip',display.windowPtr) ;
    results.TimeOutcome(t) = OutcomeTime - MegaTime;
    
%      %%%%%%%%%%%%%%%%%%%%% start new queue %%%%%%%%%%%%%%%%
%       %% Clear the response queue 
%     KbQueueFlush(display.deviceID,1); % clear the response queue 
%     % KbQueueRelease(display.deviceID); % Not sure, might save space
%     
%     %% Start response queue to see answer if there was a fix in this trial
%     KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
%     KbQueueStart(display.deviceID); % starts queue
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        
    
    % wait for feedback time
    while GetSecs-OutcomeTime < display.Timing.FeedbkTime;    end


    %% draw last fixation
    drawMyFixOrCue(display,Tgt,Master,'Fixation');
    LastFixStartTime = Screen('Flip',display.windowPtr);
    results.TimeFixation2(t) = LastFixStartTime - MegaTime;
    
  
    
     %% create Stimuli for next trial - should take 0.14 seconds
  
    
     if t<TrialNum % not on the last trial   
         
        Le     = MainDesign(t+1,1); % left
        Ri     = MainDesign(t+1,2); % right
        Tgt    = MainDesign(t+1,3); % correctness
        Le_bkg = MainDesign(t+1,7);
        Ri_bkg = MainDesign(t+1,8);
         
         
         
        OneD = (MainDesign(t+1,6)==1);% will next trial be 1d?
     % make context of next trial
         switch MainDesign(t+1,5)
            case 1
                contextNext = 'Motion';
            case 2
                contextNext = 'Color';
         end
    
   
            % here we dont need the background to be the same, so we take
            % like before the random background for each 
            switch contextNext
                case 'Motion' % motion
                    if OneD
                  Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D',Le);
                  Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D',Ri);
                    else 
                        
                  Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion', Le, Le_bkg);
                  Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion', Ri, Ri_bkg);
                    end 
                    
                case 'Color' % Color
                    if OneD
                  Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
                  Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
                    else
                                        
                  Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color', Le, Le_bkg);
                  Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color', Ri, Ri_bkg);
                    end
            end 
            
            
         
            
     end 

     %% added to result


     if results.response(t)==1 %correct
         if Tgt == Le
             Reslt = Le;
         else
             Reslt = Ri;
         end 
     elseif results.response(t)==2 %wrong choice
         if Tgt == Le
             Reslt = Ri;
         else
             Reslt = Le;
         end 
     else 
         Reslt = 5;
     end 

    if Tgt == Le
             results.designValueBackTarget(t) = results.designValueBackLeft(t);
    else % right
             results.designValueBackTarget(t) = results.designValueBackRight(t);
    end
    
     results.designValueChoosen(t) = Reslt;
     if Reslt == Le
          results.designValueBackChoosen(t) = results.designValueBackLeft(t);
          results.designValueUnchosen(t) = Ri;
          results.designValueBackUnchosen(t) = results.designValueBackRight(t);
     elseif Reslt == Ri
         results.designValueBackChoosen(t)  = results.designValueBackRight(t);
         results.designValueUnchosen(t) = Le;
         results.designValueBackUnchosen(t) = results.designValueBackLeft(t);

     else % Reslt==5, meaning no response
         results.designValueBackChoosen(t)  = 5;
         results.designValueUnchosen(t) = 5;
         results.designValueBackUnchosen(t) = 5;
     end
     
    % wait for last fixation time minus 50 ms
%     while GetSecs-LastFixStartTime < display.Timing.LastFixTime - 0.050; end
%     
%     [KeyIsDown, firstKeyPressTimes] = KbQueueCheck(display.deviceID);
%     if  KeyIsDown %a key was pressed : record the key and time pressed
%         firstKeyPressTimes(firstKeyPressTimes==0) = NaN; % little trick to get rid of 0s
%         tResponse = min(firstKeyPressTimes); % gets the RT of the first key-press and its id
%         firstKeyPressCode = find(firstKeyPressTimes==tResponse);
%         RT = tResponse-StimStartTime;
%         key_s = KbName(firstKeyPressCode);
%         
%      if iscell(key_s) % controlling that there wernt 2 keys pressed at the same exact moment. vary rare but gets stuck in analysis otherwise
%      key = key_s{1};
%    %  key = strcat(key,'_TwoKeys');
%      else 
%      key = key_s;
%      end 
%         
%           if WasThereFix % correct answer is display.keyTargets(4); 
%                            if KbName(key)==display.keyTargets(3) % correct
%                               results.fixControl(t) = 1;
%                            else % wrong key - since its under key is down, and just the wrong one, but there was a fix
%                               results.fixControl(t) = 3;
%                            
%                            end
%           elseif ~WasThereFix
%                            if KbName(key)==display.keyTargets(3) % false alarm (pressed space but no fix)
%                               results.fixControl(t) = 2;
%                            else  % random key press - no fix, pressed a key, but not space
%                               results.fixControl(t) = 4;
%                            end
%           end
% 
%      else % no key pressed
%          
%           if WasThereFix 
%                               results.fixControl(t) = 5; % Missed a fix
%           elseif ~WasThereFix
%                               results.fixControl(t) = 6; % did Well
%           end
%                         RT = NaN;
%                         key = 'No Response';
%     end
% 
%     % We want to see at the end TrialNum-8 times of 6's and 8 times 1's for
%     % correct answeres. 
%     
%                      results.fixControlRT(t) = RT;
%                      results.fixControlKey{t} = key;
% 
%                     KbQueueFlush(display.deviceID,1); % clear the response queue 
%     
                    
                    
    %% TRaining computing 
    % save the last 12 trials. 
    if results.response(t) == 1
        CorrectCount(mod(t,12)+1) = 1;
    else 
        CorrectCount(mod(t,12)+1) = 0;
    end 
    
    fprintf('\n Participant did .0%f out of 12 trials correct',sum(CorrectCount));
    
%     if WasThereFix
%         if results.fixControl(t) == 1 % identified the fix
%         IdentifiedFixi = true;
%         fprintf('\n Participant identified the FixiControl once in the 24 trials');
%         else % was fix and didnt identified it
% %         IdentifiedFixi = false;
%         end 
%     end % this way it stays the same until next time there is a fix. 
        
    % Check if passed trainig:
    if sum(CorrectCount)>=8 % && IdentifiedFixi
        Passed = true;
    end 
    
    % wait rest of time
    while GetSecs-LastFixStartTime < display.Timing.LastFixTime; end 
    
end % finished trial loop

%% Make ResOutput

    ResOutput.Block = results.Block'; 
    ResOutput.Part  = results.Part';
    ResOutput.Trial = results.Trial';
    
    ResOutput.designValueLeft = results.designValueLeft';
    ResOutput.designValueRight = results.designValueRight';
    ResOutput.designValueTarget = results.designValueTarget';  
    ResOutput.designFixControl = results.designFixControol';
        
    ResOutput.Context = results.Context';
    
    ResOutput.TargetStimMotionDirection = results.TargetStimMotionDirection';
    ResOutput.TargetStimMotionCoherence = results.TargetStimMotionCoherence';
    ResOutput.TargetStimColorType = results.TargetStimColorType';
    ResOutput.TargetStimColorSpeed = results.TargetStimColorSpeed';
    ResOutput.TargetStimColorCoherence = results.TargetStimColorCoherence';
    
    ResOutput.response = results.response';
    ResOutput.firstRT = results.firstRT';
    ResOutput.firstKey = results.firstKey';
    
    ResOutput.Anchor = (zeros(length(results.Trial),1));
    
    ResOutput.SideCorrect = results.SideCorrect';
   
    ResOutput.fixControl                         = results.fixControl';
    ResOutput.fixControlRT                       = results.fixControlRT';
    ResOutput.fixControlKey                      = results.fixControlKey';
   
    ResOutput.LeftStimMotionDirection = results.LeftStimMotionDirection';
    ResOutput.LeftStimMotionCoherence = results.LeftStimMotionCoherence';
    ResOutput.LeftStimColorType = results.LeftStimColorType';
    ResOutput.LeftStimColorSpeed = results.LeftStimColorSpeed';
    ResOutput.LeftStimColorCoherence = results.LeftStimColorCoherence';

    
    ResOutput.RightStimMotionDirection = results.RightStimMotionDirection';
    ResOutput.RightStimMotionCoherence = results.RightStimMotionCoherence';
    ResOutput.RightStimColorType = results.RightStimColorType';
    ResOutput.RightStimColorSpeed = results.RightStimColorSpeed';
    ResOutput.RightStimColorCoherence = results.RightStimColorCoherence';

    
    
    ResOutput.TimeTrial  = results.TimeTrial';
    ResOutput.TimeCueStart = results.TimeCueStart';
    ResOutput.TimeStimStarts = results.TimeStimStarts';
    ResOutput.TimeFramesPerStim = results.TimeFramesPerStim';
    ResOutput.TimeFixation1 = results.TimeFixation1';
    ResOutput.TimeOutcome = results.TimeOutcome';
    ResOutput.TimeFixation2 = results.TimeFixation2';
    ResOutput.TimeQuestion = results.TimeQuestion';
    ResOutput.TimeFixation3 = results.TimeFixation3';
    ResOutput.TimeEndQuestion = results.TimeEndQuestion';
    
% added 
    ResOutput.designValueBackLeft = results.designValueBackLeft';
    ResOutput.designValueBackRight = results.designValueBackRight';
    
    ResOutput.designValueBackTarget = results.designValueBackTarget'; % Like the Block vector, do same to this. 
    ResOutput.designValueChoosen = results.designValueChoosen';
    ResOutput.designValueUnchosen = results.designValueUnchosen';
    ResOutput.designValueBackChoosen = results.designValueBackChoosen' ;
    ResOutput.designValueBackUnchosen = results.designValueBackUnchosen' ;


end 