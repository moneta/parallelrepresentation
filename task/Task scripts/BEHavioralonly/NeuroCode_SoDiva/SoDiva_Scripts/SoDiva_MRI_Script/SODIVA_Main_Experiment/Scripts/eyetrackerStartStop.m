function worked = eyetrackerStartStop(display, General_Info, ETAction)
% this function either start recording, stops recording, or finishes and
% saves the data. 
% finish also gives indication on the screen and flips. 
switch ETAction
    case 'start'
        %START RECORDING
        Eyelink('StartRecording');
        Screen(display.windowPtr, 'Flip');
        WaitSecs(2);

        status=Eyelink('CheckRecording');
        if(status~=0)
            error('didnt start recording, status:  %.0f',status)
            worked = false;
        else
        worked = true;    
        end
        
        
    case 'stop'

        Eyelink('StopRecording')
    
    case 'finish'
        
         WaitSecs(2);
      
         texttime = datestr(datetime('now'));
            DrawFormattedText(display.windowPtr, 'EyeTracker data is being transfered',                      'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'This can take a few minutes',                                              'center', display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, texttime,                                              'center', display.text.lines(7),display.text.color);
            
            Screen('Flip',display.windowPtr);
            
        Eyelink('StopRecording')
        Eyelink('CloseFile'); 
 
        try
        edfFile = fullfile(General_Info.SubEyeTracker,General_Info.Eyetracker.files{end});
        fprintf('Receiving data file ''%s''\n', General_Info.Eyetracker.files{end} );
        status=Eyelink('ReceiveFile',General_Info.Eyetracker.files{end},edfFile);
%         status=Eyelink('ReceiveFile',General_Info.Eyetracker.files{end},General_Info.SubEyeTracker);
        if status > 0
            fprintf('ReceiveFile status %d\n', status);
            worked = true;
        else 
            worked = false;
        end
%         if 2==exist(edfFile, 'file')
%             fprintf('Data file ''%s'' can be found in ''%s''\n', edfFile, pwd );
%             
%         end
        catch rdf
            fprintf('Problem receiving data file ''%s''\n', edfFile );
            rdf;
            worked = false;

        end
        WaitSecs(2);
        
end 


Screen('TextFont', display.windowPtr, display.text.font); % select specific text font
Screen('TextSize', display.windowPtr, display.text.size); % select specific text size



end 