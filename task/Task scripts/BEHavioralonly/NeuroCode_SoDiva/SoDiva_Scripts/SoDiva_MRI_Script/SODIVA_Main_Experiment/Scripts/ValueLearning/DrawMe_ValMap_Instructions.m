function DrawMe_ValMap_Instructions(display,Part,PerCorrect)
% This function has all the text screens
 % Part 101 - General instructions 1 of 2
 % Part 102 -  General instructions 2 of 2
 % Part 200 - Within Block Break 
 % Part 300 - Between Block Break
 % 1  Motion
 % 2 Color
 % 3 Motion Adjustment
 % 4 Color Adjustment
 % 400 Thank you 

    switch Part
        case 100 % general val map instructions: Now each feature has a value: 10 30 50 or 70. 
                 % first presented all values and then choices will be
                 % given and they need to choose the highest value,
                 % regardless of if its color or motion. 
                 % Important: here only 1D, sometimes there might be only one option
                 
            DrawFormattedText(display.windowPtr, 'Jetzt bekommt jede Farbe und jede Bewegungsrichtung eine bestimmte Belohnung.',                display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Die Belohnungen sind: 10, 30, 50 und 70 Kredite.',                                             display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Im naechsten Teil hat jede Punktgruppe entweder eine bestimmte Farbe (und eine zufaellige Bewegung) ',        display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'oder eine bestimmte Bewegungsrichtung (und keine Farbe).',        display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'In den folgenden Durchgaengen bekommst du allerdings keine Hinweise.',        display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'In den ersten 4 Minuten wird nur eine Punktgruppe angezeigt werden.',                               display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du musst dann diese Punktgruppe auswaehlen, um die dazugehoerige Belohnung zu sehen.',                                          display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Danach werden gemischte Blocks kommen, in denen zwei Punktegruppen gezeigt werden.',         display.text.linestart, display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'In diesem Teil kommen Entscheidungen sowohl innerhalb einer Dimension (z.B. Farbe gegen Farbe),',    display.text.linestart, display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'als auch zwischen Dimensionen (z.B. Farbe gegen Richtung) vor.',    display.text.linestart, display.text.lines(11),display.text.color);
            DrawFormattedText(display.windowPtr, 'Deine Aufgabe ist es nun, die hoechste Belohnung zu waehlen.',     display.text.linestart, display.text.lines(12),display.text.color);
            DrawFormattedText(display.windowPtr, 'Deine Antwort kann ab jetzt ein bischen laenger dauern, aber bitte antworte so schnell wie moeglich.',     display.text.linestart, display.text.lines(13),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                               'center', display.text.lines(14),display.text.color);

        
        case 101 % Now that you know the valus, we start with some excersizes to make sure you remember them well. 
                 % please answer fast as possible which is the highest
                 % value, focus on the fixation. This block will take a few
                 % minutes. 
                 % 
                 
%             DrawFormattedText(display.windowPtr, 'Jetzt, wenn du die Behlonungen gut kennst, fangen wir mit ein paar Uebungen an,',                display.text.linestart, display.text.lines(2),display.text.color);
%             DrawFormattedText(display.windowPtr, 'um sicher zu stellen, dass sie verinnerlicht wurden.',                                           display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Jetzt fangen wir mit ein paar Uebungen an,',                display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'um sicher zu stellen, dass die Behlonungen verinnerlicht wurden.',                                           display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn du eine Farbe oder Bewegung siehst, versuche bitte ueber ihre genaue Belohnung nachzudenken.', display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Dieser Block dauert ungefaehr 4 Minuten',                                                        display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte antworte so schnell und genau wie moeglich.',                                              display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',                                          display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                   'center', display.text.lines(13),display.text.color);
            
        case 102 % Now we start with a few blocke. 
                 % Each block has minimum 50 trials. 
                 % you will do each block until you have reached 80%
                 % accuracy, or until the block is done. 
                 % We will do two of these blocks, and if we see you need
                 % more, there will be reminded with the values and we will do up to two more blocks, until you reach the
                 % accuracy needed.
            DrawFormattedText(display.windowPtr, 'Jetzt hast du die Gelegenheit, uns zu zeigen, wie gut du die Belohnungen kennst,',                display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Die Anzahl der naechsten Bloecke haengt von deiner Leistung ab.',                                   display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Es gibt maximal 4 Bloecke, die jeweils 4 Minuten dauern',                           display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Die Mindestanzahl ist 2 Bloecke. Wenn du in einem Block in mindestens 80% der Durchgaenge,',                                    display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'die hoechste Belohnung gewaehlt hast, geht der Teil zu Ende. Die Maximalanzahl ist 4 Bloecke.',                           display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Deine Entlohnung im naechsten Teil des Experiments haengt davon ab, wie gut du die Belohnungen kennst.',                                                          display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte antworte so schnell und genau wie moeglich.',                                               display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',                                           display.text.linestart, display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                    'center', display.text.lines(13),display.text.color);
            
                 
     
        case 200 % Feedback. START NEXT BLOCK
                 % in the last block you were X% correct in choosing the
                 % highest number. 
                 PerCorrect = PerCorrect*100;
                 Feedback = strcat(num2str(PerCorrect),'%',' korrekt beantwortet');
                
            DrawFormattedText(display.windowPtr, 'Im letzten Teil hast du',                'center', display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                  'center', display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um den naechsten Block zu starten.',  'center', display.text.lines(13),display.text.color);
           
         
        case 201 % Feedback. Done. 
                 % in the last block you were X% correct in choosing the
                 % highest number. 
                 PerCorrect = PerCorrect*100;
                 Feedback = strcat(num2str(PerCorrect),'%',' korrekt beantwortet');
               
            DrawFormattedText(display.windowPtr, 'Im letzten Teil hast du',                                                  'center', display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                                                    'center', display.text.lines(3),display.text.color);           
            DrawFormattedText(display.windowPtr, 'Du hast es geschafft! Vielen Dank fuer deine Teilnahme.',                   'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Ende des Teils des Experiments',                                            'center', display.text.lines(10),display.text.color); 
            DrawFormattedText(display.windowPtr, 'Bitte sag dem Versuchsleiter Bescheid',                                              'center', display.text.lines(11),display.text.color);

           
    
     end 
    
    Screen('Flip',display.windowPtr);
    
    WaitSecs(2);

end 