%% Make value maps and save

rng(sum(100*clock)); 

AllMapsCon = perms(1:4)';
folderMaps = 'C:\Users\moneta\Seafile_Folder\Seafile\My Library\SODIVA\ValueMaps_NotClocky';

Rules.Max_1D2D_WithinCon = 3;
Rules.Max_1D2D_OverAll = 5;
Rules.Max_SameContext_inArow = 3;
Rules.Max_SameFeature_WithinCon = 2; % meaning max same value 4 in a row
Rules.Max_LeftRight = 4;


% ColRand = easyrandi(1:24);
% ColorMaps = AllMapsCon(:,ColRand);
% 
% MotRand = easyrandi(1:24);
% MotionMaps = AllMapsCon(:,MotRand);

%we have 24 options for maps in color, and 24 motion maps
work = false;

while ~work
A = [];
B = [];
C = [];
D = [];
E = [];
A(:,1) = easyrandi(1:24);
A(:,2) = easyrandi(1:24);

B(:,1) = easyrandi(1:24);
B(:,2) = easyrandi(1:24);

C(:,1) = easyrandi(1:24);
C(:,2) = easyrandi(1:24);

D(:,1) = easyrandi(1:24);
D(:,2) = easyrandi(1:24);

D2(:,1) = easyrandi(1:24);
D2(:,2) = easyrandi(1:24);

E = [A;B;C;D;D2];
work = length(E)==length(unique(E,'rows'));
end 

% delete clockwise and counter clockwise options from motion 
Problem = [1, 2 ,3, 4;...
           2, 3, 4, 1;...
           3, 4, 1, 2;...
           4, 1, 2, 3;...
           4, 3, 2, 1;...
           3, 2, 1, 4;...
           2, 1, 4, 3;...
           1, 4, 3, 2]';
       
ValMaps = [];
counter = 0;
for i = 1:length(E)
   if sum(sum(AllMapsCon(:,E(i,1))==Problem)==4)==0
       % meaning not a single column has 4 yes on it
       counter = counter + 1;
   ValMaps(:,1,counter) =  AllMapsCon(:,E(i,1)); % Motion
   ValMaps(:,2,counter) =  AllMapsCon(:,E(i,2));  % Color
   end 
end 
% sanity check all are different:
if length(reshape(ValMaps,[],8)) == length(unique(reshape(ValMaps,[],8),'rows'))
    for s = 1:size(ValMaps,3)
        if s < 10
         MapName = fullfile(folderMaps,['Value_Map_0',num2str(s),'.mat']);

        else 
         MapName = fullfile(folderMaps,['Value_Map_',num2str(s),'.mat']);

        end 

         ValMap = ValMaps(:,:,s);
         save(MapName, 'ValMap');
         clear ValMap
    end 
end 
% loading when in the CD folder 
% 
% A = zeros(4,2,96);
% for s = 1:96
%      if s < 10
%      T = load(['Value_Map_0',num2str(s),'.mat']);
%     
%     else 
%     T = load(['Value_Map_',num2str(s),'.mat']);
% 
%      end 
%     A(:,:,s) = T.ValMap;
% end 
% 
% V = zeros(96,1);
% counter = 0;
% for i = 1:length(A)
%    if sum(sum(A(:,1,i)==Problem)==4)==0
%      
%    V(i) = 1;
%    
%    end 
% end 
% 
% 
% 
% 
% 
% %             save(MapName, 'ValMap');
% %             clear ValMap
% 
% % count = 1;
% % for i = 1:length(AllMapsCon)
% %     
% %         for j = 1:length(AllMapsCon)
% %             
% %             ValMap(1:4,1) = AllMapsCon(1:4,i);
% %             ValMap(1:4,2) = AllMapsCon(1:4,j);
% %     
% %             
% %             count = count + 1;
% %             SeeVal(:,:,count) = ValMap;
% %             clear ValMap
% %         end 
% %         
% % end 
% % SeeVal is 24x24 value pairs, organized so: 
% % first 24 has same in column 1, and all the types of 24 in column 2;
% % take 
% 
% MakeRand = easyrandi(1:576);
% 
% for s = 1:(count-1)
%     
%             ValMap = SeeVal(:,:,MakeRand(s));
%             MapName = fullfile(folderMaps,['Value_Map_',num2str(s),'.mat']);
%             Checking(:,:,s) = ValMap;
%             save(MapName, 'ValMap');
%             clear ValMap
%     
%     
%     
%     
% end 

