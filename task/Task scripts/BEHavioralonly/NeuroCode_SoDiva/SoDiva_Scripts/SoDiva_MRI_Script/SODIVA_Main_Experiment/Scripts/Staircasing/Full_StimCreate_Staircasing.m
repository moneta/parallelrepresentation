function Stim = Full_StimCreate_Staircasing(Master, Side, display, context, StimID, Bckg)
% Input: Master, Side, displat, context
% (Motion,Color,Motion1D,Color1D),StimID (12345)

%% give Stim info as output:    
Stim.nFrames = display.nFramesTrial;
Stim.Side = Side;
             
switch context
    case 'Motion'
                Motions = Master.Motions{StimID};
                Colors = Master.Colors{Bckg};
    case 'Color' % normal motion or color trial
                Motions = Master.Motions{Bckg};
                Colors = Master.Colors{StimID};               
    case 'Motion1D' % color should be random, motion fixed 
                Motions = Master.Motions{StimID};
                Colors = Master.Colors{5};
    case 'Color1D' % motion should be random, color fixed
                Motions = Master.Motions{5};
                Colors = Master.Colors{StimID};
               
end 
 Motion_FieldNum = 2;
Stim.motionDir = Motions.direction;
Stim.motionCohe = Motions.coherence;
Stim.colorType = Colors.type;
Stim.colorDia = Colors.dia;
Stim.colorBri = Colors.bri;
Stim.colorSpeed = Colors.speed;
Stim.colorCoherence = Colors.coherence;
Stim.ColorStartPoint = Colors.ColorStartPoint;

if strcmp(display.ColorMove,'Accumi')
   Stim.ColXCenter = Colors.ColXCenter;
   Stim.ColYCenter = Colors.ColYCenter;
end 
                %%%%%%% HERE add saving the starting point. 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  MOTION  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%   Moving in ACTUAL space   %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch Side
    case 'L' %   Center of the aperture (degrees) set left
        center = [-(display.apertureSize(1)+display.distBetweenStim)/2,0];
    case 'R' %   Center of the aperture (degrees) set right
        center = [(display.apertureSize(1)+display.distBetweenStim)/2,0];
    case 'Center'
        center = [0 0];
end 

%Zero out the screen position vectors and the 'goodDots' vector
dots.pixpos_x = zeros(display.nFramesTrial,display.dotsNum); % this has row for each frame and column for each dot 
dots.pixpos_y = zeros(display.nFramesTrial,display.dotsNum);
dots.goodDots = false(display.nFramesTrial,display.dotsNum);
%Generate a random order to draw the dots so that one field won't occlude another field.
order=  randperm(display.dotsNum);

Motion_DotsNumPerDirection = (display.dotsNum/Motion_FieldNum); % number of dots in each field
% Calculate the left, right top and bottom of each aperture (in degrees)
% this is to through back the dots once they get to the end
% Random or Back2Start - doesnt look different, Pilly used Back2Start
    l = center(1)-display.apertureSize(1)/2;
    r = center(1)+display.apertureSize(1)/2;
    b = center(2)-display.apertureSize(2)/2;
    t = center(2)+display.apertureSize(2)/2;
% Set direction for dots (2 per stimulus) and initialize their position
for d = 1:Motion_FieldNum
% 1-2 are fields of the stimulus, each going in a different direction    
   switch d
        case 1 
            dots(d).direction = Motions.direction; % or left and whithin +0 or +180
        case 2
            dots(d).direction = Motions.direction + 180; % or left and whithin +0 or +180
   end
   % Intitialize the dot positions and define some other initial parameters
   dots(d).x = (rand(1,Motion_DotsNumPerDirection)-.5)*display.apertureSize(1) + center(1);
   dots(d).y = (rand(1,Motion_DotsNumPerDirection)-.5)*display.apertureSize(2) + center(2);
end 
            
%Zero out the screen position vectors and the 'goodDots' vector
pixpos.x = zeros(1,display.dotsNum);
pixpos.y = zeros(1,display.dotsNum);
goodDots = false(size(zeros(1,display.dotsNum)));
                
MotionSpeed = display.dotsSpeed;
            
%%%%%%%%%%%% ONE D STIM %%%%%%%%%%%%%%
if Motions.direction==1000 % 1D trials
    Motion_FieldNum = 8;    
    Motion_DotsNumPerDirection = (display.dotsNum/Motion_FieldNum);    
    for d = 1:Motion_FieldNum
    % 1-2 are fields of the stimulus, each going in a different direction    
        switch d
            case {1,2,3,4} 
                 dots(d).direction = (d-1)*45;
            case {5,6,7,8}
                 dots(d).direction = (d-1-4)*45 + 180; % or left and whithin +0 or +180
        end
%    % Intitialize the dot positions and define some other initial parameters
       dots(d).x = (rand(1,Motion_DotsNumPerDirection)-.5)*display.apertureSize(1) + center(1);
       dots(d).y = (rand(1,Motion_DotsNumPerDirection)-.5)*display.apertureSize(2) + center(2);
    end % end field loop
end % end OneD condition

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% moving the dots %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% go through the frames
for i = 1:display.nFramesTrial
    if display.HowOftenRandMotion==1
       DotsOrder = randperm(Motion_DotsNumPerDirection);
    elseif mod(i,display.HowOftenRandMotion) == 1 % When its not 1, so only change every X trials
       DotsOrder = randperm(Motion_DotsNumPerDirection);
    end 
    count = 1;
   %% go through the fields
    for d = 1:Motion_FieldNum

        %%%%% for 1D %%%%%
        if Motions.direction==1000 % 1D trials
            switch d
                case {1,2,3,4}
                    Motions.coherence = Master.Motions{d}.coherence;
                case {5,6,7,8}
                    Motions.coherence = Master.Motions{d-4}.coherence;
            end 
        Stim.motionCohe = nan;
        end  
        %Create a direction vector for a given coherence level, per frame
        direct = rand(1,Motion_DotsNumPerDirection)*360;                 % set a vector length Dots random directions
        nCoherent = ceil(Motions.coherence*Motion_DotsNumPerDirection);  % how many dots should be coherent? 
        direct(1:nCoherent) = dots(d).direction;                  % Set the 'coherent' directions for the first nCoherent dots. 
        % Need to suffle the vector, in each frame the coherent dots should be differnet! (Pilly's BM algorithem)
        % (shuffeling is done above , see 'DotsOrder')   used to be:      %direct=direct(randperm(length(direct)));
        direct=direct(DotsOrder);
        dots(d).life = ceil(rand(1,Motion_DotsNumPerDirection)*display.dotsLifetime);         
        dots(d).x = dots(d).x + MotionSpeed*sin(direct*pi/180)/display.frameRate;
        dots(d).y = dots(d).y - MotionSpeed*cos(direct*pi/180)/display.frameRate;
        % using 'Back2Start' method like in Pilly - to have the option of 'Random' see at end of code - note 1
             dots(d).x(dots(d).x<l) = dots(d).x(dots(d).x<l) + display.apertureSize(1);
             dots(d).x(dots(d).x>r) = dots(d).x(dots(d).x>r) - display.apertureSize(1);
             dots(d).y(dots(d).y<b) = dots(d).y(dots(d).y<b) + display.apertureSize(2);
             dots(d).y(dots(d).y>t) = dots(d).y(dots(d).y>t) - display.apertureSize(2);
        %Increment the 'life' of each dot - based on Lifetime - at the
        %moment its the length of the trial but might change (to not follow a single dot)
        dots(d).life = dots(d).life+1;
        %Find the 'dead' dots
        if display.dotskill
            deadDots = mod(dots(d).life,display.dotsLifetime)==0;
            %Replace the positions of the dead dots to random locations
            dots(d).x(deadDots) = (rand(1,sum(deadDots))-.5)*display.apertureSize(1) + center(1);
            dots(d).y(deadDots) = (rand(1,sum(deadDots))-.5)*display.apertureSize(2) + center(2);
        end 
        %Calculate the index for this field's dots into the whole list of
        %dots.  Using the vector 'order' means that, for example, the first
        %field is represented not in the first n values, but rather is
        %distributed throughout the whole list.
        idx = order(count:(count+Motion_DotsNumPerDirection-1));
        %Calculate the screen positions for this field from the real-world coordinates
        pixpos.x(idx) = angle2pix(display,dots(d).x)+ display.resolution(1)/2;
        pixpos.y(idx) = angle2pix(display,dots(d).y)+ display.resolution(2)/2;
        %Determine which of the dots in this field are outside this field's
        %elliptical aperture
        goodDots(idx) = (dots(d).x-center(1)).^2/(display.apertureSize(1)/2)^2 + (dots(d).y-center(2)).^2/(display.apertureSize(2)/2)^2 < 1;
        count = count+Motion_DotsNumPerDirection;
    end % finished one frame
    % Save location (first location is already after one move)
    Stim.pixpos_x(i,:) =  pixpos.x;
    Stim.pixpos_y(i,:) =  pixpos.y;
    % setting 'goodDots' based on wanted shape (input)
        if strcmp(display.WantedShape,'square')
        Stim.goodDots(i,:) = true;
        elseif strcmp(display.WantedShape,'circle')
        Stim.goodDots(i,goodDots) = true;
        end

end

 
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  COLOR   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%   Movement in Color Space   %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% YCBCR (same values as YUV) in Matlab:
% left down corer is 0 and 0. middle is 0.5,0.5. top  right is 1,1 (in
% matlab's representation of YUV/ycbcr space)

% preallocate the matrix for all locations on color space
% this has row for each frame and column for each dot 
Color_pos_x = ones((display.nFramesTrial),display.dotsNum)*0.5; 
Color_pos_y = ones((display.nFramesTrial),display.dotsNum)*0.5;

% First position: 
%%% Computed in another script based on piloting:
% this is the point where :
% after 100ms they will be at the detection-point of that color (meaning the 10th percentile of 35 pilot participants)
% and at the end of each specific trial they will be at the original target we planned for (in the middle of the each color's square).
% this is the point they should start on each diagonal - the direct
% distance from 0. 
% This ran with BE400. didnt work.  
% DiagonalStartPoint = [0.0316  ,  0.0322  ,  0.0365 ,  0.0486];
%NewSpeed_Target = [0.0109 , 0.0112  ,  0.0127  ,  0.0168];

%% starting point :
% Colors.ColorStartPoint % in diagonal
% switch Colors.type % from diagonal to X Y coordinates in YUV space
%     case {'c1'} % 
%     Xd = 0.5 + Colors.ColorStartPoint/(2^0.5);
%     Yd = 0.5 + Colors.ColorStartPoint/(2^0.5);
%     case 'c2'
%     Xd = 0.5 - Colors.ColorStartPoint/(2^0.5);
%     Yd = 0.5 + Colors.ColorStartPoint/(2^0.5);
%     case 'c3'
%     Xd = 0.5 - Colors.ColorStartPoint/(2^0.5);
%     Yd = 0.5 - Colors.ColorStartPoint/(2^0.5);
%     case 'c4'
%     Xd = 0.5 + Colors.ColorStartPoint/(2^0.5);
%     Yd = 0.5 - Colors.ColorStartPoint/(2^0.5);
%     case 'c5'
%     Xd = 0.5;
%     Yd = 0.5;
% end 
Xd = 0.5;
Yd = 0.5;
pos_x = ones(1,display.dotsNum)*Xd; 
pos_y = ones(1,display.dotsNum)*Yd;    

Color_pos_x(1,:) =  pos_x;
Color_pos_y(1,:) =  pos_y;
% First color in RGB space (brightness allways 0.5)
ycbcr(:,2) = Color_pos_x(1,Stim.goodDots(1,:))';
ycbcr(:,3) = Color_pos_y(1,Stim.goodDots(1,:))';
ycbcr(:,1) = Colors.bri;

% Cell array where each cell is a frame, and each frame has an RGB color (3
% columns) for each dot in a matrix. 
Stim.RGB{1} = ycbcr2rgb(ycbcr)'*255; 


% Set color speed 
Colspeed = Colors.speed;


% switch display.ColorMove
%     case 'SpeedBased'
        for i = 2:(display.nFramesTrial) % not sure why but needs to match the actual space matrix size 
            % calculate correct angle from the previous frame
            % Set first the X Y coordinates of the goal of motion  
            switch Colors.type
                case {'c1','c5'} % doesnt matter for c5 - no movement anyway
                Xd = 0.5 + Colors.dia/(2^0.5);
                Yd = 0.5 + (Colors.dia/(2^0.5)+0.2); % making blue more far than pink
                Coldirect = mod((atan2d(Yd-pos_y,Xd-pos_x)+360),360);%*pi/180;
                case 'c2'
                Xd = 0.5 - Colors.dia/(2^0.5);
                Yd = 0.5 + Colors.dia/(2^0.5);
                Coldirect = mod((atan2d(Yd-pos_y,Xd-pos_x)+360),360);%*pi/180;
                case 'c3'
                Xd = 0.5 - Colors.dia/(2^0.5);
                Yd = 0.5 - Colors.dia/(2^0.5);
                Coldirect = mod((atan2d(Yd-pos_y,Xd-pos_x)+360),360);%*pi/180;
                case 'c4'
                Xd = 0.5 + Colors.dia/(2^0.5);
                Yd = 0.5 - (Colors.dia/(2^0.5)+0.2); % making blue more far than pink
                Coldirect = mod((atan2d(Yd-pos_y,Xd-pos_x)+360),360);%*pi/180;
            end 
            % Calculate the correct angle towards this location. 
            %Create a direction vector for a given coherence level, per frame
            notCoherent = display.dotsNum - floor(Colors.coherence*display.dotsNum);  % how many dots should not be coherent? 
            Coldirect(1:notCoherent) = rand(1,notCoherent)*360;  % Set the 'not-coherent' directions as random
            % Need to suffle the vector, in each frame the coherent dot should be
            % differnet! Pilly did that in actual space.
            Coldirect=Coldirect(randperm(length(Coldirect)));

            pos_x = pos_x + Colspeed*cos(Coldirect*pi/180);
            pos_y = pos_y + Colspeed*sin(Coldirect*pi/180);

            % keeping the dots inside the 0-1 range.
            Color_pos_x(i,:) =  pos_x; % each row is frame each columnn is dot
            Color_pos_y(i,:) =  pos_y; % each row is frame each columnn is dot  

            % making all the values above 1 or below 0 to be set to 1 and 0 respectivly. 
            % this doesnt change the values of position (pos_x or pos_y) so movement is still the same
            % this only makes sure that the movement in space stays in the matlab's YCbCr space, otherwise the X axis starts behaving funny. 
            Color_pos_x(i,(Color_pos_x(i,:)>1)) = 1;
            Color_pos_x(i,(Color_pos_x(i,:)<0)) = 0;
            Color_pos_y(i,(Color_pos_y(i,:)>1)) = 1;
            Color_pos_y(i,(Color_pos_y(i,:)<0)) = 0;
            % transforming to the smaller representation of the color space
            % what used to be a space between 0,0 until 1,1
            % is now a space between 0.1,0.2 until 0.9,1.0
            % or: X is between 0.1 and 0.9 and Y is between 0.2 and 1.0
            % the reason is unbalnced color space
            if Colspeed~=0 % not on the 1D trials
                Color_pos_x = Color_pos_x*(0.8) + 0.1;
                Color_pos_y = Color_pos_y*(0.8) + 0.2;
            end 
            %% transforming to RGB
            clear ycbcr % this is needed becasue the ycbcr changes size due to 'GoodDots'
            ycbcr(:,2) = Color_pos_x(i,Stim.goodDots(i,:))';
            ycbcr(:,3) = Color_pos_y(i,Stim.goodDots(i,:))';
            ycbcr(:,1) = Colors.bri;
            Stim.RGB{i} = ycbcr2rgb(ycbcr)'*255; % whatever is above 1 or below 0 is set automatically to the border (1 or 0)
            % this creates a cell array length of frames oer trial 
            % each cell has a 3 x number-of-good-dots matrix with RGB
            % colors for each dot
        end % end loop i-2:...
%     case 'Accumi'
%         % we want each frame that an X coherent number of dots will be
%         % colored 
%         % for Accumi see how many dots are colored per frame:
%         ColDotsPF = ceil(cumsum(ones(display.nFramesTrial,1)).*Colors.coherence);
%     %    ColDotsPF = ceil(cumsum(ones(96,1)).*0.1);
%         switch Colors.type
%             case {'c1','c5'} % doesnt matter for c5 - no movement anyway
%             Xd = 0.5 + Colors.ColXCenter;
%             Yd = 0.5 + Colors.ColYCenter;
%             case 'c2'
%             Xd = 0.5 - Colors.ColXCenter;
%             Yd = 0.5 + Colors.ColYCenter;
%             case 'c3'
%             Xd = 0.5 - Colors.ColXCenter;
%             Yd = 0.5 - Colors.ColYCenter;
%             case 'c4'
%             Xd = 0.5 + Colors.ColXCenter;
%             Yd = 0.5 - Colors.ColYCenter;
%         end
%         for i = 2:(display.nFramesTrial)
%             Order = randperm(display.dotsNum);
%             nCoherent = ColDotsPF(i); % how many dots to color
% 
%             pos_x = ones(1,display.dotsNum)*0.5;
%             pos_x(1:nCoherent) = Xd;
%             pos_y = ones(1,display.dotsNum)*0.5;
%             pos_y(1:nCoherent) = Yd;
% 
%             pos_x = pos_x(Order);
%             pos_y = pos_y(Order);
%          
%              % keeping the dots inside the 0-1 range.
%             Color_pos_x(i,:) =  pos_x; % each row is frame each columnn is dot
%             Color_pos_y(i,:) =  pos_y; % each row is frame each columnn is dot  
% 
%              % making all the values above 1 or below 0 to be set to 1 and 0 respectivly. 
%             Color_pos_x(i,(Color_pos_x(i,:)>1)) = 1;
%             Color_pos_x(i,(Color_pos_x(i,:)<0)) = 0;
%             Color_pos_y(i,(Color_pos_y(i,:)>1)) = 1;
%             Color_pos_y(i,(Color_pos_y(i,:)<0)) = 0;
%             % transforming to the smaller representation of the color space
%             % the reason is unbalnced color space
%             % not here, becasue grey isnt grey
% %             if Colspeed~=0 % not on the 1D trials
% %                     Color_pos_x = Color_pos_x*(0.8) + 0.1;
% %                     Color_pos_y = Color_pos_y*(0.8) + 0.2;
% %             end 
%             % transforming to RGB
%             clear ycbcr % this is needed becasue the ycbcr changes size due to 'GoodDots'
%             ycbcr(:,2) = Color_pos_x(i,Stim.goodDots(i,:))';
%             ycbcr(:,3) = Color_pos_y(i,Stim.goodDots(i,:))';
%             ycbcr(:,1) = Colors.bri;
%             Stim.RGB{i} = ycbcr2rgb(ycbcr)'*255; 
%         end % finish loop of frames
% end 


end 
% switch display.ColorMove
%     case 'SpeedBased'

%     case 


%% 'CoherenceBased'
% for i = 1:(display.nFramesTrial) % not sure why but needs to match the actual space matrix size 
%     % calculate correct angle from the previous frame
%     % Set first the X Y coordinates of the goal of motion 
%     Sd = Colors.CohSignalSTD;
%     SdNoise = Colors.CohNoiseSTD;
%     if display.HowOftenRandColor==1
%        Order = randperm(display.dotsNum);
%     elseif mod(i,display.HowOftenRandColor) == 1 % When its not 1, so only change every X trials
%        Order = randperm(display.dotsNum);
%     end 
% %     if mod(i,display.HowOftenRandColor) == 1 % When its not 1, so only change every X trials
% %     Order = randperm(display.dotsNum);
% %     end 
%  %   Order = randperm(display.dotsNum);
% %     switch Colors.type
% %         case 'c1'
% %         Xd = 0.5 + Colors.dia/(2^0.5);
% %         Yd = 0.5 + Colors.dia/(2^0.5);
% %         % Coldirect = mod((atan2d(Yd-pos_y,Xd-pos_x)+360),360);%*pi/180;
% %         case 'c2'
% %         Xd = 0.5 - Colors.dia/(2^0.5);
% %         Yd = 0.5 + Colors.dia/(2^0.5);
% %         % Coldirect = mod((atan2d(Yd-pos_y,Xd-pos_x)+360),360);%*pi/180;
% %         case 'c3'
% %         Xd = 0.5 - Colors.dia/(2^0.5);
% %         Yd = 0.5 - Colors.dia/(2^0.5);
% %         % Coldirect = mod((atan2d(Yd-pos_y,Xd-pos_x)+360),360);%*pi/180;
% %         case 'c4'
% %         Xd = 0.5 + Colors.dia/(2^0.5);
% %         Yd = 0.5 - Colors.dia/(2^0.5);
% %         %   Coldirect = mod((atan2d(Yd-pos_y,Xd-pos_x)+360),360);%*pi/180;
% %         case 'c5'
% %         Xd = 0.5;
% %         Yd = 0.5;
% %         Sd = 0.0001;
% %     end 
%     %Create a direction vector for a given coherence level, per frame
%     %  nCoherent = ceil(Colors.coherence*display.dotsNum);  % how many dots should be coherent? 
%     if strcmp(Colors.type,'c5')
%         Xd = 0.5;
%         Yd = 0.5;
%     nCoherent =display.dotsNum;  % how many dots should be coherent? 
%     pos_x = normrnd(0.5,SdNoise,[1,display.dotsNum]);                 % set a vector length Dots random directions
%     pos_x(1:nCoherent) = normrnd(Xd,Sd,[1,nCoherent]);                  % Set the 'coherent' directions for the first nCoherent dots. 
%     % Need to suffle the vector, in each frame the coherent dots should be differnet! (Pilly's BM algorithem)
%     pos_x = pos_x(Order);
% 
%     pos_y = normrnd(0.5,SdNoise,[1,display.dotsNum]);                 % set a vector length Dots random directions
%     pos_y(1:nCoherent) = normrnd(Yd,Sd,[1,nCoherent]);                  % Set the 'coherent' directions for the first nCoherent dots. 
%     % Need to suffle the vector, in each frame the coherent dots should be differnet! (Pilly's BM algorithem)
%     pos_y = pos_y(Order);  
%     else % not 1D so not grey
%     %%%%%%%%%%%%%%%%% Drawing the dots: big noise or 4 circles
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   %  XCenter = [0.6003    0.3925    0.3658    0.6754]*0.8 + 0.1;
%   %  YCenter = [0.6003    0.6075    0.3658    0.3246]*0.8 + 0.2; 
%     % this change was introduced to BN200 version 06.11.2018
% %     XCenter = [0.6503    0.3925    0.3658    0.6754]*0.8 + 0.1;
% %     YCenter = [0.6503    0.6075    0.3658    0.3246]*0.8 + 0.2; 
%     
%     XCenter = display.ColXCenter;
%     YCenter = display.ColYCenter;
%     
%     % these are organized by c1 c2 c3 c4 and are already transformed to
%     % center being 0.5 and the X Y coordinats. 
%     
%     % trick to get the signal color 
%     Nos = {'c1','c2','c3','c4'};
%     Sig =  Nos(strcmp(Nos,Master.Colors{StimID}.type));
%     Nos(strcmp(Nos,Master.Colors{StimID}.type)) = [];
%     % Now Sig is the target color type and Nos is the noise. 
%     SigXCenter = XCenter(str2double(Sig{1}(2)));
%     SigYCenter = YCenter(str2double(Sig{1}(2)));
%              NoiseXCenter = zeros(3,1);
%              NoiseYCenter = zeros(3,1);
%         for n = 1:3 % getting all noise centers 
%             NoiseXCenter(n) = XCenter(str2double(Nos{n}(2)));
%             NoiseYCenter(n) = YCenter(str2double(Nos{n}(2)));
%         end 
%      switch display.ColorDrawing  
%          case 'BigNoise'
%             nCoherent = ceil(Colors.coherence*display.dotsNum);  % how many dots should be coherent? 
%             pos_x = normrnd(0.5,SdNoise,[1,display.dotsNum]);     % start with noise 100%
%             pos_x(1:nCoherent) = normrnd(SigXCenter,Sd,[1,nCoherent]);    % Set the 'coherent' color for the first nCoherent dots. 
%             % Need to suffle the vector, in each frame the coherent dots should be differnet! (Pilly's BM algorithem)
%             pos_x = pos_x(Order);
% 
%             %pos_y = normrnd(0.5,SdNoise,[1,display.dotsNum]);                 % set a vector length Dots random directions
%             pos_y = normrnd(display.NoiseYCenter,SdNoise,[1,display.dotsNum]);                 % set a vector length Dots random directions
%             pos_y(1:nCoherent) = normrnd(SigYCenter,Sd,[1,nCoherent]);                  % Set the 'coherent' directions for the first nCoherent dots. 
%             % Need to suffle the vector, in each frame the coherent dots should be differnet! (Pilly's BM algorithem)
%             pos_y = pos_y(Order);  
%          case  'FourCircles'        
%              % split to 4 circles 
%              notCoherent = ceil((1-Colors.coherence)*display.dotsNum/3); 
%              nCoherent = display.dotsNum-notCoherent*3;
%              % now we use the SD for all (since they are all the same)
%              pos_x = zeros(1,display.dotsNum);
%              pos_x(1:notCoherent)                     = normrnd(NoiseXCenter(1),Sd,[1,notCoherent]);   
%              pos_x(notCoherent+1:2*notCoherent)       = normrnd(NoiseXCenter(2),Sd,[1,notCoherent]);   
%              pos_x(2*notCoherent+1:3*notCoherent)     = normrnd(NoiseXCenter(3),Sd,[1,notCoherent]);   
%              pos_x(3*notCoherent+1:end)               = normrnd(SigXCenter,Sd,[1,nCoherent]);
% 
%              pos_y = zeros(1,display.dotsNum);
%              pos_y(1:notCoherent)                     = normrnd(NoiseYCenter(1),Sd,[1,notCoherent]);   
%              pos_y(notCoherent+1:2*notCoherent)       = normrnd(NoiseYCenter(2),Sd,[1,notCoherent]);   
%              pos_y(2*notCoherent+1:3*notCoherent)     = normrnd(NoiseYCenter(3),Sd,[1,notCoherent]);   
%              pos_y(3*notCoherent+1:end)               = normrnd(SigYCenter,Sd,[1,nCoherent]);
% 
%              % randomize the dots 
%              pos_x = pos_x(Order);  
%              pos_y = pos_y(Order);  
%      end  
%     end 
% 
%     % keeping the dots inside the 0-1 range.
%     Color_pos_x(i,:) =  pos_x; % each row is frame each columnn is dot
%     Color_pos_y(i,:) =  pos_y; % each row is frame each columnn is dot  
% 
%     %% making all the values above 1 or below 0 to be set to 1 and 0 respectivly. 
%     % this doesnt change the values of position (pos_x or pos_y) so movement is still the same
%     % this only makes sure that the movement in space stays in the matlab's YCbCr space, otherwise the X axis starts behaving funny. 
% 
%     Color_pos_x(i,(Color_pos_x(i,:)>1)) = 1;
%     Color_pos_x(i,(Color_pos_x(i,:)<0)) = 0;
%     Color_pos_y(i,(Color_pos_y(i,:)>1)) = 1;
%     Color_pos_y(i,(Color_pos_y(i,:)<0)) = 0;
% 
%     %% transforming to the smaller representation of the color space
%     % what used to be a space between 0,0 until 1,1
%     % is now a space between 0.1,0.2 until 0.9,1.0
%     % or: X is between 0.1 and 0.9 and Y is between 0.2 and 1.0
%     % the reason is unbalnced color space
% %     if Colspeed~=0 % not on the 1D trials
% %          if strcmp(display.ColorMove,'SpeedBased') % meaning for now not changing
% %             Color_pos_x = Color_pos_x*(0.8) + 0.1;
% %             Color_pos_y = Color_pos_y*(0.8) + 0.2;
% %          end 
% %     end 
% 
%     %% transforming to RGB
%     clear ycbcr % this is needed becasue the ycbcr changes size due to 'GoodDots'
%     ycbcr(:,2) = Color_pos_x(i,Stim.goodDots(i,:))';
%     ycbcr(:,3) = Color_pos_y(i,Stim.goodDots(i,:))';
%     ycbcr(:,1) = Colors.bri;
% 
%     Stim.RGB{i} = ycbcr2rgb(ycbcr)'*255; % whatever is above 1 or below 0 is set automatically to the border (1 or 0)
%     % this creates a cell array length of frames oer trial 
%     % each cell has a 3 x number-of-good-dots matrix with RGB
%     % colors for each dot
% 
% 
% end

    
    
    
 % switch display.ColorStarting
%     case 'AtZero'
%         pos_x = ones(1,display.dotsNum)*0.5;
%         pos_y = ones(1,display.dotsNum)*0.5;
%     case 'DrawnGu'
%         Sd = Colors.DrawnGuSignalSTD;
%         SdNoise = Colors.DrawnGuNoiseSTD;
%         switch Colors.type
%             case {'c1'} % 
%             Xd = 0.5 + Colors.FirstFrameCenter/(2^0.5);
%             Yd = 0.5 + Colors.FirstFrameCenter/(2^0.5);
%             case 'c2'
%             Xd = 0.5 - Colors.FirstFrameCenter/(2^0.5);
%             Yd = 0.5 + Colors.FirstFrameCenter/(2^0.5);
%             case 'c3'
%             Xd = 0.5 - Colors.FirstFrameCenter/(2^0.5);
%             Yd = 0.5 - Colors.FirstFrameCenter/(2^0.5);
%             case 'c4'
%             Xd = 0.5 + Colors.FirstFrameCenter/(2^0.5);
%             Yd = 0.5 - Colors.FirstFrameCenter/(2^0.5);
%             case 'c5'
%             Xd = 0.5;
%             Yd = 0.5;
%             Sd = 0.0001;
%         end 
%         
% end

%             pos_x = normrnd(0.5,SdNoise,[1,display.dotsNum]);                 % set a vector length Dots random directions
%             pos_x(1:nCoherent) = normrnd(Xd,Sd,[1,nCoherent]);                  % Set the 'coherent' directions for the first nCoherent dots. 
%             % Need to suffle the vector, in each frame the coherent dots should be differnet! (Pilly's BM algorithem)
%             pos_x = pos_x(randperm(length(pos_x)));
% 
%             pos_y = normrnd(0.5,SdNoise,[1,display.dotsNum]);                 % set a vector length Dots random directions
%             pos_y(1:nCoherent) = normrnd(Yd,Sd,[1,nCoherent]);                  % Set the 'coherent' directions for the first nCoherent dots. 
%             % Need to suffle the vector, in each frame the coherent dots should be differnet! (Pilly's BM algorithem)
%             pos_y = pos_y(randperm(length(pos_y)));
%     case 'XFramesAfter'
%         DiagonalStartPoint = repmat(Colors.StartX*Colors.speed,1,4);
%         switch Colors.type
%             case 'c1'
%                 Xs = 0.5 + DiagonalStartPoint(1)/(2^0.5);
%                 Ys = 0.5 + DiagonalStartPoint(1)/(2^0.5);
%             case 'c2'
%                 Xs = 0.5 - DiagonalStartPoint(2)/(2^0.5);
%                 Ys = 0.5 + DiagonalStartPoint(2)/(2^0.5);
%             case 'c3'
%                 Xs = 0.5 - DiagonalStartPoint(3)/(2^0.5);
%                 Ys = 0.5 - DiagonalStartPoint(3)/(2^0.5);
%             case 'c4'
%                 Xs = 0.5 + DiagonalStartPoint(4)/(2^0.5);
%                 Ys = 0.5 - DiagonalStartPoint(4)/(2^0.5);
%             case 'c5'
%                 Xs = 0.5;
%                 Ys = 0.5;
%         end 
%          pos_x = ones(1,display.dotsNum)*Xs; 
%          pos_y = ones(1,display.dotsNum)*Ys;      
% end 
% 
% 
% Color_pos_x(1,:) =  pos_x;
% Color_pos_y(1,:) =  pos_y;
% 
% % First color in RGB space (brightness allways 0.5)
% ycbcr(:,2) = Color_pos_x(1,Stim.goodDots(1,:))';
% ycbcr(:,3) = Color_pos_y(1,Stim.goodDots(1,:))';
% ycbcr(:,1) = Colors.bri;
% 
% % Cell array where each cell is a frame, and each frame has an RGB color (3
% % columns) for each dot in a matrix. 
% Stim.RGB{1} = ycbcr2rgb(ycbcr)'*255; 

%%%% Test to see the coherent calculation works: 
% count = 0;
% dotsNum = 48;
% for c = 0.1:0.01:0.5
%     count = count+1;
%     NotCoh(count) = round((1-c)*dotsNum/3); 
%     CohActual(count) = dotsNum-NotCoh(count)*3;
%     CohTheo(count) = round(c*dotsNum);
%     
%     
% end 
% 
% CohActual + NotCoh*3
% 
% CohTheo - CohActual


% Note 1: Random vs Back2Start
%             if strcmp(display.endpoint,'back2start')
%                 %Move the dots that are outside the aperture back one aperture width.
%                 dots(d).x(dots(d).x<l) = dots(d).x(dots(d).x<l) + display.apertureSize(1);
%                 dots(d).x(dots(d).x>r) = dots(d).x(dots(d).x>r) - display.apertureSize(1);
%                 dots(d).y(dots(d).y<b) = dots(d).y(dots(d).y<b) + display.apertureSize(2);
%                 dots(d).y(dots(d).y>t) = dots(d).y(dots(d).y>t) - display.apertureSize(2);
% 
% 
%                 elseif strcmp(display.endpoint,'random')
% 
%                 dots(d).x(dots(d).x<l) = (rand(1,length(dots(d).x(dots(d).x<l)))-.5)*display.apertureSize(1) + center(1);%+dots(i).dx(dots(i).x(dots(i).x<l(i)));
%                 dots(d).x(dots(d).x>r) = (rand(1,length(dots(d).x(dots(d).x>r)))-.5)*display.apertureSize(1) + center(1);%+dots(i).dx(dots(i).x(dots(i).x<r(i)));
%                 dots(d).y(dots(d).y<b) = (rand(1,length(dots(d).y(dots(d).y<b)))-.5)*display.apertureSize(2) + center(2);%+dots(i).dy(dots(i).y(dots(i).y<b(i)));
%                 dots(d).y(dots(d).y>t) = (rand(1,length(dots(d).y(dots(d).y>t)))-.5)*display.apertureSize(2) + center(2);%+dots(i).dy(dots(i).y(dots(i).y<t(i)));
% 
%             end 
