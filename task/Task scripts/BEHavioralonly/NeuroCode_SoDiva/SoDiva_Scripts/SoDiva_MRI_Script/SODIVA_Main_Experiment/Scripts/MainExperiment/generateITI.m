function display = generateITI(display)
%% This function generates a matrix with ITI for an experiment
% input: 
% Trials: number of trials 
% MeanITI: the mean of ITI. can be single scalar or a vector with different means
% RangeITI: first column is minimum and second column is maximum
%           * number of rows in MeanITI and RangeITI needs to be the same
% MaxDiviationPerMean: maximum diviation the actual means can be 
% MaxDiviation: maximum diviation the total actual length can diviate from
% the blocks(1)*sum(MeanITI) for first block
% blocks: vector of the length of each block. sum(blocks)=Trials
% 
% % % temp here
% Trials = 444;
% MeanITI = [0.400 ; 3.400 ; 1.500];
% RangeITI = [0.300 , 2.500; 1.500, 9.000; 0.700, 6.000];
% MaxDiviationPerMean = 0.01; % 10ms difference in the mean of ITI
% MaxDiviationTotal = 0.5; % max 500ms differnece in the whole experiment's Run's ITI.
% blocks = [96 ; 96 ; 96 ; 96 ; 60];
% actual function

% ITIs = generateITI(display.ITI.Trials,display.ITI.MeanITI, display.ITI.RangeITI,display.ITI.MaxDiviationPerMean,display.ITI.MaxDiviationTotal,display.ITI.blocks );

Trials = display.ITI.Trials;
MeanITI = display.ITI.MeanITI;
RangeITI =  display.ITI.RangeITI;
MaxDiviationPerMean = display.ITI.MaxDiviationPerMean;
MaxDiviationTotal = display.ITI.MaxDiviationTotal;
blocks = display.ITI.blocks;

TotalLength = Trials.*(sum(MeanITI));

ITIs = nan(Trials,length(MeanITI));

B = [0 ; cumsum(blocks)];


WORKED = false;
while ~WORKED % checks the total diviation of all the experiment is not more than MaxDiviationTotal*length(blocks)
    ITIs = nan(Trials,length(MeanITI));    
            for b = 1:length(blocks) % for every block
                BlockLength = blocks(b).*(sum(MeanITI)); % check block dont diviate
                worked_block = false;
                while ~ worked_block % checks the diviation of each block is not more than MaxDiviationTotal
                 for d = 1:length(MeanITI) % for each ITI
                        workedITI = false;
                        while ~workedITI % checks each specific ITI diviates not more than MaxDiviationPerMean
                                R = exprnd(MeanITI(d)-RangeITI(d,1),blocks(b),1)+RangeITI(d,1);
                                R(R<RangeITI(d,1) | R>RangeITI(d,2)) = mean(R);
                                % set above range to be the wanted mean
                                R(R>RangeITI(d,2)) = MeanITI(d);

                                % sanity check
                                worked1 = RangeITI(d,1) <= min(R) &&  RangeITI(d,2) >= max(R);
                                % diviation per mean
                                worked2 = abs(blocks(b)*MeanITI(d) - sum(R))< MaxDiviationPerMean;
                               % we will add at the end another blanck screen for scanning that is
                               % longer than diviation. So we need the actual experiment to not be
                               % longer than that 
                               % the MRI sequeance will be the Trials expected time given mean plus the
                               % time of black screen at the end. 
                               % expected length given means minus actual length can not be        
                               workedITI = worked1 & worked2;
                        end 
                        % save the block specific ITI specific values
                        ITIs(B(b)+1:B(b+1),d) = R;

                 end
                 
                 worked_block =  abs(BlockLength-sum(sum(ITIs(B(b)+1:B(b+1),:))))<MaxDiviationTotal;
                end
                 
            end 
             

WORKED = abs(TotalLength-sum(sum(ITIs)))<MaxDiviationTotal*length(blocks);
end 

% assign to display
display.ITI.VBDM.PostCueFix = ITIs(:,1);
display.ITI.VBDM.PostStimFix = ITIs(:,2);
display.ITI.VBDM.PostOutcomeFix = ITIs(:,3);

% mean(ITIs)
% figure
% hold on
% hist(ITIs(:,1),444);
% hist(ITIs(:,2),444);
% hist(ITIs(:,3),444);
% hold off

end 