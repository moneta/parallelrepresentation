function [SodivaMain, MainDesign, results, ResOutput, Master, display] = VBDM_Final_Experiment(display, MainDesign, Master, General_Info, Stimuli, b)

%% general intro 
MegaTime = GetSecs; % display.Timing of begining of the whole block
SodivaMain.TimeMegaTime = MegaTime; 


%% Initialize results and stim

TrialNum = size(MainDesign,1); % number of trials

% preallocatte results: 
results.Block                       = zeros(1,TrialNum);
results.Part                        = zeros(1,TrialNum);
results.Context{TrialNum}           = [];                % motion or color
results.Trial                       = (1:TrialNum); % trial number
results.response                    = zeros(1,TrialNum); % were they right?
results.firstRT                     = zeros(1,TrialNum); % just the first reaction time
results.firstKey{TrialNum}          = [];                % just the first key
results.SideCorrect{TrialNum}       = [];                % which side was correct

results.designValueLeft        = MainDesign(:,1)';
results.designValueRight       = MainDesign(:,2)';
results.designValueTarget      = MainDesign(:,3)';
results.designFixControol = MainDesign(:,4)';
results.designContext     = MainDesign(:,5)'; % motion = 1. color = 2
results.design1D          = MainDesign(:,6)'; % yes = 1, no = 0
results.designValueBackLeft = MainDesign(:,7)';
results.designValueBackRight = MainDesign(:,8)';

% added
results.designValueBackTarget = zeros(1,TrialNum); % Like the Block vector, do same to this. 
results.designValueChoosen = zeros(1,TrialNum);
results.designValueUnchosen = zeros(1,TrialNum);
results.designValueBackChoosen = zeros(1,TrialNum);
results.designValueBackUnchosen = zeros(1,TrialNum);



results.TimeTrial                   = zeros(1,TrialNum); % Time of cue appearing;
results.TimeCueStart                = zeros(1,TrialNum); % Time of cue appearing;
results.TimeFixPostCueStart         = zeros(1,TrialNum); % Time of cue appearing;
results.TimeStimStarts              = zeros(1,TrialNum); % time stim appeared;
results.TimeFramesPerStim           = zeros(1,TrialNum); % how many frames the stim went through in the time limit;
results.TimePostStimFix             = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeOutcome                 = zeros(1,TrialNum); % time the outcome appeared
results.TimePostOutcomeFix          = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeQuestion                = zeros(1,TrialNum);
results.TimeFixation3               = zeros(1,TrialNum);
results.TimeEndQuestion             = zeros(1,TrialNum);

results.TargetStimMotionDirection     = zeros(1,TrialNum);
results.TargetStimColorType{TrialNum} = [];

results.LeftStimMotionDirection     = zeros(1,TrialNum);
results.LeftStimColorType{TrialNum} = [];

results.RightStimMotionDirection    = zeros(1,TrialNum);
results.RightStimColorType{TrialNum}= {};

results.PlannedITIsPostCueFix = zeros(1,TrialNum);
results.PlannedITIsPostStimFix = zeros(1,TrialNum);
results.PlannedITIsPostOutcomeFix = zeros(1,TrialNum);

%% if we are in the MRI 
if display.RweMRI
    % TRY TO ACCESS THE SCANNER PORT
    try
        fprintf('Trying to read the scanner port now...\n'); % display task progress
        outportb(890,32) % sets pin for tristate in base+2 to up state. This allows to read from the scanner port.
    catch
        warning('Reading from the scanner port was not successful!'); % display warning
    end
    
    % EYETRACKER %
        worked = false;
        tries = 0;
        if display.EyeTracker.OnOff
            while ~worked && tries<=3 % try 3 times
                tries = tries + 1;
                if display.EyeTracker.OnOff
                    [worked, General_Info] = StartEyeTracker(General_Info, display, true, strcat('VB',num2str(StartingBlock)));
                end 
            end 
        end 

        if worked % here if not working just moving on at the moment. 
            fprintf('\n Eyetracker recording, tried %.0f times \n', tries);
        else 
            fprintf('\n Eyetracker NOT recording, tried %.0f times \n', tries);
            display.EyeTracker.OnOff = false;
        end 


        if display.EyeTracker.OnOff
        worked2 = eyetrackerStartStop(display, General_Info, 'start');

            if worked2
                fprintf('Started recording');
            else 
                fprintf('Didnt start recording');
                display.EyeTracker.OnOff = false;
            end 
        end 
    

    % MRI-RELATED PARAMETERS
    display.MRI.scannerPort = 888; % scanner port
    display.MRI.triggerSwitches = 5; % number of TRs before the experiment starts
    
    SodivaMain.Triggers = nan(display.MRI.triggerSwitches,1); 
    SodivaMain.Tflips = []; 
    DrawFormattedText(display.windowPtr,'Bereit? Das Experiment startet gleich nach dem Countdown!','center','center',display.text.color);
    Screen('DrawingFinished', display.windowPtr); % tell PTB that stimulus drawing for this frame is finished
    SodivaMain.BereitTime = Screen('Flip',display.windowPtr); % flip to the screen
    fprintf('Waiting for MRI scanner triggers...\n'); % display task progress

    try
            display.MRI.startState = inportb(display.MRI.scannerPort); % read from the scanner port
            oldState = display.MRI.startState; % save old trigger state in a separate variable
            triggerCounter = 0; % initalize the trigger counter
            while triggerCounter < display.MRI.triggerSwitches
                newState = inportb(display.MRI.scannerPort); % read from the scanner port
                if newState ~= oldState
                    triggerCounter = triggerCounter + 1; % update the trigger counter
                    SodivaMain.Triggers(triggerCounter) = GetSecs - MegaTime; % with hannika it was  MegaTime - GetSecs
                    DrawFormattedText(display.windowPtr,num2str(display.MRI.triggerSwitches-triggerCounter),'center','center',display.text.color);
                    Screen('DrawingFinished', display.windowPtr); % tell PTB that stimulus drawing for this frame is finished
                    SwTime = Screen('Flip',display.windowPtr);% flip to the screen
                    SodivaMain.Tflips(triggerCounter) = SwTime;
                end
                oldState = newState; % update the old state
            end
            fprintf('MRI triggers were successfully recorded!\n'); % display task progress    
    catch
            warning('MRI triggers are not working properly!') % display warning
        
    end

    % save the SoDivaMain with time inside maybe? 
    
    SoDivaMainData = fullfile(General_Info.SubOutPut,['SoDivaMainData_',General_Info.subjectInfo.id,'_StartBlock_',num2str(b),'.mat']);
    save(SoDivaMainData, 'SodivaMain');
  
else % we are in behavioral lab
    DrawFormattedText(display.windowPtr,'Bereit? Das Experiment startet gleich nach dem Countdown!','center','center',display.text.color);
    Screen('DrawingFinished', display.windowPtr); % tell PTB that stimulus drawing for this frame is finished
    SodivaMain.BereitTime = Screen('Flip',display.windowPtr); % flip to the screen
    WaitSecs(2);
    for coudow = 1:5
        CDow = GetSecs;
        DrawFormattedText(display.windowPtr,num2str(6-coudow),'center','center',display.text.color);
        Screen('Flip',display.windowPtr); % flip to the screen
        while (GetSecs - CDow) < 1; end 
    end 
    
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% Go through the trials   %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% where to start counting ITIs?
B = [0 ; cumsum(display.ITI.blocks)];
count_iti = B(b);


for t = 1:TrialNum 
 % Break in the middle of block
    if t == TrialNum/2 %&& b <=4 % we are in the middle of the block, make a break - not in the last block that is anyway shorter
        
        DrawFormattedText(display.windowPtr, 'Kurze Augenpause',                                                'center', display.text.lines(5),display.text.color);
        DrawFormattedText(display.windowPtr, 'Bitte nicht bewegen und die Augen nicht schliessen (Blinzeln ist OK).',                      'center', display.text.lines(6),display.text.color);
%         DrawFormattedText(display.windowPtr, 'Du hast einige Sekunden Zeit zu entspannen.',    'center', display.text.lines(9),display.text.color);
        DrawFormattedText(display.windowPtr, 'Bevor der Block weitergeht, kommt ein Countdown.',            'center', display.text.lines(10),display.text.color);
        
        SodivaMain.TimeMidBlockBreakStarts = Screen('Flip',display.windowPtr);
        
        while (GetSecs - SodivaMain.TimeMidBlockBreakStarts) < display.Timing.VBDM.MidBlockBreak % 15 seconds
        end 
        % last 9 seconds: 4 + 5
        DrawFormattedText(display.windowPtr,'Bereit? Der Block geht gleich nach dem Countdown weiter!',                'center',display.text.lines(7),display.text.color);
        DrawFormattedText(display.windowPtr, 'Vergiss nicht, immer den Fixierungskreis anzuvisieren.',             'center', display.text.lines(8),display.text.color);   
        Screen('DrawingFinished', display.windowPtr); % tell PTB that stimulus drawing for this frame is finished
        SodivaMain.TimeMidBlockBreakBereit = Screen('Flip',display.windowPtr); % flip to the screen
        WaitSecs(4); % another 4 seconds
        for coudow = 1:5 % last 5 seconds
            CDow = GetSecs;
            DrawFormattedText(display.windowPtr,num2str(6-coudow),'center','center',display.text.color);
            Screen('Flip',display.windowPtr); % flip to the screen
            while (GetSecs - CDow) < 1; end 
        end 
        
        
        SodivaMain.TimeMidBlockBreakEnds =  GetSecs - MegaTime;
    end 
    

    
fprintf('Starting Trial %.0f \n', t);
count_iti = count_iti + 1;
    %% set trial settings (block, stim, Fixi, Context)
    block = b;
    results.Block(t) = block;
    results.Part(t) = 1;
    if t ==1
    SodivaMain.TimeBlockStarts =  GetSecs - MegaTime;
    end 
    %% Split the blocks (Instructions, Adjustment, Anchoring, ..........
    
    TrialStartTime = GetSecs;
    results.TimeTrial(t) = TrialStartTime - MegaTime;
    Le  = MainDesign(t,1); % left
    Ri  = MainDesign(t,2); % right
    Tgt = MainDesign(t,3); % correctness

        if Tgt==Le
            results.SideCorrect{t} = 'Left'; 
        elseif Tgt==Ri
            results.SideCorrect{t} = 'Right'; 
        end 
        
%     if MainDesign(t,4) == 0 % normal trial
%         WasThereFix = false;
%         ConFix = false;
%     elseif MainDesign(t,4) == 1 % FixControl trial
%         WasThereFix = true;
%         ConFix = true;
%         fw = randi([display.FixControlLength, display.nFramesTrial-display.FixControlLength],1); % setting starting frame to have the extra circle
%     end
%     
    switch MainDesign(t,5)
        case 1
            context = 'Motion';
        case 2
            context = 'Color';
    end
    
    
    results.Context{t} = context;
    RT = [];
    nKeys = 0;
    f = 1;  
         
    %% Draw cue
    drawMyFixOrCue_VBDM(display,context) % not flipping 
    CueStartTime = Screen('Flip',display.windowPtr); % get starting time
    results.TimeCueStart(t) = CueStartTime - MegaTime;  
    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeCueStart trial %d in VBDM Cue flip was at %.6f', t, CueStartTime);
    end
    
    %% START THE RESPONSE QUEUE - Only meant for PC for now
    KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
    KbQueueStart(display.deviceID); % starts queue
        
    while GetSecs - CueStartTime < display.Timing.VBDM.CueTime % wait for Cue time minus 100ms fix
    end 
    
    
    %% Adding another fixation between cue and stim. 
    drawMyFixOrCue_VBDM(display,'Fixation');
    FixPostCueStart = Screen('Flip',display.windowPtr);
    results.TimeFixPostCueStart(t) = FixPostCueStart - MegaTime;
%     while GetSecs - CueStartTime < display.Timing.VBDM.CueTime% wait for
%     Cue time
%     end  % this had in 1 and 2 pilot the fixation here for the rest of
%     time. 
    while GetSecs - FixPostCueStart < display.ITI.VBDM.PostCueFix(count_iti) % wait for Cue time
    end 
    % save planned ITI
    results.PlannedITIsPostCueFix(t) = display.ITI.VBDM.PostCueFix(count_iti);
        
    %% Draw the Stim
   
    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeStimStarts trial %d in VBDM', t);
    end    
    
    StimStartTime = GetSecs;
    results.TimeStimStarts(t) = StimStartTime - MegaTime;

    while GetSecs-StimStartTime < display.Timing.VBDM.StimTime && f<=display.nFramesTrial% +display.Timing.RespFixTime; % 1.2 stimulus plus 0.8 fix
            DrawMeSomeDots(display,f,Stimuli.Rstim{t},Stimuli.Lstim{t});                         % dont flip 
            
%             if ConFix
%                 if f >(fw-display.FixControlLength) && f < (fw + display.FixControlLength)
%                     Screen('FillOval', display.windowPtr, display.bkColor,[-display.fixation.sz/2+display.center(1),-display.fixation.sz/2+display.center(2),display.fixation.sz/2+display.center(1),display.fixation.sz/2+display.center(2)]);
%                 end
%             end 
            Screen('Flip',display.windowPtr);
            f = f +1;
    end
 
    results.TimeFramesPerStim(t) = f-1;

    %% draw fixation
    drawMyFixOrCue_VBDM(display,'Fixation');
    FixStartTime = Screen('Flip',display.windowPtr);
    results.TimePostStimFix(t) = FixStartTime - MegaTime;
        
    %% save stimuli details
    results.LeftStimMotionDirection(t)     = Stimuli.Lstim{t}.motionDir;
    results.LeftStimColorType{t}           = Stimuli.Lstim{t}.colorType;

    results.RightStimMotionDirection(t)    = Stimuli.Rstim{t}.motionDir;
    results.RightStimColorType{t}          = Stimuli.Rstim{t}.colorType;
    
    % save target, repetative but easy after maybe?
        if Tgt==Le
             results.TargetStimMotionDirection(t)     = Stimuli.Lstim{t}.motionDir;
             results.TargetStimColorType{t}           = Stimuli.Lstim{t}.colorType;
        elseif Tgt==Ri
            results.TargetStimMotionDirection(t)     = Stimuli.Rstim{t}.motionDir;
            results.TargetStimColorType{t}           = Stimuli.Rstim{t}.colorType;
        end 
   
    
    

    %% Wait until response time is over
    % only after we can start adjusting and drawing
    % This will introduce a short delay. need to check how much
    
    % SHAVING 50 ms of response, only in the perceptual part. No need for
    % it anyway and need time to compute results. 
    
    FirstPress_NotYet = true;
    % while no key was pressed and time didnt pass: check for response.
    % if there is, record it, and flush the queue and leave loop.
    % if there wasnt, then after time passed and not yet response, it will save 'no response'. 
%     while (GetSecs - FixStartTime < display.Timing.VBDM.RespFixTime-0.100) && FirstPress_NotYet % wait for response time minus 50, maximum delay before was 106ms. 
    %% Check and Get response Takes 0.04 seconds 
    [KeyIsDown, firstKeyPressTimes] = KbQueueCheck(display.deviceID);
        if  KeyIsDown %a key was pressed : record the key and time pressed
            firstKeyPressTimes(firstKeyPressTimes==0) = NaN; % little trick to get rid of 0s
            tResponse = min(firstKeyPressTimes); % gets the RT of the first key-press and its id
            firstKeyPressCode = find(firstKeyPressTimes==tResponse);
            RT = tResponse-StimStartTime;
            key_s = KbName(firstKeyPressCode);

             if iscell(key_s) % controlling that there wernt 2 keys pressed at the same exact moment. vary rare but gets stuck in analysis otherwise
             key = key_s{1};
           %  key = strcat(key,'_TwoKeys');
             else 
             key = key_s;
             end 

            switch Tgt
                case Le % Left
                    if KbName(key)==display.keyTargets(1) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(2) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end
                case Ri % Right
                    if KbName(key)==display.keyTargets(2) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(1) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end 
            end 

            % here we are at first time of KeyIsDown, so:
            FirstPress_NotYet = false; % there was a key press, no need to check again here.
            % Clear the response queue for FIXI
            KbQueueStop(display.deviceID); % stops queue
            KbQueueFlush(display.deviceID,1); % clear the response queue 
        end    
%     end 
     % now out of the loop, meaning 100ms before outcome should appear  
     if FirstPress_NotYet % meaning not yet a response
        results.response(t) = 3; % no response
        RT = NaN;
        key = 'No Response';
        KbQueueStop(display.deviceID); % stops queue
        KbQueueFlush(display.deviceID,1); % clear the response queue 
     end 
     
     results.firstRT(t)  = RT;
     results.firstKey{t} = key;

     results.Trial(t)    = t;
%%     wait for rest of the time 

%     while GetSecs - FixStartTime < display.Timing.VBDM.RespFixTime % wait for response time
%     end 

    while GetSecs - FixStartTime < display.ITI.VBDM.PostStimFix(count_iti) % wait for response time
    end 
    
    % save planned ITI
    results.PlannedITIsPostStimFix(t) = display.ITI.VBDM.PostStimFix(count_iti);
    
    
     %% draw feedback based on correctness 400ms.
     if results.response(t)==1 %correct
         if Tgt == Le
             Reslt = Le;
         else
             Reslt = Ri;
         end 
     elseif results.response(t)==2 %wrong choice
         if Tgt == Le
             Reslt = Ri;
         else
             Reslt = Le;
         end 
     else 
         Reslt = 5;
     end 

    VBDM_drawMyFeedback_Values(display,Reslt);
    OutcomeTime =  Screen('Flip',display.windowPtr) ;
    results.TimeOutcome(t) = OutcomeTime - MegaTime;

    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeOutcome trial %d in VBDM', t);
    end
    
    % wait for feedback time
    while GetSecs-OutcomeTime < display.Timing.VBDM.FeedbkTime;    end


    %% draw last fixation
    drawMyFixOrCue_VBDM(display,'Fixation');
    LastFixStartTime = Screen('Flip',display.windowPtr);
    results.TimePostOutcomeFix(t) = LastFixStartTime - MegaTime;
    
    % added to result
    if Tgt == Le
             results.designValueBackTarget(t) = results.designValueBackLeft(t);
    else % right
             results.designValueBackTarget(t) = results.designValueBackRight(t);
    end
    
     results.designValueChoosen(t) = Reslt;
     if Reslt == Le
          results.designValueBackChoosen(t) = results.designValueBackLeft(t);
          results.designValueUnchosen(t) = Ri;
          results.designValueBackUnchosen(t) = results.designValueBackRight(t);
     elseif Reslt == Ri
         results.designValueBackChoosen(t)  = results.designValueBackRight(t);
         results.designValueUnchosen(t) = Le;
         results.designValueBackUnchosen(t) = results.designValueBackLeft(t);

     else % Reslt==5, meaning no response
         results.designValueBackChoosen(t)  = 5;
         results.designValueUnchosen(t) = 5;
         results.designValueBackUnchosen(t) = 5;
     end
 
  
    % wait rest of time
%     while GetSecs-LastFixStartTime < display.Timing.VBDM.LastFixTime; end
    while GetSecs-LastFixStartTime < display.ITI.VBDM.PostOutcomeFix(count_iti); end
    % save planned ITI
    results.PlannedITIsPostOutcomeFix(t) = display.ITI.VBDM.PostOutcomeFix(count_iti); 
    
end % finished trial loop

fprintf('\n finished all trials in the VBDM  \n');
%% Make ResOutput

    ResOutput.Block = results.Block'; 
    ResOutput.Part  = results.Part';
    ResOutput.Trial = results.Trial';
    ResOutput.Context = results.Context';
    
    ResOutput.TargetStimMotionDirection = results.TargetStimMotionDirection';
    ResOutput.TargetStimColorType = results.TargetStimColorType';

    ResOutput.response = results.response';
    ResOutput.firstRT = results.firstRT';
    ResOutput.firstKey = results.firstKey';
        
    ResOutput.SideCorrect = results.SideCorrect';
   
    ResOutput.designValueLeft = results.designValueLeft';
    ResOutput.designValueRight = results.designValueRight';
    ResOutput.designValueTarget = results.designValueTarget'; 
    ResOutput.designValueBackLeft = results.designValueBackLeft';
    ResOutput.designValueBackRight = results.designValueBackRight';
    
    ResOutput.LeftStimMotionDirection = results.LeftStimMotionDirection';
    ResOutput.LeftStimColorType = results.LeftStimColorType';
    
    ResOutput.RightStimMotionDirection = results.RightStimMotionDirection';
    ResOutput.RightStimColorType = results.RightStimColorType';
    
    ResOutput.TimeTrial  = results.TimeTrial';
    ResOutput.TimeCueStart = results.TimeCueStart';
    ResOutput.TimeFixPostCueStart = results.TimeFixPostCueStart';
    ResOutput.TimeStimStarts = results.TimeStimStarts';
    ResOutput.TimeFramesPerStim = results.TimeFramesPerStim';
    ResOutput.TimePostStimFix = results.TimePostStimFix';
    ResOutput.TimeOutcome = results.TimeOutcome';
    ResOutput.TimePostOutcomeFix = results.TimePostOutcomeFix';
    ResOutput.TimeQuestion = results.TimeQuestion';
    ResOutput.TimeFixation3 = results.TimeFixation3';
    ResOutput.TimeEndQuestion = results.TimeEndQuestion';
    
    % added 
    ResOutput.designValueBackTarget = results.designValueBackTarget'; % Like the Block vector, do same to this. 
    ResOutput.designValueChoosen = results.designValueChoosen';
    ResOutput.designValueUnchosen = results.designValueUnchosen';
    ResOutput.designValueBackChoosen = results.designValueBackChoosen' ;
    ResOutput.designValueBackUnchosen = results.designValueBackUnchosen' ;

    % ITI
    ResOutput.PlannedITIsPostCueFix      = results.PlannedITIsPostCueFix';
    ResOutput.PlannedITIsPostStimFix     = results.PlannedITIsPostStimFix';
    ResOutput.PlannedITIsPostOutcomeFix  = results.PlannedITIsPostOutcomeFix';

end 