function drawMyFixOrCue(display,cue,Master,context, Arrow, WaitingTime)
% This function gets the main stimuli definitions (Master), the display,
% the cue (1-4) and the context ('Motion' or 'Color' or 'Fixation' or 'FixControl') and draws the
% relevant cue. 
% If this function gets WaitingTime as a varable, it also flips and waits
% the time. 
% Important: The cues are set so:
% cue 1 : gives color in 'c1' and motion 0-180 degrees |
% cue 2 : 'c2' and /
% cue 3 : 'c3' and -
% cue 4 : 'c4' and \
% if ~strcmp(context,'Fixation')
% fprintf('showing cue number %f in context %s \n',cue, context);
% end 
    center = display.center;
    if display.isInstr
    center(2) = display.text.lines(5);
    end
    
    sz = display.fixation.sz;
%    minisz = (1-2^(0.5))*sz;


%Screen('DrawLine', windowPtr [,color], fromH, fromV, toH, toV [,penWidth]);

a = GetSecs;

   


%sz = round(sz*2); % making color and motion cues bigger, fixation stays the same 
switch context 
    case 'Motion'
              textureIndex=Screen('MakeTexture', display.windowPtr, Arrow);
   
             %  Screen('FillOval', display.windowPtr, display.feedback.color,[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
                sz = sz + 4;
                    switch Master.Motions{cue}.direction
                        case 0
                           Screen('DrawTexture',display.windowPtr, textureIndex,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)], 90);         
                        case 45 
                          Screen('DrawTexture',display.windowPtr, textureIndex,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)], 135);
                        case 90
                            Screen('DrawTexture',display.windowPtr, textureIndex,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
                        case 135
                            Screen('DrawTexture',display.windowPtr, textureIndex,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)], 45);

                    end       
                    
                
    case 'Color'
   % textureIndex=Screen('MakeTexture', display.windowPtr, Arrow);
            fixOld = display.fixation.color;

                 X = Master.Colors{cue}.CueX;
                 Y = Master.Colors{cue}.CueY;
            
%                     switch cue
%                         case 1
%                         X = 0.5 + Master.Colors{1}.dia/(2^0.5);
%                         Y = 0.5 + Master.Colors{1}.dia/(2^0.5);
%                         case 2
%                         X = 0.5 - Master.Colors{2}.dia/(2^0.5);
%                         Y = 0.5 + Master.Colors{2}.dia/(2^0.5);
%                         case 3
%                         X = 0.5 - Master.Colors{3}.dia/(2^0.5);
%                         Y = 0.5 - Master.Colors{3}.dia/(2^0.5);
%                         case 4
%                         X = 0.5 + Master.Colors{4}.dia/(2^0.5);
%                         Y = 0.5 - Master.Colors{4}.dia/(2^0.5);
%                         otherwise 
%                             disp('The Type input is wrong'); 
%                     end 
                    
            ycbcr = [0.5, X, Y];
            display.fixation.color = ycbcr2rgb(ycbcr)'*255;

            Screen('FillOval', display.windowPtr, display.fixation.color,[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);

            display.fixation.color = fixOld;
   
    
    case 'Fixation'
        sz = display.fixation.sz;
            Screen('FillOval', display.windowPtr, display.fixation.color,[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);

%     case 'FixControl'
%             
%           %  Screen('FillOval', display.windowPtr, display.fixation.color,[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
%             Screen('FillOval', display.windowPtr, display.bkColor,[-sz/2+center(1),-sz/2+center(2),sz/2+center(1),sz/2+center(2)]);


end

           if exist('WaitingTime','var')
              Screen('Flip',display.windowPtr);
              while GetSecs-a<=WaitingTime
              end 
           end 


end 