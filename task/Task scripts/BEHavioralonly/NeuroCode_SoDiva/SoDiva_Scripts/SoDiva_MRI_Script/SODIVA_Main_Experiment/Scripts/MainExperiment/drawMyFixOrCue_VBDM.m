function drawMyFixOrCue_VBDM(display,context)

    center = display.center;
   
    switch context 
        case 'Motion'

            Screen('TextSize', display.windowPtr, display.ValOutcome.size); % select specific text font
            DrawFormattedText(display.windowPtr, 'Richtung','center','center',display.fixation.color);
        case 'Color'
            Screen('TextSize', display.windowPtr, display.ValOutcome.size); % select specific text font
            DrawFormattedText(display.windowPtr, 'Farbe','center','center',display.fixation.color);
        case 'Fixation'
            sz = display.fixation.sz;
            Screen('FillOval', display.windowPtr, display.fixation.color,[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
    end

    Screen('TextSize', display.windowPtr, display.text.size); % select specific text size
 

end 