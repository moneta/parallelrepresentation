function [SodivaMain, MainDesign, results, ResOutput, Master, display] = OneDmini_Final_Experiment(display, MainDesign, Master, General_Info, Stimuli, Phase,Study)

%% general intro 
MegaTime = GetSecs; % display.Timing of begining of the whole block
SodivaMain.TimeMegaTime = MegaTime; 


%% Initialize results and stim

TrialNum = size(MainDesign,1); % number of trials

% preallocatte results: 
results.Block                       = zeros(1,TrialNum);
results.Part                        = zeros(1,TrialNum);
results.Context{TrialNum}           = [];                % motion or color
results.Trial                       = (1:TrialNum); % trial number
results.response                    = zeros(1,TrialNum); % were they right?
results.firstRT                     = zeros(1,TrialNum); % just the first reaction time
results.firstKey{TrialNum}          = [];                % just the first key
results.SideCorrect{TrialNum}       = [];                % which side was correct

results.designValueLeft        = MainDesign(:,1)';
results.designValueRight       = MainDesign(:,2)';
results.designValueTarget      = MainDesign(:,3)';
results.designFixControol = MainDesign(:,4)';
results.designContext     = MainDesign(:,5)'; % motion = 1. color = 2
results.design1D          = MainDesign(:,6)'; % yes = 1, no = 0
results.designValueBackLeft = MainDesign(:,7)';
results.designValueBackRight = MainDesign(:,8)';

% added
results.designValueBackTarget = zeros(1,TrialNum); % Like the Block vector, do same to this. 
results.designValueChoosen = zeros(1,TrialNum);
results.designValueUnchosen = zeros(1,TrialNum);
results.designValueBackChoosen = zeros(1,TrialNum);
results.designValueBackUnchosen = zeros(1,TrialNum);



results.TimeTrial                   = zeros(1,TrialNum); % Time of cue appearing;
results.TimeCueStart                = zeros(1,TrialNum); % Time of cue appearing;
results.TimeFixPostCueStart         = zeros(1,TrialNum); % Time of cue appearing;
results.TimeStimStarts              = zeros(1,TrialNum); % time stim appeared;
results.TimeFramesPerStim           = zeros(1,TrialNum); % how many frames the stim went through in the time limit;
results.TimePostStimFix             = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeOutcome                 = zeros(1,TrialNum); % time the outcome appeared
results.TimePostOutcomeFix          = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeQuestion                = zeros(1,TrialNum);
results.TimeFixation3               = zeros(1,TrialNum);
results.TimeEndQuestion             = zeros(1,TrialNum);

results.TargetStimMotionDirection     = zeros(1,TrialNum);
results.TargetStimColorType{TrialNum} = [];

results.LeftStimMotionDirection     = zeros(1,TrialNum);
results.LeftStimColorType{TrialNum} = [];

results.RightStimMotionDirection    = zeros(1,TrialNum);
results.RightStimColorType{TrialNum}= {};

results.PlannedITIsPostCueFix = zeros(1,TrialNum);
results.PlannedITIsPostStimFix = zeros(1,TrialNum);
results.PlannedITIsPostOutcomeFix = zeros(1,TrialNum);

%% Either MRI or Behavioral we can just do a countdown 

    % if we are at the scanner, we need the Versuchstleiter to click
    % continue
    if display.RweMRI
     RestrictKeysForKbCheck([]) % first all keys
     RestrictKeysForKbCheck(display.keyTargets(3)) % only space bar
     DrawFormattedText(display.windowPtr, 'Wartet auf Versuchsleiter.',                                                       display.text.linestart, display.text.lines(9),display.text.color);
     DrawFormattedText(display.windowPtr, 'Wir fangen gleich an.',                                                          'center', display.text.lines(13),display.text.color);
     Screen('Flip',display.windowPtr);
     fprintf('\n Versuchsleiter: Nachdem du mit dem Versuchpersonen geschprochen hast,\n')
     fprintf('Und den T1 good-to-go ist,\n')
     fprintf('Bitte druck den Leertaste ZWEI MAL um den Block anzufangen,\n')
     fprintf('Waehrend des "Bereits?" gibt es 5 sekunden um den T1 anzufangen,\n')
     while ~KbCheck; end 
     while ~KbCheck; end  % on purpose needs twice input
     RestrictKeysForKbCheck([]) % back to all keys
    end 
    
    DrawFormattedText(display.windowPtr,'Bereit? Das Experiment startet gleich nach dem Countdown!','center','center',display.text.color);
    SodivaMain.BereitTime = Screen('Flip',display.windowPtr); % flip to the screen
    WaitSecs(2);
    for coudow = 1:5
        CDow = GetSecs;
        DrawFormattedText(display.windowPtr,num2str(6-coudow),'center','center',display.text.color);
        Screen('Flip',display.windowPtr); % flip to the screen
        while (GetSecs - CDow) < 1; end 
    end 
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% Go through the trials   %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% draw first Cue

switch MainDesign(1,5)
        case 1
            context = 'Motion';
        case 2
            context = 'Color';
end
drawMyFixOrCue_VBDM(display,context) % not flipping 

for t = 1:TrialNum 

fprintf('Starting Trial %.0f \n', t);

%% set trial settings (block, stim, Fixi, Context)
switch Phase
    case 'first'
         block = 1;
    case 'second'
         block = 2;
    case 'third'
         block = 3;
end 
   
    results.Block(t) = block;
    results.Part(t) = 1;
    if t ==1
    SodivaMain.TimeBlockStarts =  GetSecs - MegaTime;
    end 
    %% Split the blocks (Instructions, Adjustment, Anchoring, ..........
    
    TrialStartTime = GetSecs;
    results.TimeTrial(t) = TrialStartTime - MegaTime;
    Le  = MainDesign(t,1); % left
    Ri  = MainDesign(t,2); % right
    Tgt = MainDesign(t,3); % correctness

        if Tgt==Le
            results.SideCorrect{t} = 'Left'; 
        elseif Tgt==Ri
            results.SideCorrect{t} = 'Right'; 
        end 
        
%     if MainDesign(t,4) == 0 % normal trial
%         WasThereFix = false;
%         ConFix = false;
%     elseif MainDesign(t,4) == 1 % FixControl trial
%         WasThereFix = true;
%         ConFix = true;
%         fw = randi([display.FixControlLength, display.nFramesTrial-display.FixControlLength],1); % setting starting frame to have the extra circle
%     end
%     
    switch MainDesign(t,5)
        case 1
            context = 'Motion';
        case 2
            context = 'Color';
    end
    
    
    results.Context{t} = context;
    RT = [];
    nKeys = 0;
    f = 1;  
         
    %% Draw cue
%     drawMyFixOrCue_VBDM(display,context) % not flipping 
    CueStartTime = Screen('Flip',display.windowPtr); % get starting time
    results.TimeCueStart(t) = CueStartTime - MegaTime;
    
    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeCueStart trial %d in VBDM', t);
    end
    
    %% START THE RESPONSE QUEUE - Only meant for PC for now
    KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
    KbQueueStart(display.deviceID); % starts queue
        
    while GetSecs - CueStartTime < display.Timing.OneDMini.CueTime % wait for Cue time minus 100ms fix
    end 
    
    
    %% Adding another fixation between cue and stim. 
    drawMyFixOrCue_VBDM(display,'Fixation');
    FixPostCueStart = Screen('Flip',display.windowPtr);
    results.TimeFixPostCueStart(t) = FixPostCueStart - MegaTime;
%     while GetSecs - CueStartTime < display.Timing.VBDM.CueTime% wait for
%     Cue time
%     end  % this had in 1 and 2 pilot the fixation here for the rest of
%     time. 
    while GetSecs - FixPostCueStart < display.Timing.OneDMini.FixPostCueTime % wait for Cue time
    end 
    % save planned ITI
    results.PlannedITIsPostCueFix(t) = display.Timing.OneDMini.FixPostCueTime;
        
    %% Draw the Stim
   
    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeStimStarts trial %d in VBDM', t);
    end    
    
    StimStartTime = GetSecs;
    results.TimeStimStarts(t) = StimStartTime - MegaTime;
    
    
    
    while GetSecs-StimStartTime < display.Timing.OneDMini.StimTime && f<=display.nFramesTrial% +display.Timing.RespFixTime; % 1.2 stimulus plus 0.8 fix
            DrawMeSomeDots(display,f,Stimuli.Rstim{t},Stimuli.Lstim{t});                         % dont flip 
            
%             if ConFix
%                 if f >(fw-display.FixControlLength) && f < (fw + display.FixControlLength)
%                     Screen('FillOval', display.windowPtr, display.bkColor,[-display.fixation.sz/2+display.center(1),-display.fixation.sz/2+display.center(2),display.fixation.sz/2+display.center(1),display.fixation.sz/2+display.center(2)]);
%                 end
%             end 
            Screen('Flip',display.windowPtr);
            f = f +1;
    end
    
    
    results.TimeFramesPerStim(t) = f-1;


    %% draw fixation
    drawMyFixOrCue_VBDM(display,'Fixation');
    FixStartTime = Screen('Flip',display.windowPtr);
    results.TimePostStimFix(t) = FixStartTime - MegaTime;
        
    %% save stimuli details
    results.LeftStimMotionDirection(t)     = Stimuli.Lstim{t}.motionDir;
    results.LeftStimColorType{t}           = Stimuli.Lstim{t}.colorType;

    results.RightStimMotionDirection(t)    = Stimuli.Rstim{t}.motionDir;
    results.RightStimColorType{t}          = Stimuli.Rstim{t}.colorType;
    
    % save target, repetative but easy after maybe?
        if Tgt==Le
             results.TargetStimMotionDirection(t)     = Stimuli.Lstim{t}.motionDir;
             results.TargetStimColorType{t}           = Stimuli.Lstim{t}.colorType;
        elseif Tgt==Ri
            results.TargetStimMotionDirection(t)     = Stimuli.Rstim{t}.motionDir;
            results.TargetStimColorType{t}           = Stimuli.Rstim{t}.colorType;
        end 
   
    
    

    %% Wait until response time is over
    % only after we can start adjusting and drawing
    % This will introduce a short delay. need to check how much
    
    % SHAVING 50 ms of response, only in the perceptual part. No need for
    % it anyway and need time to compute results. 
    
    FirstPress_NotYet = true;
    % while no key was pressed and time didnt pass: check for response.
    % if there is, record it, and flush the queue and leave loop.
    % if there wasnt, then after time passed and not yet response, it will save 'no response'. 
%     while (GetSecs - FixStartTime < display.Timing.VBDM.RespFixTime-0.100) && FirstPress_NotYet % wait for response time minus 50, maximum delay before was 106ms. 
    %% Check and Get response Takes 0.04 seconds 
    [KeyIsDown, firstKeyPressTimes] = KbQueueCheck(display.deviceID);
        if  KeyIsDown %a key was pressed : record the key and time pressed
            firstKeyPressTimes(firstKeyPressTimes==0) = NaN; % little trick to get rid of 0s
            tResponse = min(firstKeyPressTimes); % gets the RT of the first key-press and its id
            firstKeyPressCode = find(firstKeyPressTimes==tResponse);
            RT = tResponse-StimStartTime;
            key_s = KbName(firstKeyPressCode);

             if iscell(key_s) % controlling that there wernt 2 keys pressed at the same exact moment. vary rare but gets stuck in analysis otherwise
             key = key_s{1};
           %  key = strcat(key,'_TwoKeys');
             else 
             key = key_s;
             end 

            switch Tgt
                case Le % Left
                    if KbName(key)==display.keyTargets(1) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(2) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end
                case Ri % Right
                    if KbName(key)==display.keyTargets(2) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(1) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end 
            end 

            % here we are at first time of KeyIsDown, so:
            FirstPress_NotYet = false; % there was a key press, no need to check again here.
            % Clear the response queue for FIXI
            KbQueueStop(display.deviceID); % stops queue
            KbQueueFlush(display.deviceID,1); % clear the response queue 
        end    
%     end 
     % now out of the loop, meaning 100ms before outcome should appear  
     if FirstPress_NotYet % meaning not yet a response
        results.response(t) = 3; % no response
        RT = NaN;
        key = 'No Response';
        KbQueueStop(display.deviceID); % stops queue
        KbQueueFlush(display.deviceID,1); % clear the response queue 
     end 
     
     results.firstRT(t)  = RT;
     results.firstKey{t} = key;

     results.Trial(t)    = t;
    
     %% draw feedback dont flip
     if results.response(t)==1 %correct
         if Tgt == Le
             Reslt = Le;
         else
             Reslt = Ri;
         end 
     elseif results.response(t)==2 %wrong choice
         if Tgt == Le
             Reslt = Ri;
         else
             Reslt = Le;
         end 
     else 
         Reslt = 5;
     end 

    VBDM_drawMyFeedback_Values(display,Reslt);
    
%% wait for rest of the time 
    
    while GetSecs - FixStartTime < display.Timing.OneDMini.RespFixTime % wait for response time
    end 
    
    % save planned ITI
    results.PlannedITIsPostStimFix(t) = display.Timing.OneDMini.RespFixTime;
    
    %% flip outcome
    OutcomeTime =  Screen('Flip',display.windowPtr) ;
    results.TimeOutcome(t) = OutcomeTime - MegaTime;

    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeOutcome trial %d in VBDM', t);
    end
    
    % draw last fix 
    drawMyFixOrCue_VBDM(display,'Fixation');
    % wait for feedback time
    while GetSecs-OutcomeTime < display.Timing.OneDMini.FeedbkTime;    end


    %% flip last fixation
%     drawMyFixOrCue_VBDM(display,'Fixation');
    LastFixStartTime = Screen('Flip',display.windowPtr);
    results.TimePostOutcomeFix(t) = LastFixStartTime - MegaTime;
    
    % added to result
    if Tgt == Le
             results.designValueBackTarget(t) = results.designValueBackLeft(t);
    else % right
             results.designValueBackTarget(t) = results.designValueBackRight(t);
    end
    
     results.designValueChoosen(t) = Reslt;
     if Reslt == Le
          results.designValueBackChoosen(t) = results.designValueBackLeft(t);
          results.designValueUnchosen(t) = Ri;
          results.designValueBackUnchosen(t) = results.designValueBackRight(t);
     elseif Reslt == Ri
         results.designValueBackChoosen(t)  = results.designValueBackRight(t);
         results.designValueUnchosen(t) = Le;
         results.designValueBackUnchosen(t) = results.designValueBackLeft(t);

     else % Reslt==5, meaning no response
         results.designValueBackChoosen(t)  = 5;
         results.designValueUnchosen(t) = 5;
         results.designValueBackUnchosen(t) = 5;
     end
 
  % draw next cue in this waiting time
if t ~= TrialNum % not on last trial
 switch MainDesign(t+1,5)
        case 1
            context = 'Motion';
        case 2
            context = 'Color';
 end
drawMyFixOrCue_VBDM(display,context) % not flipping 
end 

    % wait rest of time
%     while GetSecs-LastFixStartTime < display.Timing.VBDM.LastFixTime; end
    while GetSecs-LastFixStartTime < display.Timing.OneDMini.LastFixTime; end
    % save planned ITI
    results.PlannedITIsPostOutcomeFix(t) = display.Timing.OneDMini.LastFixTime; 

end % finished trial loop

fprintf('\n finished all trials in the VBDM  \n');
%% Make ResOutput

    ResOutput.Block = results.Block'; 
    ResOutput.Part  = results.Part';
    ResOutput.Trial = results.Trial';
    ResOutput.Context = results.Context';
    
    ResOutput.TargetStimMotionDirection = results.TargetStimMotionDirection';
    ResOutput.TargetStimColorType = results.TargetStimColorType';

    ResOutput.response = results.response';
    ResOutput.firstRT = results.firstRT';
    ResOutput.firstKey = results.firstKey';
        
    ResOutput.SideCorrect = results.SideCorrect';
   
    ResOutput.designValueLeft = results.designValueLeft';
    ResOutput.designValueRight = results.designValueRight';
    ResOutput.designValueTarget = results.designValueTarget'; 
    ResOutput.designValueBackLeft = results.designValueBackLeft';
    ResOutput.designValueBackRight = results.designValueBackRight';
    
    ResOutput.LeftStimMotionDirection = results.LeftStimMotionDirection';
    ResOutput.LeftStimColorType = results.LeftStimColorType';
    
    ResOutput.RightStimMotionDirection = results.RightStimMotionDirection';
    ResOutput.RightStimColorType = results.RightStimColorType';
    
    ResOutput.TimeTrial  = results.TimeTrial';
    ResOutput.TimeCueStart = results.TimeCueStart';
    ResOutput.TimeStimStarts = results.TimeStimStarts';
    ResOutput.TimeFramesPerStim = results.TimeFramesPerStim';
    ResOutput.TimePostStimFix = results.TimePostStimFix';
    ResOutput.TimeOutcome = results.TimeOutcome';
    ResOutput.TimePostOutcomeFix = results.TimePostOutcomeFix';
    ResOutput.TimeQuestion = results.TimeQuestion';
    ResOutput.TimeFixation3 = results.TimeFixation3';
    ResOutput.TimeEndQuestion = results.TimeEndQuestion';
    
    % added 
    ResOutput.designValueBackTarget = results.designValueBackTarget'; % Like the Block vector, do same to this. 
    ResOutput.designValueChoosen = results.designValueChoosen';
    ResOutput.designValueUnchosen = results.designValueUnchosen';
    ResOutput.designValueBackChoosen = results.designValueBackChoosen' ;
    ResOutput.designValueBackUnchosen = results.designValueBackUnchosen' ;

    % ITI
    ResOutput.PlannedITIsPostCueFix      = results.PlannedITIsPostCueFix';
    ResOutput.PlannedITIsPostStimFix     = results.PlannedITIsPostStimFix';
    ResOutput.PlannedITIsPostOutcomeFix  = results.PlannedITIsPostOutcomeFix';

end 