function SoDiva_MRI_Experiment(EyetrackerRun,StartingPoint)

Study = 'MainMRI';%Study can be BehPilot or MainMRI
%Allgood = false;
%while ~Allgood
%     %% ENTER Study Phase:
%     prompt = {'Wann soll ich anfangen?'}; % define the prompts
%     fprintf('2')
%     defaultAns = {'S'}; % define the default answers
%     StartingPoint = inputdlg(prompt,'Starting State',1,defaultAns); % create and show dialog box
%     fprintf('1')
%     if isempty(StartingPoint) % if cancel was pressed
%        f = msgbox('Process aborted: Please start again!','Error','error'); % show error message
%        uiwait(f);
%         return
%     elseif ~strcmp(StartingPoint{1},'S') && ~strcmp(StartingPoint{1},'F') && ~strcmp(StartingPoint{1},'B1') && ~strcmp(StartingPoint{1},'B2') && ~strcmp(StartingPoint{1},'B3') && ~strcmp(StartingPoint{1},'B4') && ~strcmp(StartingPoint{1},'R') 
%         f = msgbox('Wrong input! make sure you use only uppercase letters! possible inputs: S, F, B1-4, R ','Error','error'); % show error message
%         uiwait(f);
%         return
% %     else 
% %         Allgood = true; % meaning will not run again after
% %         break
%     end 
%     
%end 
%% Set Seetting 
[display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
Screen('CloseAll');
%% Get Sub Info
General_Info.ExperimentTimeStart = GetSecs;
General_Info = Get_Sub_Info(General_Info);
%% eyetracker change? 
if ~EyetrackerRun
    display.EyeTracker.OnOff = false;
end 
    
%% Run experiment
ExpFinished = false;

% open the window again after sub info was given
[display.windowPtr]=Screen('OpenWindow',display.screenNum,display.bkColor);

while ~ExpFinished
    switch StartingPoint
        case {'S'}
            Phase = 'second'; 
            Create_Stim_VBDM(Phase,display,General_Info); % generating stimuli for T1 task - needs to run on the actual scanner's computer
            %% (5.2) AGAIN 5 minutes OneD trials 
            % DURING the structural scan meant to test the colors in the projector
            OneDMini_Experiment_VBDM(Phase,Study,display,General_Info);
            StartingPoint = {'F'};
            continue
        case {'F'}
            % now fieldmaps and running in the background the stimuli
             RestrictKeysForKbCheck([]) % first all keys
             RestrictKeysForKbCheck(display.keyTargets(3)) % only space bar
             DrawFormattedText(display.windowPtr, 'Diese Messung dauert ca. 2 Minuten',           center', display.text.lines(8),display.text.color);
             DrawFormattedText(display.windowPtr, 'danach melden wir uns bei dir',           center', display.text.lines(9),display.text.color);
             DrawFormattedText(display.windowPtr, 'Bitte nicht bewegen und die Augen nicht schliessen (blinken ist ok).',           center', display.text.lines(10),display.text.color);
             Screen('Flip',display.windowPtr);
             RestrictKeysForKbCheck([]) % back to all keys
            Phase = 'third'; % keep this also in the behavioral
            Create_Stim_VBDM(Phase,display,General_Info); % generating stimuli - needs to run on the actual scanner's computer
            StartingPoint = {'B1'};
            
            fprintf('\n Versuchsleiter: Nachdem du mit dem Versuchpersonen geschprochen hast,\n')
            fprintf('Und den Fieldmap fertig ist,\n')
            fprintf('Bitte druck den Leertaste ZWEI MAL um den Block, bzw. Eyetracker anzufangen,\n')
            while ~KbCheck; end 
            while ~KbCheck; end  % on purpose needs twice input
            continue
        case {'B1'}
            StartingBlock = 1;
            LastBlock = 4;
            Main_Experiment_VBDM(StartingBlock,LastBlock,display,General_Info);
            StartingPoint = {'B2'};
            continue
        case {'B2'}
            StartingBlock = 2;
            LastBlock = 4;
            Main_Experiment_VBDM(StartingBlock,LastBlock,display,General_Info);
            StartingPoint = {'B3'};            
            continue
        case {'B3'}
            StartingBlock = 3;
            LastBlock = 4;
            Main_Experiment_VBDM(StartingBlock,LastBlock,display,General_Info);
            StartingPoint = {'B4'};
            continue
        case {'B4'}
            StartingBlock = 4;
            LastBlock = 4;
            Main_Experiment_VBDM(StartingBlock,LastBlock,display,General_Info);
            StartingPoint = {'R'};            
            continue
        case {'R'}
           GetTotalReward(Study,display,General_Info)
           ExpFinished = true; 
    end 
    
end % end experiment loop 




end   % end function 