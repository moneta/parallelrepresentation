function [Master, Anchor] = AdjustingMaster(Master, RT_ColMot, Tgt, Anchor, context, block, AdjNum, display)

if block ==1 % big adjustment 
    Alpha = 1;
    % RT_ColMot now has: 1left, 2right, 3tgt, 4fixi, 5context (1m 2c),
    % 6oneD 7response 8RT
    
    % indexing 
    conMot = RT_ColMot(:,5)==1;
    conCol = RT_ColMot(:,5)==2;
    Correct = RT_ColMot(:,7)==1 | RT_ColMot(:,7)==5;
    NotOneD = RT_ColMot(:,6)==0;
    NotFixi = RT_ColMot(:,4)==0;
    
    Index = Correct & NotOneD & NotFixi;
    
    Anchor = mean(RT_ColMot(Index&conMot,8));
    
    for f = 1:4 % all features    
        Feature =  RT_ColMot(:,3)==f;     
        % Motion
        CohOld = Master.Motions{f}.coherence;
        MeanRTMot = mean(RT_ColMot(Feature & conMot & Index,8));
        
        Master.Motions{f}.coherence = CohOld + Alpha*((MeanRTMot-Anchor)/Anchor)*CohOld; 
        % make sure coherence is not bigger than 1 and not smaller than 0.001
            if Master.Motions{f}.coherence > 1
                Master.Motions{f}.coherence =1;
            elseif Master.Motions{f}.coherence<0.001
                Master.Motions{f}.coherence =0.001;
            end 
        % Color
        switch display.ColorMove
            case 'SpeedBased'
         SpeedOld  = Master.Colors{f}.speed;
         MeanRTCol = mean(RT_ColMot(Feature & conCol & Index,8));
         Master.Colors{f}.speed = SpeedOld + (Alpha*((MeanRTCol-Anchor)/Anchor)*SpeedOld);%
            case 'Accumi'
         ColCohOld  = Master.Colors{f}.coherence;
         MeanRTCol = mean(RT_ColMot(Feature & conCol & Index,8));
         Master.Colors{f}.coherence = ColCohOld + (Alpha*((MeanRTCol-Anchor)/Anchor)*ColCohOld);%
             if Master.Colors{f}.coherence > 1
                    Master.Colors{f}.coherence =1;
             elseif Master.Colors{f}.coherence<0.001
                    Master.Colors{f}.coherence =0.001;
             end 
         end 
    end 
else
    % AdjNum can be between 1 and 6
    Alpha = 0.6 - (AdjNum-1)*0.1;
    % meaning 0.6 , 0.5, 0.4, 0.3, 0.2, 0.1
        switch context
            case 'Motion'
                CohOld = Master.Motions{Tgt}.coherence;
                Mean3RT = mean(RT_ColMot(Tgt,:));
                %Alpha = 0.5; % might want to introduce a different step size later. 0.5 makes since becasue it controlls for not too big of shifts
                Master.Motions{Tgt}.coherence = CohOld + Alpha*((Mean3RT-Anchor)/Anchor)*CohOld; 
                % make sure coherence is not bigger than 1 and not smaller than 0.001
                    if Master.Motions{Tgt}.coherence > 1
                        Master.Motions{Tgt}.coherence =1;
                    elseif Master.Motions{Tgt}.coherence<0.001
                        Master.Motions{Tgt}.coherence =0.001;
                    end 
            case 'Color'
                switch display.ColorMove
                    case 'SpeedBased'
                SpeedOld = Master.Colors{Tgt}.speed;
                Mean3RT = mean(RT_ColMot(Tgt,:));
                Master.Colors{Tgt}.speed = SpeedOld + (Alpha*((Mean3RT-Anchor)/Anchor)*SpeedOld);% (1/Master.Colors{Tgt}.coherence);      
                    case 'Accumi'
                ColCohOld = Master.Colors{Tgt}.coherence;     
                Mean3RT = mean(RT_ColMot(Tgt,:));
                Master.Colors{Tgt}.coherence = ColCohOld + (Alpha*((Mean3RT-Anchor)/Anchor)*ColCohOld);% (1/Master.Colors{Tgt}.coherence);      
                    if Master.Colors{Tgt}.coherence > 1
                        Master.Colors{Tgt}.coherence =1;
                    elseif Master.Colors{Tgt}.coherence<0.001
                        Master.Colors{Tgt}.coherence =0.001;
                    end 
                end
            case 'FirstColor' % not clear where this is used. maybe drop it? 14.11.18
                ColMean = RT_ColMot;
                Alpha = 1; 
                    for i = 1:4
                       switch display.ColorMove
                           case 'SpeedBased'
                   SpeedOld = Master.Colors{i}.speed;
                   Master.Colors{i}.speed = SpeedOld + (Alpha*((ColMean-Anchor)/Anchor)*SpeedOld);    
                           case 'Accumi'
                   ColCohOld = Master.Colors{i}.coherence;
                   Master.Colors{i}.coherence = ColCohOld + (Alpha*((ColMean-Anchor)/Anchor)*ColCohOld);    
                         if Master.Colors{i}.coherence > 1
                                Master.Colors{i}.coherence =1;
                         elseif Master.Colors{i}.coherence<0.001
                                Master.Colors{i}.coherence =0.001;
                         end
                       end
                   end 

        end 


end

    % if we have longer RTtrial so we want a positive number - increase
    % coherence to go faster
    % if we have a shoreter RTtrial than anchor we want a negative number -
    % less coherence, harder, slower



end 
