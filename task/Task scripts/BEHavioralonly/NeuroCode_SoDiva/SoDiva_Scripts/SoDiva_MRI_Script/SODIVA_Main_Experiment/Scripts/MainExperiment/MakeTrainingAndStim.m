function [MainDesign, Stimuli] = MakeTrainingAndStim(FinalDesign, Master, display)


Design = [];
for b = 1:numel(FinalDesign)
    Design = [Design ; FinalDesign{b}];
end 
    
worked = false; 
    while ~worked
        MainDesign = Design(randperm(length(Design),24),:);
        % 12 motion , 12 color 
        ConBal = length(find(MainDesign(:,5)==1))==12 && length(find(MainDesign(:,5)==2))==12;
        % 3-4 motion 1D , 3-4 color 1D
        DimBalM = length(find(MainDesign((MainDesign(:,5)==1),6)==1))<5 && length(find(MainDesign((MainDesign(:,5)==1),6)==1))>2;
        DimBalC = length(find(MainDesign((MainDesign(:,5)==2),6)==1))<5 && length(find(MainDesign((MainDesign(:,5)==2),6)==1))>2;
        % make sure there are no FixControl
        MainDesign(:,4) = 0;
        % make sure minimum 3 in a row same context 
        ctxt = MainDesign(:,5)';
        Blockis = diff([0 find(diff(ctxt)) numel(ctxt)]);
        if min(Blockis)<=2 || max(Blockis)>=8
            BlockTest = false;
        else 
            BlockTest = true;
        end 
        if ConBal && DimBalM && DimBalC
            worked = true;
        end 
    end
    
    
    
    TrialNum = size(MainDesign,1); % number of trials
    
    for t = 1:TrialNum 

    Le  = MainDesign(t,1); % left
    Ri  = MainDesign(t,2); % right
    Le_bkg = MainDesign(t,7);
    Ri_bkg = MainDesign(t,8);

    % will it be 1D trial?
    OneD = (MainDesign(t,6)==1);

    switch MainDesign(t,5)
            case 1
                context = 'Motion';
            case 2
                context = 'Color';
    end

    switch context
            case 'Motion' % motion
                if OneD
              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);
              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);
                else 
              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion', Le, Le_bkg);
              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion', Ri, Ri_bkg);
                end 

            case 'Color' % Color
                if OneD
              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
                else
              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color', Le, Le_bkg);
              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color', Ri, Ri_bkg);
                end
    end
    
    
    
    end
    

 end 

    
    






 