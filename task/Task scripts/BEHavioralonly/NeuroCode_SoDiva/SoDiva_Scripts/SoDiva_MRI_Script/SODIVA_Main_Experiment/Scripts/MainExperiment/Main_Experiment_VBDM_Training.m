function Main_Experiment_VBDM_Training(Study)
% Main Value based Experiment

%% (A) Setting initial variables
[display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
Screen('CloseAll');

% generate ITI
fprintf('\n Starting to generate ITIs');
display = generateITI(display);
fprintf('\n Finished generating ITIs \n');

% give participant Id as input get its folder 
% this is still in phase 1 so loading. 
% the switch in General info is at CreateStim('second').
[Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);
GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Sub_',IDSub,'.mat']);
clear General_Info;
load(GeneralInfoFolder, 'General_Info');

% load the Master and General info from the participant's folder
MasterFolder = fullfile(Sub_Folder,['Master_Sub_',IDSub,'.mat']); 
load(MasterFolder,'FinalMaster');
Master = FinalMaster;
clear FinalMaster

% Load design
FolderDet = (dir(fullfile(General_Info.SubDesigns,'Design_SoDivaMain_*.mat')));
Filename = fullfile(FolderDet.folder,FolderDet.name);
load(Filename,'FinalDesign');

%% Open The Window
[display.windowPtr]=Screen('OpenWindow',display.screenNum,display.bkColor);
% setting the text size and font (pretty sure it needs to happen when the wndow is open)     
Screen('TextFont', display.windowPtr, display.text.font); % select specific text font
Screen('TextSize', display.windowPtr, display.text.size); % select specific text size

HideCursor();

%% (G) General instructions: 
    DrawMeInstructions_VBDM(display,101, 4); % Also Flips
    while ~KbCheck; end 
    DrawMeInstructions_VBDM(display,102, 4); % Also Flips
    while ~KbCheck; end 
    DrawMeInstructions_VBDM(display,103, 4, Study); % Also Flips

    
    while ~KbCheck; end 
    DrawMeInstructions_VBDM(display,104, 4); % Also Flips
    while ~KbCheck; end
   % remind the values  - 1.08.18 no need to remind the values, also
    % will bring more ranking. 
%     [display, Master] = run_ValueMaps_instructions(display,Master,2);
    
    
%% (H) Run Training  
    % starting training   
    fprintf('Running The Training of the Main Study \n');
    Passed = false;
    TrainingCount = 0;
    while ~Passed && TrainingCount<=2 % maximum 3 training (start at 0)
        
    DrawMeInstructions_VBDM(display,999, 4); % Also Flips
    
    [TrainingDesign, Stimuli] = MakeTrainingAndStim(FinalDesign, Master, display);
        
    TrainingCount = TrainingCount + 1;
    %  TRAINING
    [Training_VBDM.display, Training_VBDM.SodivaMain, Training_VBDM.MainDesign, Training_VBDM.results, Training_VBDM.ResOutput, Training_VBDM.Master, Training_VBDM.Stimuli, Passed, acclvl] = VBDM_Final_Experiment_Training(display, TrainingDesign, Master, General_Info, Stimuli,TrainingCount);

    % save Training data
    Training_VBDM.NumberOfTraining = TrainingCount; 
    TrainingDataSub = fullfile(General_Info.SubOutPut,['TrainingData_VBDM' num2str(TrainingCount) '_Time_Sub_',General_Info.subjectInfo.id,'.mat']);
    save(TrainingDataSub, 'Training_VBDM');
    clear TrainingData
        if Passed 
            fprintf('\n Participant passed the training \n');
            DrawMeInstructions_VBDM(display,1001,1,acclvl) % Also Flips
            acclvl
            while ~KbCheck; end 

        elseif ~Passed && TrainingCount<=2 % about to start again
            fprintf('\n Participant needs to repeat trainig \n');
            DrawMeInstructions_VBDM(display,1002,1,acclvl) % Also Flips
            acclvl
            while ~KbCheck; end 
        elseif ~Passed && TrainingCount==3 
            fprintf('\n Participant didnt pass but 3 training is maximum. lets start and see \n');
            DrawMeInstructions_VBDM(display,1001,1,acclvl) % Also Flips
            acclvl
            while ~KbCheck; end 
        end 
        
    end 

oldType = ShowCursor();
%% ALL DONE! 
Screen('CloseAll');
% 

end
    
%     
%     
%     
% %% run main experiment, on a block-wise base
% for b = StartingBlock:6
%     % Load the stim for the block
%     StimFolder = fullfile(General_Info.SubStimuli,['Stimuli_Block_',num2str(b),'_',General_Info.subjectInfo.id,'.mat']);
%     load(StimFolder,'Stimuli');
% 
%     fprintf('\n Loaded Stimuli of Block %.0f \n', b);
%     % Run block
%     fprintf('\n Starting Block %.0f \n', b);
%     [Main_VBDM.SodivaMain, Main_VBDM.MainDesign, Main_VBDM.results, Main_VBDM.ResOutput, Main_VBDM.Master] = VBDM_Final_Experiment(display, FinalDesign{b}, Master, General_Info, Stimuli, b);
% 
%     % Save Block Results
%     BlockData = fullfile(General_Info.SubOutPut,['VBDM_Data_',General_Info.subjectInfo.id,'_Block_',num2str(b),'.mat']);
%     save(BlockData, 'Main_VBDM');
%     % Clear Stimuli
%     clear Stimuli
%     % clear block data
%     clear Main_VBDM
%     if b < 5 % end of block, start a new one      
%         DrawMeInstructions_VBDM(display,200,5); % Also Flips
%         while ~KbCheck; end 
%     end 
%     
% end 
% 
% 
% DrawMeInstructions_VBDM(display,600, 5); % Also Flips
% 
% oldType = ShowCursor();
% WaitSecs(5);
% %% ALL DONE! 
% Screen('CloseAll');
% 
% 
% 
% end 

