function Create_Stim_VBDM(Phase,display,General_Info)
% Make stim before main experiment
% get display 
% (A) Setting initial variables
%
% Phase can be: 
%             'first' - meaning choosing design 
%             'second' - meaning generating stimuli 
% 
% [display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
% Screen('CloseAll');

switch Phase
    case 'first' % still in behavioral lab
        % give participant Id as input get its folder 
        [Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);
        GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Sub_',IDSub,'.mat']);
        clear General_Info;
        load(GeneralInfoFolder, 'General_Info');
        % load the Master from the participant's folder
        MasterFolder = fullfile(Sub_Folder,['Master_Sub_',IDSub,'.mat']); 
        load(MasterFolder,'FinalMaster');
        Master = FinalMaster;
        clear FinalMaster
    case 'second' % in MRI or behavioral
         % take Get Sub Info again completly 
%         General_Info.ExperimentTimeStart = GetSecs;
%         General_Info = Get_Sub_Info(General_Info);
        Sub_Folder = General_Info.SubData;
        IDSub = General_Info.subjectInfo.id;
        % save General Info as second part (to load on a sec)
        GeneralInfoName = fullfile(General_Info.SubData,['General_Info_Part2_Sub_',General_Info.subjectInfo.id,'.mat']);
        save(GeneralInfoName, 'General_Info');
        % load the Master from the participant's folder
        MasterFolder = fullfile(Sub_Folder,['Master_Sub_',IDSub,'.mat']); 
        load(MasterFolder,'FinalMaster');
        Master = FinalMaster;
        clear FinalMaster
    case 'third' % in MRI or behavioral second time there 
%         [Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);
        Sub_Folder = General_Info.SubData;
        IDSub = General_Info.subjectInfo.id;
        GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Part2_Sub_',IDSub,'.mat']);
        clear General_Info;
        load(GeneralInfoFolder, 'General_Info');   
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%% Run the fix on context: %%%%
        %Fixcontext(General_Info)     
        NovPilot_Fixcontext(General_Info, display)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % load the New Master from the participant's folder - post fix! 
        MasterFolder = fullfile(Sub_Folder,['Master_Final_Sub_',IDSub,'.mat']); 
        load(MasterFolder,'FinalMaster');
        Master = FinalMaster;
        clear FinalMaster
end 



switch Phase
    case 'first' % still in behavioral lab
        % choose design of participant, move to folder and load it
        DesignList = dir(General_Info.pathVBDM);
        DesignNum = randi([3 length(DesignList)]);
        DesignPath = fullfile(General_Info.pathVBDM,DesignList(DesignNum).name);
        load(DesignPath,'FinalDesign');
        % Move used  design to used folder, if we are for real :)
        copyfile(DesignPath, General_Info.SubDesigns)
        if display.ForReal
            movefile(DesignPath, General_Info.pathUSEDVBDM)
        end

    case 'third' % already in the MRI seoond time to make main stim
        % load design
        FolderDet = (dir(fullfile(General_Info.SubDesigns,'Design_SoDivaMain_*.mat')));
        Filename = fullfile(General_Info.SubDesigns,FolderDet.name);
        load(Filename,'FinalDesign');

        fprintf('\n Starting to generate Stimuli for The main experiment \n Coulld take a couple of minutes \n');
        % go trial for trial, and create the stimuli
        % after making the 96th trial, meaning end of block, save, clear and make
        % next block

        for block = 1:numel(FinalDesign)
        TrialNum = size(FinalDesign{block},1); % number of trials

            for t = 1:TrialNum 

                Le  = FinalDesign{block}(t,1); % left
                Ri  = FinalDesign{block}(t,2); % right
                Le_bkg = FinalDesign{block}(t,7);
                Ri_bkg = FinalDesign{block}(t,8);

                % will it be 1D trial?
                OneD = (FinalDesign{block}(t,6)==1);

                    switch FinalDesign{block}(t,5)
                            case 1
                                context = 'Motion';
                            case 2
                                context = 'Color';
                    end

                    switch context
                            case 'Motion' % motion
                                if OneD
                              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);
                              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);
                                else 
                              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion', Le, Le_bkg);
                              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion', Ri, Ri_bkg);
                                end 

                            case 'Color' % Color
                                if OneD
                              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
                              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
                                else
                              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color', Le, Le_bkg);
                              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color', Ri, Ri_bkg);
                                end
                    end 


            end 

               StimFolder = fullfile(General_Info.SubStimuli,['Stimuli_Block_',num2str(block),'_',General_Info.subjectInfo.id,'.mat']);
               save(StimFolder,'Stimuli');
               fprintf('\n Saved Stimuli for block %.0f \n', block)
               clear Stimuli

        end   
end % end switch phase

%%%%%%%%%%%%%%%%%%%%% OneDMini %%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch Phase
    case 'first' % still in behavioral lab
        % generate 2 designs : one for each part 
        fprintf('\n Starting to generate Stimuli for Phase 1 OneD \n');
        DesignOneD.first = MakeOneDMiniDesign;
        DesignOneD.second = MakeOneDMiniDesign;
        % save both to subject's folder
        save(fullfile(General_Info.SubDesigns,'OneDDesigns.mat'),'DesignOneD');
        % keep only one and generate stimuli for it and save them
        % Notice: this is anyway only 1D trials 
        TrialNum = size(DesignOneD.first,1); % number of trials
            for t = 1:TrialNum 
                Le  = DesignOneD.first(t,1); % left
                Ri  = DesignOneD.first(t,2); % right
                % will it be 1D trial? YES
                    switch DesignOneD.first(t,5)
                            case 1 %'Motion' % motion
                              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);
                              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);
                            case 2 %'Color' % Color
                              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
                              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
                    end 
            end 
               StimFolder = fullfile(General_Info.SubStimuli,['Stimuli_OneDfirst_',General_Info.subjectInfo.id,'.mat']);
               save(StimFolder,'Stimuli');
               fprintf('\n Saved Stimuli for OneDmini Phase 1 \n')
               clear Stimuli
    case 'second' % already in the MRI
        
        % load design from participant's folder
        FolderDet = (dir(fullfile(General_Info.SubDesigns,'OneDDesigns.mat')));
        Filename = fullfile(General_Info.SubDesigns,FolderDet.name);
        load(Filename,'DesignOneD');
        
        fprintf('\n Starting to generate Stimuli for Phase 2 OneD \n Coulld take a couple of minutes \n');
        % generate stimuli (this needs to happen on the scanner's computer)
        % and save them
        % Notice: this is anyway only 1D trials 
        TrialNum = size(DesignOneD.second,1); % number of trials
            for t = 1:TrialNum 
                Le  = DesignOneD.second(t,1); % left
                Ri  = DesignOneD.second(t,2); % right
                % will it be 1D trial? YES
                    switch DesignOneD.second(t,5)
                            case 1 %'Motion' % motion
                              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);
                              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);
                            case 2 %'Color' % Color
                              Stimuli.Lstim{t} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
                              Stimuli.Rstim{t} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
                    end 
            end 
               StimFolder = fullfile(General_Info.SubStimuli,['Stimuli_OneDsecond_',General_Info.subjectInfo.id,'.mat']);
               save(StimFolder,'Stimuli');
               fprintf('\n Saved Stimuli for OneDmini Phase 2 \n')
               clear Stimuli
end % end switch phase of OneD mini


fprintf('\n Finished Create Stim for Phase %s \n \n \n you can now run the next part',Phase)
    
end 





