% tryanalyze

T = struct2table(BigData.ResOutput);
T = [T;struct2table(BigData.ResOutput)];

%FullT = T;
T = FullT(FullT.Block==2,:)
for b = 1:4
    for f = 1:4
    Color(b,f) = nanmedian(T.firstRT(T.Block==b & T.response==1 & strcmp(T.Context,'Color') & (strcmp(T.TargetStimColorType,strcat('c',num2str(f))))));
    Motion(b,f) = nanmedian(T.firstRT(T.Block==b & T.response==1 & strcmp(T.Context,'Motion') & T.TargetStimMotionDirection== (f-1)*45));
    end 
end 
figure
Feat = [Color,Motion];
bar(Feat)
ylim([0.3, 0.8])

figure 
%X = [1:height(T)]';
for c = 1:4
    subplot(4,2,c)
    plot(T.TargetStimColorCoherence(strcmp(T.Context,'Color') & strcmp(T.TargetStimColorType,strcat('c',num2str(c)))));  
    ylim([0.1, 0.6])
    title('color')
    subplot(4,2,c+4)
    plot(T.TargetStimMotionCoherence(strcmp(T.Context,'Motion') & T.TargetStimMotionDirection == (c-1)*45));  
    ylim([0.5, 1])
    title('motion')
end 

