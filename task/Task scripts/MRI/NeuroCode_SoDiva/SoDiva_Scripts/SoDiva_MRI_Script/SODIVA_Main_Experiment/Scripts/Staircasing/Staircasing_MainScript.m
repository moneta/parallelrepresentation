function Staircasing_MainScript(StartingBlock)
%%%% Staircasing5 %%%%

% addpath(genpath('D:\NirSeafile\Seafile\My Library\SODIVA\SODIVA_ADJ_PILOTING'));

% On the designs:  
% Behavioral: 
% there is a folder for each room with distinct designs
% all the value maps were created and distributed randomly between the
% rooms

%% design order 
% Trials
Order.Trials.PostAnchor             = 1 + 24;                      % after this trial we collect motion based anchor                
Order.Trials.PostMotADJ             = Order.Trials.PostAnchor + 72;              % after this motion adj is done
Order.Trials.BetweenBlocksPreValmap = 1 + 108 + 18; % after this, first ValMap based block is done (96 plus 18 choose 1)

%% (A) Setting initial variables
[display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
Screen('CloseAll');

General_Info.ExperimentTimeStart = GetSecs;

fprintf('Finished Setting basic parameters \n');

display.ShowTraining = true;

%% (B) Get Subject's info
if StartingBlock == 1
%     if display.ForReal
    General_Info = Get_Sub_Info(General_Info);
%     else 
%         load('C:\Users\moneta\Seafile_Folder\Seafile\My Library\SODIVA\SODIVA_Main_Experiment\Scripts\General_Info_Sub_TestF.mat','General_Info')
%     end 
else 
    [Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);

    GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Sub_',IDSub,'.mat']);
    clear General_Info;
    load(GeneralInfoFolder, 'General_Info');
    General_Info.STR_StartTime = GetSecs;
end 

fprintf('Took subjects information \n');

%% (C) setting basic stimuli on default to be taken and adjusted (This will be the output!)

if StartingBlock == 1
    % generate basic master         
    [Master,display] = GenerateBasicMasetr(display);
            %% Value Mapping
            MapsList = dir(General_Info.pathValueMaps);

            % MapNum = randi([3 length(MapsList)]);
            % the first 24 are randomized, so all color and all motion map types apear.
            % the next 24 is the same, sith no repeat of combinations between the two. 
            MapNum = 3; % taking the first one on the list and also transferring it to used folder.
            MapPath = fullfile(General_Info.pathValueMaps,MapsList(MapNum).name);
            load(MapPath,'ValMap');
            % Move used  design to used folder, if we are for real :)
            copyfile(MapPath, General_Info.SubDesigns)
            if display.ForReal
                movefile(MapPath, General_Info.pathUSEDValueMaps)
            end 

            fprintf('\n Mapped Value Map \n');

            Master = ValuMapping(Master, ValMap);

            fprintf('Mapped values subject-specific \n');
            % this function re-orders the order of Master to match the value mapping. 
            % Note: This will keep the perceptual identity. 
            % results.LeftStimMotionDirection     = zeros(1,TrialNum);
            % results.LeftStimColorType{TrialNum} = [];
            % And the result.designValueLeft/Right/Tgt will keep the value itself. 

            % take vlaue map
            % save the value map in the subject's folder 
            % move to Used Maps. 
            % all per room 


            % Save master. 
            MasterFolder = fullfile(General_Info.SubData,['Master_basic_',General_Info.subjectInfo.id,'.mat']); 
            save(MasterFolder,'Master');


            %% (E) Load design, move it to used
            DesignList = dir(General_Info.pathdesigns);

            DesignNum = randi([3 length(DesignList)]);
            DesignPath = fullfile(General_Info.pathdesigns,DesignList(DesignNum).name);
            load(DesignPath,'MainDesign');
            % Move used  design to used folder, if we are for real :)
            copyfile(DesignPath, General_Info.SubDesigns)
            if display.ForReal
                movefile(DesignPath, General_Info.pathUSEDdesigns)
            end
            
            %% Save General Info 
            GeneralInfoName = fullfile(General_Info.SubData,['General_Info_Sub_',General_Info.subjectInfo.id,'.mat']);
            save(GeneralInfoName, 'General_Info');


elseif StartingBlock == 2 % Master already mapped    
    
    MasterFolder = fullfile(General_Info.SubData,['Master_1stAdj_',General_Info.subjectInfo.id,'.mat']); 
            load(MasterFolder,'FinalMaster_1stAdj');
            Master = FinalMaster_1stAdj;

            % load design from subjects folder
            FolderDet = (dir(fullfile(General_Info.SubDesigns,'Staircasing_Design_*.mat')));
            Filename = fullfile(FolderDet.folder,FolderDet.name);
            load(Filename,'MainDesign');
    
else % this is after the Adjustment, so already saved mapped and adjusted
            MasterFolder = fullfile(General_Info.SubData,['Master_Sub_',General_Info.subjectInfo.id,'.mat']); 
            load(MasterFolder,'FinalMaster');
            Master = FinalMaster;

            % load design from subjects folder
            FolderDet = (dir(fullfile(General_Info.SubDesigns,'Staircasing_Design_*.mat')));
            Filename = fullfile(FolderDet.folder,FolderDet.name);
            load(Filename,'MainDesign');
end  



%% (F) Openning the window
fprintf('Opening the PTB screen \n');

[display.windowPtr]=Screen('OpenWindow',display.screenNum,display.bkColor);
% setting the text size and font (pretty sure it needs to happen when the wndow is open)     
Screen('TextFont', display.windowPtr, display.text.font); % select specific text font
Screen('TextSize', display.windowPtr, display.text.size); % select specific text size

HideCursor();


%% EyeTracker
worked = false;
tries = 0;
if display.EyeTracker.OnOff
    while ~worked && tries<=3 % try 3 times
        tries = tries + 1;
        if display.EyeTracker.OnOff
            [worked, General_Info] = StartEyeTracker(General_Info, display, true, strcat('SB',num2str(StartingBlock)));
        end 
    end 
end 

if worked % here if not working just moving on at the moment. 
    fprintf('\n Eyetracker recording, tried %.0f times \n', tries);
else 
    fprintf('\n Eyetracker NOT recording, tried %.0f times \n', tries);
    display.EyeTracker.OnOff = false;
end 

%% (G) General instructions: (only first block)
if StartingBlock == 1
    if display.ForReal
    DrawMeInstructions(display,101, 5); % Also Flips
    while ~KbCheck; end
    DrawMeInstructions(display,102, 5); % Also Flips
    while ~KbCheck; end
    DrawMeInstructions(display,103, 5); % Also Flips
    while ~KbCheck; end
    DrawMeInstructions(display,104, 4); % Also Flips
    while ~KbCheck; end
    end 
%% (H) Run Training (only first block)
    % Draw instructions of training
    
    
    if display.ForReal
    % Detailed cues Motion- what cue means what 
   [display, Master] = run_MorC_instructions(display,Master,'Motion',Arrow,ClockSign,ThumbsUp);   
   [display, Master] = run_MorC_instructions(display,Master,'Motion1D',Arrow,ClockSign,ThumbsUp);

   % Detailed cues Color- what cue means what 
   [display, Master] = run_MorC_instructions(display,Master,'Color',Arrow,ClockSign,ThumbsUp);   
   [display, Master] = run_MorC_instructions(display,Master,'Color1D',Arrow,ClockSign,ThumbsUp); 
    
    % detailed feedback info 
    [display, Master] = run_MorC_instructions(display,Master,'Feedback', Arrow,ClockSign,ThumbsUp);
    
    DrawMeInstructions(display,105) % Also Flips
    while ~KbCheck; end 
    
    end 
    % starting training   
    fprintf('Running The Training of the Perceptual Staircasing \n');
    Passed = false;
    TrainingCount = 0;
    while ~Passed && TrainingCount<=2 % maximum 3 training (start at 0)
        TrainingCount = TrainingCount + 1;
    %  TRAINING
    [TrainingData.display, TrainingData.SodivaMain, TrainingData.MainDesign, TrainingData.results, TrainingData.ResOutput, TrainingData.Master, TrainingData.Anchor, TrainingData.STIM.Lstim, TrainingData.STIM.Rstim, Passed] = Staircasing_Training(display, Master, ThumbsUp, Arrow, General_Info ,ClockSign);% , Order);
    
    % save Training data
    TrainingData.NumberOfTraining = TrainingCount; 
    TrainingDataSub = fullfile(General_Info.SubOutPut,['Training_STR_Data_' num2str(TrainingCount) '_Time_Sub_',General_Info.subjectInfo.id,'.mat']);
    save(TrainingDataSub, 'TrainingData');
    clear TrainingData
        if Passed 
            fprintf('\n Participant passed the training \n');
            DrawMeInstructions(display,1001) % Also Flips
            while ~KbCheck; end 

        elseif ~Passed && TrainingCount<=1 % about to start again
            fprintf('\n Participant needs to repeat trainig \n');
            DrawMeInstructions(display,1002) % Also Flips
            while ~KbCheck; end 
        end 
        
    end 


end

%% ACTUAL ESXPERIMENT
fprintf('Running The the Perceptual Staircasing \n');

%% EyeTrackerStarting
if display.EyeTracker.OnOff
worked2 = eyetrackerStartStop(display, General_Info, 'start');

    if worked2
        fprintf('Started recording');
    else 
        fprintf('Didnt start recording');
        display.EyeTracker.OnOff = false;
    end 
end 

for block = StartingBlock:3
   General_Info.StaircasingBlocksTime{block} = GetSecs;
   [BigData.display, BigData.SodivaMain, BigData.MainDesign, BigData.results, BigData.ResOutput, BigData.Master, BigData.Anchor, STIM.Lstim, STIM.Rstim] = Staircasing_Final_Experiment(display, MainDesign{block}, Master ,Order, ThumbsUp, Arrow, General_Info, block,ClockSign);
   fprintf('Finished the Staircasing part \n'); 
   
   % display finished block 
   if block ~= 3
    % display between block break, offer break
    DrawMeInstructions(display,300); % Also Flips
   else 
      % display thank you
      DrawMeInstructions(display,600); % Also Flips
   end 
   
   if block == 1 % just finished big adjustment
       MasterFolder = fullfile(General_Info.SubData,['Master_1stAdj_',General_Info.subjectInfo.id,'.mat']); 
       FinalMaster_1stAdj = BigData.Master;
       Master = BigData.Master;
       save(MasterFolder,'FinalMaster_1stAdj');
       
   end 
   
   if block == 2 % finished the Adjustment part
       MasterFolder = fullfile(General_Info.SubData,['Master_Sub_',General_Info.subjectInfo.id,'.mat']); 
       FinalMaster = BigData.Master;
       Master = BigData.Master;
       save(MasterFolder,'FinalMaster');
   end
   
   fprintf('\n Saving the data for the subject for block %.0f \n',block);
   
   BigDataSub = fullfile(General_Info.SubOutPut,['Staircasing_Data_Sub_',General_Info.subjectInfo.id,'_Block_0',num2str(block),'.mat']);
   save(BigDataSub, 'BigData');
   StimFolder = fullfile(General_Info.SubOutPut,['STIM_Staircasing_',General_Info.subjectInfo.id,'_Block_0',num2str(block),'.mat']);
   save(StimFolder, 'STIM'); 
   clear BigData STIM
   
   if block ~= 3
   while ~KbCheck; end     
   else 
   WaitSecs(3); 
   while ~KbCheck; end % adding wait for participant response so wont go light in 2 ppl 
   while ~KbCheck; end % adding wait for participant response so wont go light in 2 ppl 
   oldType = ShowCursor();
   end 
   
end 
%% finish eyetracker
tic
if display.EyeTracker.OnOff
worked3 = eyetrackerStartStop(display, General_Info, 'finish');

    if worked3
        fprintf('\n finished recording \n ');
    else 
        fprintf('\n Didnt finish recording \n ');
    end 
    
end
fprintf('\n time to transfer eye files was: \n')
toc
%% Save General Info - again with all timing
GeneralInfoName = fullfile(General_Info.SubData,['General_Info_Sub_',General_Info.subjectInfo.id,'.mat']);
save(GeneralInfoName, 'General_Info');

%% ALL DONE! 
Screen('CloseAll');

if display.EyeTracker.OnOff
Eyelink('Shutdown');
end 
sca


end 



