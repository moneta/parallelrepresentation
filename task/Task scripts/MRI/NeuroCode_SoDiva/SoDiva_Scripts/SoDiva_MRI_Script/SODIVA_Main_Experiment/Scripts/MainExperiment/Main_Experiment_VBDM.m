function Main_Experiment_VBDM(StartingBlock,LastBlock,display,General_Info)
% Main Value based Experiment

%% (A) Setting initial variables
% [display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
% Screen('CloseAll');

fprintf('\n Starting to generate ITIs');
display = generateITI(display);
fprintf('\n Finished generating ITIs \n');

% general Info 
% after we generated the stim for the main part (i.e. CreateStim('second'))
% we saved the Part2 General Info and we can now load it
% give participant Id as input get its folder 
% [Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);
Sub_Folder = General_Info.SubData;
IDSub = General_Info.subjectInfo.id;
GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Part2_Sub_',IDSub,'.mat']);
clear General_Info;
load(GeneralInfoFolder, 'General_Info');

% % give participant Id as input get its folder 
% if display.RweMRI
%     % take Get Sub Info again
%     General_Info = Get_Sub_Info(General_Info);
%     Sub_Folder = General_Info.SubData;
% else % we are in behavioral
%     [Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);
% end 
% 
% GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Sub_',IDSub,'.mat']);
% clear General_Info;
% load(GeneralInfoFolder, 'General_Info');

General_Info.fMRIExperimentTimeStart = GetSecs;

% load the Master and General info from the participant's folder
MasterFolder = fullfile(Sub_Folder,['Master_Final_Sub_',IDSub,'.mat']); 
load(MasterFolder,'FinalMaster');
Master = FinalMaster;
clear FinalMaster

% Load design
FolderDet = (dir(fullfile(General_Info.SubDesigns,'Design_SoDivaMain_*.mat')));
Filename = fullfile(General_Info.SubDesigns,FolderDet.name);
load(Filename,'FinalDesign');
            
% Open The Window
% [display.windowPtr]=Screen('OpenWindow',display.screenNum,display.bkColor);
% setting the text size and font (pretty sure it needs to happen when the wndow is open)     
Screen('TextFont', display.windowPtr, display.text.font); % select specific text font
Screen('TextSize', display.windowPtr, display.text.size); % select specific text size

%HideCursor();

% %% EyeTracker
% worked = false;
% tries = 0;
% if display.EyeTracker.OnOff
%     while ~worked && tries<=3 % try 3 times
%         tries = tries + 1;
%         if display.EyeTracker.OnOff
%             [worked, General_Info] = StartEyeTracker(General_Info, display, true, strcat('VB',num2str(StartingBlock)));
%         end 
%     end 
% end 
% 
% if worked % here if not working just moving on at the moment. 
%     fprintf('\n Eyetracker recording, tried %.0f times \n', tries);
% else 
%     fprintf('\n Eyetracker NOT recording, tried %.0f times \n', tries);
%     display.EyeTracker.OnOff = false;
% end 

%% (G) General instructions: 
%     DrawMeInstructions_VBDM(display,101, 3); % Also Flips
%     while ~KbCheck; end 
%     DrawMeInstructions_VBDM(display,102, 3); % Also Flips
%     while ~KbCheck; end 
%     DrawMeInstructions_VBDM(display,103, 3); % Also Flips
%     while ~KbCheck; end 

%%    %%%%%%%%%%%% EXPERIMENT STARTS %%%%%%%%%%%%%%%%%%%%
 
%% EyeTrackerStarting
% worked = false;
% tries = 0;
% if display.EyeTracker.OnOff
%     while ~worked && tries<=3 % try 3 times
%         tries = tries + 1;
%         if display.EyeTracker.OnOff
%             [worked, General_Info] = StartEyeTracker(General_Info, display, true, strcat('VB',num2str(StartingBlock)));
%         end 
%     end 
% end 
% 
% if worked % here if not working just moving on at the moment. 
%     fprintf('\n Eyetracker recording, tried %.0f times \n', tries);
% else 
%     fprintf('\n Eyetracker NOT recording, tried %.0f times \n', tries);
%     display.EyeTracker.OnOff = false;
% end 
% 
% 
% if display.EyeTracker.OnOff
% worked2 = eyetrackerStartStop(display, General_Info, 'start');
% 
%     if worked2
%         fprintf('Started recording');
%     else 
%         fprintf('Didnt start recording');
%         display.EyeTracker.OnOff = false;
%     end 
% end 
    
%% run main experiment, on a block-wise base
for b = StartingBlock:LastBlock % THIS IS SET TO 5 Blocks now.
    
     %%% Somehow make a user-dependent input here 
     RestrictKeysForKbCheck([]) % first all keys
     RestrictKeysForKbCheck(display.keyTargets(3)) % only space bar
     DrawFormattedText(display.windowPtr, 'Wartet auf Versuchsleiter.in',                           display.text.linestart, display.text.lines(4),display.text.color);
     DrawFormattedText(display.windowPtr, 'Wir fangen gleich mit dem naechsten Block an.',                          'center', display.text.lines(6),display.text.color);
     Screen('Flip',display.windowPtr);
     fprintf('\n Versuchsleiter: Nachdem du mit dem Versuchpersonen geschprochen hast,\n')
     fprintf('Bitte druck den Leertaste ZWEI MAL um den naechsten Block (bzw. EyeTracker und "Bereit?") anzufangen. \n')
     while ~KbCheck; end 
     while ~KbCheck; end  % on purpose needs twice input
     RestrictKeysForKbCheck([]) % back to all keys
    
    % Load the stim for the block
    StimFolder = fullfile(General_Info.SubStimuli,['Stimuli_Block_',num2str(b),'_',General_Info.subjectInfo.id,'.mat']);
    load(StimFolder,'Stimuli');

    fprintf('\n Loaded Stimuli of Block %.0f \n', b);
    % Run block
    fprintf('\n Starting Block %.0f \n', b);
    [Main_VBDM.SodivaMain, Main_VBDM.MainDesign, Main_VBDM.results, Main_VBDM.ResOutput, Main_VBDM.Master, Main_VBDM.display] = VBDM_Final_Experiment(display, FinalDesign{b}, Master, General_Info, Stimuli, b);

    % Save Block Results
    BlockData = fullfile(General_Info.SubOutPut,['VBDM_Data_',General_Info.subjectInfo.id,'_Block_',num2str(b),'.mat']);
    save(BlockData, 'Main_VBDM');
    % Clear Stimuli
    clear Stimuli
    if display.RweMRI
       [Reward, Kredite] = GetRewardsPerBlock(Main_VBDM,'MRI');
    else
       [Reward, Kredite] = GetRewardsPerBlock(Main_VBDM,'BEH');
    end
    % clear block data
    clear Main_VBDM
    if display.RweMRI   
        % adding here 10 seconds of this display to make sure the sequence
        % is done and sample the last BOLD (in case experiment was a bit shorter than sequence)
         DrawMeInstructions_VBDM(display,202,5,Reward); % Also Flips

    else % behavioral
        if b < 4 %3 % end of block, start a new one      
            DrawMeInstructions_VBDM(display,200,5,Reward); % Also Flips
            while ~KbCheck; end 
%         elseif b == 4 % || b == 5  % between blocks 4 and 5  - this is not anymore and between 5 and 6  different prediction of block length  
%             DrawMeInstructions_VBDM(display,201,5,Reward); % Also Flips
%             while ~KbCheck; end 
        end 
    end 
    
end % finished all blocks


DrawMeInstructions_VBDM(display,600, 3); % Also Flips

oldType = ShowCursor();


tic
if display.EyeTracker.OnOff
    if display.RweMRI
worked3 = eyetrackerStartStop(display, General_Info, 'finishMRI');
    else
worked3 = eyetrackerStartStop(display, General_Info, 'finish');
    end 
    
    if worked3
        fprintf('\n finished recording \n ');
    else 
        fprintf('\n Didnt finish recording \n ');
    end 
    
end 

fprintf('\n time to transfer eye files was: \n')
toc


%% ALL DONE! 
Screen('CloseAll');

if display.EyeTracker.OnOff
Eyelink('Shutdown');
end
sca

end 

