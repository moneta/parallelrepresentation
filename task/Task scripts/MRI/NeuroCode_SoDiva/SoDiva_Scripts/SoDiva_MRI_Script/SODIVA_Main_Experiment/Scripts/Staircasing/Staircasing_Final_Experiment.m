function [display, SodivaMain, MainDesign, results, ResOutput, Master, Anchor, Lstim, Rstim] = Staircasing_Final_Experiment(display, MainDesign, Master, Order, ThumbsUp, Arrow,  General_Info, block ,ClockSign)


%% Part 100: instructions:
% general intro 
SodivaMain.TimeStimInstruction_Start = datetime('now');

%% For testing make STR last block shorter
if display.MakeLastSTRBlockShorter
    if block == 4
        MainDesign(115:end,:) = [];
    end 
    
end 

%% Initialize results and stim

TrialNum = size(MainDesign,1); % number of trials

% preallocate LStim and RStim to save as output
Lstim{TrialNum} = {};
Rstim{TrialNum} = {};


% preallocatte results: 

results.Block                       = zeros(1,TrialNum);
results.Part                        = zeros(1,TrialNum);
results.Context{TrialNum}           = [];                % motion or color
results.Trial                       = (1:TrialNum); % trial number
results.response                    = zeros(1,TrialNum); % were they right?
results.firstRT                     = zeros(1,TrialNum); % just the first reaction time
results.firstKey{TrialNum}          = [];                % just the first key
results.SideCorrect{TrialNum}       = [];                % which side was correct

results.fixControl                  = zeros(1,TrialNum);
results.fixControlRT                = zeros(1,TrialNum);
results.fixControlKey{TrialNum}     = [];   

results.designValueLeft        = MainDesign(:,1)';
results.designValueRight       = MainDesign(:,2)';
results.designValueTarget      = MainDesign(:,3)';
results.designFixControol = MainDesign(:,4)';
results.designContext     = MainDesign(:,5)'; % motion = 1. color = 2
results.design1D          = MainDesign(:,6)'; % yes = 1, no = 0
results.designValueBackLeft = MainDesign(:,7)'; % added
results.designValueBackRight = MainDesign(:,8)'; % added 

% added
results.designValueBackTarget = zeros(1,TrialNum); % Like the Block vector, do same to this. 
results.designValueChoosen = zeros(1,TrialNum);
results.designValueUnchosen = zeros(1,TrialNum);
results.designValueBackChoosen = zeros(1,TrialNum);
results.designValueBackUnchosen = zeros(1,TrialNum);

results.TimeTrial                   = zeros(1,TrialNum); % Time of cue appearing;
results.TimeCueStart                = zeros(1,TrialNum); % Time of cue appearing;
results.TimeStimStarts              = zeros(1,TrialNum); % time stim appeared;
results.TimeFramesPerStim           = zeros(1,TrialNum); % how many frames the stim went through in the time limit;
results.TimeFixation1               = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeOutcome                 = zeros(1,TrialNum); % time the outcome appeared
results.TimeFixation2               = zeros(1,TrialNum); % when did the fixation apeared;
results.TimeQuestion                = zeros(1,TrialNum);
results.TimeFixation3               = zeros(1,TrialNum);
results.TimeEndQuestion             = zeros(1,TrialNum);


results.TargetStimMotionDirection     = zeros(1,TrialNum);
results.TargetStimMotionCoherence     = zeros(1,TrialNum);
results.TargetStimColorType{TrialNum} = [];
results.TargetStimColorSpeed          = zeros(1,TrialNum);
results.TargetStimColorCoherence      = zeros(1,TrialNum);

results.LeftStimMotionDirection     = zeros(1,TrialNum);
results.LeftStimMotionCoherence     = zeros(1,TrialNum);
results.LeftStimColorType{TrialNum} = [];
results.LeftStimColorSpeed          = zeros(1,TrialNum);
results.LeftStimColorCoherence      = zeros(1,TrialNum);


results.RightStimMotionDirection    = zeros(1,TrialNum);
results.RightStimMotionCoherence    = zeros(1,TrialNum);
results.RightStimColorType{TrialNum}= {};
results.RightStimColorSpeed         = zeros(1,TrialNum);
results.RightStimColorCoherence      = zeros(1,TrialNum);


SodivaMain.NumbOfAdjustments           = zeros(4,2);       % number of adjustment for each direction
SodivaMain.StagesOfAdjustments         = zeros(4,12);% the actual values of each adjustment done

 Lstim{TrialNum} = [];
 Rstim{TrialNum} = [];

 % starting with 0.
 Anchor = 0;
 
%% create first stimuli
Le  = MainDesign(1,1); % left
Ri  = MainDesign(1,2); % right
Tgt = MainDesign(1,3); % correctness
Le_bkg = MainDesign(1,7);
Ri_bkg = MainDesign(1,8);


% will it be 1D trial?
OneD = (MainDesign(1,6)==1);

    switch MainDesign(1,5)
            case 1
                context = 'Motion';
            case 2
                context = 'Color';
    end

    switch context
            case 'Motion' % motion
                if OneD
              Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D', Le);
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D', Ri);
                else 

              %design.Bkgcount(Tgt,1) = design.Bkgcount(Tgt,1)+1;                
              %Bckg = design.background(design.Bkgcount(Tgt,1),Tgt);

              Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion', Le, Le_bkg);
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion', Ri, Ri_bkg);
                end 

            case 'Color' % Color
                if OneD
              Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
                else

              %design.Bkgcount(Tgt,2) = design.Bkgcount(Tgt,2)+1;
              %Bckg = design.background(design.Bkgcount(Tgt,2),Tgt);

              Lstim{1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color', Le, Le_bkg);
              Rstim{1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color', Ri, Ri_bkg);
                end
     end 


%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% Go through the trials   %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MegaTime = GetSecs; % display.Timing of begining of the whole block


for t = 1:TrialNum 

fprintf('Stating Trial %.0f  of block %.0f \n', t, block);

    %% set trial settings (block, stim, Fixi, Context)
    results.Block(t) = block;

    %% Split the blocks (Instructions, Adjustment, Anchoring, ..........

    % Show instructions when needed:
    
    switch block 
        case 1
            if t == 1
            Part = 1;
            
            SodivaMain.TimePerceptionM_Inst_Start = datetime('now');
            DrawMeInstructions(display,400) % Also Flips            
            while ~KbCheck; end 

            Adjust = false;
            end 
        case 2
            switch t
                case 1
                    Part = 2;
                    fprintf('\n finished first Perception Measure \n');
                    SodivaMain.TimeMotionBlock_Start = datetime('now');

                    DrawMeInstructions(display,1) % Motion only instructions, Also Flips
                    while ~KbCheck; end 

                    Adjust = false;
            
                case Order.Trials.PostAnchor
                    Part = 3;
                    fprintf('\n finished Anchor \n');
                    SodivaMain.TimeMotionAdj_Start = datetime('now');
                    DrawMeInstructions(display,3) %Motion ADJ NO break offering, Also Flips
            
                        % collect anchor and give mini instructions
                        Correct = (results.response==1);
                        RT_until_now  = results.firstRT;
                        RT_until_now(1,25:end) = nan;
                        Anchor = nanmean(RT_until_now(Correct));

                        % also starting to adjust
                        Adjust = true;
                        Count = zeros(4,2); % row for each direction: 1st correct, 2nd wrond (just to keep track on how many wrong)
                        RT_ColMot = zeros(4,3); % row for each direction, three RT options
                        AdjutNum = zeros(4,1); % Counting how many adjjustments for each one 
                        AdjustStages = zeros(4,6); % 4 directions, each one max every 3 times adjusted
                
                    while ~KbCheck; end 
             
                case Order.Trials.PostMotADJ
                    Part = 4;
                    fprintf('\n finished Motion Adjustment \n');
                    SodivaMain.TimeColorAdj_Start = datetime('now');

                    DrawMeInstructions(display,2) % Color ADJ. NO break offering, Also Flips

                        SodivaMain.NumbOfAdjustments(:,1)  = AdjutNum; % Counting how many adjjustments for each one 
                        SodivaMain.StagesOfAdjustments(:,1:6) = AdjustStages; % 4 directions, each one max every 6 times adjusted

                        % also still keeping to adjust
                        Adjust = true;
                        Count = zeros(4,2); % row for each direction: 1st correct, 2nd wrond (just to keep track on how many wrong)
                        RT_ColMot = zeros(4,3); % row for each direction, three RT options
                        AdjutNum = zeros(4,1); % Counting how many adjjustments for each one 
                        AdjustStages = zeros(4,6); % 4 directions, each one max every 3 times adjusted
                        
                    while ~KbCheck; end 
             
                    
            end             
        case 3
            % no more adjusting
            Adjust = false;
            if t == 1
                Part = 5;
                fprintf('\n finished Color Adjustment \n');
                SodivaMain.TimePerceptionM_Second_Inst_Start = datetime('now');
                DrawMeInstructions(display,400) % Also Flips
                while ~KbCheck; end 
            
            elseif t == Order.Trials.BetweenBlocksPreValmap
                 fprintf('\n finished first block of PostVal Perception Measure \n');
                 DrawMeInstructions(display,200) % within block break done, NO break offering, Also Flips            
                 while ~KbCheck; end
            
            end
            
            
            if (t == round(TrialNum*(1/4)) ||  t == round(TrialNum*(3/4))) 
            % Block 3 is made of 2 blocks: so we are in the middle of the
            % first or the second which are the 1/4 and 3/4 point.
                DrawFormattedText(display.windowPtr, 'Kurze Augenpause',                                                'center', display.text.lines(5),display.text.color);
                DrawFormattedText(display.windowPtr, 'Bitte nicht bewegen und die Augen nicht schliessen (Blinzeln ist OK).',                      'center', display.text.lines(6),display.text.color);
                DrawFormattedText(display.windowPtr, 'Du hast einige Sekunden Zeit zu entspannen.',    'center', display.text.lines(9),display.text.color);
                DrawFormattedText(display.windowPtr, 'Bevor der Block weitergeht, kommt ein Countdown.',            'center', display.text.lines(10),display.text.color);

                SodivaMain.TimeMidBlockBreakStarts = Screen('Flip',display.windowPtr);

        while (GetSecs - SodivaMain.TimeMidBlockBreakStarts) < 8 % 10 seconds
        end 
        % last 9 seconds: 4 + 5
        DrawFormattedText(display.windowPtr,'Bereit? Der Block geht gleich nach dem Countdown weiter!',                'center',display.text.lines(7),display.text.color);
        DrawFormattedText(display.windowPtr, 'Vergiss nicht, immer den Fixierungskreis anzuvisieren.',             'center', display.text.lines(8),display.text.color);   
        Screen('DrawingFinished', display.windowPtr); % tell PTB that stimulus drawing for this frame is finished
        SodivaMain.TimeMidBlockBreakBereit = Screen('Flip',display.windowPtr); % flip to the screen
        WaitSecs(4); % another 4 seconds
        for coudow = 1:5 % last 5 seconds
            CDow = GetSecs;
            DrawFormattedText(display.windowPtr,num2str(6-coudow),'center','center',display.text.color);
            Screen('Flip',display.windowPtr); % flip to the screen
            while (GetSecs - CDow) < 1; end 
        end 
        
        
        SodivaMain.TimeMidBlockBreakEnds =  GetSecs - MegaTime;
      
            end % end if middle of block
    end % end switch block 
    
    results.Part(t) = Part;

    
    TrialStartTime = GetSecs;
    results.TimeTrial(t) = TrialStartTime - MegaTime;
    Le  = MainDesign(t,1); % left
    Ri  = MainDesign(t,2); % right
    Tgt = MainDesign(t,3); % correctness

        if Tgt==Le
            results.SideCorrect{t} = 'Left'; 
        elseif Tgt==Ri
            results.SideCorrect{t} = 'Right'; 
        end 
        
%     if MainDesign(t,4) == 0 % normal trial
%         WasThereFix = false;
%         ConFix = false;
%     elseif MainDesign(t,4) == 1 % FixControl trial
%         WasThereFix = true;
%         ConFix = true;
%         fw = randi([display.FixControlLength, display.nFramesTrial-display.FixControlLength],1); % setting starting frame to have the extra circle
%     end
    
    switch MainDesign(t,5)
        case 1
            context = 'Motion';
        case 2
            context = 'Color';
    end
    
    
    results.Context{t} = context;
    RT = [];
    nKeys = 0;
    f = 1;  
         
    %% Draw cue
    drawMyFixOrCue(display,Tgt,Master,context, Arrow) % not flipping 
    CueStartTime = Screen('Flip',display.windowPtr); % get starting time
    results.TimeCueStart(t) = CueStartTime - MegaTime;
    
    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeCueStart trial %d in Staircasing', t);
    end
    
    %% Save the results and Master inbetween for backup in last block middle 
    
    if block == 3 && t == Order.Trials.BetweenBlocksPreValmap
            TempResultsFolder = fullfile(General_Info.SubOutPut,['StairCasing_Results_Middle_Block_4_',num2str(t),'_',General_Info.subjectInfo.id, date, '.mat']);
            save(TempResultsFolder, 'results');
    end 
    
    %% START THE RESPONSE QUEUE - Only meant for PC for now
    KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
    KbQueueStart(display.deviceID); % starts queue
        
    while GetSecs - CueStartTime < display.Timing.CueTime % wait for Cue time
    end 
        
    %% Draw the Stim
    StimStartTime = GetSecs;
    results.TimeStimStarts(t) = StimStartTime - MegaTime;
    
    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeStimStarts trial %d in Staircasing', t);
    end
    
    % Always have a noraml fixation
    % in some cases we will have some trials with also black dot on it
    
    while GetSecs-StimStartTime < display.Timing.StimTime && f<=display.nFramesTrial% +display.Timing.RespFixTime; % 1.2 stimulus plus 0.8 fix
            DrawMeSomeDots(display,f,Rstim{t},Lstim{t});                         % dont flip 
            
%             if ConFix
%                 if f >(fw-display.FixControlLength) && f < (fw + display.FixControlLength)
%                     Screen('FillOval', display.windowPtr, display.bkColor,[-display.fixation.sz/2+display.center(1),-display.fixation.sz/2+display.center(2),display.fixation.sz/2+display.center(1),display.fixation.sz/2+display.center(2)]);
%                 end
%             end 
            Screen('Flip',display.windowPtr);
            f = f +1;
    end
    
    
    results.TimeFramesPerStim(t) = f-1;


    %% draw fixation
    drawMyFixOrCue(display,Tgt,Master,'Fixation');
    FixStartTime = Screen('Flip',display.windowPtr);
    results.TimeFixation1(t) = FixStartTime - MegaTime;
        
    %% save stimuli details
    results.LeftStimMotionDirection(t)     = Lstim{t}.motionDir;
    results.LeftStimMotionCoherence(t)     = Lstim{t}.motionCohe;
    results.LeftStimColorType{t}           = Lstim{t}.colorType;
    results.LeftStimColorSpeed(t)          = Lstim{t}.colorSpeed;
    results.LeftStimColorCoherence(t)      = Lstim{t}.colorCoherence;
    
    results.RightStimMotionDirection(t)    = Rstim{t}.motionDir;
    results.RightStimMotionCoherence(t)    = Rstim{t}.motionCohe;
    results.RightStimColorType{t}          = Rstim{t}.colorType;
    results.RightStimColorSpeed(t)         = Rstim{t}.colorSpeed;
    results.RightStimColorCoherence(t)     = Rstim{t}.colorCoherence;
    
    % save target, repetative but easy after maybe?
        if Tgt==Le
             results.TargetStimMotionDirection(t)     = Lstim{t}.motionDir;
             results.TargetStimMotionCoherence(t)     = Lstim{t}.motionCohe;
             results.TargetStimColorType{t}           = Lstim{t}.colorType;
             results.TargetStimColorSpeed(t)          = Lstim{t}.colorSpeed;
             results.TargetStimColorCoherence(t)      = Lstim{t}.colorCoherence;

        elseif Tgt==Ri
            results.TargetStimMotionDirection(t)     = Rstim{t}.motionDir;
            results.TargetStimMotionCoherence(t)     = Rstim{t}.motionCohe;
            results.TargetStimColorType{t}           = Rstim{t}.colorType;
            results.TargetStimColorSpeed(t)          = Rstim{t}.colorSpeed;
            results.TargetStimColorCoherence(t)      = Rstim{t}.colorCoherence;

        end 
   
    
    

    %% Wait until response time is over
    % only after we can start adjusting and drawing
    % This will introduce a short delay. need to check how much
    
    % SHAVING 50 ms of response, only in the perceptual part. No need for
    % it anyway and need time to compute results. 
    
    FirstPress_NotYet = true;
    % while no key was pressed and time didnt pass: check for response.
    % if there is, record it, and flush the queue and leave loop.
    % if there wasnt, then after time passed and not yet response, it will save 'no response'. 
%     while (GetSecs - FixStartTime < display.Timing.RespFixTime-0.100) && FirstPress_NotYet % wait for response time minus 100, maximum delay before was 106ms. 
    %% Check and Get response Takes 0.04 seconds 
    [KeyIsDown, firstKeyPressTimes] = KbQueueCheck(display.deviceID);
        if  KeyIsDown %a key was pressed : record the key and time pressed
            firstKeyPressTimes(firstKeyPressTimes==0) = NaN; % little trick to get rid of 0s
            tResponse = min(firstKeyPressTimes); % gets the RT of the first key-press and its id
            firstKeyPressCode = find(firstKeyPressTimes==tResponse);
            RT = tResponse-StimStartTime;
            key_s = KbName(firstKeyPressCode);

             if iscell(key_s) % controlling that there wernt 2 keys pressed at the same exact moment. vary rare but gets stuck in analysis otherwise
             key = key_s{1};
           %  key = strcat(key,'_TwoKeys');
             else 
             key = key_s;
             end 

            switch Tgt
                case Le % Left
                    if KbName(key)==display.keyTargets(1) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(2) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end
                case Ri % Right
                    if KbName(key)==display.keyTargets(2) % correct
                        results.response(t) = 1;
                    elseif KbName(key)==display.keyTargets(1) % wrong
                        results.response(t) = 2;
                    else  % there is a key, just not the right one - 4 not a valid answer 
                        results.response(t) = 4;
                    end 
            end 

            % here we are at first time of KeyIsDown, so:
            FirstPress_NotYet = false; % there was a key press, no need to check again here.
            % Clear the response queue for FIXI
            KbQueueStop(display.deviceID); % stops queue
            KbQueueFlush(display.deviceID,1); % clear the response queue 
            % KbQueueRelease(display.deviceID); % Not sure, might save space
            % Start response queue to see answer if there was a fix in this trial
%             
%             KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
%             KbQueueStart(display.deviceID); % starts queue
        end    
%     end 
     % now out of the loop, meaning 100ms before outcome should appear  
     if FirstPress_NotYet % meaning not yet a response
        results.response(t) = 3; % no response
        RT = NaN;
        key = 'No Response';
        KbQueueStop(display.deviceID); % stops queue
        KbQueueFlush(display.deviceID,1); % clear the response queue 
            
     end 
     
     results.firstRT(t)  = RT;
     results.firstKey{t} = key;

     % Check for timing under 1 second, if not so set to 5 - clock feedback
     if results.response(t) == 1 % if correct 
         if RT > display.STRtimeLimit % if longer than 1 second 
             results.response(t) = 5;
         end 
     end 
     
     results.Trial(t)    = t;
%%     wait for rest of the time 

    while GetSecs - FixStartTime < display.Timing.RespFixTime % wait for response time
    end 

     %% draw feedback based on correctness 400ms.  
    drawMyFeedback(display, results.response(t),ThumbsUp,ClockSign); % not flipping, could be used to save and adjust
    OutcomeTime =  Screen('Flip',display.windowPtr) ;
    results.TimeOutcome(t) = OutcomeTime - MegaTime;    
    if display.EyeTracker.OnOff
        Eyelink('Message', 'TimeOutcome trial %d in Staircasing', t);
    end

    % Adjusting part
%%% Only in case of adjustment %%%
    if Adjust
            % save RT for adjustment: 
            if results.response(t) == 1 || results.response(t) == 5 % Correct or Correct but too slow
                Count(Tgt,1) = Count(Tgt,1) + 1;
                RT_ColMot(Tgt,Count(Tgt,1)) = results.firstRT(t);
            elseif results.response(t) ==2 % Wrong
                Count(Tgt,2) = Count(Tgt,2) + 1; % keeping track on how many wrong ones
            end 

            if  Count(Tgt,1) ==3 % there were 3 correct ones
                AdjutNum(Tgt,1)=AdjutNum(Tgt,1) + 1; % Counting how many adjjustments for each one 
                
                switch context
                    case 'Motion'
                AdjustStages(Tgt,AdjutNum(Tgt,1)) = Master.Motions{Tgt}.coherence; % 4 directions, each one max every 3 times adjusted
                    case 'Color'
                AdjustStages(Tgt,AdjutNum(Tgt,1)) = Master.Colors{Tgt}.speed; 
                end 
                
                [Master , Anchor] = AdjustingMaster(Master, RT_ColMot, Tgt, Anchor, context, block, AdjutNum(Tgt,1), display);
                Count(Tgt,1)=0;
            end  
            
    end

 
  
    
    % wait for feedback time
    while GetSecs-OutcomeTime < display.Timing.FeedbkTime;    end


    %% draw last fixation
    drawMyFixOrCue(display,Tgt,Master,'Fixation');
    LastFixStartTime = Screen('Flip',display.windowPtr);
    results.TimeFixation2(t) = LastFixStartTime - MegaTime;
    
  
    
    %% Making the big adjustment after the first 72  trials 
     
     if (block==1) && (t==TrialNum)
          % make BIG Adjustment 
          fprintf('\n Making Big Adjustment \n');
            RT_ColMot      =   MainDesign (25:72,1:6); % only the last 48 trials so far 
            RT_ColMot(:,7) =   results.response(25:72)';
            RT_ColMot(:,8) =   results.firstRT(25:72)';
            
            [Master, Anchor] = AdjustingMaster(Master, RT_ColMot, Tgt, 1, context, 1, 1, display); %Making sure one before last (the block) is 1 ! ! 
            
     end 
    %% create Stimuli for next trial - should take 0.14 seconds
   
    
     if t<TrialNum % not on the last trial      
         
        Le     = MainDesign(t+1,1); % left
        Ri     = MainDesign(t+1,2); % right
        Tgt    = MainDesign(t+1,3); % correctness
        Le_bkg = MainDesign(t+1,7);
        Ri_bkg = MainDesign(t+1,8);

         
     OneD = (MainDesign(t+1,6)==1);% will next trial be 1d?
     % make context of next trial
     switch MainDesign(t+1,5)
        case 1
            contextNext = 'Motion';
        case 2
            contextNext = 'Color';
     end
    
   
            % here we dont need the background to be the same, so we take
            % like before the random background for each 
            switch contextNext
                case 'Motion' % motion
                    if OneD
                  Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion1D',Le);
                  Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion1D',Ri);
                    else 
                        
                  Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Motion', Le, Le_bkg);
                  Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Motion', Ri, Ri_bkg);
                    end 
                    
                case 'Color' % Color
                    if OneD
                  Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color1D', Le);
                  Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color1D', Ri);
                    else
                                        
                  Lstim{t+1} =  Full_StimCreate_Staircasing(Master, 'L', display, 'Color', Le, Le_bkg);
                  Rstim{t+1} =  Full_StimCreate_Staircasing(Master, 'R', display, 'Color', Ri, Ri_bkg);
                    end
            end 
            
            
         
            
     end 

     %% added to result
     if results.response(t)==1 %correct
         if Tgt == Le
             Reslt = Le;
         else
             Reslt = Ri;
         end 
     elseif results.response(t)==2 %wrong choice
         if Tgt == Le
             Reslt = Ri;
         else
             Reslt = Le;
         end 
     else 
         Reslt = 5;
     end 
     
     
    if Tgt == Le
             results.designValueBackTarget(t) = results.designValueBackLeft(t);
    else % right
             results.designValueBackTarget(t) = results.designValueBackRight(t);
    end
    
     results.designValueChoosen(t) = Reslt;
     if Reslt == Le
          results.designValueBackChoosen(t) = results.designValueBackLeft(t);
          results.designValueUnchosen(t) = Ri;
          results.designValueBackUnchosen(t) = results.designValueBackRight(t);
     elseif Reslt == Ri
         results.designValueBackChoosen(t)  = results.designValueBackRight(t);
         results.designValueUnchosen(t) = Le;
         results.designValueBackUnchosen(t) = results.designValueBackLeft(t);

     else % Reslt==5, meaning no response
         results.designValueBackChoosen(t)  = 5;
         results.designValueUnchosen(t) = 5;
         results.designValueBackUnchosen(t) = 5;
     end

     
     
     
    % wait for last fixation time minus 50 ms
%     while GetSecs-LastFixStartTime < display.Timing.LastFixTime - 0.050
%     end
%     
%     
%     [KeyIsDown, firstKeyPressTimes] = KbQueueCheck(display.deviceID);
%     if  KeyIsDown %a key was pressed : record the key and time pressed
%         firstKeyPressTimes(firstKeyPressTimes==0) = NaN; % little trick to get rid of 0s
%         tResponse = min(firstKeyPressTimes); % gets the RT of the first key-press and its id
%         firstKeyPressCode = find(firstKeyPressTimes==tResponse);
%         RT = tResponse-StimStartTime;
%         key_s = KbName(firstKeyPressCode);
%         
%      if iscell(key_s) % controlling that there wernt 2 keys pressed at the same exact moment. vary rare but gets stuck in analysis otherwise
%      key = key_s{1};
%    %  key = strcat(key,'_TwoKeys');
%      else 
%      key = key_s;
%      end 
%         
%           if WasThereFix % correct answer is display.keyTargets(4); 
%                            if KbName(key)==display.keyTargets(3) % correct
%                               results.fixControl(t) = 1;
%                            else % wrong key - since its under key is down, and just the wrong one, but there was a fix
%                               results.fixControl(t) = 3;
%                            
%                            end
%           elseif ~WasThereFix
%                            if KbName(key)==display.keyTargets(3) % false alarm (pressed space but no fix)
%                               results.fixControl(t) = 2;
%                            else  % random key press - no fix, pressed a key, but not space
%                               results.fixControl(t) = 4;
%                            end
%           end
% 
%      else % no key pressed
%          
%           if WasThereFix 
%                               results.fixControl(t) = 5; % Missed a fix
%           elseif ~WasThereFix
%                               results.fixControl(t) = 6; % did Well
%           end
%                         RT = NaN;
%                         key = 'No Response';
%     end

    % We want to see at the end TrialNum-8 times of 6's and 8 times 1's for
    % correct answeres. 
    
%                      results.fixControlRT(t) = RT;
%                      results.fixControlKey{t} = key;
%                     
%                     KbQueueFlush(display.deviceID,1); % clear the response queue 
    
    % wait rest of time
    while GetSecs-LastFixStartTime < display.Timing.LastFixTime; end
    

   if block == 2 && t == TrialNum
      SodivaMain.NumbOfAdjustments(:,2)  = AdjutNum; % Counting how many adjjustments for each one 
      SodivaMain.StagesOfAdjustments(:,7:12) = AdjustStages; % 4 directions, each one max every 6 times adjusted  
   end 
    
end % finished trial loop

fprintf('\n finished all trials in Block %.0f of the Staircasing Procedure \n', block);
%% Make ResOutput

    ResOutput.Block = results.Block'; 
    ResOutput.Part  = results.Part';
    ResOutput.Trial = results.Trial';
    ResOutput.Context = results.Context';
    
    ResOutput.TargetStimMotionDirection = results.TargetStimMotionDirection';
    ResOutput.TargetStimMotionCoherence = results.TargetStimMotionCoherence';
    ResOutput.TargetStimColorType = results.TargetStimColorType';
    ResOutput.TargetStimColorSpeed = results.TargetStimColorSpeed';
    ResOutput.TargetStimColorCoherence = results.TargetStimColorCoherence';
    
    ResOutput.response = results.response';
    ResOutput.firstRT = results.firstRT';
    ResOutput.firstKey = results.firstKey';
    
    ResOutput.Anchor = (ones(length(results.Trial),1)).*Anchor;
%     ResOutput.Anchor(ResOutput.Block==1,1) = Anchor(1);
%     ResOutput.Anchor(ResOutput.Block==2,1) = Anchor(2);
%     
    ResOutput.SideCorrect = results.SideCorrect';
   
    ResOutput.designValueLeft = results.designValueLeft';
    ResOutput.designValueRight = results.designValueRight';
    ResOutput.designValueTarget = results.designValueTarget';  
    ResOutput.designFixControl = results.designFixControol';  
        
    ResOutput.fixControl                         = results.fixControl';
    ResOutput.fixControlRT                       = results.fixControlRT';
    ResOutput.fixControlKey                      = results.fixControlKey';
   
    ResOutput.LeftStimMotionDirection = results.LeftStimMotionDirection';
    ResOutput.LeftStimMotionCoherence = results.LeftStimMotionCoherence';
    ResOutput.LeftStimColorType = results.LeftStimColorType';
    ResOutput.LeftStimColorSpeed = results.LeftStimColorSpeed';
    ResOutput.LeftStimColorCoherence = results.LeftStimColorCoherence';

    
    ResOutput.RightStimMotionDirection = results.RightStimMotionDirection';
    ResOutput.RightStimMotionCoherence = results.RightStimMotionCoherence';
    ResOutput.RightStimColorType = results.RightStimColorType';
    ResOutput.RightStimColorSpeed = results.RightStimColorSpeed';
    ResOutput.RightStimColorCoherence = results.RightStimColorCoherence';

    ResOutput.TimeTrial  = results.TimeTrial';
    ResOutput.TimeCueStart = results.TimeCueStart';
    ResOutput.TimeStimStarts = results.TimeStimStarts';
    ResOutput.TimeFramesPerStim = results.TimeFramesPerStim';
    ResOutput.TimeFixation1 = results.TimeFixation1';
    ResOutput.TimeOutcome = results.TimeOutcome';
    ResOutput.TimeFixation2 = results.TimeFixation2';
    ResOutput.TimeQuestion = results.TimeQuestion';
    ResOutput.TimeFixation3 = results.TimeFixation3';
    ResOutput.TimeEndQuestion = results.TimeEndQuestion';
    

    % added 
    ResOutput.designValueBackLeft = results.designValueBackLeft';
    ResOutput.designValueBackRight = results.designValueBackRight';
    
    ResOutput.designValueBackTarget = results.designValueBackTarget'; % Like the Block vector, do same to this. 
    ResOutput.designValueChoosen = results.designValueChoosen';
    ResOutput.designValueUnchosen = results.designValueUnchosen';
    ResOutput.designValueBackChoosen = results.designValueBackChoosen' ;
    ResOutput.designValueBackUnchosen = results.designValueBackUnchosen' ;




%% Extra Question part, every 12 trials asking if there was the special mark
%                 if mod(t,12)==0
%                     % START THE RESPONSE QUEUE - Only meant for PC for now
%                     KbQueueCreate(display.deviceID,display.keyList); % creates queue, restricted to the relevant key targets
%                     KbQueueStart(display.deviceID); % starts queue
% 
%                     %sz = display.fixation.sz*4;
%                     %Screen('TextSize', display.windowPtr, 50); % select specific text size      
%                     DrawFormattedText(display.windowPtr, 'Schwarzer Punkt?', 'center', display.text.lines(3),display.text.color);
%                     Screen('TextSize', display.windowPtr, display.text.size); % select specific text size      
%                     Screen('FillOval', display.windowPtr, display.fixation.color,[-display.fixation.sz*4+display.center(1),-display.fixation.sz*4+display.center(2),display.fixation.sz*4+display.center(1),display.fixation.sz*4+display.center(2)]);
%                     Screen('FillOval', display.windowPtr, display.bkColor,[-display.fixation.sz*4/2+display.center(1),-display.fixation.sz*4/2+display.center(2),display.fixation.sz*4/2+display.center(1),display.fixation.sz*4/2+display.center(2)]);
%                     DrawFormattedText(display.windowPtr, 'Ja (hoch)' , 'center', display.text.lines(15),display.text.color);
%                     DrawFormattedText(display.windowPtr, 'Nein (runter)', 'center', display.text.lines(18),display.text.color);
%                     TimeQuestion  = Screen('Flip',display.windowPtr);
%                     results.TimeQuestion(t) = TimeQuestion - MegaTime;
%                     WaitSecs(0.2); % making sure not to skip question with the last answer
%                     % wait for answer, we have to trust them to be fast 
% 
%                     while ~KbCheck;    end 
%                     results.TimeEndQuestion(t) = GetSecs - MegaTime;
%                     while GetSecs - TimeQuestion < display.Timing.FixQue ; end 
% 
%                     % (extra) last fixation 
%                     drawMyFixOrCue(display,1,Master,'Fixation')
%                     LastFix  = Screen('Flip',display.windowPtr);
%                     results.TimeFixation3(t) = LastFix - MegaTime;
% 
%                     % Get response
%                     % if they are correct, all the last 12 trials will be set
%                     % results.fixControl = 1, if not so 0. 
%                     % Also all the last 12 trials will get the RT (easier to build the
%                     % result struct later)
%                     [KeyIsDown, firstKeyPressTimes] = KbQueueCheck(display.deviceID);
%                     if  KeyIsDown %a key was pressed : record the key and time pressed
%                         firstKeyPressTimes(firstKeyPressTimes==0) = NaN; % little trick to get rid of 0s
%                         tResponse = min(firstKeyPressTimes); % gets the RT of the first key-press and its id
%                         firstKeyPressCode = find(firstKeyPressTimes==tResponse);
%                         RT = tResponse-TimeQuestion;
%                         key_s = KbName(firstKeyPressCode);
% 
%                      if iscell(key_s) % controlling that there wernt 2 keys pressed at the same exact moment. vary rare but gets stuck in analysis otherwise
%                      key = key_s{1};
%                 %      key = strcat(key ,'_TwoKeys');
%                      else 
%                      key = key_s;
%                      end 
% 
% 
%                         if WasThereFix % correct answer is display.keyTargets(4); 
%                            if KbName(key)==display.keyTargets(4) % correct
%                               results.fixControl(t-11:t) = 1;
%                            elseif KbName(key)==display.keyTargets(3) % wrong
%                               results.fixControl(t-11:t) = 0;
%                            else  % there is a key, just not the right one - 4 not a valid answer 
%                               results.fixControl(t-11:t) = NaN;
%                            end
%                         elseif ~WasThereFix
%                            if KbName(key)==display.keyTargets(3) % correct
%                               results.fixControl(t-11:t) = 1;
%                            elseif KbName(key)==display.keyTargets(4) % wrong
%                               results.fixControl(t-11:t) = 0;
%                            else  % there is a key, just not the right one - 4 not a valid answer 
%                               results.fixControl(t-11:t) = NaN;
%                            end
% 
% 
%                         end
% 
%                     else 
%                         results.fixControl(t-11:t) = NaN; % no response
%                         RT = NaN;
%                         key = 'No Response';
%                     end
% 
%                 %        
%                 %     if iscell(key_s) % saving it was actually 2 keys
%                 %     key = strcat(key,'_TwoKeys');
%                 %     end
% 
%                     results.fixControlRT(t-11:t) = RT;
%                     [results.fixControlKey{t-11:t}] = deal(key);
% 
%                     KbQueueFlush(display.deviceID,1); % clear the response queue 
% 
%                     % wait the last extra fixation
%                     while GetSecs - LastFix > display.Timing.FixQueFix; end   
%                     WasThereFix = false;
% 
%                 end












end 