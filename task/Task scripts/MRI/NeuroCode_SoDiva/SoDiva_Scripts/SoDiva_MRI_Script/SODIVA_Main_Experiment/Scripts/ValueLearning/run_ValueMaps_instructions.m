function [display, Master] = run_ValueMaps_instructions(display,Master,time)

    display.isInstr = true;
    
    OldDuration = display.Max_Trial_duration;
    OldnFrames = display.nFramesTrial;
    OldTrialLength = display.TrialLength;
    
    if time == 1
    display.Max_Trial_duration = 5; % this can be a bit longer, initial learning
    elseif time == 2
    display.Max_Trial_duration = 3; % this can be a bit longer, initial learning
    elseif time == 3
    display.Max_Trial_duration = OldDuration; % keep it the same 
    end 
    display.nFramesTrial = round(display.Max_Trial_duration*display.frameRate); %Calculate total number of temporal frames
    display.TrialLength = display.nFramesTrial/display.frameRate;

%% Making Instruction stimuli - presented at center and longer
% Stim for the center instructions
    for inst = 1:4       
                CenterStimMotion{inst} =  Full_StimCreate_Staircasing(Master, 'Center', display, 'Motion1D', inst ,randi(4)); %Full_StimCreate2(Master.Colors{randi(4)}, Master.Motions{inst}, 'Center', display);
                CenterStimColor{inst} =  Full_StimCreate_Staircasing(Master, 'Center', display, 'Color1D', inst ,randi(4)); %Full_StimCreate2(Master.Colors{inst}, Master.Motions{randi(4)}, 'Center', display);
    end 

% Presenting the initial stimuli  

 for d = 1:2
        for inst = 1:4

            Value = 10+(inst-1)*20;


               switch d 
                case 1 % 'Motion'
                    DrawFormattedText(display.windowPtr, 'Diese Bewegungsrichtung repraesentiert eine Belohnung von:', 'center', display.text.lines(2),display.text.color);    
                    DrawFormattedText(display.windowPtr, num2str(Value), 'center', display.text.lines(4),display.text.color);
                    Screen('Flip',display.windowPtr);
                    WaitSecs(1.5);


                          for w = 1:CenterStimMotion{inst}.nFrames 
                                    DrawFormattedText(display.windowPtr, 'Diese Bewegungsrichtung repraesentiert eine Belohnung von:', 'center', display.text.lines(2),display.text.color);    
                                    DrawFormattedText(display.windowPtr, num2str(Value), 'center', display.text.lines(4),display.text.color);
                                    DrawMeSomeDots(display,w,CenterStimMotion{inst});
                                    Screen('Flip',display.windowPtr);
                          end 



                case 2 % 'Color'

                    DrawFormattedText(display.windowPtr, 'Diese Farbe repraesentiert eine Belohnung von:', 'center', display.text.lines(2),display.text.color);    
                    DrawFormattedText(display.windowPtr, num2str(Value), 'center', display.text.lines(4),display.text.color);
                    Screen('Flip',display.windowPtr);
                    WaitSecs(1.5);


                          for w = 1:CenterStimColor{inst}.nFrames 
                                    DrawFormattedText(display.windowPtr, 'Diese Farbe repraesentiert eine Belohnung von:', 'center', display.text.lines(2),display.text.color);    
                                    DrawFormattedText(display.windowPtr, num2str(Value), 'center', display.text.lines(4),display.text.color);
                                    DrawMeSomeDots(display,w,CenterStimColor{inst});
                                    Screen('Flip',display.windowPtr);
                          end 

               end   



        end 
   
 end             

    DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.', 'center', 'center',display.text.color);
    Screen('Flip',display.windowPtr);
    while ~KbCheck;  end

 
    display.isInstr = false;
    
    display.Max_Trial_duration = OldDuration;
    display.nFramesTrial = OldnFrames;
    display.TrialLength = OldTrialLength;

end 