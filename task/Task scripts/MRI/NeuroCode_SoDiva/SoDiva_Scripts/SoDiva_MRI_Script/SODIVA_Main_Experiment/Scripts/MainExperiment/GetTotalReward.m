function GetTotalReward(Study,display,General_Info)%% Get sum of points
%             Rewards to participants: Instructions In each choice you can earn 10 30 50 70 points. 
%             The next part will determine your pay for the entire experiment all together. 
%             In this part, for every (mri = 500, behavioral = 630) points are worth a euro. 
%                 If you are wrong in all of your choices, you will earn the base amount for the experiemtn which is (mri = 30, beh = 25) euros. 
%                 If you are correct in all your choices, you will earn the maximum for the experiment which is (mri 45 beh 35) euros.

%                            (MRI  / BEH)
% 100% correct 22200 kredite (44.4  / 35.23) new 22560
% 80% correct 20424 kredite  (40.85 / 32.41) new 20760
% 50% correct 17760 kredite  (35.5  / 28.19) new 18060
% 0% correct 13320 kredite   (26.64 / 21.15) new 13560

% MRI : every 600 points Euro, rounding up. 
% BEH : every 630 points Euro, rounding up, unless more than 35. 

%% (A) Setting initial variables
% [display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
% Screen('CloseAll');
% 
% % give participant Id as input get its folder 
% [Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);
Sub_Folder = General_Info.SubData;
IDSub = General_Info.subjectInfo.id;
GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Part2_Sub_',IDSub,'.mat']);
clear General_Info;
load(GeneralInfoFolder, 'General_Info');

%% STR 
FolderDetstr = (dir(fullfile(General_Info.SubOutPut,['Staircasing_Data_Sub_',General_Info.subjectInfo.id,'_Block_*.mat'])));

Resp = [];
for s = 1:3
    Filename = fullfile(General_Info.SubOutPut,FolderDetstr(s).name);
    %General_Info.SubOutPut = 'C:\Users\moneta\Seafile_Folder\Seafile\My Library\SODIVA\SODIVA_Main_Experiment\Data\Nir4C\OutPut'
    load(Filename,'BigData')
    Resp = [Resp ; BigData.ResOutput.response];
end 

Add2Euro = (sum(Resp==1)/length(Resp))>=0.8;


%% Load the results for each of the 4 blocks 
% Load design
FolderDet = (dir(fullfile(General_Info.SubOutPut,['VBDM_Data_',General_Info.subjectInfo.id,'_Block_*.mat'])));
% FolderDet = dir(fullfile('C:\Users\moneta\Seafile_Folder\Seafile\My Library\SODIVA\SODIVA_Main_Experiment\Data\Ntest\OutPut',['VBDM_Data_',General_Info.subjectInfo.id,'_Block_*.mat']))

AllKredite = [];
for b = 1:4
    AllKredite(b) = 0;
    Filename = fullfile(General_Info.SubOutPut,FolderDet(b).name);
    load(Filename,'Main_VBDM')
    ResTableAcc = Main_VBDM.ResOutput.response;
    Correct = ResTableAcc==1;
    fprintf('\n in this block participant was %.3f correct \n',sum(Correct)/length(Correct));
    
    for t = 1:size(Main_VBDM.ResOutput.firstKey,1)
        switch Main_VBDM.ResOutput.firstKey{t} % first response key. 
            case {'b'} % right
                Rew = 10 + (Main_VBDM.ResOutput.designValueRight(t)-1)*20;
                AllKredite(b) = AllKredite(b) + Rew; 
            case {'g'} % left
                Rew = 10 + (Main_VBDM.ResOutput.designValueLeft(t)-1)*20;
                AllKredite(b) = AllKredite(b) + Rew; 
        end    
    end
    
    clear Main_VBDM ResTableAcc
end 

% Add the OneDmini Reward of both parts:
FolderDetOneD = (dir(fullfile(General_Info.SubOutPut,['Kredite_OneDMini_',General_Info.subjectInfo.id,'_Phase_*.mat'])));
for p = 1:2
    Filename = fullfile(General_Info.SubOutPut,FolderDetOneD(p).name);
    load(Filename,'Kredite')
    AllKredite(b+p) = Kredite;
    clear Kredite FileName
end 



% Sum up and display on screen/output

% for bb=1:6
% fprintf('\n In %d. Block , %d Kredite \n',bb,Kredite(bb));
% end 
switch Study
    case 'BehPilot'
        FinalReward = ceil(sum(AllKredite)/850);
        if FinalReward > 33
            FinalReward = 33;
        end 
    case 'MainMRI'
         FinalReward = ceil(sum(AllKredite)/600);       
         if FinalReward > 46
             FinalReward = 46;
         end 
         if FinalReward < 36
             FinalReward = 36;
         end 
end 

 fprintf('\n Participant did %d Euro in the OneD and VBDM parts \n',FinalReward);

 % we add 4 euros for STR no matter what: 
FinalReward = FinalReward + 4;

 fprintf('\n \n Der Versuchtperson bekommt %d Euro \n \n vielen Dank!! \n ',FinalReward);

%  if Add2Euro
%      fprintf('\n Participant did well in the STR (meaning more than 0.8 correct and fast) \n');
%  else 
%      fprintf('\n Participant was too slow in the STR (meaning less than 0.8 correct and fast) \n');
%  end 
% 
%  
% if display.RweMRI
%    FinalReward = ceil(sum(AllKredite)/500);
%    if FinalReward > 45
%        FinalReward = 45;
%    end 
% else % behavioral
%    FinalReward = ceil(sum(AllKredite)/650);
%    if FinalReward > 35
%        FinalReward = 35;
%    end 
% end 

end 
