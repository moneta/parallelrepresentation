function ValueMap_MainScript(Type) % ValueMap Experiment
% input 'Kashe' means no display of values in order and FC are mixed. 
% input of 'Kal' means with the above and FC one dim after the other. 

% first part    seeing the values
% second part 40 color forced choice
%             40 motion forced choice
% third part: 3-4 b locks (minimum 3) of mixed everything all 1D trials
% also has FC in it
% minimum 48-50 trials. maximum 72. 

%% (A) Setting initial variables
[display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
Screen('CloseAll');

Block_Correctness = zeros(5,1);

% give participant Id as input get its folder 
[Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);

% load the Master and General info from the participant's folder
MasterFolder = fullfile(Sub_Folder,['Master_Sub_',IDSub,'.mat']); 
load(MasterFolder,'FinalMaster');


GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Sub_',IDSub,'.mat']);
clear General_Info;
load(GeneralInfoFolder, 'General_Info');
General_Info.ValMap_StartTime =GetSecs;

Master = FinalMaster;
clear FinalMaster
% run Value Training

% Open The Window
[display.windowPtr]=Screen('OpenWindow',display.screenNum,display.bkColor);
% setting the text size and font (pretty sure it needs to happen when the wndow is open)     
Screen('TextFont', display.windowPtr, display.text.font); % select specific text font
Screen('TextSize', display.windowPtr, display.text.size); % select specific text size

HideCursor();

%% Value Training
% NO NEED FOR CUE IN THIS PART!! 
% could do: fix 0.5, Stim 1.6, Res 0.4, Outcome 0.5. = 3 seconds per trial.
%% Load Master and Value map
% the matrix ValMap shows which perceptual stimuli will be in which value
% order. 
% If in the ValMap on the third line is the number 1. 
% this means that what used to be Master.color{1} is now on the third cell
% in NewMaster. 
% so in the design matrix, it will say 'Choose number 3', it will take the
% third cell in the NewMaster which is now what used to be the perceptual
% feature in position 1 in the old Master. 
% This way we mixed the perceptual order, but the NewMaster has within it
% the value order (so NewMaster{1} is value 1 for example).

%% INSTRUCTIONS! ! ! 

% explain its only 1D trials
% each trial has a value 
% choose the heighest value
% structure: first learn values, then choose highest for few minutes
% then they start with 'until they reach 90%, no less than 25 trials'

DrawMe_ValMap_Instructions(display,100)
while ~KbCheck; end 


%% present the values of each feature one after the other:
% both value 1, both 2 etc....
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(Type,'Kal')
[display, Master] = run_ValueMaps_instructions(display,Master,1);
end 
%% Run Design 

DrawMe_ValMap_Instructions(display,101)
while ~KbCheck; end 


CorrectCount = zeros(10,2,3); % mod(t,12)+1 % trial count, context, target
FeatCount = zeros(2,3);
% we can be acurate on 2 3 4 on each dimension


% first once the Design only Forced Choices
block = 1;

switch Type
    case 'Kashe' % mixed between color and motion
    Design = MakeValueDesign;
    [display, ValMap.SodivaValMap{1}, ValMap.MainDesign{1}, ValMap.results{1}, ValMap.ResOutput{1}, ValMap.Master{1}, ValMap.Lstim{1}, ValMap.Rstim{1}, Passed, CorrectCount, FeatCount]  = ValueMap_Final_Experiment(display, Master, General_Info, Design.PartFC  ,block, CorrectCount, FeatCount ); % 1 = block 1 
    case 'Kal' % first one then the other - not using this. 
    Design = MakeValueDesignForcedChoices;
    [display, ValMap.SodivaValMap{1}, ValMap.MainDesign{1}, ValMap.results{1}, ValMap.ResOutput{1}, ValMap.Master{1}, ValMap.Lstim{1}, ValMap.Rstim{1}, Passed, CorrectCount, FeatCount]  = ValueMap_Final_Experiment(display, Master, General_Info, Design  ,block, CorrectCount, FeatCount ); % 1 = block 1
end 

ValMap.display{1} = display;

ValMapFolder = fullfile(General_Info.SubOutPut,['ValMapData_Sub_',General_Info.subjectInfo.id,'_UpToBlock', num2str(block), '.mat']);
save(ValMapFolder, 'ValMap');

%% Setting counter to 0, from now on testing all. 
CorrectCount = zeros(10,2,3); % mod(t,12)+1 % trial count, context, target
FeatCount = zeros(2,3);
PassCount = 0; 
% we can be acurate on 2 3 4 on each dimension

DrawMe_ValMap_Instructions(display,102)
while ~KbCheck; end 

%% Changing here for learning: when presenting the two values
% this way when only showing one value its shorter (less to process)
OldFeedbackTime = display.Timing.FeedbkTime;
display.Timing.FeedbkTime = 0.8;

%%
GetOut = false;
while block<=4 && ~GetOut %&& PassCount<1 % it was <=2 but im trying to lower a block, since there is training in OneDmini as well. 
% Out of loop when:
% at least three times passed a mixed block.
% Or last block is when block = 4 enters loop( so becomes block = 5 in loop, and 6 including the first one that has forced choices)
% minimum is total of 3 blocks (first one and two succesfull ones)
% so they all do 1 forced choice block, 3 mixed, and whoever didnt have in
% all blocks above 85%, does another block. 
block = block + 1;    


Design = MakeValueDesign;
[display, ValMap.SodivaValMap{block}, ValMap.MainDesign{block}, ValMap.results{block}, ValMap.ResOutput{block}, ValMap.Master{block}, ValMap.Lstim{block}, ValMap.Rstim{block}, Passed, CorrectCount, FeatCount] = ValueMap_Final_Experiment(display, Master, General_Info, Design.PartMix, block, CorrectCount, FeatCount);
 
ValMap.display{block} = display;                                                                                                                                                                                                                 
% showing feedback
    PerCor = ComputeValMapCorrectness(ValMap.results{block});
    Block_Correctness(block) = PerCor;
    
if Passed
      PassCount = PassCount + 1;
elseif ~Passed && PerCor>=0.85
      PassCount = PassCount + 1;
end


ValMapFolder = fullfile(General_Info.SubOutPut,['ValMapData_Sub_',General_Info.subjectInfo.id,'_UpToBlock', num2str(block), '.mat']);
save(ValMapFolder, 'ValMap');


% show next block is didnt pass but not on last block
if block == 1 % no matter what another block
    DrawMe_ValMap_Instructions(display,200,PerCor)
    while ~KbCheck; end 
elseif PassCount <1 && block < 5 % if not block 1 but before the end of 5 - so as long as didnt pass show next block
    DrawMe_ValMap_Instructions(display,200,PerCor)
    while ~KbCheck; end 
end    

% if PassCount >=1 && block < 5
%     DrawMe_ValMap_Instructions(display,200,PerCor)
%     while ~KbCheck; end 
% end    

% show congrats only if passed and already did 2 blocks
if PassCount>=1 && block >2 % minimum 2 mixed blocks
    DrawMe_ValMap_Instructions(display,201,PerCor)
    while ~KbCheck; end 
    % break while loop of blocks
    GetOut = true; % safty
    break
elseif block == 5 % meaning did 4 blocks already 
    DrawMe_ValMap_Instructions(display,201,PerCor)
    while ~KbCheck; end 
    % break while loop of blocks
    GetOut = true; % safty
    break
end

end % while loop of blocks 





    


%% Save everything
ValMap.Block_Correctness = Block_Correctness;
fprintf('Saving the data for the subject\n');
ValMapFolder = fullfile(General_Info.SubOutPut,['ValMapData_Sub_',General_Info.subjectInfo.id, '_Everything.mat']);
save(ValMapFolder, 'ValMap');

fprintf('\n Participant was this much correct in all five blocks:\n');
Block_Correctness

% already showing passed the training
while ~KbCheck; end % adding wait for participant response so wont go light in 2 ppl 
while ~KbCheck; end % adding wait for participant response so wont go light in 2 ppl 

oldType = ShowCursor();

Screen('CloseAll');
 

end 

% % Run temp testing for values:
% % % open window
% % 
% [display.windowPtr]=Screen('OpenWindow',display.screenNum,display.bkColor);
% % setting the text size and font (pretty sure it needs to happen when the wndow is open)     
% Screen('TextFont', display.windowPtr, display.text.font); % select specific text font
% Screen('TextSize', display.windowPtr, display.text.size); % select specific text size
% 
% HideCursor();
% [display, Master] = Temp_Test_Val_Learning(display,Master)
% 
% 
% 
% oldType = ShowCursor();
% 
% Screen('CloseAll');


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% once the full thing. 3.5 minutes. 
% then run with a running average of accuracy.
% we have 48 trials of all options in mixed
% that is 16 options per target

% if we keep a runing counter of each stim, meaning running on 10 trials,
% counting that 9 out of 10 was correct:
% if reaches 90% accuracy in all Mixed trials, then move on to the open
% question part. 

%% Open question test: 'What is the value of: ?'

% (this could be used inside the scanner as well maybe)

% for now can be done with pen an paper maybe. 

%% check for accuracy and run until accuracy is at a limit

% mixed of 8 FC trials and all the rest of 7*8 trials (ALL 1D ! )
%(meaning 64 trials' block, meaning 3.2 minutes a block. 
% need to add a few 'choose 1' trials. so maybe 70 trials, 3.5 minutes. 

