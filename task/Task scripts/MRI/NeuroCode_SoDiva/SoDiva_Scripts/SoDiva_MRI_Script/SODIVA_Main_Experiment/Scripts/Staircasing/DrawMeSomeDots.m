function DrawMeSomeDots(display,NumOfFrame,RStim,LStim)

Screen('DrawDots',display.windowPtr,[RStim.pixpos_x(NumOfFrame,RStim.goodDots(NumOfFrame,:));RStim.pixpos_y(NumOfFrame,RStim.goodDots(NumOfFrame,:))], display.dotsSize,RStim.RGB{NumOfFrame},[0,0],1);

if exist('LStim','var')
Screen('DrawDots',display.windowPtr,[LStim.pixpos_x(NumOfFrame,LStim.goodDots(NumOfFrame,:));LStim.pixpos_y(NumOfFrame,LStim.goodDots(NumOfFrame,:))], display.dotsSize,LStim.RGB{NumOfFrame},[0,0],1);
    
    
    if display.fixi % only if indicated in set_settings
       center = display.resolution/2;
       sz = angle2pix(display,display.fixation.size/2);
       Screen('FillOval', display.windowPtr, display.fixation.color,[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
    end
    
    
end 

end 