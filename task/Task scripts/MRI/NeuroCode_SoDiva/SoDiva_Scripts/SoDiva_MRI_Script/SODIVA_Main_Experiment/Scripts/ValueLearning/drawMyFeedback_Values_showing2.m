function   drawMyFeedback_Values_showing2(display,Reslt,Side,Big)

Value = 10+(Reslt-1)*20;

Screen('TextSize', display.windowPtr, display.ValOutcome.size); % select specific text font

switch Side
    case 'L' %   Center of the aperture (degrees) set left
        center = [-(display.apertureSize(1)+display.distBetweenStim)/2,0];
    case 'R' %   Center of the aperture (degrees) set right
        center = [(display.apertureSize(1)+display.distBetweenStim)/2,0];
    case 'Center'
        center = [0 0];
end 

XLoc = angle2pix(display,center(1)) + display.resolution(1)/2;
YLoc =  angle2pix(display,center(2)) + display.resolution(2)/2;

    switch Reslt
        case {1,2,3,4}

            [nx, ny, textbounds] = DrawFormattedText(display.windowPtr, num2str(Value) , XLoc , YLoc , display.feedback.color);     
        case 5
             Screen('TextSize', display.windowPtr, display.text.size); % select specific text size
            [nx, ny, textbounds] = DrawFormattedText(display.windowPtr,'keine Antwort', 'center' , 'center' ,display.feedback.color);
        case 0
            [nx, ny, textbounds] = DrawFormattedText(display.windowPtr, '0' , XLoc , YLoc , display.feedback.color); 

    end
     
    
%     textbounds(1) = textbounds(1)
%     textbounds(2) = textbounds(2) + 
%     textbounds(3) = textbounds(3) + 
%     textbounds(4) = textbounds(4) + 
      textbounds = textbounds + [ -4 , 12 , 4 , 5];
%     textbounds
    
    if Big
        
        Screen('FrameRect', display.windowPtr , display.feedback.color , textbounds, 1);   
    end 
    
    Screen('TextSize', display.windowPtr, display.text.size); % select specific text size

  
    
    
    
end 
