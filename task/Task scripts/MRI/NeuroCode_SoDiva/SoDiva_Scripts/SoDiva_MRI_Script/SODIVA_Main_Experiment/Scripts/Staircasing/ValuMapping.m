function NewMaster = ValuMapping(Master, ValMap)
% the natrix ValMap shows which perceptual stimuli will be in which value
% order. 

% If in the ValMap on the third line is the number 1. 
% this means that what used to be Master.color{1} is now on the third cell
% in NewMaster. 

% so in the design matrix, it will say 'Choose number 3', it will take the
% third cell in the NewMaster which is now what used to be the perceptual
% feature in position 1 in the old Master. 

% This way we mixed the perceptual order, but the NewMaster has within it
% the value order (so NewMaster{1} is value 1 for example).


% Reorganize motion dimension according to ValMap(1:4,1)
% Reorganize Color dimension according to ValMap(1:4,2)

for mc = 1:4
    NewMaster.Motions{mc} = Master.Motions{ValMap(mc,1)};
    NewMaster.Colors{mc} = Master.Colors{ValMap(mc,2)};
   
end 

% 1D stays the same 
NewMaster.Motions{5} = Master.Motions{5};
NewMaster.Colors{5} = Master.Colors{5};



end 