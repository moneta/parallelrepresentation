function [Sub_Folder,IDSub]  = Get_Sub_Info_Mini(General_Info)
Allgood = false;
while true && ~Allgood
    %% SELECT THE STUDY MODE:
    General_Info.studyMode = 'Staircasing Piloting'; % save selection of the study mode
    
    %% ENTER PARTICIPANT DETAILS:
    prompt = {'id'}; % define the prompts
    defaultAns = {'99999'}; % define the default answers
    General_Info.subjectInfo = inputdlg(prompt,'Subject Info',1,defaultAns); % create and show dialog box
    if isempty(General_Info.subjectInfo) % if cancel was pressed
        f = msgbox('Process aborted: Please start again!','Error','error'); % show error message
        uiwait(f);
        continue
    else
        General_Info.subjectInfo = cell2struct(General_Info.subjectInfo,prompt,1); % turn into structure array
    end
    
    % CHECK INPUTS (INTERNALLY):
    if numel(General_Info.subjectInfo.id) ~= 5 % if ID has not been correctly specified
        f = msgbox('ID must contain 5 digits!','Error','error');
        uiwait(f);
    else % check if a folder already exist with the data of the participant
         % if not - make a directory 
        General_Info.SubData = fullfile(General_Info.pathData,General_Info.subjectInfo.id);
        if exist(General_Info.SubData,'dir')
            Allgood = true;
            Sub_Folder = General_Info.SubData;
        else 
            Allgood = false;
        end 
    end       
    
end  

IDSub = General_Info.subjectInfo.id;
end 

