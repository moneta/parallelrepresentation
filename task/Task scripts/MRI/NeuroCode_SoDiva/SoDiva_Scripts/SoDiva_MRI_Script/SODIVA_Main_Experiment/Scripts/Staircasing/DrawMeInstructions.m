function DrawMeInstructions(display,Part,WaitTime)
% This function has all the text screens
 % Part 101 - General instructions 1 of 2
 % Part 102 -  General instructions 2 of 2
 % Part 200 - Within Block Break 
 % Part 300 - Between Block Break
 % 1  Motion
 % 2 Color
 % 3 Motion Adjustment
 % 4 Color Adjustment
 % 400 Thank you 

    switch Part
        case 101 % Part 101 - General instructions 1 of 4
            DrawFormattedText(display.windowPtr, 'Du wirst gleich zwei Gruppen von sich bewegenden Punkten in verschiedenen Farben sehen.',                display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Jede Gruppe wird sich in einer kohaerenten Richtung vor- und zurueckbewegen.',                           display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Vor jedem Durchgang wirst du auf eine bestimmete Richtung oder eine bestimmte Farbe hingewiesen werden.',display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Deine Aufgabe wird es sein, in jedem Durchgang die richtige Richtung oder Farbe zu identifizieren.',      display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Nach jedem Durchgang wird ein Fixierungskreis gezeigt werden.',                                          display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst nur wenn die sich bewegenden Punkte auf dem Bildschirm zu sehen sind,',                           display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'mithilfe der Pfeiltasten die richtige Antwort, Links oder Rechts, angeben. ',                  display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte antworte so schnell und genau wie moeglich.',                                                      display.text.linestart, display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Nach deiner Antwort wirst du eine Rueckmeldung bekommen, ob du richtig oder falsch lagst.',              display.text.linestart, display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                          'center', display.text.lines(13),display.text.color);
        
        case 102 % Part 102 -  General instructions 2 of 4
            DrawFormattedText(display.windowPtr, 'Die Hintergrunddimension, bzw. die Dimension, auf die du nicht hingewiesen wurdest, wird entweder nicht vorhanden sein,',     display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'gleich sein oder unterschiedlich sein.',                                                                                  display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn du beispielsweise darauf hingewiesen wirst, eine bestimmte Farbe zu identifizieren,',                                 display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'koennen die Punktgruppen sich entweder in die gleiche Richtung, in verschiedene Richtungen oder ganz zufaellig bewegen.', display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Ignoriere bitte in jedem Fall diejenige Dimension, auf die du nicht hingewiesen wurdest. ',                               display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn du beispielsweise eine bestimmte Richtung identifizieren sollst, ignoriere die Farbe.',                              display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Es folgen mehrere Durchgaenge derselben Dimension hintereinander.',                              display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn du beispielsweise mit einer bestimmten Farbe anfaengst, folgen einige Durchgaenge, in denen du ebenfalls auf Farben hingewiesen werden wirst.',                              display.text.linestart, display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Nach einigen Durchgaengen aendert sich die hingewiesene Dimension zu den Richtungen, danach zu den Farben usw.',                              display.text.linestart, display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                                            'center', display.text.lines(13),display.text.color);
            
        case 103 % Part 103 -  General instructions 3 of 4
            DrawFormattedText(display.windowPtr, 'Waehrend des gesamten Experiments wird in der Mitte des Bildschirms ein Fixierungskreis angezeigt werden.',              display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Es ist sehr wichtig, dass du immer diesen Kreis fixierst, auch wenn die Punkte sich bewegen.',                            display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Um dies zu pruefen, werden deine Augenbewegungen aufgezeichnet.',                                               display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Dieser Teil des Experiments dauert ca. 35 Minuten',                                                                 display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte bewege dich waehrend des gesamten Teils nicht.',     display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Nur wenn dir eine Pause angeboten wurde, kannst du dich kurz bewegen und dich ein bisschen dehnen.',                                                                         display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte versuche auch in der kurzen Pause, den Stuhl nicht zu bewegen,',                                               display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte versuche nach der Pause so genau wie moeglich in deine vorherige Position zuruekzukehren.',                                               display.text.linestart, display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Nach 35 Minuten wird auf dem Bildschirm "Ende des Teils des Experiments" erscheinen. ',                                                   display.text.linestart, display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Dann kannst du dich gerne bewegen.',                                                                                          display.text.linestart, display.text.lines(11),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                                            'center', display.text.lines(13),display.text.color);
       
        case 104 % Part 104 -  General instructions 4 of 4
            DrawFormattedText(display.windowPtr, 'Es ist sehr wichtig, dass du deine Antwort sowohl korrekt, als auch schnell gibst.',              display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn du in diesem Teil (d.h. in den naechsten 30 Minuten) deine Antwort korrekt UND schneller als 1 Sekunde gibst,',                            display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'bekommst du 2 zusaetzliche Euro am Ende des Experiments.',                                               display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Um zu wissen, ob du korrekt und/oder schnell genug warst, ',                                                                 display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'gibt es besondere Rueckmeldungen, die sofort angezeigt werden. ',     display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                                            'center', display.text.lines(13),display.text.color);

            
        case 105 % Training instructions
            DrawFormattedText(display.windowPtr, 'Sobald alles verstanden ist, fangen wir mit einem kurzen Training an.',                                          'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Das Training dauert an, bis du 8 von 12 korrekten Antworten gegeben hast.',    'center', display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um das Training zu beginnen.',                                                                    'center', display.text.lines(13),display.text.color);
      

        case 200 % Part 200 - Within Block Break 
            DrawFormattedText(display.windowPtr, 'Super! Jetzt hast du noch einen Teil geschafft.',                       'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',              'center', display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                          'center', display.text.lines(13),display.text.color);

            
        case 300 % Part 300 - Between Block Break
            
            DrawFormattedText(display.windowPtr, 'Block geschafft! Das hast du super gemacht.',                         'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst jetzt eine kurze Pause machen',                             'center', display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',             'center', display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um den naechsten Block zu starten.',        'center', display.text.lines(13),display.text.color);
             
        case 400 % begining of third or sixth block
            DrawFormattedText(display.windowPtr, 'Jetzt wird es einen Block mit gemischten Durchgaengen geben.',                       display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'In jedem Durchgang wirst du darauf hingewiesen werden, ',                            display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'entweder eine bestimmte Farbe oder eine bestimmte Richtung zu identifizieren.',      display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran immer den Fixierungskreis anzuvisieren.',                           display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                        'center', display.text.lines(13),display.text.color);

       
        case 600 % 400 Thank you 
            
            DrawFormattedText(display.windowPtr, 'Du hast es geschafft! Vielen Dank fuer deine Teilnahme.',                      'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Ende des Teils des Experiments',                                              'center', display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte sag dem Versuchsleiter Bescheid',                                              'center', display.text.lines(7),display.text.color);
           
                
        case 1 % 1  Motion
            DrawFormattedText(display.windowPtr, 'In den folgenden Durchgaengen geht es um die Bewegung der Punkte.'  ,  display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du wirst auf bestimmte Richtungen hingewiesen werden.',                display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst die Farben ignorieren. Sie haben hier keine Bedeutung.',     display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',             display.text.linestart, display.text.lines(5),display.text.color);         
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                         'center', display.text.lines(13),display.text.color);

        case 2 % 2 Color
            DrawFormattedText(display.windowPtr, 'In den folgenden Durchgaengen geht es um die Farbe der Punkte.',                  display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du wirst auf eine bestimmte Farbe hingewiesen werden.',                                display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst die Bewegung ignorieren. Sie haben hier keine Bedeutung.',             display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'In diesem Teil wird sich manchmal die Schnelligkeit des Farbwechsels aendern.',   display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte antworte so schnell und genau wie moeglich.',                               display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',                        display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                    'center', display.text.lines(13),display.text.color);


        case 3 % 3 Motion Adjustment
            DrawFormattedText(display.windowPtr, 'Super! Jetzt hast du noch einen Teil geschafft.',                                  display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Im naechsten Teil wird sich manchmal der Kohaerenzgrad der Bewegung aendern.',     display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte antworte so schnell und genau wie moeglich.',                                display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran immer den Fixierungskreis anzuvisieren.',                         display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                     'center', display.text.lines(13),display.text.color);
                      
        case 1001
             DrawFormattedText(display.windowPtr, 'Du hast das Training geschafft!',                                                  display.text.linestart, display.text.lines(5),display.text.color);
             DrawFormattedText(display.windowPtr, 'Wir fangen gleich mit dem Experiment an.',                                        display.text.linestart, display.text.lines(6),display.text.color);
             DrawFormattedText(display.windowPtr, 'Pausen werden regelmaessig zwischen den Bloecken angeboten.',                        display.text.linestart, display.text.lines(7),display.text.color);
             DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                    'center', display.text.lines(13),display.text.color);
            
        case 1002
             DrawFormattedText(display.windowPtr, 'Du hast das Training noch nicht geschafft.',                                     display.text.linestart, display.text.lines(5),display.text.color);
             DrawFormattedText(display.windowPtr, 'Du wirst das Training erneut machen.',                                         display.text.linestart, display.text.lines(6),display.text.color);
             DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                    'center', display.text.lines(13),display.text.color);
 
    
    end 
    
    Screen('DrawingFinished', display.windowPtr); % tell PTB that stimulus drawing for this frame is finished
    Screen('Flip',display.windowPtr);
    
    if ~exist('WaitTime','var')
        WaitTime = 2;
    end 
    
    WaitSecs(WaitTime);

end 


% 'Es folgen mehrere Durchgaenge der selben Dimension hintereinander. Wenn du beispielsweise mit einer bestimmten Farbe anfaengst, folgen einige Durchgaenge in denen du ebenfalls auf Farben hingewiesen wirst. Nach einigen Durchgaengen aendert sich die hingewiesene Dimension zu den Richtungen, danach zu den Farben usw. 
% 
% 
% 'Auch hier folgen mehrere Durchgaenge der selben Dimension hintereinander. Nach einigen Durchgaengen mit dem Hinweis �Farbe�, aendert sich die hingewiesene Dimension zu Richtung, danach zu Farbe usw. 
% 
% 
% 
% 
% die eine Farbe als Hinweiss haben, before es sich zu Richtung waechslen"
