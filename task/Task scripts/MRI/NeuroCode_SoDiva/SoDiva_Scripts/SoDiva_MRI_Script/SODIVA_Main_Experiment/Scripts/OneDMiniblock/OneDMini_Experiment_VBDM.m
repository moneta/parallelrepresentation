function OneDMini_Experiment_VBDM(Phase,Study,display,General_Info)
% OneDMini based Experiment
% This part is meant to run twice: once in the behavioral part as the last final part: after they know well the values and trained the main experiment 
% and once during the structural part. 
% The idea is to test later that the RT for each color/motion feature is the same and no bias in the presentation of the colors/motions (mainly colors). 

% Phase can be 'MRI' or 'BEH'.
% Study can be BehPilot or MainMRI

%% (A) Setting initial variables

% [display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
% Screen('CloseAll');

%% No eyetracker
display.EyeTracker.OnOff = false;

%% No need for ITI

% % The OneDmini part
% display.Timing.OneDMini.CueTime =
% display.Timing.OneDMini.FixPostCueTime = 
% display.Timing.OneDMini.StimTime = display.Max_Trial_duration;% now 1.6 % was 1.2;
% display.Timing.OneDMini.RespFixTime = 
% display.Timing.OneDMini.FeedbkTime = 
% display.Timing.OneDMini.LastFixTime =

% general Info 
% [Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);
Sub_Folder = General_Info.SubData;
IDSub = General_Info.subjectInfo.id;
switch Phase
    case 'first'
        GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Sub_',IDSub,'.mat']);
    case 'second'
        GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Part2_Sub_',IDSub,'.mat']);
end 
% GeneralInfoFolder = fullfile(Sub_Folder,['General_Info_Part2_Sub_',IDSub,'.mat']);
clear General_Info;
load(GeneralInfoFolder, 'General_Info');

switch Phase
    case 'first'
        General_Info.OneDMiniExperimentTimeStart_Phase1 = GetSecs;
    case 'second'
        General_Info.OneDMiniExperimentTimeStart_Phase2 = GetSecs;
end 

% load the Master and General info from the participant's folder
MasterFolder = fullfile(Sub_Folder,['Master_Sub_',IDSub,'.mat']); 
load(MasterFolder,'FinalMaster');
Master = FinalMaster;
clear FinalMaster

% load the design
FolderDet = (dir(fullfile(General_Info.SubDesigns,'OneDDesigns.mat')));
Filename = fullfile(General_Info.SubDesigns,FolderDet.name);
load(Filename,'DesignOneD');
Design = DesignOneD.(Phase);
            
% Open The Window - only if not in MRI
if ~display.RweMRI
 [display.windowPtr]=Screen('OpenWindow',display.screenNum,display.bkColor);
end 
% setting the text size and font (pretty sure it needs to happen when the wndow is open)     
Screen('TextFont', display.windowPtr, display.text.font); % select specific text font
Screen('TextSize', display.windowPtr, display.text.size); % select specific text size

%HideCursor();

%% EyeTracker
worked = false;
tries = 0;
while ~worked && tries<=3 % try 3 times
    tries = tries + 1;
    if display.EyeTracker.OnOff
        [worked, General_Info] = StartEyeTracker(General_Info, display, true, strcat('VB',num2str(StartingBlock)));
    end 
end 


if worked % here if not working just moving on at the moment. 
    fprintf('\n Eyetracker recording, tried %.0f times \n', tries);
else 
    fprintf('\n Eyetracker NOT recording, tried %.0f times \n', tries);
    display.EyeTracker.OnOff = false;
end 

%% (G) General instructions: 
%     DrawMeInstructions_VBDM(display,101, 3); % Also Flips
%     while ~KbCheck; end 
%     DrawMeInstructions_VBDM(display,102, 3); % Also Flips
%     while ~KbCheck; end 
%     DrawMeInstructions_VBDM(display,103, 3); % Also Flips
%     while ~KbCheck; end 

%%    %%%%%%%%%%%% EXPERIMENT STARTS %%%%%%%%%%%%%%%%%%%%
 
%% EyeTrackerStarting
if display.EyeTracker.OnOff
worked2 = eyetrackerStartStop(display, General_Info, 'start');

    if worked2
        fprintf('Started recording');
    else 
        fprintf('Didnt start recording');
        display.EyeTracker.OnOff = false;
    end 
end 
    
%% run experiment  
    % Load the stim for the block
    switch Phase
        case 'first'
            StimFolder = fullfile(General_Info.SubStimuli,['Stimuli_OneDfirst_',General_Info.subjectInfo.id,'.mat']);
        case 'second'  
            StimFolder = fullfile(General_Info.SubStimuli,['Stimuli_OneDsecond_',General_Info.subjectInfo.id,'.mat']);
    end 
    load(StimFolder,'Stimuli');
    fprintf('\n Loaded Stimuli \n');
    
    % Instructions for the OneD part
    switch Study % sets the reward instructions
        case 'BehPilot'
            DrawMeInstructions_VBDM(display,2001,4,'BEH'); % always BEH reward amount
            while ~KbCheck; end 
        case 'MainMRI'
            switch Phase
                case 'first'
                    DrawMeInstructions_VBDM(display,2001,4,'MRI'); % Always MRI reward amount
                    while ~KbCheck; end 
                case 'second'
                    if display.RweMRI
                     RestrictKeysForKbCheck([]) % first all keys
                     RestrictKeysForKbCheck(display.keyTargets(3)) % only space bar
                     DrawMeInstructions_VBDM(display,2002,2,'MRI'); % Also Flips % Always MRI reward amount
                     fprintf('\n Versuchsleiter: Nachdem du mit dem Versuchpersonen geschprochen hast,\n')
                     fprintf('Bitte druck den Leertaste ZWEI MAL um weiter zu machen \n')
                     while ~KbCheck; end 
                     while ~KbCheck; end  % on purpose needs twice input
                     RestrictKeysForKbCheck([]) % back to all keys
                    else
                     DrawMeInstructions_VBDM(display,2002,2,'MRI'); % Also Flips % Always MRI reward amount
                     while ~KbCheck; end 
                    end 
            end % end switch MRI or BEH within the MRI study
    end % end switch or MRI study or Behavioral pilot
    
    
    % Run block
    sprintf('\n Starting OneD mini %s', Phase);
    [OneDMini.SodivaMain, OneDMini.MainDesign, OneDMini.results, OneDMini.ResOutput, OneDMini.Master, OneDMini.display] = OneDmini_Final_Experiment(display, Design, Master, General_Info, Stimuli, Phase,Study);

    % Save Block Results
    BlockData = fullfile(General_Info.SubOutPut,['OneDMini_Data_',General_Info.subjectInfo.id,'_Phase_',Phase,'.mat']);
    save(BlockData, 'OneDMini');
    % Clear Stimuli
    clear Stimuli
    
    switch Study
        case 'BehPilot'
              [Reward, Kredite] = GetRewardsPerBlock(OneDMini,'BEH');
        case 'MainMRI'
              [Reward, Kredite] = GetRewardsPerBlock(OneDMini,'MRI');
    end 
    % save reward from first part to load later and add to the Compute
    % reward
    RewardData = fullfile(General_Info.SubOutPut,['Kredite_OneDMini_',General_Info.subjectInfo.id,'_Phase_',Phase,'.mat']);
    save(RewardData, 'Kredite');
    RewardData = fullfile(General_Info.SubOutPut,['Reward_OneDMini_',General_Info.subjectInfo.id,'_Phase_',Phase,'.mat']);
    save(RewardData, 'Reward');
    
%     Reward = GetRewardsPerBlock(OneDMini,Phase);
    % clear block data
    clear OneDMini
    if display.RweMRI   
        % adding here 10 seconds of this display to make sure the sequence
        % is done and sample the last BOLD (in case experiment was a bit shorter than sequence)
         DrawMeInstructions_VBDM(display,204,5,Reward); % Also Flips
         %%% Somehow make a user-dependent input here 
%          RestrictKeysForKbCheck([]) % first all keys
%          RestrictKeysForKbCheck(display.keyTargets(3)) % only space bar
%          fprintf('\n Versuchsleiter: Nachdem du mit dem Versuchpersonen geschprochen hast,\n')
%          fprintf('Bitte druck den Leertaste ZWEI MAL um den Block zu beenden \n')
%          while ~KbCheck; end 
%          while ~KbCheck; end  % on purpose needs twice input
%          RestrictKeysForKbCheck([]) % back to all keys
         
    else % behavioral
         DrawMeInstructions_VBDM(display,203,5,Reward); % Also Flips
    end 
    
% end of running  
oldType = ShowCursor();

tic
if display.EyeTracker.OnOff
worked3 = eyetrackerStartStop(display, General_Info, 'finish');

    if worked3
        fprintf('\n finished recording \n ');
    else 
        fprintf('\n Didnt finish recording \n ');
    end 
    
end 
fprintf('\n time to transfer eye files was: \n')
toc


%% ALL DONE! 
%Screen('CloseAll');

if display.EyeTracker.OnOff
Eyelink('Shutdown');
end
sca

end 

