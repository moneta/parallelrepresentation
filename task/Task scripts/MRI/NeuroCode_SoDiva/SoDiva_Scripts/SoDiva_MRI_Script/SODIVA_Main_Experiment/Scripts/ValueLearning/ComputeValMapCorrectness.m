function PerCor = ComputeValMapCorrectness(Results)

% input: vector with correctness
% 1 - means correct
% eliminate all zeros from the end


i = find(Results.response, 1, 'last');

VectorCorr = Results.response(1:i);

VectorCorr(VectorCorr~=1)=0;

PerCor = sum(VectorCorr)/length(VectorCorr);







end 