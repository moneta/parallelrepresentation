function [Master,display] = GenerateBasicMasetr(display)     
%% Possible ways to generate the colors:  according to display.PilotType
%
 for i = 1:4
    Master.Colors{i}.type = strcat('c',num2str(i));
    Master.Colors{i}.dia = display.colorDiagonal;
    Master.Colors{i}.bri = 0.5;
    Master.Colors{i}.coherence = display.colorCoherence; %
    Master.Colors{i}.ColorStartPoint = display.ColorStartPoint(i);
    Master.Colors{i}.speed = ((Master.Colors{i}.dia - display.ColorStartPoint(i))/(display.nFramesTrial))*(1/Master.Colors{i}.coherence); % All dots are colored by diagonal value by the end of the trial. 
%     if strcmp(display.ColorMove,'Accumi')
%     Master.Colors{i}.ColXCenter = display.ColXCenter(i);
%     Master.Colors{i}.ColYCenter = display.ColYCenter(i);
%     end 
    Master.Motions{i}.direction=(i-1)*45;
    Master.Motions{i}.coherence=display.StartingMotionCoherence; % was 0.7
    % Color
     switch i
        case 1
        Master.Colors{i}.CueX  = 0.5 + ((0.5^0.5)/2)/(2^0.5);
        Master.Colors{i}.CueY  = 0.5 + ((0.5^0.5)/2)/(2^0.5);
        case 2
        Master.Colors{i}.CueX  = 0.5 - ((0.5^0.5)/2)/(2^0.5);
        Master.Colors{i}.CueY  = 0.5 + ((0.5^0.5)/2)/(2^0.5);
        case 3
        Master.Colors{i}.CueX  = 0.5 - ((0.5^0.5)/2)/(2^0.5);
        Master.Colors{i}.CueY  = 0.5 - ((0.5^0.5)/2)/(2^0.5);
        case 4
        Master.Colors{i}.CueX  = 0.5 + ((0.5^0.5)/2)/(2^0.5);
        Master.Colors{i}.CueY  = 0.5 - ((0.5^0.5)/2)/(2^0.5);
     end 
 end
% 1D stim
Master.Colors{5}.type = 'c5';
Master.Colors{5}.dia = (0.5^0.5)/2;
Master.Colors{5}.bri = 0.5;
Master.Colors{5}.coherence = 0; %
Master.Colors{5}.ColorStartPoint = 0;
Master.Colors{5}.speed = 0; % no speed stays grey
Master.Motions{5}.direction=1000;
Master.Motions{5}.coherence=0; % was 0.6
% if strcmp(display.ColorMove,'Accumi')
%     Master.Colors{5}.ColXCenter = 0.5;
%     Master.Colors{5}.ColYCenter = 0.5;
% end 
    
  %BE400  DiagonalStartPoint = [0.0316  ,  0.0322  ,  0.0365 ,  0.0486];

% 
%          for i = 1:4
% %              switch display.ColorMove
% %                 case 'SpeedBased'
% %                     if LowCohHighSpeed
% %                         % if we want to test the low coherenc high speed
% %                         Master.Colors{i}.coherence = 0.1; %
% %                         Master.Colors{i}.speed = Master.Colors{i}.dia/4;    
% %                     end 
% %                     if strcmp(display.ColorStarting,'DrawnGu')
% %                         % we need:
% %                         Master.Colors{i}.FirstFrameCenter = FFC;
% %                         Master.Colors{i}.FirstFrameCoherence = FFCoh;
% %                         Master.Colors{i}.DrawnGuSignalSTD = DGSSTD ;
% %                         Master.Colors{i}.DrawnGuNoiseSTD = DGNSTD;
% %                     elseif strcmp(display.ColorStarting,'XFramesAfter')
% %                         % we need how many frames after to start
% %                         Master.Colors{i}.StartX = Xframes;
% %                     end
% %                  case 'CoherenceBased'
%                        Master.Colors{i}.CohSignalSTD = CohSigSTD;
%                        Master.Colors{i}.CohNoiseSTD = CohNoiSTD;
%                        Master.Colors{i}.coherence = StartingCoherence;
%                        % if wanting to change the center of signel: 
% %                        Master.Colors{i}.dia = na;%NewDiagonal(i);  
% %              end 
%          end 
% 
%          % just in case adding the 1D option:
%             Master.Colors{5}.FirstFrameCenter = 0.5;
%             Master.Colors{5}.FirstFrameCoherence = 1;
%             Master.Colors{5}.DrawnGuSignalSTD = 0.0001;
%             Master.Colors{5}.DrawnGuNoiseSTD = 0.1;
%             Master.Colors{5}.CohSignalSTD =  0.0001;
%             Master.Colors{5}.CohNoiseSTD =  0.0001;




% (1) Speed Based adjustment:
%   1.1 Starting at zero (now we reward STR)
%   1.2 Half the dots start a bit later
% This ran with BE400. didnt work.  
% DiagonalStartPoint = [0.0316  ,  0.0322  ,  0.0365 ,  0.0486];
%NewSpeed_Target = [0.0109 , 0.0112  ,  0.0127  ,  0.0168];
% (.0316/0.0129*3)*0.016

% display.ColorMove = 'SpeedBased';
% Need to decide where to start
% display.ColorStarting = 'AtZero';
% display.ColorStarting = 'After';
    % need to shave ca. 100ms of old speed (0.0129)
    % but also make speed much slower (i.e. closer target)
  
% (2) Coherence based adjustment
%  display.ColorStarting = 'AtZero'; % doesnt matter how to start
%  display.ColorMove = 'CoherenceBased';
%  CohSigSTD = 0.1;
%  CohNoiSTD = 0.2;
%%
% option 1: draw from two circles and then move normaly:
% display.ColorMove = 'SpeedBased';
% LowCohHighSpeed = false;
% display.ColorStarting = 'DrawnGu';
% % FirstFrameCenter, FirstFrameCoherence, Master.Colors{5}.dia = (0.5^0.5)/2;
% dia = (0.5^0.5)/2;
% FFC = (dia)/8/(2^0.5); % Diagonal point conerted to the X axis 
% FFCoh = 0.1; % how many will be drawn on the correct color
% %DrawnGuSignalSTD
% DGSSTD = 0.01;
% DGNSTD = 0.01;

%%
% %option 2: adjust the coherence, colorful noise but not over the whole
% space
% display.ColorMove = 'CoherenceBased';
% display.ColorStarting = 'AtZero';
% LowCohHighSpeed = false;
% CohSigSTD = 0.05; % all quarter 
% CohNoiSTD = 0.1; % whole screen
% StartingCoherence = 0.3;
% NewDiagonal = (0.5^0.5)/4;

%Or: taking more space in the color space - more colorful 
% CohSigSTD = 0.1; % all quarter 
% CohNoiSTD = 0.2; % whole screen
% StartingCoherence = 0.3;
% NewDiagonal = (0.5^0.5)/2;

% %%
% option 3: adjust the coherence, grey noise
% FFC = NewDiagonal/(2^0.5);

 % NewDiagonal =  [0.1003    0.1075    0.1342    0.1754];
% Since those diagonals were presented in the small space, we need to run
% the change on them if we want to draw them on the big space
% XCenter = [0.6003    0.3925    0.3658    0.6754]*0.8 + 0.1;
% YCenter = [0.6003    0.6075    0.3658    0.3246]*0.8 + 0.2; 
 %stays here
end 