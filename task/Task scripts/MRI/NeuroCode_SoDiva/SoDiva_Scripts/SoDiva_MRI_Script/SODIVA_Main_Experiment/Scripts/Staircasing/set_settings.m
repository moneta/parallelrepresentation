function [display,General_Info, ThumbsUp, Arrow, ClockSign] = set_settings(duration)
% This function creates the display structure with the fields: 
%       input: duration - if not given, 1.6
%
%       output: 
% 
% display: structure with the fields: 
%             keyTargets: [Number of response keys]
%                keyList: [All possible keys]
%         deviceKeyNames: PTP device setting
%            deviceNames: {'Keyboard'}
%               deviceID: 0
%              screenNum: 2
%                   dist: 60 - This is the distance of participants from the screen. Crucial to measure in real life.
%                  width: of the screen- also crucial to measure in real life
%             skipChecks: 1
%                bkColor: Background color
%              windowPtr: Pointer to PTP screen
%              frameRate: Frames per second
%            refreshRate: seconds per frame
%             resolution: Of the screen (comes automatically from PTP)
%                 center: Of the screen (comes automatically from PTP)
%     Max_Trial_duration: (=stimuli and not trial) Based on Timing, only added half a refresh rate for accuracy. 
%           nFramesTrial: Calculated based on Max duration
%            TrialLength: (=stimuli and not trial) Same as Max Trial Duration
%                 Timing: structure with all the timing of each trial
%                         fields: 
%                                 CueTime: 0.5000
%                                StimTime: 1.6083
%                             RespFixTime: 0.4000
%                              FeedbkTime: 0.4000
%                             LastFixTime: 0.3000
%                                  FixQue: 1.5000
%                               FixQueFix: 0.5000
%                   text: structure with all information on text presented
%                         fields:
%                                   color: of text
%                                   style: of text
%                                    size: of text
%                                    font: of text
%                                   lines: set lines to present the text with DrawFormattedText
%                   fixi: do we want fixation with the dots? 
%               fixation: structure with all information about the fixation
%                         fields:
%                                   size: in angles
%                                     sz: in pixels
%                                   mask: 
%                                  color: 
%                                   flip: 
%       FixControlLength: number of frames for the control condition with black spot on the fixation
%        MotionLineWidth: motion cue details (now not used)
%       MotionArrowWidth: motion cue details (now not used)
%               feedback: structure with info on feedback:
%                         fields:
%                                    sz: 
%                                  size: 
%                                 color: 
%                                  flip: 
%                                  text: 
%           apertureSize: of each group of dots
%        distBetweenStim: between the two groups
%              dotsSpeed: 
%               dotsSize: 
%            dotsDensity: see formula in code
%                dotsNum: 
%           dotsLifetime: set as length of trial for now
%               endpoint: 'back2start'
%            WantedShape: 'circle'
%                isInstr: in case of instructions we need one in the middle
%                
% General_Info: structure with the fields:
% 
%        pathPsychtoolbox: Path
%               studyName: 'SODIVA'
%            computerName: 
%            computerHost: 
%          computerMatlab: 
%                pathRoot: 
%                pathTask:
%             pathScripts: 
%                pathData: 
%     ExperimentTimeStart: 
%            
% 
% ThumbsUp: matrix wit hthe feedback image
% Arrow: matrix with the motion cue image



%% initalize random number generator

rng(sum(100*clock)); 

%% Study name (will set the folder structure as well)
General_Info.studyName = 'SODIVA_Main_Experiment';

%% Get computer details
General_Info.computerName = computer; % save information about computer
General_Info.computerHost = char(getHostName(java.net.InetAddress.getLocalHost));
General_Info.computerMatlab = ['R' version('-release')]; % save information about operating systemversion('-release')

%% are we running MRI?
% set basic logic variable if it is MRI
% try acessing mri port
% set distance and width of screen. 
if strcmp(General_Info.computerHost,'LIP-XP-165-167') % MRI
   display.RweMRI  = true;
   DistanceFromScreen = 91;
   WidthOfScreen = 27;
   % TRY TO ACCESS THE SCANNER PORT
    try
        fprintf('\n Trying to read the scanner port now...\n'); % display task progress
        outportb(890,32) % sets pin for tristate in base+2 to up state. This allows to read from the scanner port.
    catch
        warning('Reading from the scanner port was not successful! \n '); % display warning
    end
else 
   display.RweMRI  = false;
   DistanceFromScreen = 60;
   WidthOfScreen = 52;
end 

%% set task paths
% SET ALL NECESSARY TASK PATHS

if strcmp(General_Info.computerHost,'lip-win-001728') % Nirs workstation
    General_Info.pathRoot = ('C:\Users\moneta\Seafile_Folder\Seafile\My Library\SODIVA'); % set root path
    General_Info.pathPsychtoolbox = fullfile('C:\Users\moneta\Google Drive\Matlab\toolboxes\Psychtoolbox'); % set root path
    
elseif strcmp(General_Info.computerHost,'NRCD-W7-472061') % Computer in behavioral on the corner
    General_Info.pathRoot = fullfile('D:\NirSeafile\Seafile\My Library\SODIVA'); % set root path
    General_Info.pathPsychtoolbox = fullfile('C:\Users\Public\MatLabToolbox\Psychtoolbox'); % set root path
  
elseif strcmp(General_Info.computerHost,'NRCD-W7-472062') % Computer in behavioral not on the corner
    General_Info.pathRoot = fullfile('D:\NirSeafile\Seafile\My Library\SODIVA'); % set root path
    General_Info.pathPsychtoolbox = fullfile('C:\Users\Public\MatLabToolbox\Psychtoolbox'); % set root path
 
elseif strcmp(General_Info.computerHost,'Nirs-computer') % my laptop
    General_Info.pathRoot = ('C:\Users\Nir\Google Drive\Matlab\MPI\VBDM'); % set root path
    General_Info.pathPsychtoolbox = fullfile('C:\Users\Nir\Google Drive\Matlab\toolboxes\Psychtoolbox'); % set root path

elseif strcmp(General_Info.computerHost,'LIP-XP-165-167') % MRI
    General_Info.pathRoot = fullfile('D:','Dokumente und Einstellungen', 'guest','Desktop','NeuroCode_SoDiva','SoDiva_Scripts','SoDiva_MRI_Script');
    General_Info.pathPsychtoolbox = fullfile(PsychtoolboxRoot); % set root path
end

General_Info.pathTask = fullfile(General_Info.pathRoot,General_Info.studyName); % path to the task folder

% add to path
addpath(genpath(General_Info.pathTask));

% all designs paths
General_Info.pathALLdesigns    = fullfile(General_Info.pathTask,'Designs');
General_Info.pathdesigns       = fullfile(General_Info.pathALLdesigns,'Staircasing');
General_Info.pathUSEDdesigns   = fullfile(General_Info.pathALLdesigns,'Staircasing_Used');
General_Info.pathTraindesigns  = fullfile(General_Info.pathALLdesigns,'Staircasing_Training');
General_Info.pathValueMaps     = fullfile(General_Info.pathALLdesigns,'ValMap');
General_Info.pathUSEDValueMaps = fullfile(General_Info.pathALLdesigns,'ValMap_Used');
General_Info.pathVBDM          = fullfile(General_Info.pathALLdesigns,'VBDM');
General_Info.pathUSEDVBDM      = fullfile(General_Info.pathALLdesigns,'VBDM_Used');

General_Info.pathScripts       = fullfile(General_Info.pathTask,'Scripts'); % path to the task script folder
General_Info.pathData          = fullfile(General_Info.pathTask,'Data'); % path to the task data folder
General_Info.pathETData          = fullfile(General_Info.pathTask,'ET_Data'); % path to the task data folder


addpath(genpath(General_Info.pathScripts));

cd(General_Info.pathScripts) % set the current directory to the script folder
load('WhiteArrow.mat', 'Arrow');
load('ThumbsUp.mat','ThumbsUp');
load('ClockSign.mat','ClockSign');


%% should it move used design to used one?
display.ForReal = true;

%% PsychToolBox
% TRY TO ADD PSYCHTOOLBOX TO THE SEARCH PATH
try
    Psychtoolboxversion
catch ME
    if strcmp(ME.identifier,'MATLAB:UndefinedFunction')
        fprintf(2,'Psychtoolbox was not found in the MATLAB search path!\n');
        fprintf(1,'Trying to add Psychtoolbox to the MATLAB search path now...\n');
        addpath(genpath(General_Info.pathPsychtoolbox));
    end
    clear ME
end
fprintf('Psychtoolbox was added to the MATLAB search path.\n');

% change path to make it work only if in behavioral computer:
if strcmp(General_Info.computerHost,'NRCD-W7-472062') || strcmp(General_Info.computerHost,'NRCD-W7-472061')
oldpath = path;
newfolder = fullfile('C:\Users\Public\MatLabToolbox\Psychtoolbox\PsychBasic\MatlabWindowsFilesR2007a\');
path(newfolder,oldpath);
end 

% clearing
sca

%% Set keyboard variables, flush, dummy calls etc... 
% RUN KBQUEUE COMMANDS ONCE, TO AVOID CONFLICT WITH GETCHAR
KbQueueCreate; % initalize response queue
KbQueueStop; % stop queue

% DUMMY CALLS TO PSYCHTOOLBOX FUNCTIONS:
KbCheck(-1); % dummy call to KbCheck
WaitSecs(0.1); % dummay call to WaitSecs
GetSecs; clear ans; % dummy call to GetSecs

KbName('UnifyKeyNames'); % ensure cross-platform compatibility of keynaming
% display.keyTargets = [KbName('LeftArrow'),KbName('RightArrow'), KbName('DownArrow'),  KbName('UpArrow')];
if  display.RweMRI % R we in MRI? 
display.keyTargets = [KbName('g'),KbName('b'), KbName('space')]; %[KbName('g'),KbName('r'), KbName('space')]; 
else
display.keyTargets = [KbName('LeftArrow'),KbName('RightArrow'), KbName('space')];
end 
display.keyList = zeros(1,256); % initalize a key list of 256 zeros
display.keyList(display.keyTargets) = 1; % set keys of interest to 1

[display.deviceKeyNames,display.deviceNames] = GetKeyboardIndices; % get a list of all devices connected
display.deviceID =  display.deviceKeyNames; 

%% Screen Settings:
% number, distance, width, skipchecks
screens = Screen('Screens');
if display.RweMRI
        display.screenNum = 1; % choosing the disply screen, max is the attached screen. 
else 
        display.screenNum = max(screens); % choosing the disply screen, max is the attached screen. 
end 

display.dist = DistanceFromScreen;  %cm distance of participant from the screen. was 50 but common is 60. 

% width   

if strcmp(General_Info.computerHost,'lip-win-001728') % Nirs workstation
    set(0,'units','centimeters');
    b=get(0,'MonitorPositions');
    display.width = b(end,3);
elseif strcmp(General_Info.computerHost,'NRCD-W7-472061') % Computer in behavioral on the corner
    display.width = WidthOfScreen; % needs to be measured by hand. in centimeters. the automatic command is not always accurate
elseif strcmp(General_Info.computerHost,'NRCD-W7-472062') 
    display.width = WidthOfScreen;
elseif strcmp(General_Info.computerHost,'Nirs-computer') % my laptop
    set(0,'units','centimeters');
    Cent_SS = get(0,'screensize');
    display.width = Cent_SS(3);
elseif strcmp(General_Info.computerHost,'LIP-XP-165-167') % MRI
    display.width = WidthOfScreen;
end 
    
display.skipChecks = 1; %avoid Screen's timing checks and verbosity

% PSYCHTOOLBOX SETTINGS
if display.skipChecks
    %Screen('Preference', 'Verbosity', 0); this was the old one
    Screen('Preference','Verbosity',2);
    Screen('Preference', 'SkipSyncTests',1); % for maximum accuracy and reliability
    %Screen('Preference', 'VisualDebugLevel',0); This was the old one
    Screen('Preference','VisualDebugLevel',3);
    Screen('Preference','SuppressAllWarnings',1);
    set(0,'DefaultFigureWindowStyle','normal');
    Screen('Preference','TextEncodingLocale','UTF-8'); % set text encoding preference to UTF-8
    Screen('Preference', 'TextRenderer', 0);    
end


%% DISPLAY SETTINGS

% background color:
display.bkColor = [0,0,0]; %black

% get WindowPtr, resolution, center
[display.windowPtr,res]=Screen('OpenWindow',display.screenNum,display.bkColor); %Open the window
display.frameRate = 1/Screen('GetFlipInterval',display.windowPtr); %Hz %Set the display General_Info 'frameRate' and 'resolution'
display.refreshRate = 1/display.frameRate;
display.resolution = res([3,4]);
display.center = floor(display.resolution/2);
if ~exist('duration','var')
    duration = 1.6; % was 1.2 info on trial % Trial Time %%% at the moment its presenting ca. 3 times what im giving it
end 
%% Stim Duration
display.Max_Trial_duration = duration+0.5*display.refreshRate; % we want on average for stim to be 1.2 seconds. 
display.nFramesTrial = round(display.Max_Trial_duration*display.frameRate); %Calculate total number of temporal frames, adding 1 becasue the first one is set to time 0. 
display.TrialLength = display.nFramesTrial/display.frameRate; % this is in refresh rate units

%% Trial timing

% staircasing
display.Timing.CueTime = 0.5;
display.Timing.StimTime = display.Max_Trial_duration;% noe 1.6 % was 1.2;
display.Timing.RespFixTime = 0.4; % Create stim - should be under 0.8 seconds  - only first one takes longer probably becasue its creating the fields of the structures
display.Timing.FeedbkTime = 0.4; % create the other?
display.Timing.LastFixTime = 0.4;
display.Timing.FixQue = 1.5;
display.Timing.FixQueFix = 0.5;

display.MakeLastSTRBlockShorter = false; 

% STR time limit 
display.STRtimeLimit = 1;

% ValLearning (adding what is missing)
display.Timing.ValLearning.Firstfix = 0.2;
display.Timing.ValLearning.Lastfix = 0.4;
% feedback starts in 0.4 for the forced choices (only one value to process)
% and it changes for the 2 choice to be 0.8 becasue its 2 values to process
%
% ITI for VBDM (will be generated inside the main script)
display.ITI.Trials = 444; % in pilot 1 and 2 we had 504 trials. we decided to drop the last block.
display.ITI.MeanITI = [0.600 ; 3.400 ; 1.250];
display.ITI.RangeITI = [0.500 , 2.500; 1.500, 9.000; 0.700, 6.000];
display.ITI.MaxDiviationTotal = 0.5; % max 500ms differnece in the whole experiment's ITI.
display.ITI.MaxDiviationPerMean = 0.01; % 10ms difference in the mean of ITI
display.ITI.blocks = [96 ; 96 ; 96 ; 96 ; 60]; % length of each block
% generate them only in VBDM parts
% fprintf('\n Starting to generate ITIs');
% % ITIs = generateITI(display.ITI.Trials,display.ITI.MeanITI, display.ITI.RangeITI,display.ITI.MaxDiviationPerMean,display.ITI.MaxDiviationTotal,display.ITI.blocks );
% display = generateITI(display);
% fprintf('\n Finished generating ITIs \n');

% The VBDM: 
display.Timing.VBDM.CueTime = 0.6; % was in 2nd pilot this is 1.0 and another 0.1 fix between cue and stim, 28.08.18
display.Timing.VBDM.StimTime = display.Max_Trial_duration;% now 1.6 % was 1.2;
display.Timing.VBDM.FeedbkTime = 0.8; %
display.Timing.VBDM.MidBlockBreak = 15; % plus 4 sec reading after plus 5 seconds countdown
%display.Timing.VBDM.FeedbkTime = 0.5; % 

% The OneDmini part
display.OneDMiniTrials = 60; % length is total of 4.2 per trial. 
display.Timing.OneDMini.CueTime = 0.6; % this is 1.0 and another 0.1 fix between cue and stim
display.Timing.OneDMini.FixPostCueTime = 0.5; % before MRI started changed from 0.6 % this is 1.0 and another 0.1 fix between cue and stim
display.Timing.OneDMini.StimTime = display.Max_Trial_duration;% now 1.6 % was 1.2;
display.Timing.OneDMini.RespFixTime = 0.4; % Create stim - should be under 0.8 seconds  - only first one takes longer probably becasue its creating the fields of the structures
display.Timing.OneDMini.FeedbkTime = 0.6; % create the other?
display.Timing.OneDMini.LastFixTime = 0.5;% Since this is a bit more demanding, trying to increase from 0.4 to 0.5 seconds the last fixxation (or the last ITI)
% trial is 4.6 seconds, we can have 60 trials, meaning 30 color and 30
% motion, 10 for each specific value within context. 
display.Timing.Training.FixPostCuelimit = 0.8;
display.Timing.Training.PostStimFixlimit = 4;
display.Timing.Training.PostOutcomeFixlimit = 2;
%%%%% OLD VBDM VALUES %%%%%
% % The VBDM: will change when we introduce the ITI.
% display.Timing.VBDM.CueTime = 1.1; % this is 1.0 and another 0.1 fix between cue and stim
% display.Timing.VBDM.StimTime = display.Max_Trial_duration;% noe 1.6 % was 1.2;
% % display.Timing.VBDM.RespFixTime = 0.4; % Create stim - should be under 0.8 seconds  - only first one takes longer probably becasue its creating the fields of the structures
% display.Timing.VBDM.FeedbkTime = 1.0; % create the other?
% % display.Timing.VBDM.LastFixTime = 0.5;% Since this is a bit more demanding, trying to increase from 0.4 to 0.5 seconds the last fixxation (or the last ITI)

%% Text info: 
display.text.color = [255, 255, 255]; % white 
display.text.style = 1; %Bold
% if display.RweMRI
% display.text.size  = 22;
% else 
display.text.size  = 18;    
% end 
display.text.font  = 'Arial'; 
Screen('TextFont', display.windowPtr, display.text.font); % select specific text font
Screen('TextSize', display.windowPtr, display.text.size); % select specific text size      

% lines for text:
for txt = 1:round(display.resolution(2)/(3*display.text.size))
display.text.lines(txt) = txt*(3*display.text.size)+(3*display.text.size);
end 

display.text.linestart = 100;

%% Fixation details
% do we want fixation with dots? 
display.fixi = true; 
% display.fixi = true;
if display.RweMRI
display.fixation.size = 0.25; % MRI is 0.25 times 2 so total 0.5 angle
else 
display.fixation.size = 0.5;   %Size of fix also circular default is 0.5 degrees- now 
end 

display.fixation.sz = angle2pix(display,display.fixation.size/2); % size in pixels
display.fixation.mask = 2;   %Size of circular 'mask' that surrounds the fixation default is 2 degrees
display.fixation.color = [127.5,127.5,127.5];    %  default is white [255,255,255]
display.fixation.flip = 1; %      Flag for whether or not to call Screen's 'Flip function at the end.  Default is 1   

%%  FixiControl
display.FixControlLength = 20; % in frames, probably doubled later? 
%% Cue details
display.MotionLineWidth = 3;
display.MotionArrowWidth = 10;

%% Feedback details
display.feedback.sz = display.fixation.sz*3;
display.feedback.size  = display.text.size;
display.feedback.color = display.fixation.color; % in pilot 1 and 2 it was [255,255,255], 28.08.18
display.feedback.flip  = 1;
display.feedback.text = {'Correct!', 'Wrong', 'No answer', 'Not a valid answer', 'Correct on Fix!', 'Missed the Fix'};

display.ValOutcome.size = 40;
display.VBDMCue.size = 30;

display.feedback.xlocation = display.center(1) - display.ValOutcome.size; % display.center = floor(display.resolution/2)
display.feedback.ylocation = display.center(2) + display.ValOutcome.size; % display.center = floor(display.resolution/2)

%% Dots info:  
%stim size
%display.apertureSize = [18,18]; % [7 , 7] %size of elliptical aperture (degrees) % Pilly used 18x18 but one stim in the center. 
display.apertureSize = [7,7]; % [7 , 7] %size of elliptical aperture (degrees) % Pilly used 18x18 but one stim in the center. 
display.distBetweenStim = 4; % first and second full pilots had 2 in them. 28.08.18


%display.dotsSpeed = 12; % 12 degrees per second % Pilly used 12
display.dotsSpeed = round((2/3)*display.apertureSize(1));% relative to the size, same ratio Pilly had
%display.dotsSize = 3; %3 5 10 % display.dotsSize of dots in pixels % Pilly used 3
%display.dotsDensity = 100; % 16.7; % Pilly et al. % only in case of square/circle:


display.dotsLifetime = display.nFramesTrial;% number of frames for each dot to live, double 3 so will always stay the same dot

display.dotskill = false;



% do we want them to be put back or random?
display.endpoint = 'back2start'; % Pilly used this in all
% display.endpoint = 'random';

% do we want circle or square? 
display.WantedShape = 'circle';
% display.WantedShape = 'square';

%% Info fo instruction
display.isInstr = false;

%% EyeTracker
display.EyeTracker.dummymode = 0;
display.EyeTracker.OnOff = true;
General_Info.Eyetracker.files = {};

% display = orderfields(display); % orders all fields in the structure alphabetically

Screen('CloseAll');

display.NewColorStart = true;



%% color Testing

%% Pilots 
% display.PiloyType = 'BE500'; % BE500: just like BE100 only this time with rewarding the STR
 display.PiloyType = 'BE600';  % BE600: starting at 1/4 of BE400, closer diagonal aim and starting speed slower (i.e. calculated as distance from starting point to diagonal)
% display.PiloyType = 'AC100'; % AC100: dots are colored every frame. more dots.

% in all cases: 
display.StartingMotionCoherence = 0.7;
display.HowOftenRandMotion = 1; % lets not play with the motion too much
% display.dotsSpeed = display.dotsSpeed -1;
display.HowOftenRandColor = 1;

%switch display.PiloyType
%    case 'BE500' 
% Dots Size and nummber    
display.dotsSize = 3; 
display.dotsDensity = 100; % 100 is 48, 150 is 72, 200 is 96
display.dotsNum = 2*(ceil((display.dotsDensity*pi*((display.apertureSize(1)/2)^2)/85)/2)); % Dots density - 16.7 x pie x r*2 x 1/85
% for the one Dimension trials, display.dotsNum needs to be divided by 8
% rounding up the number of dots
display.dotsNum = display.dotsNum + 8 - mod(display.dotsNum,8);

display.ColorMove = 'SpeedBased';
display.ColorStarting = 'AtZero';

display.colorDiagonal = (0.5^0.5)/3;
display.colorCoherence = 0.3;
display.ColorStartPoint = [0,0,0,0];

%     case 'BE600'
% display.dotsSize = 3; 
% display.dotsDensity = 100; % 100 is 48, 150 is 72, 200 is 96
% display.dotsNum = 2*(ceil((display.dotsDensity*pi*((display.apertureSize(1)/2)^2)/85)/2)); % Dots density - 16.7 x pie x r*2 x 1/85
% % for the one Dimension trials, display.dotsNum needs to be divided by 8
% % rounding up the number of dots
% display.dotsNum = display.dotsNum + 8 - mod(display.dotsNum,8);
% 
% display.ColorMove = 'SpeedBased';
% display.ColorStarting = 'After'; % meaning half the dots start after
% 
% display.colorDiagonal = (0.5^0.5)/3;
% display.colorCoherence = 0.3;
% display.ColorStartPoint = [0.0316  ,  0.0322  ,  0.0365 ,  0.0486]./4;
% this should save from the BE500: these timing: TimeSaved =   0.053681485260845   0.054700753968330   0.062005513038634   0.082560765306237
% but notice that this script starts with a different speed, i.e. from the
% diagonal place to the target. meaning that it should be slower to see the
% color. 
% this was in BE400: display.ColorStartPoint = [0.0316  ,  0.0322  ,  0.0365 ,  0.0486]
%     case 'AC100'
% display.dotsSize = 3; 
% display.dotsDensity = 100; % 100 is 48, 150 is 72, 200 is 96
% display.dotsNum = 2*(ceil((display.dotsDensity*pi*((display.apertureSize(1)/2)^2)/85)/2)); % Dots density - 16.7 x pie x r*2 x 1/85
% % for the one Dimension trials, display.dotsNum needs to be divided by 8
% % rounding up the number of dots
% display.dotsNum = display.dotsNum + 8 - mod(display.dotsNum,8);
% 
% 
% display.ColorMove = 'Accumi'; 
% display.ColXCenter = [0.6503    0.3925    0.3658    0.7554]*0.8 + 0.1;
% display.ColYCenter = [0.6503    0.6075    0.3658    0.2546]*0.8 + 0.2;  
% %display.ColXCenter = [0.6503    0.3925    0.3658    0.6754]*0.8 + 0.1;
% %display.ColYCenter = [0.6503    0.6075    0.3658    0.3246]*0.8 + 0.2;  
% %display.ColorDrawing = 'BigNoise';
% 
% display.colorCoherence = 0.3; % trying here this 
% % not relevant but easier to script
% display.colorDiagonal = (0.5^0.5)/2;
% display.ColorStartPoint = [0,0,0,0];
% 
% end 


%% For testing: 
% display.EyeTracker.OnOff = false;
% display.ForReal = false;
display.MakeLastSTRBlockShorter = false; 

end 
%% Compute the time saved given the diagonal 
% Diagonal = [0.0316  ,  0.0322  ,  0.0365 ,  0.0486]./4;
% speednow = 0.008184106263733 ;% BE500;
% 
% nFrames = (Diagonal./speednow)./0.3;
% TimeSaved = nFrames*display.refreshRate







%%
% display.dotskill = true;
% display.dotsLifetime = 5;% number of frames for each dot to live, double 3 so will always stay the same dot
%display.dotsLifetime = display.nFramesTrial;% number of frames for each dot to live, double 3 so will always stay the same dot
%display.dotsSpeed = round((2/3)*display.apertureSize(1));

% 
% % (2) Color starting point 
% display.ColorStarting = 'AtZero';
% display.ColorStarting = 'XFramesAfter';
% display.ColorStarting = 'DrawnGu';
% 
% % (3) Color movement
% display.ColorMove = 'SpeedBased';
% display.ColorMove = 'CoherenceBased';

% Possible Combis:
% 
% display.ColorStarting = 'AtZero';
% % but high speed and low coherence
% 
% display.ColorStarting = 'XFramesAfter';
% display.ColorMove = 'SpeedBased';
% % set frames to start late (not sure what speed)
% 
% 
% display.ColorStarting = 'DrawnGu';
% display.ColorMove = 'SpeedBased';
% % Set starting ration of starting in place and in random
% 
% display.ColorStarting = 'DrawnGu';
% display.ColorMove = 'CoherenceBased';
% % Set coherence level
% 
% %%%% CHANGES TO BE DONE %%%%%
% 1. Set parameters in colors: 
% 2. Save coherenec in Training and in STR - add in Staircaidng create the coherence 
% 3. Adjusting Master
% 4. StimCreate
% 
% Variables for first frame 
% Colors.StartX
% Colors.DrawnGuSignalSTD;
% Colors.DrawnGuNoiseSTD;
% Colors.FirstFrameCoherence;
% 
% Variables for changing coherenec:
% Colors.CohNoiseSTD;
% Colors.CohSignalSTD;






  % Do we want to make motion easier? add this number ! 
% 
% 
% display.GraySNR = true;
% 
% if display.GraySNR
% display.ColorDrawing = 'BigNoise';
% display.CohSigSTD = 0.025;
% display.CohNoiSTD = 0.005;
% display.NoiseYCenter = 0.5;
% display.StartingColorCoherence = 0.3; % 0.4
% display.HowOftenRandColor = 2;
% display.ColXCenter = [0.6803    0.3825    0.3058    0.6854]*0.8 + 0.1;
% display.ColYCenter = [0.6603    0.6675    0.2458    0.2746]*0.8 + 0.2; 
% end 
