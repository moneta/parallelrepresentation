% SoDiva Main Experiment Script, Part 1, preparation room:
% behavioral
% restoredefaultpath;matlabrc
% addpath(genpath('D:\NirSeafile\Seafile\My Library\SODIVA\SODIVA_Main_Experiment'));
% addpath(genpath(fullfile('D:','Dokumente und Einstellungen', 'guest','Desktop','NeuroCode_SoDiva')));
% Each part requires input of subject's detials (at least ID)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  PHASE 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% IN THE BEHAVIORAL LAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (1) Starting the staircasing procedure
% In case the experiment was interrupted, one cane start in a later block
clear all
clc

Study = 'MainMRI';%Study can be BehPilot or MainMRI
Phase = 'first';

StartingBlock = 1;
Staircasing_MainScript(StartingBlock)

%% (2) Starting the value learning procedure
ValueMap_MainScript('Kashe')

%% (3) MiniBreak 

[display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
Screen('CloseAll');
Create_Stim_VBDM(Phase,display,General_Info); % choosing design. 

%% (4) Training on the main experiment
Main_Experiment_VBDM_Training(Study);

%% (5.1) 5 minutes OneD trials 
% meant to test the colors in the projector
[display,General_Info,ThumbsUp, Arrow, ClockSign] = set_settings(1.6); % input is stim duration
Screen('CloseAll');
[Sub_Folder,IDSub] = Get_Sub_Info_Mini(General_Info);
OneDMini_Experiment_VBDM(Phase,Study,display,General_Info);

%%%%%%%%%%%%%%%%%%% Dont forget to copy files ! ! %%%%%%%%%%%%%%%%%%%%%%%%%
%% COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY %%
%% COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY %%
%% COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY %%
%% COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY %%
%% COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY %%
%% COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY %%
%% COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY %%
%% COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY COPY %%