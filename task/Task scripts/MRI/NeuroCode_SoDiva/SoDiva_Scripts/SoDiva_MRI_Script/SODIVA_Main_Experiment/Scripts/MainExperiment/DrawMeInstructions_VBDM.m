function DrawMeInstructions_VBDM(display,Part,WaitTime, acclvl)
% This function has all the text screens
 % Part 101 - General instructions 1 of 2
 % Part 102 -  General instructions 2 of 2
 % Part 200 - Within Block Break 
 % Part 300 - Between Block Break
 % 1  Motion
 % 2 Color
 % 3 Motion Adjustment
 % 4 Color Adjustment
 % 400 Thank you 

    switch Part
        case 101 % Part 101 - General instructions 1 of 2
            DrawFormattedText(display.windowPtr, 'Du wirst gleich zwei Gruppen von sich bewegenden Punkten in verschiedenen Farben sehen.',                display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'Jede Gruppe wird sich in einer kohaerenten Richtung vor- und zurueckbewegen.',                           display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Vor jedem Durchgang wirst du entweder auf "Farbe" oder "Richtung" hingewiesen werden.'                   ,display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Deine Aufgabe ist es, die hoechste Belohnung laut der indizierten Dimension zu waehlen.',                 display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Nach jedem Durchgang wird ein Fixierungskreis gezeigt werden.',                                          display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst nur, wenn die sich bewegenden Punkte auf dem Bildschirm sind,',               display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'mithilfe der Pfeiltasten die richtige Antwort, links oder rechts, angeben. ',                             display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte antworte so schnell und genau wie moeglich.',                                                       display.text.linestart, display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                          'center', display.text.lines(13),display.text.color);
           
        
        case 102 % Part 102 -  General instructions 2 of 2
            DrawFormattedText(display.windowPtr, 'Die Hintergrunddimension, bzw. die Dimension, auf die du nicht hingewiesen wurdest, wird entweder nicht vorhanden sein,',     display.text.linestart, display.text.lines(2),display.text.color);
            DrawFormattedText(display.windowPtr, 'gleich sein oder unterschiedlich sein.',                                                                                  display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Ignoriere bitte in jedem Fall diejenige Dimension, auf die du nicht hingewiesen wurdest. ',                               display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn du beispielsweise auf "Richtung" hingewiesen wurdest, ignoriere die Farbe.',                                         display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Auch hier folgen mehrere Durchgaenge derselben Dimension hintereinander',                                         display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Nach einigen Durchgaengen mit dem Hinweis "Farbe", aendert sich die hingewiesene Dimension zu "Richtung", danach zu "Farbe" usw.',       display.text.linestart, display.text.lines(7),display.text.color);         
            %%%% ONE LINE BREAK %%%%
            DrawFormattedText(display.windowPtr, 'Auch hier ist es wichtig, dass du immer den Fixirungskreis fixierst, und auch hier werden deine Augenbewegungen aufgezeichnet.',             display.text.linestart, display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Dieser Teil des Experiments dauert ca. 60 Minuten',                                                                                           display.text.linestart, display.text.lines(11),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte bewege dich nur in den Pausen zwischen den Bloecken und versuche danach so genau wie moeglich in deine vorherige Position zuruekzukehren.',    display.text.linestart, display.text.lines(12),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                                            'center',           display.text.lines(13),display.text.color);
 
            
        case 103 % Part 103 the reward structure
            % getting Phase as input on acclvl
            DrawFormattedText(display.windowPtr, 'Nach deiner Antwort wirst du eine Rueckmeldung bekommen, welche Belohnung du ausgewaehlt hast.',           display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Die Belohnungen sind: 10, 30, 50 und 70 Kredite.',                                                         display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'In diesem Teil verdienst du deine Belohnungen  f�r das ganze Experiment.',                                 display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Das bedeutet, dass du mit 0 Euro startest, und du dann deine gesamte Bezahlung verdienst.',                         display.text.linestart, display.text.lines(6),display.text.color);

            switch acclvl % which is Phase actually
                    case 'BehPilot'
                          DrawFormattedText(display.windowPtr, 'Fuer jede 850 Kredite, die du gesammelt hast, bekommst du am Ende des Experiments einen Euro.',    display.text.linestart, display.text.lines(7),display.text.color);
                          DrawFormattedText(display.windowPtr, 'Wenn du jetzt alles richtig machst, kannst du insgesamt 35 Euro verdienen',                        display.text.linestart, display.text.lines(8),display.text.color);
                          DrawFormattedText(display.windowPtr, 'Die Mindestbezahlung fuer das gesamte Experiment ist auf jeden Fall 25 Euro.',                                      display.text.linestart, display.text.lines(9),display.text.color); 
                    case 'MainMRI'
                          DrawFormattedText(display.windowPtr, 'Fuer jede 600 Kredite, die du gesammelt hast, bekommst du am Ende des Experiments einen Euro.',   display.text.linestart, display.text.lines(7),display.text.color);
                          DrawFormattedText(display.windowPtr, 'Wenn du jetzt alles richtig machst, kannst du insgesamt 50 Euro verdienen',                       display.text.linestart, display.text.lines(8),display.text.color);
                          DrawFormattedText(display.windowPtr, 'Die Mindestbezahlung fuer das gesamte Experiment, inklusiv alle Teilen, ist 40 Euro.',                                     display.text.linestart, display.text.lines(9),display.text.color);     
            end 
               
            DrawFormattedText(display.windowPtr, 'Nach jedem Block bekommst du ein Feedback, das dir zeigt, wie viel du in diesem Block verdient hast',                       display.text.linestart, display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Es ist sehr unwahrscheinlich, dass du die Mindestzahlung nicht erreichen wuerdest.',                       display.text.linestart, display.text.lines(11),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn dies doch passieren wuerde, zahlen wir dir immer noch die Mindestzahlung.',                            display.text.linestart, display.text.lines(12),display.text.color);

            
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                                             'center', display.text.lines(13),display.text.color);

%             Rewards to participants: Instructions In each choice you can earn 10 30 50 70 points. 
%             The next part will determine your pay for the entire experiment all together. 
%             In this part, for every (mri = 500, behavioral = 630) points are worth a euro. 
%                 If you are wrong in all of your choices, you will earn the base amount for the experiemtn which is (mri = 30, beh = 25) euros. 
%                 If you are correct in all your choices, you will earn the maximum for the experiment which is (mri 45 beh 35) euros.
            
        case 104 % Training instructions
            DrawFormattedText(display.windowPtr, 'Bitte beachte: In diesem Teil aendert sich die Dauer der Darstellung des Fixationskreises.',                               'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Das bedeutet, dass es manchmal nur eine halbe Sekunde und manchmal sogar acht Sekunden dauern kann.',                      'center', display.text.lines(6),display.text.color);
            
            DrawFormattedText(display.windowPtr, 'Sobald alles verstanden ist, fangen wir mit einem kurzen Training an.',                                                   'center', display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Das Training dauert an, bis du in 80% der Duerchgaenge die hoechste Belohnung ausgewaehlt hast.',                         'center', display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um das Training zu beginnen.',                                                                    'center', display.text.lines(13),display.text.color);
      
       
        case 200 % Part 200 - end of block 1, 2, 3,
            % got Reward as inpput to vaiable: acclvl
            Feedback = strcat('In diesem Block hast du ',num2str(acclvl),' Euro verdient');
            
            DrawFormattedText(display.windowPtr, 'Block geschafft! Das hast du super gemacht.',                         'center', display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                                  'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst jetzt eine kurze Pause machen',                             'center', display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst dich kurz bewegen und ein bisschen dehnen.',                             'center', display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wichtig ist, dass du so genau wie moeglich in deine vorherige Position zurueckkehrst.',                             'center', display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Der naechste Block dauert ca. 14 Minuten',                                     'center', display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',             'center', display.text.lines(10),display.text.color);   
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um den naechsten Block zu starten.',        'center', display.text.lines(13),display.text.color);
             
  
        case 201 % Part 200 - end of block  4 5
            % got Reward as inpput to vaiable: acclvl
            Feedback = strcat('In diesem Block hast du ',num2str(acclvl),' Euro verdient');

            DrawFormattedText(display.windowPtr, 'Block geschafft! Das hast du super gemacht.',                         'center', display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                                  'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst jetzt eine kurze Pause machen',                             'center', display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst dich kurz bewegen und ein bisschen dehnen.',                             'center', display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wichtig ist, dass du so genau wie moeglich in deine vorherige Position zurueckkehrst.',                             'center', display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Der naechste Block dauert ca. 8 Minuten',                                     'center', display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',             'center', display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um den naechsten Block zu starten.',        'center', display.text.lines(13),display.text.color);

%         case 400 % begining of third or sixth block
%             DrawFormattedText(display.windowPtr, 'Jetzt wird es einen Block mit gemischten Durchgaengen geben.',                       display.text.linestart, display.text.lines(2),display.text.color);
%             DrawFormattedText(display.windowPtr, 'In jedem Durchgang wirst du darauf hingewiesen werden, ',                            display.text.linestart, display.text.lines(3),display.text.color);
%             DrawFormattedText(display.windowPtr, 'entweder eine bestimmte Farbe oder eine bestimmte Richtung zu identifizieren.',      display.text.linestart, display.text.lines(4),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Bitte denke daran immer den Fixierungskreis zu visieren.',                           display.text.linestart, display.text.lines(5),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzulesen.',                                        'center', display.text.lines(13),display.text.color);
% 
%        
        case 202 % For the MRI
            % got Reward as inpput to vaiable: acclvl
            Feedback = strcat('In diesem Block hast du ',num2str(acclvl),' Euro verdient');
            
            DrawFormattedText(display.windowPtr, 'Block geschafft! Das hast du super gemacht.',                         'center', display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                                  'center', display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Gleich melden wir uns bei dir',                             'center', display.text.lines(10),display.text.color);
        
        case 203 % end of OneDmini in behavioral
            Feedback = strcat('In diesem Block hast du ',num2str(acclvl),' Euro verdient');
            
            DrawFormattedText(display.windowPtr, 'Block geschafft! Das hast du super gemacht.',                         'center', display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                                  'center', display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Ende des Teils des Experiments',                             'center', display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte sag dem Versuchsleiter Bescheid',                             'center', display.text.lines(11),display.text.color);
            
            
        case 204 % end of OneDmini in behavioral in MRI
             % got Reward as inpput to vaiable: acclvl
            Feedback = strcat('In diesem Block hast du ',num2str(acclvl),' Euro verdient');
            
            DrawFormattedText(display.windowPtr, 'Block geschafft! Das hast du super gemacht.',                         'center', display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                                  'center', display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte bleibe noch ruhig liegen bis die Messung fertig ist.',                             'center', display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wir melden uns in ein paar Sekunden bei dir.',                             'center', display.text.lines(11),display.text.color);
       
        case 205 % pilot between blocks 
             % got Reward as inpput to vaiable: acclvl
            Feedback = strcat('In diesem Block hast du ',num2str(acclvl),' Euro verdient');

            DrawFormattedText(display.windowPtr, 'Block geschafft! Das hast du super gemacht.',                         'center', display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                                  'center', display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Du kannst jetzt eine kurze Pause machen',                             'center', display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Der naechste Block dauert ca. 4 Minuten',                                     'center', display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis anzuvisieren.',             'center', display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um den naechsten Block zu starten.',        'center', display.text.lines(11),display.text.color);

        case 206 % pilot ends
            Feedback = strcat('In diesem Block hast du ',num2str(acclvl),' Euro verdient');
            
            DrawFormattedText(display.windowPtr, 'Block geschafft! Das hast du super gemacht.',                         'center', display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, Feedback,                                                  'center', display.text.lines(9),display.text.color);
           DrawFormattedText(display.windowPtr, 'Ende des Experiments',                             'center', display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Bitte sag dem Versuchsleiter Bescheid',                             'center', display.text.lines(11),display.text.color);
        
        case 600 % 400 Thank you 
            
            DrawFormattedText(display.windowPtr, 'Du hast es geschafft! Vielen Dank fuer deine Teilnahme.',                      'center', display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Ende des Experiments',                                              'center', display.text.lines(9),display.text.color);
%            
%                 
%         case 1 % 1  Motion
%             DrawFormattedText(display.windowPtr, 'In den folgenden Durchgaengen geht es um die Bewegung der Punkte.'  ,  display.text.linestart, display.text.lines(2),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Du wirst auf bestimmte Richtungen hingewiesen werden.',                display.text.linestart, display.text.lines(3),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Du kannst die Farben ignorieren. Sie haben hier keine Bedeutung.',     display.text.linestart, display.text.lines(4),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis zu visieren.',             display.text.linestart, display.text.lines(5),display.text.color);         
%             DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                         'center', display.text.lines(13),display.text.color);
% 
%         case 2 % 2 Color
%             DrawFormattedText(display.windowPtr, 'In den folgenden Durchgaengen geht es um die Farbe der Punkte.',                  display.text.linestart, display.text.lines(2),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Du wirst auf eine bestimmte Farbe hingewiesen werden.',                                display.text.linestart, display.text.lines(3),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Du kannst die Bewegung ignorieren . Sie haben hier keine Bedeutung.',             display.text.linestart, display.text.lines(4),display.text.color);
%             DrawFormattedText(display.windowPtr, 'In diesem Teil wird sich manchmal die Schnelligkeit des Farbwechsels aendern.',   display.text.linestart, display.text.lines(6),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Bitte antworte so schnell und genau wie moeglich.',                               display.text.linestart, display.text.lines(7),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Bitte denke daran, immer den Fixierungskreis zu visieren.',                        display.text.linestart, display.text.lines(8),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                    'center', display.text.lines(13),display.text.color);
% 
% 
%         case 3 % 3 Motion Adjustment
%             DrawFormattedText(display.windowPtr, 'Super! Jetzt hast du noch einen Teil geschafft.',                                  display.text.linestart, display.text.lines(2),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Im naechsten Teil wird sich manchmal der Kohaerenzgrad der Bewegung aendern.',     display.text.linestart, display.text.lines(3),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Bitte antworte so schnell und genau wie moeglich.',                                display.text.linestart, display.text.lines(4),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Bitte denke daran immer den Fixierungskreis zu visieren.',                         display.text.linestart, display.text.lines(5),display.text.color);
%             DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                     'center', display.text.lines(13),display.text.color);
%                       
        case 1001
             acclvl = acclvl*100;
             Feedback = strcat('Du warst ',num2str(acclvl),'% korrekt');
             DrawFormattedText(display.windowPtr, Feedback,                                                  'center', display.text.lines(5),display.text.color);
             DrawFormattedText(display.windowPtr, 'Du hast das Training geschafft!',                                                  'center', display.text.lines(6),display.text.color);
             DrawFormattedText(display.windowPtr, 'Ende des Teils des Experiments',                                        'center', display.text.lines(7),display.text.color);
             DrawFormattedText(display.windowPtr, 'Bitte sag dem Versuchsleiter Bescheid',                                        'center', display.text.lines(8),display.text.color);
             
        case 1002
            acclvl = acclvl*100;
            Feedback = strcat('Du warst ',num2str(acclvl),'% korrekt');
            DrawFormattedText(display.windowPtr, Feedback,                                                  display.text.linestart, display.text.lines(5),display.text.color);
             DrawFormattedText(display.windowPtr, 'Du hast das Training noch nicht geschafft.',                                     display.text.linestart, display.text.lines(6),display.text.color);
             DrawFormattedText(display.windowPtr, 'Die Instruktionen werden dir nochmal angezeigt,',                                         display.text.linestart, display.text.lines(7),display.text.color);
             DrawFormattedText(display.windowPtr, 'danach wirst du das Training erneut machen.',                                         display.text.linestart, display.text.lines(8),display.text.color);
             DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                    'center', display.text.lines(11),display.text.color);
 
             
        case 999
             DrawFormattedText(display.windowPtr, 'Vorbereitung des Trainings',                                     'center', display.text.lines(6),display.text.color);
             DrawFormattedText(display.windowPtr, 'Dauert 5-30 Sekunden',                                         'center', display.text.lines(7),display.text.color);
      
        case 2001 % OneDMini BEH 
            % insert instructions here for the behavioral part so reward
            % amount, one D only, short part and reward is on! 
            DrawFormattedText(display.windowPtr, 'Der naechste Block dauert ca. 4 Minuten',                                        display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Vor jedem Durchgang wirst du entweder auf "Farbe" oder eine "Richtung" hingewiesen werden.',                        display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'In diesem Block haben die Punktegruppe in jedem Duerchgang entweder Farben (und zufaellige Bewegung) oder Bewegungsrichtung (und keine Farbe).',                        display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Deine Aufgabe ist es, die hoechste Belohnung laut der hingewiesenden Dimension zu waehlen.',                        display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wichtig ist: ab jetzt faengst du an, Kredite zu sammeln!',                        display.text.linestart, display.text.lines(9),display.text.color);
            switch acclvl
                case 'BEH'
            DrawFormattedText(display.windowPtr, 'Fuer jede 850 Kredite, die du gesammelt hast, bekommst du am Ende des Experiments einen Euro.',   display.text.linestart, display.text.lines(10),display.text.color);
                case 'MRI'
            DrawFormattedText(display.windowPtr, 'Fuer jede 600 Kredite, die du gesammelt hast, bekommst du am Ende des Experiments einen Euro.',   display.text.linestart, display.text.lines(10),display.text.color);
            end
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                    'center', display.text.lines(13),display.text.color);
            
        case 2002 % OneDMini MRI
            % insert instructions so MRI part (within the scanner) dont
            % freak out when its loud.. just like before, we have only OneD
            % trials. and reward is on! wir fangen gleich an
            DrawFormattedText(display.windowPtr, 'Der naechste Block ist genau wie der letzte Block und dauert ca. 4 Minuten',                                        display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Vor jedem Durchgang wirst du entweder auf "Farbe" oder "Richtung" hingewiesen werden.',                        display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'In diesem Block haben die Gruppe in jeder Duerchgang entweder Farben (und zufaellige Bewegung) oder Bewegungsrichtung (und keine Farbe).',                        display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'Deine Aufgabe ist, die hoechste Belohnung laut der hingewiesenen Dimension zu waehlen.',                        display.text.linestart, display.text.lines(8),display.text.color);
            switch acclvl
                case 'BEH'
            DrawFormattedText(display.windowPtr, 'Fuer jede 850 Kredite, die du gesammelt hast, bekommst du am Ende des Experiments einen Euro.',   display.text.linestart, display.text.lines(9),display.text.color);
                case 'MRI'
            DrawFormattedText(display.windowPtr, 'Fuer jede 600 Kredite, die du gesammelt hast, bekommst du am Ende des Experiments einen Euro.',   display.text.linestart, display.text.lines(9),display.text.color);
            end
            DrawFormattedText(display.windowPtr, 'Gleich melden wir uns bei dir.',                                    'center', display.text.lines(11),display.text.color);
        case 2003 % pilot
             % insert instructions here for the behavioral part so reward
            % amount, one D only, short part and reward is on! 
            DrawFormattedText(display.windowPtr, 'Der naechste Block dauert ca. 4 Minuten',                                                                                                       display.text.linestart, display.text.lines(3),display.text.color);
            DrawFormattedText(display.windowPtr, 'Vor jedem Durchgang wirst du entweder auf "Farbe" oder eine "Richtung" hingewiesen werden.',                                                       display.text.linestart, display.text.lines(4),display.text.color);
            DrawFormattedText(display.windowPtr, 'In diesem Block haben die Punktegruppe in jedem Duerchgang entweder Farben (und zufaellige Bewegung) oder Bewegungsrichtung (und keine Farbe).',          display.text.linestart, display.text.lines(5),display.text.color);
            DrawFormattedText(display.windowPtr, 'Deine Aufgabe ist es, die hoechste Belohnung laut der hingewiesenden Dimension zu waehlen.',                                                         display.text.linestart, display.text.lines(6),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wichtig ist: jetzt sammelst du die Kredite!',                                                                                               display.text.linestart, display.text.lines(7),display.text.color);
            DrawFormattedText(display.windowPtr, 'In diesem Teil verdienst du deine Belohnungen f�r das ganze Experiment.',                                                                     display.text.linestart, display.text.lines(8),display.text.color);
            DrawFormattedText(display.windowPtr, 'Fuer jede 450 Kredite, die du gesammelt hast, bekommst du am Ende des Experiments einen Euro.',                                            display.text.linestart, display.text.lines(9),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn du jetzt alles richtig machst, kannst du insgesamt (den ersten Teil eingeschlossen) 22 Euro verdienen',                                                display.text.linestart, display.text.lines(10),display.text.color);
            DrawFormattedText(display.windowPtr, 'Die Mindestbezahlung fuer das gesamte Experiment ist auf jeden Fall 22 Euro.',                                      display.text.linestart, display.text.lines(11),display.text.color); 
            DrawFormattedText(display.windowPtr, 'Es ist sehr unwahrscheinlich, dass du die Mindestzahlung nicht erreichen wuerdest.',                                     display.text.linestart, display.text.lines(12),display.text.color);
            DrawFormattedText(display.windowPtr, 'Wenn dies doch passieren wuerde, zahlen wir dir immer noch die Mindestzahlung.',                            display.text.linestart, display.text.lines(13),display.text.color);                     
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.',                                    'center', display.text.lines(15),display.text.color);
                   
    end 
    
    Screen('DrawingFinished', display.windowPtr); % tell PTB that stimulus drawing for this frame is finished
    
    Screen('Flip',display.windowPtr);
    
    if ~exist('WaitTime','var')
        WaitTime = 2;
    end 
    
    WaitSecs(WaitTime);
    

end 