function General_Info = Get_Sub_Info(General_Info)
Allgood = false;
while true && ~Allgood
    %% SELECT THE STUDY MODE:
    General_Info.studyMode = 'Staircasing Piloting'; % save selection of the study mode
    
    %% ENTER PARTICIPANT DETAILS:
    prompt = {'id','age','gender'}; % define the prompts
    defaultAns = {'99999','99999','m/f/anderes'}; % define the default answers
    General_Info.subjectInfo = inputdlg(prompt,'Subject Info',1,defaultAns); % create and show dialog box
    if isempty(General_Info.subjectInfo) % if cancel was pressed
        f = msgbox('Process aborted: Please start again!','Error','error'); % show error message
        uiwait(f);
        continue
    else
        General_Info.subjectInfo = cell2struct(General_Info.subjectInfo,prompt,1); % turn into structure array
    end
    
    % CHECK INPUTS (INTERNALLY):
    if numel(General_Info.subjectInfo.id) ~= 5 % if ID has not been correctly specified
        f = msgbox('ID must contain 5 digits!','Error','error');
        uiwait(f);
    elseif ~strcmp(General_Info.subjectInfo.gender,'m') && ~strcmp(General_Info.subjectInfo.gender,'f') && ~strcmp(General_Info.subjectInfo.gender,'anderes')
        f = msgbox('Gender must be either m or f','Error','error');
        uiwait(f);
    else % check if a folder already exist with the data of the participant
         % if not - make a directory 
        General_Info.SubData = fullfile(General_Info.pathData,General_Info.subjectInfo.id);
        if exist(General_Info.SubData,'dir')
            choice = questdlg([{'There is already a folder for this subject, continue anyway?'};...
            {''};...
            strcat('study mode',{': '},General_Info.studyMode);...
            strcat(transpose(prompt),{': '},struct2cell(General_Info.subjectInfo))], ...
            'Continue?', ...
            'Cancel','OK','OK');
                if strcmp(choice,'OK')
                else
                    f = msgbox('Process aborted: Please start again!','Error','error');
                    uiwait(f);
                    continue
                end
        else 
        mkdir(General_Info.SubData)
        end 
    end       
       
    % CHECK INPUTS (EXTERNALLY):
    choice = questdlg([{'Would you like to continue with this setup?'};...
        {''};...
        strcat('study mode',{': '},General_Info.studyMode);...
        strcat(transpose(prompt),{': '},struct2cell(General_Info.subjectInfo))], ...
        'Continue?', ...
        'Cancel','OK','OK');

    if strcmp(choice,'OK')
        General_Info.subjectInfo.age = str2double(General_Info.subjectInfo.age); % turn into double
        Allgood = true;
    else
        f = msgbox('Process aborted: Please start again!','Error','error');
        uiwait(f);
    end
    
    General_Info.SubDesigns = fullfile(General_Info.SubData,'SubjectsDesigns');
    General_Info.SubOutPut = fullfile(General_Info.SubData,'OutPut');
    General_Info.SubStimuli = fullfile(General_Info.SubData,'Stimuli');
    %General_Info.SubEyeTracker = fullfile(General_Info.SubData,'EyeTrackerData');
    General_Info.SubEyeTracker = fullfile(General_Info.pathETData,General_Info.subjectInfo.id,'EyeTrackerData');
    
    if ~exist(General_Info.SubDesigns,'dir')
        mkdir(General_Info.SubDesigns)
    end 

    if ~exist(General_Info.SubOutPut,'dir')
        mkdir(General_Info.SubOutPut)
    end
    
    if ~exist(General_Info.SubStimuli,'dir')
        mkdir(General_Info.SubStimuli)
    end 

    if ~exist(General_Info.SubEyeTracker,'dir')
        mkdir(General_Info.SubEyeTracker)
    end 
    
end  

end 

