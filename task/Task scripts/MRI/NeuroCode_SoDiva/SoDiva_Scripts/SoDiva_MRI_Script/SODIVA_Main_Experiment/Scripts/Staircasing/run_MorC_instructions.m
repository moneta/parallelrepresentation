function [display, Master] = run_MorC_instructions(display,Master,context, Arrow,ClockSign,ThumbsUp)

    display.isInstr = true;
    
    OldDuration = display.Max_Trial_duration;
    OldnFrames = display.nFramesTrial;
    OldTrialLength = display.TrialLength;
    
    display.Max_Trial_duration = 3; % this can be a bit longer, initial learning
    display.nFramesTrial = round(display.Max_Trial_duration*display.frameRate); %Calculate total number of temporal frames
    display.TrialLength = display.nFramesTrial/display.frameRate;

%% Making Instruction stimuli - presented at center and longer
% Stim for the center instructions
    for inst = 1:4
        switch context
            case 'Motion'
                CenterStimMotion{inst} =  Full_StimCreate_Staircasing(Master, 'Center', display, 'Motion', inst ,randi(4)); %Full_StimCreate2(Master.Colors{randi(4)}, Master.Motions{inst}, 'Center', display);
               % CenterStimMotion{inst} = Full_StimCreate2(Master.Colors{inst}, Master.Motions{inst}, 'Center', display);
            case 'Color'
                CenterStimMotion{inst} =  Full_StimCreate_Staircasing(Master, 'Center', display, 'Color', inst ,randi(4)); %Full_StimCreate2(Master.Colors{inst}, Master.Motions{randi(4)}, 'Center', display);
            case 'Motion1D'
                CenterStimMotion{inst} =  Full_StimCreate_Staircasing(Master, 'Center', display, 'Motion1D', inst ,randi(4)); %Full_StimCreate2(Master.Colors{inst}, Master.Motions{randi(4)}, 'Center', display);
            case 'Color1D'
                CenterStimMotion{inst} =  Full_StimCreate_Staircasing(Master, 'Center', display, 'Color1D', inst ,randi(4)); %Full_StimCreate2(Master.Colors{inst}, Master.Motions{randi(4)}, 'Center', display);
        end 
    
    end 

% Presenting the initial stimuli  
    switch context 
        case 'Motion'
            DrawFormattedText(display.windowPtr, 'Dies sind die vier Richtungen und ihre jeweiligen Hinweise, die du benoetigst, um die richtige Bewegung zu identifizieren', 'center', display.text.lines(1),display.text.color);    
            DrawFormattedText(display.windowPtr, 'Bedenke: Die Farbe ist in diesen Durchgaengen nicht von Bedeutung', 'center', display.text.lines(2),display.text.color);
            Screen('Flip',display.windowPtr);
            WaitSecs(6);

        case 'Color'

            DrawFormattedText(display.windowPtr, 'Dies sind die vier Farben und ihre jeweiligen Hinweise, die du benoetigst, um die richtige Farbe zu identifizieren', 'center', display.text.lines(1),display.text.color);    
            DrawFormattedText(display.windowPtr, 'Bedenke: Die Bewegungsrichtung ist in diesen Durchgaengen nicht von Bedeutung', 'center', display.text.lines(2),display.text.color);
            Screen('Flip',display.windowPtr);
            WaitSecs(6);
            case 'Motion1D'
            
            DrawFormattedText(display.windowPtr, 'Manchnal in den Bewegungsdurchgaengen wird es kein Farbe gezeigt', 'center', display.text.lines(1),display.text.color);    
            DrawFormattedText(display.windowPtr, 'Bedenke: Da die Farbe in diesen Durchgaengen nicht von Bedeutung ist, soll deine Antwort sich nicht veraendern', 'center', display.text.lines(2),display.text.color);
            Screen('Flip',display.windowPtr);
            WaitSecs(6);
            
            case 'Color1D'
            DrawFormattedText(display.windowPtr, 'Manchnal in den Farbedurchgaengen wird es keine Bewegungsrichtung gezeigt', 'center', display.text.lines(1),display.text.color);    
            DrawFormattedText(display.windowPtr, 'Bedenke: Da die Bewegungsrichtungen in diesen Durchgaengen nicht von Bedeutung sind, soll deine Antwort sich nicht veraendern', 'center', display.text.lines(2),display.text.color);
            Screen('Flip',display.windowPtr);
            WaitSecs(6);         
            
    end 

    
    switch context 
        
        case {'Motion', 'Color'}
            
             for InstMo = 1:4
           

                  for w = 1:CenterStimMotion{InstMo}.nFrames 
                switch context
                    case 'Motion'
                         DrawFormattedText(display.windowPtr, 'Dies sind die vier Richtungen und ihre jeweiligen Hinweise, die du benoetigst, um die richtige Bewegung zu identifizieren', 'center', display.text.lines(1),display.text.color);
                         DrawFormattedText(display.windowPtr, 'Bedenke: Die Farbe ist in diesen Durchgaengen nicht von Bedeutung', 'center', display.text.lines(2),display.text.color);
                         DrawFormattedText(display.windowPtr, 'So wird dir diese Richtung angezeigt:', 'center', display.text.lines(4),display.text.color);        

                         Screen('TextSize', display.windowPtr, 50); % select specific text font
                    case 'Color'
                         DrawFormattedText(display.windowPtr, 'Dies sind die vier Farben und ihre jeweiligen Hinweise, die du benoetigst, um die richtige Farbe zu identifizieren', 'center', display.text.lines(1),display.text.color);
                         DrawFormattedText(display.windowPtr, 'Bedenke: Die Bewegungsrichtung ist in diesen Durchgaengen nicht von Bedeutung', 'center', display.text.lines(2),display.text.color);
                         DrawFormattedText(display.windowPtr, 'So wird dir diese Farbe angezeigt:', 'center', display.text.lines(4),display.text.color);        
                end 
                
                 switch InstMo
                     case 1 
                         drawMyFixOrCue(display,InstMo,Master,context, Arrow);
                     case 2 
                         drawMyFixOrCue(display,InstMo,Master,context, Arrow);
                     case 3 
                         drawMyFixOrCue(display,InstMo,Master,context, Arrow);
                     case 4
                         drawMyFixOrCue(display,InstMo,Master,context, Arrow);
                 end 

             Screen('TextSize', display.windowPtr, display.text.size); % select specific text font
                                  
             DrawMeSomeDots(display,w,CenterStimMotion{InstMo});           
             Screen('Flip',display.windowPtr);
             

        
                  end

            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.', 'center', 'center',display.text.color);
            Screen('Flip',display.windowPtr);
            while ~KbCheck;  end
            
             end
            
        case {'Motion1D', 'Color1D'}
            
            InstMo1D = randi(4);
            
                for w = 1:CenterStimMotion{InstMo1D}.nFrames 
                switch context
                   case 'Motion1D'         
                        DrawFormattedText(display.windowPtr, 'Manchnal in den Bewegungsdurchgaengen wird es kein Farbe gezeigt', 'center', display.text.lines(1),display.text.color);    
                        DrawFormattedText(display.windowPtr, 'Bedenke: Da die Farbe in diesen Durchgaengen nicht von Bedeutung ist, soll deine Antwort sich nicht veraendern', 'center', display.text.lines(2),display.text.color);
                        DrawFormattedText(display.windowPtr, 'Hier ist ein Beispiel:', 'center', display.text.lines(4),display.text.color);        
                        
                        drawMyFixOrCue(display,InstMo1D,Master,'Motion', Arrow);

                  case 'Color1D'
                        DrawFormattedText(display.windowPtr, 'Manchnal in den Farbedurchgaengen wird es keine Bewegungsrichtung gezeigt', 'center', display.text.lines(1),display.text.color);    
                        DrawFormattedText(display.windowPtr, 'Bedenke: Da die Bewegungsrichtung in diesen Durchgaengen nicht von Bedeutung ist, soll deine Antwort sich nicht veraendern', 'center', display.text.lines(2),display.text.color);
                        DrawFormattedText(display.windowPtr, 'Hier ist ein Beispiel:', 'center', display.text.lines(4),display.text.color);        

                        drawMyFixOrCue(display,InstMo1D,Master,'Color', Arrow);

                end 
                
                 

             Screen('TextSize', display.windowPtr, display.text.size); % select specific text font
                                  
             DrawMeSomeDots(display,w,CenterStimMotion{InstMo1D});           
             Screen('Flip',display.windowPtr);
       
                end
                
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.', 'center', 'center',display.text.color);
            Screen('Flip',display.windowPtr);
            while ~KbCheck;  end
        
        
    end 
 
    if strcmp(context,'Feedback')
        Text4Feedback = {'Wenn du korrekt und innerhalb 1 Sekunde antwortest,'...
            'Wenn du korrekt antwortest, aber laenger als 1 Sekunde brauchst,'...
            'Wenn du falsch antwortest,'...
            'Wenn du garnicht antwortest,'...
            'bekommst du dieses Feedback:'};
        % 1, 5, 2, 3
        Feed = [1,5,2];
        for inst = 1:3
            DrawFormattedText(display.windowPtr,Text4Feedback{inst}, 'center', display.text.lines(2),display.text.color);    
            DrawFormattedText(display.windowPtr,Text4Feedback{5}, 'center', display.text.lines(3),display.text.color);
            Screen('Flip',display.windowPtr);
            WaitSecs(2);
            
            DrawFormattedText(display.windowPtr,Text4Feedback{inst}, 'center', display.text.lines(2),display.text.color);    
            DrawFormattedText(display.windowPtr,Text4Feedback{5}, 'center', display.text.lines(3),display.text.color);
            drawMyFeedback(display,Feed(inst),ThumbsUp,ClockSign)
            DrawFormattedText(display.windowPtr, 'Druecke irgendeine Taste, um weiterzumachen.', 'center', 'center',display.text.color);
            Screen('Flip',display.windowPtr);
            WaitSecs(2);
            while ~KbCheck;  end 
        end 
    end 
      
    
    display.isInstr = false;
    
    display.Max_Trial_duration = OldDuration;
    display.nFramesTrial = OldnFrames;
    display.TrialLength = OldTrialLength;

end 