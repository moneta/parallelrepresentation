function   drawMyFeedback(display,Reslt,ThumbsUp,ClockSign)

center = display.center;
    if display.isInstr
    center(2) = display.text.lines(5);
    end
    
    sz = display.feedback.sz;
    textureIndex=Screen('MakeTexture', display.windowPtr, ThumbsUp);
    textureIndexClock=Screen('MakeTexture', display.windowPtr, ClockSign);
    switch Reslt
        case 1 % Correct! 
        Screen('DrawTexture',display.windowPtr, textureIndex,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
        case 2 % Wrong
        Screen('DrawTexture',display.windowPtr, textureIndex,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)],180);
        case 3 % No wnaswer
        %Screen('TextSize', display.windowPtr, display.text.size); % select specific text font
        DrawFormattedText(display.windowPtr,'keine Antwort', 'center', 'center' ,display.feedback.color);
       % Screen('TextSize', display.windowPtr, display.text.size); % select specific text font    
        case 4 % Not a valid answer
      %  Screen('TextSize', display.windowPtr, 60); % select specific text font
        DrawFormattedText(display.windowPtr,'falsche Taste', 'center', 'center' ,display.feedback.color);
        %Screen('TextSize', display.windowPtr, display.text.size); % select specific text font
        case 5 % correct but under 1 second
        Screen('DrawTexture',display.windowPtr, textureIndexClock,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
    
    end 
    
%     if exist('WaitingTime','var')
%         WaitSecs(WaitingTime);
%     end 
end 

%% Playing around 
% 
% A = imread('C:\Users\moneta\Google Drive\Matlab\MPI\VBDM\SODIVA\Scripts\TU256.png');
% Ab = (A(:,:,:)==0);
% Aw = (A(:,:,:)==255);
% ThumbsUp=A;
% ThumbsUp(A==255)= 0; % white into black
% ThumbsUp(A==0)= 255 ; % making black into white
% clear A;
% 
% center = display.center;
%     if display.isInstr
%     center(2) = display.text.lines(5);
%     end
%     
%     sz = display.fixation.sz;
% sz = sz*3;
% textureIndex=Screen('MakeTexture', display.windowPtr, ThumbsUp);
% clear ThumbsUp
% %Screen('FillOval', display.windowPtr, display.fixation.color,[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
% Screen('DrawTexture',display.windowPtr, textureIndex,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)]);
% %Screen('FrameOval', display.windowPtr, display.bkColor,[-sz+center(1)-5,-sz+center(2)-5,sz+center(1)+5,sz+center(2)+5],5);
% Screen('Flip',display.windowPtr);
% WaitSecs(5)
% 
% drawMyFixOrCue(display,1,Master,'Fixation',5)% also flips
% 
% Screen('DrawTexture',display.windowPtr, textureIndex,[],[-sz+center(1),-sz+center(2),sz+center(1),sz+center(2)],180);
% Screen('FrameOval', display.windowPtr, display.bkColor,[-sz+center(1)-5,-sz+center(2)-5,sz+center(1)+5,sz+center(2)+5],5);
% Screen('Flip',display.windowPtr);
% WaitSecs(5)
% 
% Screen('CloseAll');
