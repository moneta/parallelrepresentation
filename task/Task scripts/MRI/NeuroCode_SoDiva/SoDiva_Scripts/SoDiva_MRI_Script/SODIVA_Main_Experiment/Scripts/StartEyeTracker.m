function [worked, General_Info] = StartEyeTracker(General_Info, display, ShouldCalibrate, ExpPart)
% This function starts eyetracking using Eyelink toolbox of Psychtoolbox
%
% input:
% ShouldCalibrate - logical. if its begining of block or not. if not - just
% generating a new file. 
% ExpPart - goes to the name of the file. 
%
% Output: logical, if all worked (if not, run again)
%         a new file name in the cell: General_Info.Eyetracker.files
%
% IMPORTANT: this needs to run when the PTB window is open. 



% STEP 1
% Open a graphics window on the main screen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This needs to run when the window is open
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% commandwindow;

worked = true; % will change if there is an error

% STEP 2
% Provide Eyelink with details about the graphics environment
% and perform some initializations. The information is returned
% in a structure that also contains useful defaults
% and control codes (e.g. tracker state bit and Eyelink key values)
el=EyelinkInitDefaults(display.windowPtr);

% Initialization of the connection with the Eyelink Gazetracker.
% exit program if this fails.
if ~EyelinkInit(display.EyeTracker.dummymode, 1)
    fprintf('Eyelink Init aborted.\n');
    worked = false;
    %return;
end


[v vs]=Eyelink('GetTrackerVersion');
fprintf('Running experiment on a ''%s'' tracker.\n', vs );

General_Info.Eyetracker.version = vs;

%addin a file to the list
General_Info.Eyetracker.files{numel(General_Info.Eyetracker.files)+1} = sprintf('%s%s.edf', General_Info.subjectInfo.id,ExpPart); %NB: must be number, not string!!!
fileName = General_Info.Eyetracker.files{end};
status = Eyelink('Openfile',fileName);
 if status~=0
        error('openfile error, status:  %.0f',status)
        worked = false; 
        %return;
 end
 
%configuration settings
% Eyelink('command','screen_pixel_coords = %ld %ld %ld %ld', 0, 0,display.resolution(1)-1,display.resolution(1)-1); %mX and mY are max x and y screen coordinates
% Eyelink('message', 'DISPLAY_COORDS %ld %ld %ld %ld', 0, 0, display.resolution(1)-1, display.resolution(1)-1); 
 
% make sure that we get gaze data from the Eyelink
% status = Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA,GAZERES,HREF,PUPIL,STATUS,INPUT');
status = Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA,PUPIL,STATUS,INPUT');

 if status~=0
        error('link_sample_data error, status:  %.0f',status)
        worked = false; 
       % return;
 end
 
if ShouldCalibrate
% Calibrate the eye tracker
 el.backgroundcolour = display.bkColor;
 el.calibrationtargetcolour = display.fixation.color;
 EyelinkUpdateDefaults(el);
OldCol = Screen('TextColor', display.windowPtr, display.fixation.color);

EyelinkDoTrackerSetup(el);

Screen('TextColor', display.windowPtr, OldCol);

end 


if ~worked 
    error('Something went wrong, deleting the file name, shutting down the Eyelink')
    General_Inf.Eyetracker.files{end} = {};
    Eyelink('Shutdown');
else 
    fprintf('\n Started EyeTracking, Good luck! \n');
end 

%% to put it later on:
% %STOP RECORDING
%  Eyelink('StopRecording');
% Eyelink('CloseFile');



% %% To transfer the file after closing it
% 
% try
%         fprintf('Receiving data file ''%s''\n', edfFile );
%         status=Eyelink('ReceiveFile');
%         if status > 0
%             fprintf('ReceiveFile status %d\n', status);
%         end
%         if 2==exist(edfFile, 'file')
%             fprintf('Data file ''%s'' can be found in ''%s''\n', edfFile, pwd );
%         end
% catch rdf
%         fprintf('Problem receiving data file ''%s''\n', edfFile );
%         rdf;
%    
% end
% 
% Eyelink('Shutdown');
% sca


end 