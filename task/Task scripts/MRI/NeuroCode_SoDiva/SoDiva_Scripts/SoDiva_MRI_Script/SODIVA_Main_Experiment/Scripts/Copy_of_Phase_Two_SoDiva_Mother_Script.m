% SoDiva Main Experiment Script
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PHASE 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IN THE MRI LAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
clc

addpath(genpath(fullfile('D:','Dokumente und Einstellungen', 'guest','Desktop','NeuroCode_SoDiva')));

EyetrackerRun = true; 
StartingPoint = {'S'};
 
SoDiva_MRI_Experiment(EyetrackerRun,StartingPoint{1})

   %% ENTER Study Phase:
%     prompt = {'Wann soll ich anfangen?'}; % define the prompts
%     fprintf('2')
%     defaultAns = {'S'}; % define the default answers
%     StartingPoint = inputdlg(prompt,'Starting State',1,defaultAns); % create and show dialog box
%     fprintf('1')
%     if isempty(StartingPoint) % if cancel was pressed
%        f = msgbox('Process aborted: Please start again!','Error','error'); % show error message
%        uiwait(f);
%     elseif ~strcmp(StartingPoint{1},'S') && ~strcmp(StartingPoint{1},'F') && ~strcmp(StartingPoint{1},'B1') && ~strcmp(StartingPoint{1},'B2') && ~strcmp(StartingPoint{1},'B3') && ~strcmp(StartingPoint{1},'B4') && ~strcmp(StartingPoint{1},'R') 
%         f = msgbox('Wrong input! make sure you use only uppercase letters! possible inputs: S, F, B1-4, R ','Error','error'); % show error message
%         uiwait(f);
%     else  
%         fprintf('\n Starting main script')
%     SoDiva_MRI_Experiment(EyetrackerRun,StartingPoint{1})
%     end 



