% Make All Designs:
%[fList,pList] = matlab.codetools.requiredFilesAndProducts('myFun.m');

%% StairCasingDesign:


% This script creates and saves designs according to these requierments:

% Creating MainDesign (saved as 'Staircasing_Design_XXX')
% A matrix of the entire staircasing design:
% columns: 
% 1 - Left main feature
% 2 - right main feature
% 3 - target feature
% 4 - FixControl (1 means yes) - FIX THIS! ! ! 
% 5 - Context (1-motion, 2-color)
% 6 - One Dim (1) or not (0)
% 7 - left background
% 8 - right background

% Also creating 'design' (saved as 'Design_Struct_XXX') - same as old
% design structure, just in case something is missing. 


% Rules on everything: 
% 1D/2D                 : not more than 3 in a row for each context
%                        AND: at the end, maximum 5 times in a row the same 1D/2D                         
% FixControl            : every 12 trials block, in one of the last 6 trials, alweays exist in the first 12 trial block (so they see it)
%                        also not given to the same choice within each part
% Same feature in a row: for each context, maximum choosing the same value twice in a row
%                        (important: they will then be splitted for context, so maximum posability is 4 times same value in a row, but wil be rare). 
% 

% (1) First Perceptual Measure
% This has 3*24 trials, changing context trial by trial:
% columns: 
% 1 - Left main feature
% 2 - right main feature
% 3 - target feature
% 4 - FixControl (1 means yes) - FIX THIS! ! ! 
% 5 - Context (1-motion, 2-color)
% 6 - One Dim (1) or not (0)


% (2) Perceptual Adjustment (including Design.background, Design.Bkgcount)
% design.OrderP2  - Getting anchor motion (after first adjustment)
% design.OrderP3  - Adjusting motion
% design.OrderP5  - Adjusting Color

% here we always have the same background (i.e. only passive trials) 
% what would be the background is taken from a matrix with 4 columns, one
% for each feature, made of randperm(4) one after the other, and indexd by
% a counter for each of the 4 features. 

% (3) last, full, Perceptual Measure
% here we have 6*24 trials, same as in (1). 

% Rules on everything: 
% 1D/2D                : not more than 3 in a row for each context
%                        AND: at the end, not more than 5 times in a row the same 1D/2D                         
% FixControl           : every 12 trials block, in one of the last 6 trials, alweays exist in the first 12 trial block (so they see it)
%                        also not given to the same choice within each part
% Same feature in a row: for each context, maximum choosing the same value twice in a row
%                        (important: they will then be splitted for context, so maximum posability is 4 times same value in a row, but wil be rare). 
% 
%addpath(genpath('D:\Google Drive\Git_SODIVA_scripts\Task\DesignMaking'))
%% Run STR?
RunSTR = true; 
DesNum = 1000;

%% Run VBDM?
RunVBDM = true; 
NumOfVBDM = 1000;
%% STR

if RunSTR
% Saving?
ShouldISave = true;
% Random
rng(sum(100*clock)); 
DesignsFolder = 'D:\Google Drive\Git_SODIVA_scripts\Task\DesignMaking\Staircasing and Main Task\Designs\Straircasing';
%TrainingFolder = 'C:\Users\moneta\Seafile_Folder\Seafile\My Library\SODIVA\SODIVA_ADJ_PILOTING\TrainingDesigns';
            Combi{1} = [ 1 2 1; 1 3 1; 1 4 1; 2 1 1; 3 1 1; 4 1 1];
            Combi{2} = [ 2 1 2; 2 3 2; 2 4 2; 1 2 2; 3 2 2; 4 2 2];
            Combi{3} = [ 3 1 3; 3 2 3; 3 4 3; 1 3 3; 2 3 3; 4 3 3];
            Combi{4} = [ 4 1 4; 4 2 4; 4 3 4; 1 4 4; 2 4 4; 3 4 4];
            AllChoices = [Combi{1}; Combi{2}; Combi{3}; Combi{4}]; % length 24
            Block_1_3_Trials = 3*24;                  
            P2 = 1*24; % 24 Getting Anchor from Motion
            P3 = 3*24; % 72 Adjusting Motion (5-6 adjustments per feature)
            P5 = 3*24; % 72 Adjusting Color (5-6 adjustments per feature)
            Block2_Trials = P2+P3+P5;
           

%% start making design
AllDesigns{DesNum} = {};
for des = 1:DesNum
            
            %% (3) last full PerMes
            
            % The idea here: 3x24 color and 3x24 motion
            % whithin we should have 1x24 1D (third)
            % first create 2x24 and 2x24 of each context
            % and another 1x24 and 1x24 of each context (1D)
            % then spread the 1D within the 2D
     
                % Set all rules for first 2 blocks:
                Rules.Max_1D2D_WithinCon = 3;
                Rules.Max_1D2D_OverAll = 5;
                Rules.Max_SameContext_inArow = 3;
                Rules.Max_SameFeature_WithinCon = 2; % meaning max same value 4 in a row
                %Rules.Max_SameFeature_WithinCon = 1; % meaning max same value 2 in a row
                Rules.Max_LeftRight = 4;

                %% Block 1
                [design.Part1, DesignB1] =  MixedBlockDesignMaking(Block_1_3_Trials, AllChoices, 1, Rules);
                
%                 fprintf('\n Finished Part 1');
                %% Block 2
                [design.Part2, DesignB2] =  MixedBlockDesignMaking_ADJ(AllChoices, P2, P3, P5, Rules);
%                 fprintf('\n Finished Part 2');
%                 %% Block 3
%                 [design.Part3, DesignB3] =  MixedBlockDesignMaking(Block_1_3_Trials, AllChoices, 1, Rules); % This is now same as block 1
% %                 fprintf('\n Finished Part 3');
% %                 datetime             
%                 
                %% Block 4 
                % setting rules for block 4: 

                Rules.Max_1D2D_WithinCon = 3;
                Rules.Max_1D2D_OverAll = 5;
                Rules.Max_SameContext_inArow = 3;
                Rules.Max_SameFeature_WithinCon_within1D2D = 1; % meaning max same value 2 in a row within a context, leading to between blockies max 4 :/
                Rules.Max_SameFeature_WithinCon = 1; %meaning max same value 1 in a row within a context, leading to between blockies max 2 !
                Rules.Max_LeftRight_withinContext_within1D2D = 2; 
                Rules.Max_LeftRight_withinContext = 6; % this means that if it has more, it is likely that once its in blocky it will be more than 4 in a row
                Rules.Max_LeftRight = 4; % this is once the blockies are joined together, this is time consuming. 
                % mas 3 1D/2D in a row,so within color max is L R R R R L - so 4
                % could be color: L R L R motion R R R L
                Rules.Max_Same_Condition_Row_withinCtxt = 3;
                Rules.Max_Same_Condition_Row = 3;
                
                
                Blocks =2;
                DesignBlock = SoDiva_Staircasing_LasBlock_NoPassive(Blocks,Rules);
                
       %         TestsWorked = RunTestsOnDesign_StaircasingLastB(DesignBlock,Rules,Blocks);

                DesignB3 = [DesignBlock{1};DesignBlock{2}];
          
                %% Merge designs
                %MainDesignOne = [DesignB1 ; DesignB2 ; DesignB3; DesignB4];
                MainDesignOne = [DesignB1 ; DesignB2 ; DesignB3];
                %save design
                AllDesigns{des} = MainDesignOne;

                MainDesign{1} = DesignB1;
                MainDesign{2} = DesignB2;
                MainDesign{3} = DesignB3;
%             MainDesign{4} = DesignB4;

            if ShouldISave
            DesName = fullfile(DesignsFolder,['Design_struct_',num2str(des),'.mat']);
            save(DesName, 'design');
            DesName = fullfile(DesignsFolder,['Staircasing_Design_',num2str(des),'.mat']);
            save(DesName, 'MainDesign');
            end 
            clear design MainDesign MainDesignOne
            
            fprintf('Saved Perc Adj Design %f \n',des)
end 
if ShouldISave
DesName = fullfile(DesignsFolder,'AllDesigns.mat');
save(DesName, 'AllDesigns');
end
fprintf('\n Starting the training designs \n');
for tra = 1:DesNum 
    
      %% Make design for the trainig 
      
    worked = false; 
    while ~worked
        MainDesign = AllDesigns{tra}(randperm(length(AllDesigns{tra}),24),:);
        % 12 motion , 12 color 
        ConBal = length(find(MainDesign(:,5)==1))==12 && length(find(MainDesign(:,5)==2))==12;
        if ~ConBal;continue;end
        % 3-4 motion 1D , 3-4 color 1D
        DimBalM = length(find(MainDesign((MainDesign(:,5)==1),6)==1))<5 && length(find(MainDesign((MainDesign(:,5)==1),6)==1))>2;
        DimBalC = length(find(MainDesign((MainDesign(:,5)==2),6)==1))<5 && length(find(MainDesign((MainDesign(:,5)==2),6)==1))>2;
        if ~(DimBalM&&DimBalC);continue;end
        % make sure there are 2 FixControl - CANCELED
        MainDesign(:,4) = 0;
%         MainDesign(randperm(24,2),4) = 1;
        % check that there is minimum 2 same context in a row but maximum 5
        ctxt = MainDesign(:,5)';
        Blockis = diff([0 find(diff(ctxt)) numel(ctxt)]);
        if min(Blockis)<=3 || max(Blockis)>=8
            BlockTest = false;
        else 
            BlockTest = true;
        end 

        if ConBal && DimBalM && DimBalC && BlockTest
            worked = true;
        end 
    end 
    
    
    if ShouldISave
      DesName = fullfile(DesignsFolder,['Training_Design_',num2str(tra),'.mat']);
      save(DesName, 'MainDesign');
      fprintf('Saved Training Design %f \n',tra)
    end  
      clear MainDesign

    
    
end 
% countDes = 0;
countDesNoFixi = 0;
% designSame = zeros(100,1);
designSameNoFixi = zeros(DesNum,1);
      for t = 1:DesNum
          DesignCenter = AllDesigns{t};
          DesignCenter(:,4:end) = [];
%           for j = 1:200
%               if DesignCenter == AllDesigns{j}
%                   countDes = countDes + 1; % should be 100 at the end
%                   designSame(t) = designSame(t) + 1;
%               end 
%           end 

          % not caring if fixcontrol was the same for now
          % making sure its not just the fixcontrol that is different
          for h = 1:DesNum
              DesignComp = AllDesigns{h};
              DesignComp(:,4:end) = [];
              if DesignCenter == DesignComp
                 countDesNoFixi = countDesNoFixi + 1; % should be 200 at the end
                 designSameNoFixi(h) = designSameNoFixi(h) + 1; % to know what design has doubles
              end 
          end 
                   
          
      end 
      
      if countDesNoFixi==DesNum
          fprintf('\n All Designs are different \n');
      else 
          fprintf('\n Need to check the designs, some are the same \n');
      end 
      clear AllDesigns DesignsFolder des    
end      
      



%% MAIN EXPERIMENT
if RunVBDM
DesNum = NumOfVBDM;
% Saving?
ShouldISave = true;
% Random
rng(sum(100*clock)); 
DesignsFolder = 'Straircasing';
DesignsFolder = 'D:\Google Drive\Git_SODIVA_scripts\Task\DesignMaking\Staircasing and Main Task\Designs\MainExperimentDesigns';
%TrainingFolder = 'C:\Users\moneta\Seafile_Folder\Seafile\My Library\SODIVA\SODIVA_ADJ_PILOTING\TrainingDesigns';
%% Set all rules:
Rules.Max_1D2D_WithinCon = 3;
Rules.Max_1D2D_OverAll = 5;
Rules.Max_SameContext_inArow = 3;
Rules.Max_SameFeature_WithinCon_within1D2D = 1; % meaning max same value 2 in a row within a context, leading to between blockies max 4 :/
Rules.Max_SameFeature_WithinCon = 1; %meaning max same value 1 in a row within a context, leading to between blockies max 2 !
Rules.Max_LeftRight_withinContext_within1D2D = 2; 
Rules.Max_LeftRight_withinContext = 6; % this means that if it has more, it is likely that once its in blocky it will be more than 4 in a row
Rules.Max_LeftRight = 4; % this is once the blockies are joined together, this is time consuming. 
% mas 3 1D/2D in a row,so within color max is L R R R R L - so 4
% could be color: L R L R motion R R R L
Rules.Max_Same_Condition_Row_withinCtxt = 3;
Rules.Max_Same_Condition_Row = 3;

RepPerD = 0;
AllDesigns{DesNum} = {};

RepCountFull = 1;
RepCountMini = 1;

datetime('now')
for des = 1:DesNum
    
% Blocks = 4;
% Design.DesignFullBlocks =  SoDivaDesigns_Making_Full_Blocks2(Blocks,Rules);
% Blocks = 2;
% Design.DesignMiniBlocks =  SoDivaDesigns_Making_Mini_Blocks2(Blocks,Rules);
%

Blocks = 4;
Design.DesignFullBlocks =  SoDivaDesigns_Making_VBDM_Blocks_NoPassive(Blocks,Rules);

% fprintf('\n time to generate 4 blocks: \n');
% Blocks = 2;
% Design.DesignMiniBlocks =  SoDivaDesigns_Making_Mini_Blocks2_TESTING(Blocks,Rules);
% fprintf('\n time to generate 2 blocks: \n');
% tic
% Blocks = 1;
% Design.DesignFullBlocks =  SoDivaDesigns_Making_VBDM_Blocks_NoPassive(Blocks,Rules);
% toc
FinalDesign{1} = Design.DesignFullBlocks{1};
FinalDesign{2} = Design.DesignFullBlocks{2};
FinalDesign{3} = Design.DesignFullBlocks{3};
FinalDesign{4} = Design.DesignFullBlocks{4};
% FinalDesign{5} = Design.DesignMiniBlocks{1};
% FinalDesign{6} = Design.DesignMiniBlocks{2};

% Make the congruent and incongruent more extreme
% FinalDesign = MakeCongIncongExt(FinalDesign);
% FinalDesign = MakeCongIncongExt_trial2(FinalDesign);

for repF = 1:4
    FullBlocks{RepCountFull} =  Design.DesignFullBlocks{repF};
    RepCountFull = RepCountFull+1;
end 

% for repM = 1:2
%     MiniBlocks{RepCountMini} =  Design.DesignMiniBlocks{repM};
%     RepCountMini = RepCountMini+1;
% end

AllDesigns{des} = FinalDesign;

TestsWorked = RunTestsOnDesign(FinalDesign,Rules,Blocks);

if TestsWorked
    if ShouldISave
       DesName = fullfile(DesignsFolder,['Design_SoDivaMain_',num2str(des),'.mat']);
       save(DesName, 'FinalDesign');
       fprintf('\n Saved Main Exp Design and Desing num %f passed all tests \n',des)
    end    
else 
    fprintf('\n\n\n\n DESIGN  %f DIDNT PASS THE TESTS! NOT SAVING MOVING TO NEXT \n DESIGNS DIDNT PASS THE TESTS! NOT SAVING MOVING TO NEXT  \n\n\n\n\n',des)
end 
    
clear Design FinalDesign

% fprintf('\n time to generate a design was: \n');

end 

if ShouldISave
DesName = fullfile(DesignsFolder,'AllDesigns.mat');
save(DesName, 'AllDesigns');
end

datetime('now')

% countDes = 0;
countDesNoFixiFull = 0;
% designSame = zeros(100,1);
designSameNoFixiFull = zeros(DesNum*4,1);
      for t = 1:DesNum*4
          DesignCenterFull = FullBlocks{t};
          DesignCenterFull(:,4:end) = [];

          for h = 1:DesNum*4
              DesignCompFull = FullBlocks{h};
              DesignCompFull(:,4:end) = [];
              if DesignCenterFull == DesignCompFull
                 countDesNoFixiFull = countDesNoFixiFull + 1; % should be 200 at the end
                 designSameNoFixiFull(h) = designSameNoFixiFull(h) + 1; % to know what design has doubles
              end 
          end 
                   
          
      end 
      
      if countDesNoFixiFull==DesNum*4
          fprintf('\n All Full Designs are different \n');
      else 
          fprintf('\n Need to check the Full designs, some are the same \n');
      end 
      
      
  
% % countDes = 0;
% countDesNoFixiMini = 0;
% % designSame = zeros(100,1);
% designSameNoFixiMini = zeros(DesNum*2,1);
%       for t = 1:DesNum*2
%           DesignCenterMini = MiniBlocks{t};
%           DesignCenterMini(:,4:end) = [];
% 
%           for h = 1:DesNum*2
%               DesignCompMini = MiniBlocks{h};
%               DesignCompMini(:,4:end) = [];
%               if DesignCenterMini == DesignCompMini
%                  countDesNoFixiMini = countDesNoFixiMini + 1; % should be 200 at the end
%                  designSameNoFixiMini(h) = designSameNoFixiMini(h) + 1; % to know what design has doubles
%               end 
%           end 
%                    
%           
%       end 
%       
%       if countDesNoFixiMini==DesNum*2
%           fprintf('\n All Mini Designs are different \n');
%       else 
%           fprintf('\n Need to check the Mini designs, some are the same \n');
%       end 
    
end    
      
      