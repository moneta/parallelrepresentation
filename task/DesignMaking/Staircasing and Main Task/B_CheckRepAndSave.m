%%% Check Designs Repetition and Save the ones that are relevant

load('D:\Google Drive\Git_SODIVA_scripts\Task\DesignMaking\Staircasing and Main Task\Designs\MainExperimentDesigns\AllDesigns.mat','AllDesigns')


% "In order to avoid repetition suppression, 
% we made sure that no more than 10% of the trials:
% within the 1D, within the congruent and writhing the incongruent,and
% passive 
% per each expected value (2-3-4) are proceeding the same EV."

Rep.EV = zeros(3,numel(AllDesigns));
Rep.Cong = zeros(3,numel(AllDesigns));
Rep.InCong = zeros(3,numel(AllDesigns));
Rep.OneD = zeros(3,numel(AllDesigns));
Rep.Passive = zeros(3,numel(AllDesigns));

Rep.PEV = zeros(3,numel(AllDesigns));
Rep.PCong = zeros(3,numel(AllDesigns));
Rep.PInCong = zeros(3,numel(AllDesigns));
Rep.POneD = zeros(3,numel(AllDesigns));
Rep.PPassive = zeros(3,numel(AllDesigns));

for i = 1:numel(AllDesigns)
     D = AllDesigns{i}{1};
    for b = 2:4
        D = [D ; AllDesigns{i}{b}];
    end 
    
    Chosen = zeros(length(D),2);
    UnChosen = zeros(length(D),2);
    cong = false(length(D),1);
    incong = false(length(D),1);
    oneD = false(length(D),1);
    Passive = false(length(D),1);
    
    for t = 1:length(D)
        Chosen(t,1) = max(D(t,1),D(t,2));
        UnChosen(t,1) = min(D(t,1),D(t,2));
        
        if D(t,3)==D(t,1) % chosen on left
            Chosen(t,2) = D(t,7);
            UnChosen(t,2) = D(t,8);
            if D(t,8)<D(t,7) % congruent
                cong(t) = true;
            elseif D(t,8)>D(t,7) % incongruent
                incong(t) = true;
            elseif D(t,8) == 0 % 1D
                oneD(t) = true;
            else % has to be passive
                Passive(t) = true;
            end 
        else % chosen on right
            Chosen(t,2) = D(t,8);
            UnChosen(t,2) = D(t,7);
            if D(t,8)>D(t,7) % congruent
                cong(t) = true;
            elseif D(t,8)<D(t,7) % incongruent
                incong(t) = true;
            elseif D(t,8) == 0 % 1D
                oneD(t) = true;
            else % has to be passive
                Passive(t) = true;
            end 
        end 
    end 
    
    Repev = false(length(D),1);
    for t = 2:length(D)
        if t~=109 && t~=217 && t~=325 % not the first trial in blocks 2 3 4 5, and starting from 2 becasue first trial cant repeat
            if D(t,3)==D(t-1,3)
            Repev(t)= true;
            end 
        end 
    end 
    
    for Val = 2:4
        Mval{Val} = D(:,3)==Val;
        % absolute numbers 
        Rep.EV(Val-1,i) = sum(Mval{Val} & Repev);
        Rep.Cong(Val-1,i) = sum(Mval{Val} & Repev & cong);
        Rep.InCong(Val-1,i) = sum(Mval{Val} & Repev & incong);
        Rep.OneD(Val-1,i) = sum(Mval{Val} & Repev & oneD);
        Rep.Passive(Val-1,i) = sum(Mval{Val} & Repev & Passive);
        
        % proportion of trials
        Rep.PEV(Val-1,i) = sum(Mval{Val} & Repev)/sum(Mval{Val}); % general EV, across all conditions
        Rep.PCong(Val-1,i) = sum(Mval{Val} & Repev & cong)/sum(Mval{Val} & cong); % Congruent trial proceeding any trial with the same EV 
        Rep.PInCong(Val-1,i) = sum(Mval{Val} & Repev & incong)/sum(Mval{Val} & incong);  % InCongruent trial proceeding any trial with the same EV 
        Rep.POneD(Val-1,i) = sum(Mval{Val} & Repev & oneD)/sum(Mval{Val} & oneD); %  % OneD trial proceeding any trial with the same EV 
        Rep.PPassive(Val-1,i) = sum(Mval{Val} & Repev & Passive)/sum(Mval{Val} & Passive); %  % OneD trial proceeding any trial with the same EV 
    end 
  
end 

fprintf('\n \n Total number of repetitions- or trials preceding the same EV can range from %.2f to %.2f',min(min(Rep.PEV)),max(max(Rep.PEV)));
fprintf('\n \n number of OneD trials preceding the same EV can range from %.2f to %.2f',min(min(Rep.POneD)),max(max(Rep.POneD)));
fprintf('\n \n number of Congruent trials preceding the same EV can range from %.2f to %.2f',min(min(Rep.PCong)),max(max(Rep.PCong)));
fprintf('\n \n number of Incongruent trials preceding the same EV can range from %.2f to %.2f ',min(min(Rep.PInCong)),max(max(Rep.PInCong)));
fprintf('\n \n number of Passive trials preceding the same EV can range from %.2f to %.2f \n \n ',min(min(Rep.PPassive)),max(max(Rep.PPassive)));

% Testing which one has less than 10% 
MaxP = 0.10; % maximum 10% repetition per condition: 1D, Cong, Incong of same EV - in the entire design
% Maxabs = 3; 
% GoodDeisngs = false(1,numel(AllDesigns));

% Across EV general 
%GoodDeisgns = (sum(Rep.PEV<=MaxP)==3) & (sum(Rep.PCong<=MaxP)==3) & (sum(Rep.PInCong<=MaxP)==3) & (sum(Rep.POneD<=MaxP)==3) & (sum(Rep.PPassive<=MaxP)==3);
GoodDeisgns = (sum(Rep.PEV<=MaxP)==3) & (sum(Rep.PCong<=MaxP)==3) & (sum(Rep.PInCong<=MaxP)==3) & (sum(Rep.POneD<=MaxP)==3);
fprintf('The number of designs answering the 10%% rule is %.0f \n', sum(GoodDeisgns));

% TestP = sum(sum(Rep.PInCong<=MaxP)==3 & sum(Rep.PCong<=MaxP)==3 & sum(Rep.POneD<=MaxP)==3);

%% Saving the designs for the experiment
count = 1;
OldAllDesigns = AllDesigns;
AllDesigns = {};
DesignsFolder = 'D:\Google Drive\Git_SODIVA_scripts\Task\DesignMaking\Staircasing and Main Task\Designs\FinalDesings_NoRep';

for s = 1:numel(OldAllDesigns)
    if GoodDeisgns(s)
       DesName = fullfile(DesignsFolder,['Design_SoDivaMain_',num2str(count),'.mat']);
       AllDesigns{count} = OldAllDesigns{s};
       FinalDesign = OldAllDesigns{s};
       save(DesName, 'FinalDesign');
       fprintf('\n Saved Main Exp Design num %.0f that is now number %.0f \n',s,count)
       count = count + 1;
    end  
end 

DesName = fullfile(DesignsFolder,'AllDesigns.mat');
save(DesName, 'AllDesigns');
