function [design, MainDesign] =  MixedBlockDesignMaking(Trials, AllChoices, blk, Rules)


% columns: 
% 1 - Left main feature
% 2 - right main feature
% 3 - target feature
% 4 - FixControl (1 means yes) - FIX THIS! ! ! 
% 5 - Context (1-motion, 2-color)
% 6 - One Dim (1) or not (0)
% 7 - left background
% 8 - right background

% Rules on everything: 
% 1D/2D                 : not more than 3 in a row for each context
%                        AND: at the end, maximum 5 times in a row the same 1D/2D                         
% FixControl            : every 12 trials block, in one of the last 6 trials, alweays exist in the first 12 trial block (so they see it)
%                        also not given to the same choice within each part
% Same feature in a row: for each context, maximum choosing the same value twice in a row
%                        (important: they will then be splitted for context, so maximum posability is 4 times same value in a row, but wil be rare). 
% 


            
                     
            % The idea here: 2/3 2d, 1/3 1D
            DimTest = false;
            LeftRightCheck = false;
            while ~DimTest || ~LeftRightCheck

                    design.PerceptualMeasure = zeros(Trials,6);

                          MotionWorked = false;
                          ColorWorked = false;

                          %% Make Motion trials                        
                          while ~MotionWorked 
                              % make 2D
                               if blk==1
                                 idm = easyrandi(1:24);
                              elseif blk==3
                                 idm = [easyrandi(1:24),easyrandi(1:24)];     
                               end                               
                               desiMotion = AllChoices(idm,:);
                               desiMotion(:,6) = 0; % setting 1D to 'no'
                               
                               desMot1Dsplit = false; 
                               while ~desMot1Dsplit
                                          % make 1D either 24 or 12
                                          desiMotion1D = AllChoices(easyrandi(1:24),:);                              
                                          if blk==1   
                                          desiMotion1D(13:24,:) = [];
                                          
                                          % desiMotion1D(:,3) should not have more than
                                          % 2 of each value
                                          desMot1Dsplit = sum(desiMotion1D(:,3)==1)<4 && sum(desiMotion1D(:,3)==2)<4 && sum(desiMotion1D(:,3)==3)<4 && sum(desiMotion1D(:,3)==4)<4;
                                          elseif blk==3
                                          % nothing changed  
                                          desMot1Dsplit = true;
                                          end

                               end                   
                              desiMotion1D(:,6) = 1; % setting 1D to 'yes'
                              
                         % MAKE desiDim FOR MOTION
                              DimWork = false;
                                    while ~DimWork

                                        desiDim = [];
                                            pcon(1:2)  = 1;
                                            pcon(3:6)  = 0;
                                            desiDim = pcon(easyrandi(1:6))';
                                        for j = 1:(Trials/(2*6)-1) %(All motion trials divided by 6)
                                            pcon = pcon(easyrandi(1:6));
                                            desiDim = [desiDim; pcon'];
                                        end 
                                         
                                        % check if there are 4 of the same in a row (meaning not more than 3)
                                        DimWork = CheckMeSomeSeq2(desiDim,Rules.Max_1D2D_WithinCon);
                                    end 
                         % INSERT FROM desiMotion and Motion1D into design.motion
                                     C1d = 1;
                                     C2d = 1;
                                     % INSERT FROM desiColor and Color1D into design.color
                                     for k = 1:length(desiDim)
                                       if desiDim(k) == 0
                                            design.Motion(k,1:6) = desiMotion(C2d,1:6);
                                            C2d = C2d+1;
                                       elseif desiDim(k) ==1
                                            design.Motion(k,1:6) = desiMotion1D(C1d,1:6);
                                            C1d = C1d+1;
                                       end 
                                     end 

                         % run tests on design.motion (not choosing the same
                         % feature 3 times in a row. max 2)
                         % will also be spread later even more with the context   
                         MotionWorked = CheckMeSomeSeq2(design.Motion(:,3),Rules.Max_SameFeature_WithinCon);
                 
                          end   
                %%          Make Color Trials                
                           while ~ColorWorked
                              % make 2D
                              if blk==1
                                 idc = easyrandi(1:24);
                              elseif blk==3
                                 idc = [easyrandi(1:24),easyrandi(1:24)];
                              end 
                                                                     
                             desiColor = AllChoices(idc,:);
                             desiColor(:,6)  = 0; % setting 1D to 'no'
 
                             desColDsplit = false; 
                               while ~desColDsplit
                                          % make 1D
                                          desiColor1D  = AllChoices(easyrandi(1:24),:);
                                          if blk==1   
                                          desiColor1D(13:24,:) = [];
                                          desColDsplit = sum(desiColor1D(:,3)==1)<4 && sum(desiColor1D(:,3)==2)<4 && sum(desiColor1D(:,3)==3)<4 && sum(desiColor1D(:,3)==4)<4;

                                          elseif blk==3
                                          % nothing changed  
                                          desColDsplit = true; 
                                          end         
                               end 
                              
                              desiColor1D(:,6)  = 1; % setting 1D to 'yes'

                              %make dimension vector for color, making sure not more
                              %than 3 in a row of 1D or 2D
                                 DimWork = false;
                                    while ~DimWork

                                        desiDim = [];
                                            pcon(1:2)  = 1;
                                            pcon(3:6)  = 0;
                                            desiDim = pcon(easyrandi(1:6))';
                                        for j = 1:(Trials/(2*6)-1) %(All color trials divided by 6)
                                            pcon = pcon(easyrandi(1:6));
                                            desiDim = [desiDim; pcon'];
                                        end 
                                        
                                        DimWork = CheckMeSomeSeq2(desiDim,Rules.Max_1D2D_WithinCon); 
                                    end 


                                 C1d = 1;
                                 C2d = 1;
                         % INSERT FROM desiColor and Color1D into design.color
                                 for k = 1:length(desiDim)
                                   if desiDim(k) == 0
                                        design.Color(k,1:6) = desiColor(C2d,1:6);
                                        C2d = C2d+1;
                                   elseif desiDim(k) ==1
                                        design.Color(k,1:6) = desiColor1D(C1d,1:6);
                                        C1d = C1d+1;
                                   end 
                                 end 

                         % run tests on design.motion and design.color (not choosing the same
                         % feature 3 times in a row? max 2)
                         % will also be spread later even more with the context
                                    ColorWorked = CheckMeSomeSeq2(design.Color(:,3),Rules.Max_SameFeature_WithinCon);
                           end
                          
                                                      
                             %% Add FixControl to both designs

                        % Add fixControl trials
                        design.Motion(1:Trials/2,4) = 0;

                        design.Color(1:Trials/2,4) = 0;


                          %% MAKE CONTEXT VECTOR
                          
                          % make the ocntext vector
                    design.Context =  MakeCtxt('PerMes');
                    % make 144 trials for context 1-motion, 2-color
                    % rule: no more than 3 of the same in a row
                    % same number of 1 and 2
%                         ContextWork = false;
%                         ContextWork2 = false;
%                         while ~ContextWork || ~ContextWork2
% 
%                             design.Context = [];
%                                 pcon(1:3)  = 1;
%                                 pcon(4:6)  = 2;
%                                 design.Context = pcon(easyrandi(1:6))';
%                             for j = 1:(Trials/6)-1
%                                 pcon = pcon(easyrandi(1:6));
%                                 design.Context = [design.Context; pcon'];
%                             end 
% 
%                             % check if there are 4 of the same in a row
%                             % (max 3)
%                            ContextWork = CheckMeSomeSeq2(design.Context,Rules.Max_SameContext_inArow);
%                            % same number of each context
%                            ContextWork2 = (numel(find(design.Context==1)))==(numel(find(design.Context==2)));
%                            
%                         end 


                      
                    %% setting the main design matrix:
                    
                    % The idea:
                    % based on the context, take the color or the motion
                    % we also need a count for that

                    %design.ContextCount = zeros(1,2); % column 1 = motion, Column 2 = color
                       % make full design 
                       Mcount = 1;
                       Ccount = 1;
                       for k = 1:length(design.Context)
                           if design.Context(k) ==1
                                design.PerceptualMeasure(k,1:6) = design.Motion(Mcount,1:6);
                                Mcount = Mcount+1;
                           elseif design.Context(k) ==2
                                design.PerceptualMeasure(k,1:6) = design.Color(Ccount,1:6);
                                Ccount = Ccount+1;
                           end 
                       end 
                          design.PerceptualMeasure(:,5) = design.Context;


                 %% Run last tests on all experiment
                 % maximum 5 times 1d/2d in a row 
                  DimTest = CheckMeSomeSeq2(design.PerceptualMeasure(:,6),Rules.Max_1D2D_OverAll);
                 % CHECK THERE ARE NO MORE THAN 4 RIGHT/LEFT IN A ROW
                  SideVec = zeros(length(design.PerceptualMeasure),1);
            
                    for s = 1:length(design.PerceptualMeasure)
                        if design.PerceptualMeasure(s,1) == design.PerceptualMeasure(s,3) % choose left
                            SideVec(s) = 1;
                        elseif design.PerceptualMeasure(s,2) == design.PerceptualMeasure(s,3) % choose Right
                            SideVec(s) = 2;
                        end 
                    end 
                  % Not more than choosing 4 times the same side (Max 4)
                  LeftRightCheck = CheckMeSomeSeq2(SideVec,Rules.Max_LeftRight);
                            
           
            end 

            %% Setting background 


        BkgOpt(:,:,1) = [ 1 2 ; 1 3 ; 1 4 ; 2 1 ; 3 1 ; 4 1 ; 2 3 ; 2 4 ; 3 2 ; 4 2 ; 3 4 ; 4 3 ; 1 1 ; 2 2 ; 3 3 ; 4 4];
        BkgOpt(:,1,2) = BkgOpt(:,2,1) ;
        BkgOpt(:,2,2) = BkgOpt(:,1,1) ;
        
        for feat = 1:4
            for Oth = 1:4 % whithin, the feat = Oth will not be used. but needs to be 16 in total, only 12 will be used. 
                for con = 1:2
                    idx = randperm(length(BkgOpt(:,:,1)));
                    BkgChoType(:,:,1:2,feat,Oth,con) = BkgOpt(idx,1:2,1:2); % BkgOpt(order, leftRight, 2OptionsOfChoiceType)
                end 
            end 
            
        end 
        
% BkgChoType(Trial, LeftStim/RightStim, ChoiceTypeOption(tgt on left or right), Feature 1-4, Other 1-3, Context 1-2)
% when target on the left, choose (:,:,1,choice type)
% when target on the left, choose (:,:,2,choice type)
% shuffle them synced (so same order, just a random one)
% 
% each time this choice type comes, take for the left one the left vector and
% for the right one the right. 
%     
% IMPORTANT: the counter goes by the choice type, so:
% C1 (1) vs C2 (2) OR C2(2) C1(1). But not both. 

% in blk 1 , the first 4 backgrounds will apear. 
% in blk 3, the first 8 backgrounds will apear. 

% think on passive control. 
% 
%             Combi{1} = [ 1 2 ; 1 3 ; 1 4 ; 1 1 ];
%             Combi{2} = [ 2 3 ; 2 4 ; 2 2 ];
%             Combi{3} = [ 3 4 ; 3 3 ];
%             Combi{4} = [ 4 4 ];
% 
%             AllBack2 = [Combi{1}; Combi{2}; Combi{3}; Combi{4}]; % length 24
%                     % This way we dont have control over which side gets
%                     % what, but we dont have repition : so pink square and
%                     % blue triangle will be compared once. 
% 
%                     
%                     % each feature is chosen within a block, maximal (in
%                     % the last one, 6*24/8 = 18 times. so we can have a
%                     % randiperm of 28 options from AllBackFeat to make sure
%                     % no repeats. 
%                     for T = 1:24
%                         AllBackFeat{T} = AllBack2(easyrandi(1:length(AllBack2)),:); 
%                     end 
                    % Implement the background into the design
                    
        MainDesign = design.PerceptualMeasure;
        MainDesign(:,7) = nan;
        MainDesign(:,8) = nan;
        % go through all trials
        % for 1D, background should be 0
        % for 2D, AllBackFeat{f}

        Bkgcount = ones(4,4,2); % (Target, Other option, Context) row for each feature, columns: 1 motion, 2 color
        
        for t = 1:Trials

           if MainDesign(t,6) == 1 % 1D
              MainDesign(t,7) = 0;
              MainDesign(t,8) = 0;

           elseif MainDesign(t,6) == 0 % 2D
           % BkgChoType(Trial, LeftStim/RightStim, ChoiceTypeOption(tgt on left or right), Feature 1-4, Other 1-3, Context 1-2)

            Context = MainDesign(t,5);
            % we have (Tgt, Other, Cont) - we know the choice type. 

            % when target on the left, choose (:,:,1,choice type)
            % when target on the left, choose (:,:,2,choice type)

            tgt = MainDesign(t,3);

                        if MainDesign(t,1)==tgt % Tgt on the left
                          Other = MainDesign(t,2);

                          Rowidx = Bkgcount(tgt,Other,Context);
                          Bkgcount(tgt,Other,Context) = Bkgcount(tgt,Other,Context) + 1;

                          MainDesign(t,7:8) = BkgChoType(Rowidx,1:2,1,tgt,Other,Context);

                        elseif MainDesign(t,2)==tgt % Tgt on the right
                          Other = MainDesign(t,1);

                          Rowidx = Bkgcount(tgt,Other,Context);
                          Bkgcount(tgt,Other,Context) = Bkgcount(tgt,Other,Context) + 1;

                          MainDesign(t,7:8) = BkgChoType(Rowidx,1:2,2,tgt,Other,Context);

                        end

           end 

        end 
                    
                    % For each time the target is 1, we take the one its compared to and pick up a random option from all possible 16 backgrounds. 
                    
                    
               
            %save design
          
end 



%% OLD 
%    for T = 1:4
%                         for O = 1:3
%                          AllBackFeat{T,O} = AllBack2(easyrandi(1:28),:);
%                         end 
%                     end 
%                     % Implement the background into the design
%                     MainDesign = design.PerceptualMeasure;
%                     MainDesign(:,7) = nan;
%                     MainDesign(:,8) = nan;
%                     % go through all trials
%                     % for 1D, background should be 0
%                     % for 2D, AllBackFeat{f}
%                     
%                     Bkgcount = ones(4,3,2); % (Target, Other option, Context) row for each feature, columns: 1 motion, 2 color
%                     for t = 1:Trials
%                         
%                        if MainDesign(t,6) == 1 % 1D
%                           MainDesign(t,7) = 0;
%                           MainDesign(t,8) = 0;
%                           
%                        elseif MainDesign(t,6) == 0 % 2D
%                          % for each time the target is 1, and the other option is one of the other 3 , we take from a list. 
%                          % meaning we have 4 targets and for each 3 options
%                          % 
%                         tgt = MainDesign(t,3);
%                         if  tgt==MainDesign(t,2)
%                             Other = MainDesign(t,1);
%                         elseif tgt==MainDesign(t,1)
%                             Other = MainDesign(t,2);
%                         end 
%                          MainDesign(t,7:8) = AllBackFeat{tgt,Other}(Bkgcount(tgt,Other,MainDesign(t,5)),1:2);
% %                           MainDesign(t,8) = AllBackFeat{tgt}(Bkgcount(tgt,MainDesign(t,5)),2);
%                          Bkgcount(tgt,Other,MainDesign(t,5))= Bkgcount(tgt,Other,MainDesign(t,5))+1;
%                        end 
%                        
%                     end 
   