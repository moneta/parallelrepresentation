function [design, MainDesign] =  MixedBlockDesignMaking_ADJ(AllChoices, P2, P3, P5, Rules)

% 1 - Left main feature
% 2 - right main feature
% 3 - target feature
% 4 - FixControl (1 means yes) - FIX THIS! ! ! 
% 5 - Context (1-motion, 2-color)
% 6 - One Dim (1) or not (0)
% 7 - left background
% 8 - right background

% Converting to a loop 

Part{1} = P2;
Part{2} = P3;
Part{3} = P5;

     
     for p = 1:numel(Part)
        FeatWorked = false;
        LeftRightCheck = false;
             while ~FeatWorked || ~LeftRightCheck

                idP{p} = [];
                
                for r = 1:Part{p}/24
                   idP{p} = [idP{p} ,easyrandi(1:24)];  
                end 
                
                design.Order{p} = AllChoices(idP{p},:);
                
                % not more than choosing same feature max 2 in a row (per
                % context)
                 FeatWorked = CheckMeSomeSeq(design.Order{p}(:,3),Rules.Max_SameFeature_WithinCon);

                % CHECK THERE ARE NO MORE THAN 4 RIGHT/LEFT IN A ROW
                    SideVec = zeros(length(design.Order{p}),1);

                    for s = 1:length(design.Order{p})
                        if design.Order{p}(s,1) == design.Order{p}(s,3) % choose left
                            SideVec(s) = 1;
                        elseif design.Order{p}(s,2) == design.Order{p}(s,3) % choose Right
                            SideVec(s) = 2;
                        end 
                    end 
                   % Not more than choosing 4 times the same side
                   % here its always same context so not splitting
                   LeftRightCheck = CheckMeSomeSeq(SideVec,Rules.Max_LeftRight);

             end 
         
         
         %% Fixation (added outside)                              
        design.Order{p}(1:Part{p},4)    = 0;
        
     end 
     
     
      
        %% Background, making syre there is no repeat

% 
% design.background = zeros(round(P2+P3+P5)/4,4); % creates too much, its fine 
% for i = 0:4:size(design.background,1)
%     design.background(i+1:i+4,1) = randperm(4)';
%     design.background(i+1:i+4,2) = randperm(4)';
%     design.background(i+1:i+4,3) = randperm(4)';
%     design.background(i+1:i+4,4) = randperm(4)';
% end 
% design.Bkgcount = ones(4,2); % row for each feature, columns: 1 motion, 2 color


% 1 - Left main feature
% 2 - right main feature
% 3 - target feature
% 4 - FixControl (1 means yes) - FIX THIS! ! ! 
% 5 - Context (1-motion, 2-color)
% 6 - One Dim (1) or not (0)
% 7 - left background
% 8 - right background

MainDesign = zeros(P2+P3+P5,8);

MainDesign(1:P2,1:4) = design.Order{1};
MainDesign(P2+1:P2+P3,1:4) = design.Order{2};
MainDesign(P2+1+P3:P2+P3+P5,1:4) = design.Order{3};

% mapping context, not randomized of course. 
MainDesign(1:P2,5) = 1;
MainDesign(P2+1:P2+P3,5) = 1;
MainDesign(P2+1+P3:P2+P3+P5,5) = 2;

% mapping all to be 2D (meaning not 1D)
MainDesign(:,6) = 0;



% mapping background

% all options:
 BkgOpt(:,:,1) = [ 1 2 ; 1 3 ; 1 4 ; 2 1 ; 3 1 ; 4 1 ; 2 3 ; 2 4 ; 3 2 ; 4 2 ; 3 4 ; 4 3 ; 1 1 ; 2 2 ; 3 3 ; 4 4];
        BkgOpt(:,1,2) = BkgOpt(:,2,1) ;
        BkgOpt(:,2,2) = BkgOpt(:,1,1) ;
        
        for feat = 1:4
            for Oth = 1:4 % whithin, the feat = Oth will not be used. but needs to be 16 in total, only 12 will be used. 
                for con = 1:2
                    idx = randperm(length(BkgOpt(:,:,1)));
                    BkgChoType(:,:,1:2,feat,Oth,con) = BkgOpt(idx,1:2,1:2); % BkgOpt(order, leftRight, 2OptionsOfChoiceType)
                end 
            end 
            
        end 
        
  MainDesign(:,7) = nan; % to double check myself
  MainDesign(:,8) = nan; % same
  
  Trials = length(MainDesign);
  
  Bkgcount = ones(4,4,2); % (Target, Other option, Context) row for each feature, columns: 1 motion, 2 color
                    for t = 1:Trials
                        
                      
                       % BkgChoType(Trial, LeftStim/RightStim, ChoiceTypeOption(tgt on left or right), Feature 1-4, Other 1-3, Context 1-2)
                        
                        Context = MainDesign(t,5);
                        % we have (Tgt, Other, Cont) - we know the choice type. 
                        
                        % when target on the left, choose (:,:,1,choice type)
                        % when target on the left, choose (:,:,2,choice type)
                        
                        tgt = MainDesign(t,3);

                                    if MainDesign(t,1)==tgt % Tgt on the left
                                      Other = MainDesign(t,2);

                                      Rowidx = Bkgcount(tgt,Other,Context);
                                      Bkgcount(tgt,Other,Context) = Bkgcount(tgt,Other,Context) + 1;

                                      MainDesign(t,7:8) = BkgChoType(Rowidx,1:2,1,tgt,Other,Context);

                                    elseif MainDesign(t,2)==tgt % Tgt on the right
                                      Other = MainDesign(t,1);

                                      Rowidx = Bkgcount(tgt,Other,Context);
                                      Bkgcount(tgt,Other,Context) = Bkgcount(tgt,Other,Context) + 1;

                                      MainDesign(t,7:8) = BkgChoType(Rowidx,1:2,2,tgt,Other,Context);

                                    end
                        
                      

                    end 

end











 