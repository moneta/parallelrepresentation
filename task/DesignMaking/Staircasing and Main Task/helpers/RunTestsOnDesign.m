function TestsWorked = RunTestsOnDesign(FinalDesign,Rules,Blocks)
% This function is testing the design to have enough of each trial type:
% 1. motion = color = 252
% 2. congruent = incongruent = 108
% 3. passive = 96
% 4. 1D = 192
% 5. 2D = 312
% 6. MainVal = 2 3 4 / each 168
% 7. Within Congruent and incongruent:
%     EVback = 2 3 4 / each 2x3x6=  36 (context x main val x blocks)
% 8. Within Passive
%     EVback = 1 2 3 4 / each 2x3x4 = 24 (context x main val x blocks)
% 9. for values 2-3-4, same amount of time target on left and right
% 10 sequences:
%         Rules.Max_1D2D_OverAll = 5;
% 11 side control
%         Rules.Max_LeftRight = 4;
    
% bring all blocks together
Design = FinalDesign{1};
for b = 2:Blocks
    Design = [Design ; FinalDesign{b}];
end  

% 1. motion = color = 252
idx.motion = Design(:,5) == 1;
idx.color = Design(:,5) == 2;
worked(1) = sum(idx.motion)==length(Design)/2 & sum(idx.color)==length(Design)/2;

% 2. congruent = incongruent = 108 is old. now we have 72/2 x 4 = 144
idx.Cong = (Design(:,1)>Design(:,2) & Design(:,7)>Design(:,8)) | (Design(:,1)<Design(:,2) & Design(:,7)<Design(:,8));
idx.InCong = (Design(:,1)<Design(:,2) & Design(:,7)>Design(:,8)) | (Design(:,1)>Design(:,2) & Design(:,7)<Design(:,8));
%idx.Pass =  Design(:,7)==Design(:,8) & Design(:,7)~=0;
worked(2) = sum(idx.Cong)==144 & sum(idx.InCong)==144;

% 3. passive = 96
worked(3) = true; %sum(idx.Pass)==96;

% 4. 1D = 192 old in 6 blocks, now we have 36 x 4 = 144
idx.OneD = Design(:,7)==0 & Design(:,8)==0;
idx.TwoD = ~idx.OneD;
worked(4) = sum(idx.OneD)==144;
% 5. 2D = 312 old, now  72 x 4
worked(5) = sum(idx.TwoD)==288;
% 6. MainVal = 2 3 4 / each 168
for val = 2:4
idx.TgtVal{val-1}=  Design(:,3) == val;
end
worked(6) = sum(idx.TgtVal{1})==144 & sum(idx.TgtVal{2})==144 & sum(idx.TgtVal{3})==144 ;

% 7. Within Congruent and incongruent:
%     EVback = 2 3 4 / each 2x3x6=  36 (context x main val x blocks)
for valB = 1:4
    idx.EVback{valB} = max(Design(:,7),Design(:,8))==valB;
end 
% new : 2 is 1, 3 is 2 and 4 is 3 all this X 2 X 3 X 4 (context x main val
% x blocks) = 24 / 48 / 72
workedCong = sum(idx.EVback{2} & idx.Cong)==24 & sum(idx.EVback{3} & idx.Cong)==48 & sum(idx.EVback{4} & idx.Cong)==72;
workedInCong = sum(idx.EVback{2} & idx.InCong)==24 & sum(idx.EVback{3} & idx.InCong)==48 & sum(idx.EVback{4} & idx.InCong)==72;
worked(7) = workedCong & workedInCong;
% the logic  is that within each main value, the back in incongruent can be
% 2 3 4 2 3 4 and for the second row 3 4 3 4 3 4 and lastly 4 4 4 4 4 4
% this is across blocks and all of this times 2 (motion color) and times 3
% (main val). 


% 8. Within Passive
%     EVback = 1 2 3 4 / each 2x3x4 = 24 (context x main val x blocks)
worked(8) = true; %sum(idx.EVback{1} & idx.Pass)==24 & sum(idx.EVback{2} & idx.Pass)==24 & sum(idx.EVback{3} & idx.Pass)==24 & sum(idx.EVback{4} & idx.Pass)==24;

% 9. for values 2-3-4, same amount of time target on left and right
worked(9) = sum(Design(:,1)==Design(:,3)) == sum(Design(:,2)==Design(:,3));
% 10 sequences:
%         Rules.Max_1D2D_OverAll = 5;
% 11 side control
%         Rules.Max_LeftRight = 4;
for b = 1:numel(FinalDesign)
workedSeq(b) = CheckMeSomeSeq2(FinalDesign{b}(:,6),Rules.Max_1D2D_OverAll);
workedSide(b) = CheckMeSomeSeq2((FinalDesign{b}(:,1)==FinalDesign{b}(:,3)),Rules.Max_LeftRight);
Congr = (FinalDesign{b}(:,1) > FinalDesign{b}(:,2)) & (FinalDesign{b}(:,7) > FinalDesign{b}(:,8)) | (FinalDesign{b}(:,1) < FinalDesign{b}(:,2)) & (FinalDesign{b}(:,7) < FinalDesign{b}(:,8));
InCongr = (FinalDesign{b}(:,1) < FinalDesign{b}(:,2)) & (FinalDesign{b}(:,7) > FinalDesign{b}(:,8)) | (FinalDesign{b}(:,1) > FinalDesign{b}(:,2)) & (FinalDesign{b}(:,7) < FinalDesign{b}(:,8));
CheckBcond = zeros(length(FinalDesign{b}),1);
CheckBcond(Congr) = 1;
CheckBcond(InCongr) = 2;
workedCondRep(b) = CheckMeSomeSeq2(CheckBcond,Rules.Max_Same_Condition_Row);
end 
worked(10) =  sum(workedSeq)==length(workedSeq);
worked(11) =  sum(workedSide)==length(workedSide);
worked(12) =  sum(workedCondRep)==length(workedCondRep);

%12 . Within Congruent and incongruent: Unexpected value (min)
%     EVback = 2 3 4 / each 2x3x6=  36 (context x main val x blocks)
for valB = 1:4
    idx.UnEVback{valB} = min(Design(:,7),Design(:,8))==valB;
end 
workedCong2 = sum(idx.UnEVback{1} & idx.Cong)==72 & sum(idx.UnEVback{2} & idx.Cong)==48 & sum(idx.UnEVback{3} & idx.Cong)==24;
workedInCong2 = sum(idx.UnEVback{1} & idx.InCong)==72 & sum(idx.UnEVback{2} & idx.InCong)==48 & sum(idx.UnEVback{3} & idx.InCong)==24;
worked(13) = workedCong2 & workedInCong2;



TestsWorked = sum(worked)==length(worked);











end 