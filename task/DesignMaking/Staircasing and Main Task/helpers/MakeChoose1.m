function Choose1Final = MakeChoose1(blocks)

% THIS FUNCTION ONLY WORKS WITH 2 BLOCKS! %

               %% Add Choose = 1 trials
                    Combi1 = [ 1 2 1; 2 1 1; 1 3 1; 3 1 1; 1 4 1; 4 1 1];
                    Combi1Bkg = [ 1 2 ; 1 3 ; 1 4 ; 2 1 ; 3 1 ; 4 1 ; 2 3 ; 2 4 ; 3 2 ; 4 2 ; 3 4 ; 4 3];
                    %% make the 'choose random' from each group less random               
% % % % % % % % %       
% % % % % % % % %                 % a is the random background (out of length(Combi1Bkg = 1:16)) that we take per length(Combi1 = 1:6)
% % % % % % % % %                 % in it, each coulumn of X rows need to have unique numbers
% % % % % % % % %                 % the Z dimension is the blocks:
% % % % % % % % %                 % in it, no row can repeat between blocks
% % % % % % % % %                 % (So each row is the 
% % % % % % % % %                 a = zeros(length(Combi1Bkg),length(Combi1));


Pas = false;
while ~Pas
            for d = 1:2 % going through context
                for i = 1:length(Combi1)
                        A = randperm(length(Combi1Bkg),2);
                        a(i,d,1) = A(1); % (i,ctxt,blo)
                        a(i,d,2) = A(2); % blo = 2
                end 
                    B1{d,1} = randperm(2,2);             
                    B2{d,1} = randperm(2,2)+2;
                    B3{d,1} = randperm(2,2)+4;
            end
for blo = 1:blocks
    for d = 1:2
                Choose1Final{blo}{d} = zeros(6,8);
                count = 0;
                for c = 1:length(Combi1)
                    for b = 1:length(Combi1Bkg)
                     idx = randperm(length(Combi1Bkg));   
                     Choose1All{d}(count+b,1:3) =Combi1(c,:);
                     Choose1All{d}(count+b,7:8) =Combi1Bkg(b,1:2);
                    end                      
                    count = count + length(Combi1Bkg);
                end 
               Choose1All{d}(:,5) = d;
                      % choose random 2 out of each group
                      count = 0;
                for c = 1:length(Combi1)
                    Choose1Final{blo}{d}(c,:) = Choose1All{d}(a(c,d,blo)+count,:);
                    count = count + length(Combi1Bkg); 
                end 
                    ChooseOneD{d}(1,:) = Choose1Final{blo}{d}(B1{d}(blo),:);
                    ChooseOneD{d}(2,:) = Choose1Final{blo}{d}(B2{d}(blo),:);
                    ChooseOneD{d}(3,:) = Choose1Final{blo}{d}(B3{d}(blo),:);

                    ChooseOneD{d}(:,6) = 1;
                    ChooseOneD{d}(:,7:8) = 0;

                    Choose1Final{blo}{d} = [Choose1Final{blo}{d} ; ChooseOneD{d}];
    end 
end                
            %% testing if it all worked, if not - again the randomized numbers. 
            countTest = 0;
            for test = 1:blocks
                if numel(find(Choose1Final{test}{d}(:,7)==Choose1Final{test}{d}(:,8)))==3 % 3 is the 1D trials, no passive here anymore
                  countTest = countTest + 1;
                end 
            end 
            
            if countTest == blocks
               Pas = true;
            end 
end


end 

