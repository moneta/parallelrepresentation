function Isit = CheckMeSomeSeq(Vec,Maxval)
% This funciton gets as input a vector of numbers (one column/row) and the
% maximum times a single value can appear in a row
%
% this function returns a logical True if there are not more than Maxval in
% a row, and false otherwise.
check = 0;
for i = 1:(length(Vec)-Maxval)
    
    inArow = [];
    
    for j = 1:(Maxval+1)
        inArow = [inArow , Vec(i+(j-1))];
    end 
    
    if numel(unique(inArow))>=2 % not a single one in a vector of Macval+1, could be more if its the Tgt Vector. Main point that its not 1. 
        check = check + 1;
    end 
end 

Isit = (check == (length(Vec)-Maxval));

% 
% N = diff([0 find(diff(Vec2)) numel(Vec2)])

end 