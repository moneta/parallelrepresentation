function Vec = MakeCtxt(Part,DimStart)
% make context vector 
% input: 
%   Part:     string indicating which part of the experiment we are
%           'PerMes' Perceptual measure, 72 trials : [ 4 4 4 4 4 5 5 6 ]x 2
%           'ValFull' full block of VBDM, 96 trials : [ 7 6 5 5 5 4 4 4 4 4 ]x 2
%           'ValMini' mini block of VBDM, 60 trials: [ 4 4 4 5 6 7 ]x 2
%           'Training' one of the training, 24 trials: [4 4 4] x 2
% 
%   DimStart: which context to start with:
%           1 for motion, 2 for color.
%           if doesnt exist, we generate randomly
% 
% output: 
%   Vec: vector of 1 for motion and 2 for color. 




switch Part
    case 'PerMes'
        TrialNum = 72;
        b = [4,4,4,4,4,5,5,6];
        bs = [easyrandi(b)',easyrandi(b)'];
    case 'ValFull'
        TrialNum = 96;
        b = [7 6 5 5 5 4 4 4 4 4];
        bs = [easyrandi(b)',easyrandi(b)'];
    case 'ValMini'
        TrialNum = 60;
        b = [4 4 4 5 6 7];
        bs = [easyrandi(b)',easyrandi(b)'];
    case 'Training'
        TrialNum = 24;
        b = [4 4 4];
        bs = [easyrandi(b)',easyrandi(b)'];
    case 'PreOne'
        TrialNum = 96+18;
        b = [4 4 4 4 4 4 5 5 5 5 6 7];
       % b = [6 5 5 5 4 4 4 3 3 3 3 3]; % will be 4 4 4 4 4 4 5 5 5 5 6 7 after insert choose 1. 
        bs = [easyrandi(b)',easyrandi(b)'];
    case 'ValFullNoPassive'
        TrialNum = 108;
        b = [7 6 6 5 5 5 4 4 4 4 4];
        bs = [easyrandi(b)',easyrandi(b)'];
    case 'PreOne_NoPassive'
        TrialNum = 108+18;
        b = [4 4 4 4 4 4 5 5 5 5 6 6 7]; % added another 6 to be 12 in total more trials
       % b = [6 5 5 5 4 4 4 3 3 3 3 3]; % will be 4 4 4 4 4 4 5 5 5 5 6 7 after insert choose 1. 
        bs = [easyrandi(b)',easyrandi(b)'];
end 
        
        
        
if ~exist('DimStart','var')
    if  rand>0.5 % should start with motion or color? 
        D = 1;
    else 
        D = 2;
    end 
else
    D = DimStart;
end 

% Vec = zeros(TrialNum,1);
% Vec(1:bs(,1
% 
% A = [1,2,3; 4,5,6; 7,8,9];
% B = [10,11,12; 13,14,15; 16,17,18];
% C = B(:,[1;1]*(1:size(B,2)));
% C(:,1:2:end) = A
    %% mapping the miniblocks into a vector of:
    % 1 is motion, 2 is color
%  TrialNum = 108;
%  b = [7 6 6 5 5 5 4 4 4 4 4];
%  bs = [easyrandi(b)',easyrandi(b)'];
% Vec = zeros(TrialNum,1);
% Vec2 = zeros(TrialNum,1);
% 
% tic
% bs2 = [0,cumsum(reshape(bs,1,[]))];
% if D == 1
%     D2(1:2:length(b)*2) = true;
% else 
%     D2(2:2:length(b)*2) = true;
% end 
% 
% for t2 = 1:length(b)*2-1
%     if D2(t2)
%         Vec((bs2(t2)+1):bs2(t2+1))= 1;
%     else
%         Vec((bs2(t2)+1):bs2(t2+1))= 2;
%     end 
% end 
% toc
% 
% bs_save = bs;

% tic
Vec = zeros(TrialNum,1);
count = [1,1];
    for t = 1:TrialNum
        Vec(t,1) = D;
        bs(count(D),D) =bs(count(D),D) - 1;
        if  bs(count(D),D) == 0 % no more block
            count(D) = count(D) + 1;
            if D == 1
                D = 2;
            elseif D == 2
                D = 1;
            end 
        end 

    end 
% toc
% 
% N = diff([0 find(diff(Vec')) numel(Vec')])
% N2 = diff([0 find(diff(Vec2')) numel(Vec2')])
% 
% [sort(N2);sort(N)]
% reshape(bs_save,1,[])
end 




% 
% change to this:
% 
% we decide given a block length (lets say 48 color trials in a VBDM block)
% how many miniblock of 4, 5 and 6 will there be? 
% Given that 48/mean of distribution (3.8) is aprox. 12: we will have 12 miniblocks of color. 
% for example: 6 times miniblock of 4, twice 5, twice 6 ---- only this needs to sum up to 48!!
% This process we need to do to all of the options of MegaBlock length. 
% 
% So crunch the numbers and think what should be what size. 
% 
% 
% 





% switch TrialNum
%     case 96 % VBDM fullblock
%     case 60 % VBDM miniblock
%     case 72 % STR perceptual measure
%     case 168 % STR ADJustment part
%     case 192 % STR postValMap
% end 
% for each block we need a number of after which trial it will switch? 
% 3: 50%
% 4: 25%
% 5: 12.5%
% 6: 12.5%
% how many blocks within this block? 
% set N random in a range, until it makes sense?
% the mean of block length is around 3.86-3.87 and std is 1.05
% so to make the nember of blocks in each MegaBlock not more/less than
% 1std. from one another, we can do: Also make sure even numbers
% 
% 
% CtxtTrialNum = TrialNum/2;
% MinL = floor(CtxtTrialNum/(3.8677+0.5));
%         if mod(MinL,2)==1
%             MinL = MinL+1;
%         end
%         
%     MaxL = ceil(CtxtTrialNum/(3.8677-0.5));
%         if mod(MaxL,2)==1
%             MaxL = MaxL-1;
%         end
%         
% N = randsample([MinL:2:MaxL],1); % outside of loop to have same number of miniblocks in both contexts        
% for d = 1:2
% worked = false;
%         while ~worked % mega fast. 
%         Len{d} = randsample([3,4,5,6],N,true,[0.5,0.25,0.125,0.125]);
%         
%         % check that the blocks of 3 are not too often (they should be 50% so maybe between 40 and 60?
%         % check at least one 5 and two 4 exist
%             if sum(Len{d})==CtxtTrialNum && sum(Len{d}==3)/length(Len{d}) > 0.4 && sum(Len{d}==3)/length(Len{d}) <0.6  && sum(Len{d}==4)>=1 && sum(Len{d}==5)>=1 
%                     worked = true; 
%             end
%                C = C + 1;
%  
%         end
% end 


