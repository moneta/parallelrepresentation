function [DesignBlock] =  SoDivaDesigns_Making_VBDM_Blocks_NoPassive(Blocks,Rules)

%%% THIS CODE IS BEST WITH EVEN NUMBER OF BLOCKS %%%
% because otherwise it chooses more left or more right and not balanced. 

 % columns: 
            % 1 - Left main feature
            % 2 - right main feature
            % 3 - target feature
            % 4 - FixControl (1 means yes) - FIX THIS! ! ! 
            % 5 - Context (1-motion, 2-color)
            % 6 - One Dim (1) or not (0)
            % 7 - left background
            % 8 - right background
        Trials = 108;
        %% Make AllChoices/AllChoices1D
        % 3 main dimension trial
        MainDim = [1 2 2; 2 3 3; 3 4 4]; % 2 1 2; 3 2 3; 4 3 4];
        % 10 background conditions
       % BkgDim = [1 2; 2 3; 3 4; 2 1; 3 2; 4 3; 1 1; 2 2; 3 3; 4 4];
        % 12 backgrounds: 6 congruent then 6 incongruent
       BkgDim = [1 2; 1 3; 1 4;...
                  2 3; 2 4;...
                  3 4;
                  2 1; 3 1; 4 1;...
                  3 2; 4 2;...
                  4 3];

        % RightSide = rand(3,10)>0.5; % Will the target be on the right side? Starts with random assignment for each option
        % Make all combinations twice, once for each dimenstion 
        AllChoices = zeros(36,8);
        
        AllChoices1D = zeros(3,8);
        AllChoices1D(1:length(MainDim),1:3) = MainDim;
        Temp1D = repmat(AllChoices1D,6,1);
        AllOneD = Temp1D;
        AllOneD(1:9,1) = Temp1D(1:9,2);
        AllOneD(1:9,2) = Temp1D(1:9,1);
        AllOneD(1:9,3) = Temp1D(1:9,3);
        
        
        count = 1;
        for i = 1:length(MainDim)
            AllChoices(count:count+length(BkgDim)-1,1) = MainDim(i,1);
            AllChoices(count:count+length(BkgDim)-1,2) = MainDim(i,2);
            AllChoices(count:count+length(BkgDim)-1,3) = MainDim(i,3);
                for b = 1:length(BkgDim)
                    AllChoices(count + b-1 ,7) = BkgDim(b,1);
                    AllChoices(count + b-1 ,8) = BkgDim(b,2);
                end 
            count = count + (length(BkgDim));
        end       
        % this side balance is based on hand written computation
       % Temp2D = logical([1; 0; 1; 1; 0; 0; 0; 1; 1; 0; 1; 0; 1; 0; 0; 1; 1; 1; 0; 0; 0; 1; 0; 1; 1; 0; 0; 0; 1; 1]);
        % based on writing R is 1. 
       Temp2D = logical([1 0 0 1 0 1 0 1 0 1 0 1 0 1 1 0 1 0 0 1 0 1 0 1 1 0 0 1 1 0 1 0 1 0 1 0]');
        AllChoicesDim{1} = zeros(36,8);
        AllChoicesDim{2} = zeros(36,8);

if rand>0.5
    DimStart = 1;
else 
    DimStart = 2;
end 
      
        
for block = 1:Blocks
 %   tic
    %% change DimStart every beginning of block
    if DimStart == 1
    DimStart = 2;
    else 
    DimStart = 1;
    end   
    
    
    %% Making DesignBlock{block}
        
          
        % setting sides for 2D
        % every second block Temp is switched. 
        % if temp says so, we need to change the motion and not color. 
        % otherwise we change the color and not motion 
        switch block
            case {1,3}
                Temp2D = ~Temp2D;
            case {2,4}
        end 
        for t = 1:length(Temp2D)
            if Temp2D(t)
                % motion 
                AllChoicesDim{1}(t,1) = AllChoices(t,2);
                AllChoicesDim{1}(t,2) = AllChoices(t,1);
                AllChoicesDim{1}(t,3:6) = AllChoices(t,3:6);
                AllChoicesDim{1}(t,7) = AllChoices(t,8);
                AllChoicesDim{1}(t,8) = AllChoices(t,7);
                %color
                AllChoicesDim{2}(t,:) = AllChoices(t,:);
            else
                % color 
                AllChoicesDim{2}(t,1) = AllChoices(t,2);
                AllChoicesDim{2}(t,2) = AllChoices(t,1);
                AllChoicesDim{2}(t,3:6) = AllChoices(t,3:6);
                AllChoicesDim{2}(t,7) = AllChoices(t,8);
                AllChoicesDim{2}(t,8) = AllChoices(t,7);
                % motion
                AllChoicesDim{1}(t,:) = AllChoices(t,:);
            end   
         end 
        
      % left right 
       LeftRightCheck = false;
       while ~LeftRightCheck 
           % OneD 2D check
        DimTest = false;
        while ~DimTest
            DesignBlock{block} = zeros(Trials,8);
            DesignDim{1}       = zeros(Trials/2,8); % Motion
            DesignDim{2}       = zeros(Trials/2,8); % Color
            %% making trials:
                for d = 1:2 
                    DesignDim{d}       = zeros(Trials/2,8); % Motion
                    CheckDim = false;
                    
                    while ~CheckDim
                          % Making 2D trials 
                          CheckDim2 = false;
                          while ~CheckDim2
                              EVtest = false;
                              while ~EVtest
                                  EVidx = zeros(1,36);
                                  for newEV = 1:12
                                  idxV234 = easyrandi(1:3);
                                  st = (1+(newEV-1)*3);
                                  EVidx(st:st+2) = idxV234(1:3);
                                  end 
                                  EVtest = CheckMeSomeSeq2(EVidx,Rules.Max_SameFeature_WithinCon_within1D2D);
                              end 
                               % randimize within each main val and insert one
                               % by one 
                               V2 = reshape([easyrandi(1:6);easyrandi(7:12)],1,[]);
                               V3 = reshape([easyrandi(13:18);easyrandi(19:24)],1,[]);
                               V4 = reshape([easyrandi(25:30);easyrandi(31:36)],1,[]);
                               %V234 = [easyrandi(1:12)', easyrandi(13:24)',easyrandi(25:36)'];
                               V234 = [V2',V3',V4'];
                             DesiDim{d}  = zeros(size(AllChoicesDim{d}));
                             cv123 = [1,1,1];
                             for dsd = 1:36
                                 valtype = EVidx(dsd);
                                 DesiDim{d}(dsd,:) = AllChoicesDim{d}(V234(cv123(valtype), valtype),:);
                                 cv123(valtype) = cv123(valtype) + 1;
                             end 
                             % DesiDim{d} = AllChoicesDim{d}(easyrandi(1:length(AllChoices)),:);
                             % sanity check
                             CheckDim_OneDTwoD = CheckMeSomeSeq2(DesiDim{d}(:,3),Rules.Max_SameFeature_WithinCon_within1D2D);
                              % check cingruent in a row 
                              % adding 1 becasue then its 1 and 2 and im
                              % not sure it works with 0? 
                             CheckDim_Condition = CheckMeSomeSeq2(double((DesiDim{d}(:,1) > DesiDim{d}(:,2)) & (DesiDim{d}(:,7) > DesiDim{d}(:,8)) | (DesiDim{d}(:,1) < DesiDim{d}(:,2)) & (DesiDim{d}(:,7) < DesiDim{d}(:,8))),Rules.Max_Same_Condition_Row_withinCtxt);
                              
                              % could already look if we have 6 times
                              % choose left in a row, meaning with max 2 2d
                              % in a row we have high risk for 5 same in a
                              % row same side. 
                              CheckDim_Side =  CheckMeSomeSeq2(double(DesiDim{d}(:,1)==DesiDim{d}(:,3)),6);
                              CheckDim2 = CheckDim_OneDTwoD  & CheckDim_Side & CheckDim_Condition;
                          end
                         
                          % Making 1D trials   
                          CheckDim3 = false;
                          while ~CheckDim3
                              DesiDimOneD{d} = AllOneD(easyrandi(1:length(AllOneD)),:);
                              CheckDim3 = CheckMeSomeSeq2(DesiDimOneD{d}(:,3),Rules.Max_SameFeature_WithinCon_within1D2D);
                          end                         
                         % Make dimension vector for both color and motion
                          DimWork(d) = false;
                          % Since its no longer 1/3 and 2/3 - we need to do it in a different way. 
                           while ~DimWork(d)
                                  DimVec{d} = [];
                                  % we have ratio of 1d:2d of
                                  % 18:36 meaning 1:2

                                  pcon = [];
                                  pcon(1)  = 1; % 1D
                                  pcon(2:3)  = 0; % 2D
                                  DimVec{d} = pcon(easyrandi(1:3))';
                                    for j = 1:((length(DesiDimOneD{d})+length(DesiDim{d}))/length(pcon)-1) %(All motion trials divided by lengthpcon)
                                        pcon = pcon(easyrandi(1:3));
                                        DimVec{d} = [DimVec{d}; pcon'];
                                    end 
                                    % check if there are 4 of the same in a row (meaning not more than 3)
                                    DimWork(d) = CheckMeSomeSeq2(DimVec{d},Rules.Max_1D2D_WithinCon);
                            end 

                        % combining the 1d and 2 d to a single dimension specific design
                         C1d = 1;
                         C2d = 1;
                         % INSERT FROM desiColor and Color1D into design.color
                         TwoD = DesiDim{d};
                         OneD = DesiDimOneD{d};
                         
                         for k = 1:length(DimVec{d})
                             if k == 1 % first one normal
                                   if DimVec{d}(k) == 0 % 2D
                                        DesignDim{d}(k,1:8) = TwoD(C2d,1:8);
%                                                     C2d = C2d+1; keeping it 1
                                        TwoD(C2d,:) = []; % deleteing
                                   elseif DimVec{d}(k) ==1 % 1D 
                                        DesignDim{d}(k,1:8) = OneD(C1d,1:8);
%                                                     C1d = C1d+1; keeping it 1
                                        OneD(C1d,:) = []; % deleteing
                                   end
                             else % now complicated counting 
                                 % starting to look from the first location
                                 % in 1D and 2D
                                     C2d = 1; % starting from the first
                                     C1d = 1; % starting from the first     
                                 if DimVec{d}(k) == 0 % is it 2D?
                                     Check2d = false;
                                     while ~Check2d
                                         % if we started and we are already after the end of C2d, start all over again
                                         if C2d > size(TwoD,1)
                                             break % inner while loop
                                         end 
                                         if TwoD(C2d,3)==DesignDim{d}(k-1,3)
                                             % its a
                                             % repetition, skip
                                             C2d = C2d+1;
                                         else % not a repetition. all good
                                             DesignDim{d}(k,1:8) = TwoD(C2d,1:8);
                                             TwoD(C2d,:) = []; % deleteing
                                             Check2d = true;
                                         end 
                                     end 
                                   if ~Check2d  % meaning we had to break
                                       break % the k loop
                                   end

                                 elseif DimVec{d}(k) ==1 % 1D

                                     Check1d = false;
                                     while ~Check1d
                                         % if we started and we are already after the end of C2d, start all over again
                                         if C1d > size(OneD,1)
                                             break
                                         end 
                                         if OneD(C1d,3)==DesignDim{d}(k-1,3)
                                             % its a
                                             % repetition, skip
                                             C1d = C1d+1;
                                         else % not a repetition. all good
                                             DesignDim{d}(k,1:8) = OneD(C1d,1:8);
                                             OneD(C1d,:) = []; % deleteing
                                             Check1d = true;
                                         end 
                                     end 
                                       if ~Check1d  % meaning we had to break
                                           break % the k loop
                                       end
                                 end % finished if statement of 1/2 D per k
                             end % finished if we are first k
                         end % finished k loop

                         % check if it mapped all of it
                         if ~isempty(OneD)
                             continue % the while loop, start again 
                         end 
                         if ~isempty(TwoD)
                             continue % the while loop, start again 
                         end
%                          if k was the last and didnt find a match, it would not do continue.
%                          if k < length(DimVec{d}) % didnt finish the loop
%                              continue % the while loop, start again 
%                          end 


                          % checking left right within context first, so
                          % if one context is already has 6 in a row, it is likely that once its in blocky it will have 4    
                          %    LeftRightCheckCon = CheckMeSomeSeq2(SideVeccon,Rules.Max_LeftRight_withinContext);
                            LeftRightCheckCon = CheckMeSomeSeq2(double(DesignDim{d}(:,1) == DesignDim{d}(:,3)),Rules.Max_LeftRight_withinContext);
                            
                            if ~LeftRightCheckCon
                                continue % no need to CheckDim even 
                            end 
                         % Checking if dimension is fine
                         CheckDim = CheckMeSomeSeq2(DesignDim{d}(:,3),Rules.Max_SameFeature_WithinCon);
                    end 
                    
                     % assigning the OneD / 2D vectore to design matrix
                     DesignDim{d}(:,6) = DimVec{d};
                end 
                
                ConVecWorked = false;
                countConVec = 1;
                while ~(ConVecWorked)% && (countConVec>100000))
                % Make context vector 
                
                ConVec = MakeCtxt('ValFullNoPassive',DimStart);
                
                % first see if there is a point in the ConVec
               % SideVec = zeros(size(DesignDim{1},1));
                
                SideVec1 = logical(DesignDim{1}(:,6));
                SideVec2 = logical(DesignDim{2}(:,6));
                ConVecLog = false(size(ConVec));
                ConVecLog(ConVec==1) = SideVec1;
                ConVecLog(ConVec==2) = SideVec2;
                ConVecWorked = CheckMeSomeSeq2(double(ConVecLog),Rules.Max_1D2D_OverAll);
                
                countConVec = countConVec + 1;
                    if countConVec > 1000 % 
                                  %  39916800
                        break
                    end 
                end 
                % make full design 
                % Map  DesignDim{d} into DesignBlock{block} based on ConVec
                   Count(1) = 1;
                   Count(2) = 1;
                   for kk = 1:length(ConVec)
                            DesignBlock{block}(kk,:) = DesignDim{ConVec(kk)}(Count(ConVec(kk)),:);
                            Count(ConVec(kk)) = Count(ConVec(kk)) + 1;
                   end 

                   DesignBlock{block}(:,5) = ConVec;

                        % Run last test - maximum 5 times 1d/2d in a row (of course
                        % more 2D than 1D)
%                         ConVecWorked = CheckMeSomeSeq2(DesignBlock{block}(:,6),Rules.Max_1D2D_OverAll);
                        
                
                % still need to check because maybe left the loop 
                           DimTest = CheckMeSomeSeq2(DesignBlock{block}(:,6),Rules.Max_1D2D_OverAll);

                           
           %CheckDim_ConditionFull = CheckMeSomeSeq2(double((DesignBlock{block}(:,1) > DesignBlock{block}(:,2)) & (DesignBlock{block}(:,7) > DesignBlock{block}(:,8)) | (DesignBlock{block}(:,1) < DesignBlock{block}(:,2)) & (DesignBlock{block}(:,7) < DesignBlock{block}(:,8))),Rules.Max_Same_Condition_Row);
            Congr = (DesignBlock{block}(:,1) > DesignBlock{block}(:,2)) & (DesignBlock{block}(:,7) >DesignBlock{block}(:,8)) | (DesignBlock{block}(:,1) < DesignBlock{block}(:,2)) & (DesignBlock{block}(:,7) < DesignBlock{block}(:,8));
            InCongr = (DesignBlock{block}(:,1) < DesignBlock{block}(:,2)) & (DesignBlock{block}(:,7) > DesignBlock{block}(:,8)) | (DesignBlock{block}(:,1) > DesignBlock{block}(:,2)) & (DesignBlock{block}(:,7) < DesignBlock{block}(:,8));
            CheckBcond = zeros(length(DesignBlock{block}),1);
            CheckBcond(Congr) = 1;
            CheckBcond(InCongr) = 2;
            CheckDim_ConditionFull = CheckMeSomeSeq2(CheckBcond,Rules.Max_Same_Condition_Row);
           
           DimTest = DimTest & CheckDim_ConditionFull;
        end        
        % final side check
%                 for s = 1:length(DesignBlock{block})
%                     if DesignBlock{block}(s,1) == DesignBlock{block}(s,3) % choose left
%                         SideVec(s) = 1;
%                     elseif DesignBlock{block}(s,2) == DesignBlock{block}(s,3) % choose Right
%                         SideVec(s) = 2;
%                     end 
%                 end 
%                % Not more than choosing 4 times the same side
           LeftRightCheck = CheckMeSomeSeq2(double(DesignBlock{block}(:,1) == DesignBlock{block}(:,3)),Rules.Max_LeftRight);
              % LeftRightCheck = CheckMeSomeSeq2(SideVec,Rules.Max_LeftRight);
 
       end % finishing the leftRight check  
     %  fprintf('finished block');
      % toc
end % finished block 

% DesignFinal = zeros(size(DesignBlock{1},1)*Block,8);
%% make final design 
% for blo = 1:Blocks
%     
%     if blo ==1
%         DesignFinal = DesignBlock{blo};
%     else 
%         DesignFinal = [DesignFinal ; DesignBlock{blo}];
%     end 
%     
% end 
end 