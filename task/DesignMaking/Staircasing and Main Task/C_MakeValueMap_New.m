%% Make value maps and save

rng(sum(100*clock)); 
folderMaps = 'C:\Users\moneta\Seafile_Folder\Seafile\My Library\SODIVA\MakingDesigns\DesignsForExperiment\ValueMaps_48Balancedish';

AllMaps.Color = perms(1:4)';
AllMaps.Motion = zeros(4,16);


% delete clockwise and counter clockwise options from motion 
Pre_AllMaps.Motion = perms(1:4)';
Problem = [1, 2 ,3, 4;...
           2, 3, 4, 1;...
           3, 4, 1, 2;...
           4, 1, 2, 3;...
           4, 3, 2, 1;...
           3, 2, 1, 4;...
           2, 1, 4, 3;...
           1, 4, 3, 2]';
       
counter = 0;
for i = 1:length(Pre_AllMaps.Motion)
   if sum(sum(Pre_AllMaps.Motion(:,i)==Problem)==4)==0
       % meaning not a single column has 4 yes on it
       counter = counter + 1;
   AllMaps.Motion(:,counter) =  Pre_AllMaps.Motion(:,i); % Motion
   end 
end 

%we have 24 options for maps in color, and 16 motion maps
% to have equal repetition we need 48 designs
ValMaps = [];

ColCounter = 1;
MotCounter = 1; 

for m = 1:48
    ValMaps(:,1,m) = AllMaps.Motion(:,MotCounter) ; % motion
    ValMaps(:,2,m) = AllMaps.Color(:,ColCounter) ; % color
    if MotCounter<16
        MotCounter = MotCounter + 1;
    else 
        MotCounter = 1; 
    end 
    
    if ColCounter<24
        ColCounter = ColCounter + 1;
    else 
        ColCounter = 1; 
    end 
    
end 
    
% sanity check all are different:
if length(reshape(ValMaps,8,[])') == length(unique(reshape(ValMaps,8,[])','rows'))
    for s = 1:size(ValMaps,3)
        if s < 10
         MapName = fullfile(folderMaps,['Value_Map_0',num2str(s),'.mat']);
        else 
         MapName = fullfile(folderMaps,['Value_Map_',num2str(s),'.mat']);
        end 
         ValMap = ValMaps(:,:,s);
         save(MapName, 'ValMap');
         clear ValMap
    end 
end 

% loading when in the CD folder 
% 
% A = zeros(4,2,96);
% for s = 1:96
%      if s < 10
%      T = load(['Value_Map_0',num2str(s),'.mat']);
%     
%     else 
%     T = load(['Value_Map_',num2str(s),'.mat']);
% 
%      end 
%     A(:,:,s) = T.ValMap;
% end 
% 
% V = zeros(96,1);
% counter = 0;
% for i = 1:length(A)
%    if sum(sum(A(:,1,i)==Problem)==4)==0
%      
%    V(i) = 1;
%    
%    end 
% end 
% 
% 
% 
% 
% 
% %             save(MapName, 'ValMap');
% %             clear ValMap
% 
% % count = 1;
% % for i = 1:length(AllMapsCon)
% %     
% %         for j = 1:length(AllMapsCon)
% %             
% %             ValMap(1:4,1) = AllMapsCon(1:4,i);
% %             ValMap(1:4,2) = AllMapsCon(1:4,j);
% %     
% %             
% %             count = count + 1;
% %             SeeVal(:,:,count) = ValMap;
% %             clear ValMap
% %         end 
% %         
% % end 
% % SeeVal is 24x24 value pairs, organized so: 
% % first 24 has same in column 1, and all the types of 24 in column 2;
% % take 
% 
% MakeRand = easyrandi(1:576);
% 
% for s = 1:(count-1)
%     
%             ValMap = SeeVal(:,:,MakeRand(s));
%             MapName = fullfile(folderMaps,['Value_Map_',num2str(s),'.mat']);
%             Checking(:,:,s) = ValMap;
%             save(MapName, 'ValMap');
%             clear ValMap
%     
%     
%     
%     
% end 


