function Design = MakeOneDMiniDesign

% Output:
% Design for the OneDMini part
% length is total of 4.2 per trial.
% we have 4:40 minutes for it so 280 seconds
% meaning 60 trials which is 252 seconds 
% this results in 30 trials of motion and 30 of color
% which are 10 of each EV for 1D 
% Blockies can be: [4 4 4 5 6 7];
% All of this will happen twice - once in behavioral and once in MRI
%
% This is the timing from set settings on 13.09.18
% display.Timing.OneDMini.CueTime = 0.6; % this is 1.0 and another 0.1 fix between cue and stim
% display.Timing.OneDMini.FixPostCueTime = 0.5; % this is 1.0 and another 0.1 fix between cue and stim
% display.Timing.OneDMini.StimTime = display.Max_Trial_duration;% now 1.6 % was 1.2;
% display.Timing.OneDMini.RespFixTime = 0.4; % Create stim - should be under 0.8 seconds  - only first one takes longer probably becasue its creating the fields of the structures
% display.Timing.OneDMini.FeedbkTime = 1.0; % create the other?
% display.Timing.OneDMini.LastFixTime = 0.5;% Since this is a bit more demanding, trying to increase from 0.4 to 0.5 seconds the last fixxation (or the last ITI)
% % trial is 4.6 seconds, we can have 60 trials, meaning 30 color and 30

% New Design Type
% 1 - left vlaue
% 2 - right value 
% 3 - Tgt (bzw. the highest of the two)
% 4 - context 

DesignPre = zeros(60,4);
Design = zeros(60,8);
% Make Basic choices - Already both left and right
DesignCH = [1 2 2; 2 3 3; 3 4 4; 2 1 2; 3 2 3; 4 3 4];


worked2 = false;

    while ~worked2
        for d = 1:2
            DimDesign{d} = repmat(DesignCH,5,1);  
            worked = false;
            while ~worked
            DimDesign{d} = DimDesign{d}(easyrandi(1:length(DimDesign{d})),:);
            RSide =  double(DimDesign{d}(:,1)<DimDesign{d}(:,2));
            worked = CheckMeSomeSeq2(DimDesign{d}(:,3),1) & CheckMeSomeSeq2(RSide,3); % checking EV not more than 1 within conext so 2 in general and not more than 3 times same side withing context
            end 
            DimDesign{d}(:,4) = d;
        end 
        if rand>0.5
            DimStart = 1;
        else 
            DimStart = 2;
        end 
        Ctxt = MakeCtxtOneD('ValMini',DimStart);

        Count = [1 1];
        for t = 1:length(DesignPre)
            DesignPre(t,:) = DimDesign{Ctxt(t)}(Count(Ctxt(t)),:);  
            Count(Ctxt(t)) = Count(Ctxt(t)) + 1;
        end 
            RSide2 =  double(DesignPre(:,1)<DesignPre(:,2));
            worked2 =  CheckMeSomeSeq2(RSide2,4); % not more than 4 times choosing same side
    end 

Design(:,1:3) = DesignPre(:,1:3);
Design(:,5) = DesignPre(:,4); % Context
Design(:,6) = 1; % all are oneD
end 